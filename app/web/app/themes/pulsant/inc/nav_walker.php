<?php

class Pulsant_Nav_Walker extends Walker {
	public $tree_type = array( 'post_type', 'taxonomy', 'custom' );
	public $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat( "\t", $depth );
		$output .= "\n$indent<ul class=\"no-list site-navigation__sub" . ( $depth >= 1 ? 'sub' : '' ) . "-nav js-sub" . ( $depth >= 1 ? '-sub' : '' ) . "-nav\">\n";
	}

	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat( "\t", $depth );
		$output .= "$indent</ul>\n";
	}

	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$classes   = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		$class_names = 'site-navigation__item js-site-nav-item';

		$is_repeated = false;

		if($item->menu_item_parent > 0){
			$parent_object_id = get_post_meta( $item->menu_item_parent, '_menu_item_object_id', true );
			if($parent_object_id){
				$parent_object = get_post($parent_object_id);
				if($parent_object){
					if($parent_object->ID == $item->object_id){
						$is_repeated = true;
					}
				}
			}
		}

		if ( $depth > 0 ) {
			$class_names = 'site-navigation__sub-item';
		}
		if ( $depth > 1 ) {
			$class_names = 'site-navigation__subsub-item';
		}

			if ( in_array( 'menu-item-has-children', $item->classes ) ) {
				if ( $depth < 1 ) {
					$class_names .= ' js-site-nav-item-with-children';
				} else {
					$class_names .= '  js-site-nav-sub-item js-site-nav-sub-item-with-children';
				}
			}

		$output .= $indent . '<li class="' . $class_names . '">';

		$atts           = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target ) ? $item->target : '';
		$atts['rel']    = ! empty( $item->xfn ) ? $item->xfn : '';
		$atts['href']   = ! empty( $item->url ) ? $item->url : '';

		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

		$atts['class'] = 'site-navigation__link';

		if ( $depth > 0 ) {
			$atts['class'] = ' site-navigation__sub' . ( $depth > 1 ? 'sub' : '' ) . '-link';
		}

		if ( in_array( 'menu-item-has-children', $item->classes ) ) {
			$atts['class'] .= ' site-navigation__' . ( $depth >= 1 ? 'sub-' : '' ) . 'link--hide-link site-navigation__' . ( $depth >= 1 ? 'sub-' : '' ) . 'link--has-children';
		}

		if($is_repeated){
			$atts['class'] .= ' site-navigation__sub' . ( $depth > 1 ? 'sub' : '' ) . '-link--repeated';
		}

		$atts['class'] = trim( $atts['class'] );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		$item_output = $args->before;
		$item_output .= '<a' . $attributes . '>';

		if ( $depth == 0 ) {
			$item_output .= '<span class="line-height-adjust">';
		}

		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

		if ( $depth == 0 ) {
			$item_output .= '</span>';
		}

		$item_output .= '</a>';
		$item_output .= $args->after;

		if ( in_array( 'menu-item-has-children', $item->classes ) ) {
			$item_output .= '   <input type="checkbox" id="subnav-checkbox-' . $item->ID . '" class="site-navigation__sub'. ( $depth >= 1 ? 'Sub' : '' ) .'nav-checkbox js-sub-nav-checkbox" data-depth="'. ( $depth >= 1 ? '2' : '1' ) .'">
                                <label class="site-navigation__'. ( $depth >= 1 ? 'sub-' : '' ) .'link site-navigation__'. ( $depth >= 1 ? 'sub-' : '' ) .'link--label site-navigation__'. ( $depth >= 1 ? 'sub-' : '' ) .'link--has-children js-sub-nav-label" for="subnav-checkbox-' . $item->ID . '" data-depth="'. ( $depth >= 1 ? '2' : '1' ) .'">'
                            . '<span class="line-height-adjust">'. apply_filters( 'the_title', $item->title, $item->ID ) . '<span>'
			                .'</label>';
		}

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	public function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "</li>\n";
	}

}
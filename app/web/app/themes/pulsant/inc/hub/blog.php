<section class="knowledge-hub-article">
	<div class="knowledge-hub-article__wrap section-wrap section-wrap--smaller js-social-nav-follow-container">

		<div class="knowledge-hub-article__main inner-wrap-@-sm">
			<?php
			include 'common/article_content.php';
			include 'common/paging.php';
			?>
		</div>

		<div class="knowledge-hub-article__social">
			<div class="inner-wrap-@-sm">
				<div class="knowledge-hub-article__social-inner js-social-nav-follow">
					<p class="knowledge-hub-article__social-title">Share</p>
					<?php get_template_part( 'inc/partials/social_nav_vertical' ); ?>
				</div>
			</div>
		</div>

		<div class="knowledge-hub-article__aside inner-wrap-@-sm">
			<?php $logo = get_field( 'logo' );
			if ( $logo ):?>
				<div class="knowledge-hub-article__logo">

					<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" class="responsive-img">
				</div>
			<?php endif;

			if ( get_field( 'show_form' ) == '1' ):
				include 'common/form.php';
			endif;

			include 'common/categories.php'; ?>
		</div>
	</div>
</section>
<?php include 'common/related.php';
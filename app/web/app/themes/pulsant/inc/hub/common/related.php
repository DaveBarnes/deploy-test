<?php

// find related KH articles in this section

$cats = wp_list_pluck( get_the_category(), 'term_id' );

if(count($cats) == 1 && current($cats) == 1){
	unset($cats);
}

if ( $cats ):

	$args = array(
		'posts_per_page'   => 3,
		'paged'            => FALSE,
		'offset'           => 0,
		'post__not_in'     => array( get_the_ID() ),
		'category__in'     => $cats,
		'orderby'          => 'post_date',
		'order'            => 'DESC',
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'suppress_filters' => TRUE
	);

	$the_query = new WP_Query( $args );
//	var_dump($the_query);

	if ( $the_query->have_posts() ):

		?>
		<section class="bgc-white">
			<div class="section-wrap">
				<h1 class="bgc-white ta-center pt-l fc-dark-blue fw-semibold fs-xl">Explore our resources</h1>
			</div>
			<div class="bgc-white pt-l">
				<div class="section-wrap section-wrap--restricted">
					<div class="simple-grid">
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<div class="simple-grid__item">
								<div class="simple-grid__item-spacing">
									<div class="inner-wrap-@-sm">
										<article class="article-attract">
											<a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="responsive-img article-attract__img"></a>
											<div class="article-attract__body">
												<div class="article-attract__main-heading">
													<h2 class="fc-dark-blue fw-semibold fs-l"><?php the_title(); ?></h2>
												</div>
												<div class="article-attract__copy">
													<p><?php the_field('tagline'); ?></p>
												</div>
												<div class="article-attract__cta">
													<a href="<?php the_permalink(); ?>" class="cta">Find out more</a>
												</div>
											</div>
										</article>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</section>
	<?php endif;
endif;

wp_reset_query();
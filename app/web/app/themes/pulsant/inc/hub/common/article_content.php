<div class="knowledge-hub-article__intro js-hub-article-intro">
	<div class="knowledge-hub-article__mini-header">
		<p class="knowledge-hub-article__label"><?php echo $type['label']; ?></p>
		<p class="knowledge-hub-article__date"><?php if ( get_field( 'event_date' ) ) {
				the_field( 'event_date' );
			} else {
				the_date( 'j M Y' );
			} ?></p>
	</div>

	<h1 class="fs-xl fc-dark-blue fw-semibold simple-motif mb-m"><?php the_title(); ?></h1>
	<!--	<div class="knowledge-hub-article__body">-->
	<!--		<h2>--><?php //the_field( 'tagline' ); ?><!--</h2>-->
	<!--	</div>-->
</div>

<?php
if ( $type['value'] != 'casestudy' ):
	if ( get_field( 'hide_main_image' ) !== TRUE ): ?>
		<div class="knowledge-hub-article__content-img">
			<?php the_post_thumbnail( '600x400', array( 'class' => 'responsive-img' ) ); ?>
		</div>
	<?php endif;
endif; ?>

<div class="knowledge-hub-article__body">
	<?php the_content(); ?>
</div>
<?php
$download = get_field( 'file' );

$cta_text = get_field( 'cta_text' );
if ( $cta_text == '' ) {
	$cta_text = 'Download ' . $type['label'];
}

$hs_form['uid'] = 'form-' . rand( 9999, 99999 );

if ( $download ):
	?>
	<div class="knowledge-hub-article__cta">
		<a href="#kh-download-form" data-form-id="<?php echo $hs_form['uid']; ?>" class="cta cta--large cta--primary js-open-conversion-form">
			<span class="line-height-adjust">
				<?php echo $cta_text; ?>
			</span>
		</a>
	</div>
<?php endif; ?>
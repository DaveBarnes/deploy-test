<div class="knowledge-hub-article__local-nav">
	<div class="mini-menu mini-menu--hub">
		<h4 class="mini-menu__heading">Categories</h4>
		<?php
		$terms = get_terms( 'category', array(
			'hide_empty' => TRUE,
			'post_type' => array('post'),

		) );

		if ( $terms ):
			?>
			<ul class="mini-menu__list no-list">
				<?php foreach ( $terms as $term ):
					if ( $term->slug != 'uncategorised' ): ?>
						<li class="mini-menu__item">
							<a class="mini-menu__link" href="<?php echo $hub_home . 'category/' . $term->slug; ?>"><?php echo $term->name; ?></a>
						</li>
					<?php endif;
				endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>
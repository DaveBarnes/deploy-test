<?php
$redirect = get_field( 'file' );
$redirect = $redirect['url'];
$fullURL  = 'http' . ( $_SERVER['SERVER_PORT'] == 80 ? '' : 's' ) . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$redirect = $redirect ? $redirect : $fullURL;
$cta_text = get_field( 'cta_text' );
if ( ! $cta_text ) {
	$cta_text = 'Download';
}
$form_id = 'b2b14e51-eb4b-4fd1-8074-6d5ce1b2fbea';
$form = new HubSpotForm( $form_id, get_the_title(), $redirect );

$hs_form['title']       = get_field( 'form_title' ) . ' ' . get_the_title();
$hs_form['hs_id']       = $form_id;
$hs_form['redirect']    = $redirect;
$hs_form['submit_text'] = $cta_text;

if ( get_field( 'event_form' ) ) {
	$form = get_field( 'event_form' );
	$id   = $form->ID;

	$hs_form['hs_id']       = get_field( 'form_id', $id );
	$hs_form['redirect']    = get_field( 'redirect_url', $id );
	$hs_form['submit_text'] = get_field( 'submit_text', $id );
	$form = new HubSpotForm( $hs_form['hs_id'], get_the_title(), $hs_form['redirect'] );
}

?>
	<div class="knowledge-hub-article__form">
		<div class="short-form">
			<h3 class="fc-brand-blue fw-semibold fs-l pb-s"><?php if ( get_field( 'form_title' ) ) {
					the_field( 'form_title' );
				} else {
					echo 'Download ' . $type['label'];
				} ?></h3>
			<p class="fc-white mb-m"><?php the_field( 'form_text' ); ?></p>
			<form action="/api/" enctype="application/x-www-form-urlencoded" id="kh-download-form" method="post" data-parsley-validate>
				<ul class="form">
					<?php foreach ( $form->formData['fields'] as $field ):
						if ( $field['type'] != 'textarea' ): ?>
							<li class="mb-m">
								<label class="form__label fc-white" for="<?php print $field['id']; ?>"><?php print $field['label']; ?>:</label>
								<?php print $field['markup']; ?>
							</li>
						<?php endif;
					endforeach; ?>
					<li>
						<?php echo $form->formData['hiddenFields']; ?>
						<button type="submit" class="cta cta--large cta--primary"><?php echo $cta_text; ?></button>
					</li>
				</ul>
				<input type="hidden" name="eventcategory" class="js-eventcategory" value="Download">
				<input type="hidden" name="eventaction" class="js-eventaction" value="<?php echo $type['label']; ?>">
				<input type="hidden" name="eventlabel" class="js-eventlabel" value="<?php the_title(); ?>">
			</form>
		</div>
	</div>
<?php
$override_eventcategory = 'Download';
$override_action = $type['label'];
include realpath( dirname( __FILE__ ) ) . '/../../partials/conversion-form.php';
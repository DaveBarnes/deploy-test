<div class="knowledge-hub-article__intro js-hub-article-intro">
	<div class="knowledge-hub-article__mini-header">
		<p class="knowledge-hub-article__label"><?php echo $type['label']; ?></p>
		<p class="knowledge-hub-article__date"><?php if ( get_field( 'event_date' ) ) {
				the_field( 'event_date' );
			} else {
				the_date( 'j M Y' );
			} ?></p>
	</div>

	<h1 class="fs-xl fc-dark-blue fw-semibold simple-motif mb-m"><?php the_title(); ?></h1>
</div>

<div class="knowledge-hub-article__body">
	<?php the_content(); ?>
</div>
<?php
$download = get_field( 'file' );

$cta_text = get_field( 'cta_text' );
if ( $cta_text == '' ) {
	$cta_text = 'Download ' . $type['label'];
}

if ( $download ): ?>
	<div class="knowledge-hub-article__cta">
		<a href="<?php echo $download['url']; ?>" target="_blank" class="cta cta--large cta--primary js-analytics-event" data-eventcategory="Download" data-eventaction="Case Study" data-eventlabel="<?php the_title(); ?>"><?php echo $cta_text; ?></a>
	</div>
<?php endif; ?>
<div class="knowledge-hub-article__simple-nav">
	<div class="knowledge-hub-article__simple-nav-step">
		<?php if ( $prev_post ): ?>
			<a href="<?php echo get_permalink( $prev_post[0]->ID ); ?>">&#8592; Previous</a>
		<?php endif;
		if ( $next_post ): ?>
			<a href="<?php echo get_permalink( $next_post[0]->ID ); ?>">Next &#8594;</a>
		<?php endif; ?>
	</div>
	<?php
	if ( substr( $type['label'], - 1 ) == 'y' ) {
		$type_link = substr( $type['label'], 0, - 1 ) . 'ies';
	} else {
		$type_link = $type['label'] . 's';
	}
	?>
	<a class="" href="<?php echo $hub_home .  $type['value'] .'/'; ?>">&#x2637; View All <?php echo $type_link; ?></a>
</div>
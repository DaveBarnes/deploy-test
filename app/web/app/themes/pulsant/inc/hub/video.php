<section class="knowledge-hub-article">
	<div class="knowledge-hub-article__wrap section-wrap section-wrap--smaller">

		<div class="knowledge-hub-article__main knowledge-hub-article__main--no-social-nav inner-wrap-@-sm">
			<div class="knowledge-hub-article__intro js-hub-article-intro">
				<div class="knowledge-hub-article__mini-header">
					<p class="knowledge-hub-article__label">Video</p>
					<p class="knowledge-hub-article__date"><?php the_date( 'j M Y' ); ?></p>
				</div>

				<h1 class="fs-xl fc-dark-blue fw-semibold simple-motif mb-m"><?php the_title(); ?></h1>
				<div class="knowledge-hub-article__body">
					<h2><?php the_field( 'tagline' ); ?></h2>
				</div>
			</div>
		</div>

		<div class="knowledge-hub-article__aside inner-wrap-@-sm">
			<?php include 'common/categories.php'; ?>
		</div>
	</div>

	<div class="knowledge-hub-article__wrap section-wrap section-wrap--smaller">

		<div class="knowledge-hub-article__social knowledge-hub-article__social--video">
			<div class="inner-wrap-@-sm">
				<div class="knowledge-hub-article__social-inner">
					<p class="knowledge-hub-article__social-title">Share</p>
					<?php include( 'social_nav_vertical.php' ); ?>
				</div>
			</div>
		</div>

		<div class="knowledge-hub-article__video">
			<div class="inner-wrap-@-sm">

				<div class="responsive-video">
					<iframe src="<?php the_field( 'video_url' ); ?>" frameborder="0" allowfullscreen></iframe>
				</div>

				<?php include 'common/paging.php'; ?>
			</div>
		</div>
	</div>
</section>
<?php include 'common/related.php';
<?php
global $extend_header;
?><!doctype html>
<html class="no-js" lang="">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>"/>
	<title><?php wp_title(); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> RSS2 Feed" href="<?php bloginfo( 'rss2_url' ); ?>"/>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>
	<?php wp_head(); ?>

	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600" rel="stylesheet">

	<?php if ( get_field( 'schema' ) ) {
		the_field( 'schema' );
	} ?>

	<script>
		document.getElementsByTagName('html')[0].className = 'js';
	</script>
	<!-- Start Visual Website Optimizer Asynchronous Code -->
	<script type='text/javascript'>
		var _vwo_code=(function(){
			var account_id=281372,
				settings_tolerance=2000,
				library_tolerance=2500,
				use_existing_jquery=false,
				/* DO NOT EDIT BELOW THIS LINE */
				f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
	</script>
	<!-- End Visual Website Optimizer Asynchronous Code -->
</head>
<body <?php body_class(); ?>>

<div class="site-wrap">

	<div class="site-search js-site-search is-closed hide-overflow">
		<div class="site-search__close-wrap">
			<div class="section-wrap site-search__close-wrap-inner">
				<button class="site-search__close-button js-close-button">
					Close
					<i><span class="cross"></span></i>
				</button>
			</div>
		</div>
		<div class="section-wrap site-search__wrap">
			<div class="site-search__wrap-mobile inner-wrap-@-sm">
				<div class="site-search__fieldset js-search-fieldset">
					<input class="site-search__input js-search-input" placeholder="Enter search term" type="text">
					<button class="site-search__button cta cta--secondary cta--large js-search-button">
						<span class="site-search__button-text">Search</span>
						<svg class="site-search__button-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.46 14.46"><title></title>
							<path d="M14.1,12.39,11,9.25l-.05,0a5.95,5.95,0,1,0-1.7,1.7l0,.05,3.14,3.14a1.21,1.21,0,0,0,1.71-1.71ZM5.94,9.83A3.88,3.88,0,1,1,9.83,5.94,3.88,3.88,0,0,1,5.94,9.83Zm0,0" fill="#ffffff" fill-rule="evenodd"/>
						</svg>
					</button>
					<div class="site-search__results js-results">
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<header class="site-header site-header--extended">

		<!-- adding empty div fixes IE disappearing elements bug - https://www.sitepoint.com/fix-disappearing-absolute-position-element-ie/ -->
		<div></div>

		<div class="site-header__wrap">
			<div class="inner-wrap-@-sm">
				<a href="/" class="site-header__logo">
					<img src="<?php print get_template_directory_uri(); ?>/images/logo.svg">
				</a>
			</div>

			<div class="site-header__search-icon-wrap">
				<button class="site-navigation__link site-navigation__link--search js-site-search-button">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.46 14.46"><title></title>
						<path d="M14.1,12.39,11,9.25l-.05,0a5.95,5.95,0,1,0-1.7,1.7l0,.05,3.14,3.14a1.21,1.21,0,0,0,1.71-1.71ZM5.94,9.83A3.88,3.88,0,1,1,9.83,5.94,3.88,3.88,0,0,1,5.94,9.83Zm0,0" fill="#ffffff" fill-rule="evenodd"/>
					</svg>
				</button>
			</div>

			<input type="checkbox" id="nav-checkbox" class="site-header__nav-checkbox js-nav-checkbox">
			<label class="site-header__nav-icon-wrap js-toggle-nav-icon" for="nav-checkbox">
                <span class="site-header__nav-icon js-toggle-nav-icon-inside">
                    <i></i>
                    <span></span>
                </span>
			</label>

			<div class="site-header__site-nav js-site-nav">

				<!-- adding empty div fixes IE disappearing elements bug - https://www.sitepoint.com/fix-disappearing-absolute-position-element-ie/ -->
				<div></div>
				
				<ul class="no-list site-navigation site-navigation--important">
					<li class="site-navigation__item site-navigation__item--number site-navigation__item--sales">
						<a href="tel:03451199911" class="site-navigation__link js-analytics-event" data-eventcategory="Sales Call" data-eventaction="Click" data-eventlabel="Sales Phone Number">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 578.106 578.106" style="enable-background:new 0 0 578.106 578.106;" xml:space="preserve" width="512px" height="512px">
								<g>
									<g>
										<path
											d="M577.83,456.128c1.225,9.385-1.635,17.545-8.568,24.48l-81.396,80.781    c-3.672,4.08-8.465,7.551-14.381,10.404c-5.916,2.857-11.729,4.693-17.439,5.508c-0.408,0-1.635,0.105-3.676,0.309    c-2.037,0.203-4.689,0.307-7.953,0.307c-7.754,0-20.301-1.326-37.641-3.979s-38.555-9.182-63.645-19.584    c-25.096-10.404-53.553-26.012-85.376-46.818c-31.823-20.805-65.688-49.367-101.592-85.68    c-28.56-28.152-52.224-55.08-70.992-80.783c-18.768-25.705-33.864-49.471-45.288-71.299    c-11.425-21.828-19.993-41.616-25.705-59.364S4.59,177.362,2.55,164.51s-2.856-22.95-2.448-30.294    c0.408-7.344,0.612-11.424,0.612-12.24c0.816-5.712,2.652-11.526,5.508-17.442s6.324-10.71,10.404-14.382L98.022,8.756    c5.712-5.712,12.24-8.568,19.584-8.568c5.304,0,9.996,1.53,14.076,4.59s7.548,6.834,10.404,11.322l65.484,124.236    c3.672,6.528,4.692,13.668,3.06,21.42c-1.632,7.752-5.1,14.28-10.404,19.584l-29.988,29.988c-0.816,0.816-1.53,2.142-2.142,3.978    s-0.918,3.366-0.918,4.59c1.632,8.568,5.304,18.36,11.016,29.376c4.896,9.792,12.444,21.726,22.644,35.802    s24.684,30.293,43.452,48.653c18.36,18.77,34.68,33.354,48.96,43.76c14.277,10.4,26.215,18.053,35.803,22.949    c9.588,4.896,16.932,7.854,22.031,8.871l7.648,1.531c0.816,0,2.145-0.307,3.979-0.918c1.836-0.613,3.162-1.326,3.979-2.143    l34.883-35.496c7.348-6.527,15.912-9.791,25.705-9.791c6.938,0,12.443,1.223,16.523,3.672h0.611l118.115,69.768    C571.098,441.238,576.197,447.968,577.83,456.128z"
											fill="#00a1e4"/>
									</g>
								</g>
							</svg>
							SALES: 0345&nbsp;119&nbsp;9911
						</a>
					</li>
					<li class="site-navigation__item site-navigation__item--number site-navigation__item--support">
						<a href="tel:03451199999" class="site-navigation__link js-analytics-event" data-eventcategory="Support Call" data-eventaction="Click" data-eventlabel="Support Phone Number">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 578.106 578.106" style="enable-background:new 0 0 578.106 578.106;" xml:space="preserve" width="512px" height="512px">
								<g>
									<g>
										<path
											d="M577.83,456.128c1.225,9.385-1.635,17.545-8.568,24.48l-81.396,80.781    c-3.672,4.08-8.465,7.551-14.381,10.404c-5.916,2.857-11.729,4.693-17.439,5.508c-0.408,0-1.635,0.105-3.676,0.309    c-2.037,0.203-4.689,0.307-7.953,0.307c-7.754,0-20.301-1.326-37.641-3.979s-38.555-9.182-63.645-19.584    c-25.096-10.404-53.553-26.012-85.376-46.818c-31.823-20.805-65.688-49.367-101.592-85.68    c-28.56-28.152-52.224-55.08-70.992-80.783c-18.768-25.705-33.864-49.471-45.288-71.299    c-11.425-21.828-19.993-41.616-25.705-59.364S4.59,177.362,2.55,164.51s-2.856-22.95-2.448-30.294    c0.408-7.344,0.612-11.424,0.612-12.24c0.816-5.712,2.652-11.526,5.508-17.442s6.324-10.71,10.404-14.382L98.022,8.756    c5.712-5.712,12.24-8.568,19.584-8.568c5.304,0,9.996,1.53,14.076,4.59s7.548,6.834,10.404,11.322l65.484,124.236    c3.672,6.528,4.692,13.668,3.06,21.42c-1.632,7.752-5.1,14.28-10.404,19.584l-29.988,29.988c-0.816,0.816-1.53,2.142-2.142,3.978    s-0.918,3.366-0.918,4.59c1.632,8.568,5.304,18.36,11.016,29.376c4.896,9.792,12.444,21.726,22.644,35.802    s24.684,30.293,43.452,48.653c18.36,18.77,34.68,33.354,48.96,43.76c14.277,10.4,26.215,18.053,35.803,22.949    c9.588,4.896,16.932,7.854,22.031,8.871l7.648,1.531c0.816,0,2.145-0.307,3.979-0.918c1.836-0.613,3.162-1.326,3.979-2.143    l34.883-35.496c7.348-6.527,15.912-9.791,25.705-9.791c6.938,0,12.443,1.223,16.523,3.672h0.611l118.115,69.768    C571.098,441.238,576.197,447.968,577.83,456.128z"
											fill="#00a1e4"/>
									</g>
								</g>
							</svg>
							SUPPORT: 0345&nbsp;119&nbsp;9999
						</a>
					</li>
					<li class="site-navigation__item site-navigation__item--search">
						<button class="site-navigation__link site-navigation__link--search js-site-search-button">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.46 14.46"><title></title>
								<path d="M14.1,12.39,11,9.25l-.05,0a5.95,5.95,0,1,0-1.7,1.7l0,.05,3.14,3.14a1.21,1.21,0,0,0,1.71-1.71ZM5.94,9.83A3.88,3.88,0,1,1,9.83,5.94,3.88,3.88,0,0,1,5.94,9.83Zm0,0" fill="#1d9fda" fill-rule="evenodd"/>
							</svg>
							Search
						</button>
					</li>
					<li class="site-navigation__item site-navigation__item--my-pulsant">
						<a href="https://portal.pulsant.com/" class="site-navigation__link">
							<i><i></i></i><!-- icon -->
							<span>My Pulsant</span>
						</a>
					</li>
				</ul>
				<?php wp_nav_menu( array(
					'menu'       => 2,
					'menu_class' => 'no-list site-navigation js-site-navigation',
					'walker'     => new Pulsant_Nav_Walker(),
					'container'  => FALSE,
					'depth'      => 3,
				) );
				?>
			</div>
			<!-- adding empty div fixes IE disappearing elements bug - https://www.sitepoint.com/fix-disappearing-absolute-position-element-ie/ -->
			<div></div>
		</div>
	</header>
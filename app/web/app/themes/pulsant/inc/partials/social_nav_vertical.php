<?php
$tweet_text = urlencode(get_the_title());
$tweet_url = wp_get_shortlink();
if(get_field('tweet_text')){
	$tweet_text = urlencode_deep(get_field('tweet_text'));
}

?><ul class="no-list social-nav social-nav--vertical">
	<li class="social-nav__list-item">
		<a href="https://twitter.com/intent/tweet?url=<?php echo urlencode($tweet_url); ?>&text=<?php echo $tweet_text; ?>">
			<svg version="1.1" class="social-nav__icon social-nav__icon--twitter" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			     viewBox="0 0 24 19.5" enable-background="new 0 0 24 19.5" xml:space="preserve">
                <path id="XMLID_4_" fill="#96968e" d="M21.6,4.9c0,0.2,0,0.4,0,0.6c0,6.5-5,14-14,14c-2.8,0-5.4-0.8-7.6-2.2c0.4,0,0.8,0.1,1.2,0.1
                    c2.3,0,4.4-0.8,6.1-2.1c-2.2,0-4-1.5-4.6-3.4c0.3,0.1,0.6,0.1,0.9,0.1c0.4,0,0.9-0.1,1.3-0.2c-2.3-0.5-4-2.4-4-4.8c0,0,0,0,0-0.1
                    c0.7,0.4,1.4,0.6,2.2,0.6C1.9,6.6,1,5.1,1,3.4c0-0.9,0.2-1.7,0.7-2.5c2.4,3,6.1,4.9,10.2,5.1c-0.1-0.4-0.1-0.7-0.1-1.1
                    c0-2.7,2.2-4.9,4.9-4.9c1.4,0,2.7,0.6,3.6,1.6c1.1-0.2,2.2-0.6,3.1-1.2c-0.4,1.2-1.1,2.1-2.2,2.7c1-0.1,1.9-0.4,2.8-0.8
                    C23.4,3.3,22.5,4.2,21.6,4.9z"/>
            </svg>
		</a>
	</li>
	<li class="social-nav__list-item">
		<a href="https://facebook.com/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>">
			<svg version="1.1" class="social-nav__icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22.3 22.3" enable-background="new 0 0 22.3 22.3" xml:space="preserve">
            	<path id="XMLID_6_" fill="#96968e" d="M21.1,0H1.2C0.6,0,0,0.6,0,1.2v19.9c0,0.7,0.6,1.2,1.2,1.2h10.7v-8.6H9v-3.4h2.9V7.8
                c0-2.9,1.8-4.5,4.3-4.5c1.2,0,2.3,0.1,2.6,0.1v3l-1.8,0c-1.4,0-1.7,0.7-1.7,1.6v2.2h3.3l-0.4,3.4h-2.9v8.6h5.7
                c0.7,0,1.2-0.6,1.2-1.2V1.2C22.3,0.6,21.8,0,21.1,0z"/>
            </svg>
		</a>
	</li>
	<li class="social-nav__list-item">
		<a href="https://plus.google.com/share?url=<?php echo urlencode(get_the_permalink()); ?>">
			<svg version="1.1" class="social-nav__icon social-nav__icon--google" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31.3 20.3" enable-background="new 0 0 31.3 20.3" xml:space="preserve">
	            <g>
	                <path fill="#96968e" d="M18,16.6c-2.5,3.6-7.6,4.6-11.6,3.1c-4-1.5-6.8-5.7-6.5-10C0.1,4.4,4.9-0.2,10.2,0C12.7-0.1,15,1,17,2.5
	                    c-0.8,0.9-1.7,1.8-2.6,2.7C12.1,3.6,8.8,3.1,6.5,5C3.2,7.3,3,12.7,6.2,15.1c3.1,2.8,8.9,1.4,9.8-2.9c-1.9,0-3.9,0-5.8-0.1
	                    c0-1.2,0-2.3,0-3.5c3.2,0,6.5,0,9.7,0C20.1,11.4,19.7,14.3,18,16.6z"/>
	                <path fill="#96968e" d="M28.4,11.6c0,1,0,1.9,0,2.9c-1,0-1.9,0-2.9,0c0-1,0-1.9,0-2.9c-1,0-1.9,0-2.9,0c0-1,0-1.9,0-2.9
	                    c1,0,1.9,0,2.9,0c0-1,0-1.9,0-2.9c1,0,1.9,0,2.9,0c0,1,0,1.9,0,2.9c1,0,1.9,0,2.9,0c0,1,0,1.9,0,2.9C30.3,11.6,29.4,11.6,28.4,11.6
	                    z"/>
	            </g>
	        </svg>
		</a>
	</li>

	<li class="social-nav__list-item">
		<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_the_permalink()); ?>">
			<svg version="1.1" class="social-nav__icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 19.6 19.5" enable-background="new 0 0 19.6 19.5" xml:space="preserve">
                <g>
                    <rect x="0.3" y="6.5" fill="#96968e" width="4.1" height="13"/>
                    <circle fill="#96968e" cx="2.4" cy="2.4" r="2.4"/>
                    <path fill="#96968e" d="M19.6,19.5h-4.1v-6.3c0-1.5,0-3.5-2.1-3.5c-2.1,0-2.4,1.6-2.4,3.3v6.5H6.9v-13h3.9v1.8h0.1
                        c0.5-1,1.9-2.1,3.8-2.1c4.1,0,4.9,2.7,4.9,6.2V19.5z"/>
                </g>
            </svg>
		</a>
	</li>
</ul>
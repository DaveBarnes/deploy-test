<svg id="home-cloud-svg" xmlns="http://www.w3.org/2000/svg" width="477.2" height="469.1" viewBox="0 0 492.2 469.1">
  <path d="M427,252.9l-22.1-12.7c6.5-4.4,6.2-11-1-15.1-7.5-4.4-19.7-4.4-27.2,0s-7.4,11.3.1,15.7,19.3,4.4,26.8.3l22.7,13.1a.69.69,0,0,0,.9-.3.75.75,0,0,0-.2-1Z" transform="translate(0)" fill="#16204b"/>
  <circle cx="426.4" cy="198.1" r="22" fill="#009983"/>
  <line x1="426.7" y1="193.3" x2="426.7" y2="253.3" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
  <line x1="426.7" y1="212.8" x2="437.1" y2="203.7" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
  <path d="M426.7,253.3" transform="translate(0)" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.4"/>
  <path d="M426.7,193.3" transform="translate(0)" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
  <path d="M420,200" transform="translate(0)" fill="none" stroke="#6f58a2" stroke-miterlimit="10" stroke-width="1.4"/>
  <line x1="421.9" y1="204.1" x2="426.7" y2="207.2" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
  <line x1="431.5" y1="194.6" x2="426.7" y2="197.7" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
  <path d="M364.4,246.2c-41.8-22.4-96.5-33.6-151.1-33.7-54.8,0-109.5,11.2-151.2,33.7C20.6,268.6-.1,297.9,0,327.3v.7a10.87,10.87,0,0,0,.1,1.8v.8a14.92,14.92,0,0,0,.2,2.1v.4c.1.8.2,1.6.3,2.5,0,.2.1.5.1.7.1.6.2,1.2.3,1.9,0,.3.1.5.1.8.1.7.3,1.3.4,2l.1.5c.2.8.4,1.6.6,2.5l.2.6a13.32,13.32,0,0,1,.5,1.9l.2.8a15.87,15.87,0,0,0,.7,2l.2.5.9,2.4c.2.5.4.9.6,1.4s.4.9.6,1.3.4.9.7,1.4a13.92,13.92,0,0,1,.6,1.3l.3.6c.3.6.7,1.2,1,1.8l.4.8c.3.6.7,1.2,1.1,1.9l.3.6c.5.8,1,1.6,1.5,2.3.3.5.6.9.9,1.4a14.63,14.63,0,0,0,.9,1.3c.3.4.6.9,1,1.4a10.14,10.14,0,0,1,.9,1.3,14.7,14.7,0,0,0,1,1.3,11.32,11.32,0,0,0,1,1.3c.4.4.7.9,1.1,1.3a15,15,0,0,1,1.1,1.3l.6.7c.5.6,1,1.1,1.5,1.7l.8.9c.5.5.9,1,1.4,1.5l.9,1,1.5,1.5,1,1,1.5,1.4,1,.9,1.7,1.4,1,.8,2,1.7.7.6c.9.8,1.9,1.5,2.8,2.3l.6.4,2.4,1.8,1.1.8,2,1.4,1.2.9,2,1.3,1.3.9,2,1.3,1.3.9,2,1.3,1.3.8,2.4,1.4,1.1.7c1.2.7,2.4,1.3,3.7,2l.3.1c2.4,1.3,4.9,2.6,7.4,3.8.9.4,1.8.8,2.6,1.2,1.8.8,3.5,1.7,5.3,2.5,1.1.5,2.2.9,3.2,1.4,1.7.7,3.3,1.4,5,2.1,1.2.5,2.4.9,3.5,1.4,1.7.7,3.4,1.3,5.2,1.9l2.2.8c2.2.8,4.4,1.5,6.7,2.2l1.5.5c2.7.9,5.4,1.7,8.2,2.5l2.1.6,5.5,1.5c2.1.6,4.3,1.1,6.5,1.6,1.3.3,2.6.6,4,.9l7.2,1.5c1.1.2,2.3.5,3.4.7l6.6,1.2,3.6.6c2.9.5,5.8.9,8.7,1.3l1.7.2,10.2,1.2c.8.1,1.6.1,2.4.2,2.6.3,5.3.5,7.9.7,1.3.1,2.5.2,3.8.2,2.2.1,4.4.3,6.6.4,1.4.1,2.8.1,4.3.2,2,.1,4.1.2,6.1.2h10.5c1.5,0,2.9,0,4.4-.1,2.1,0,4.2,0,6.2-.1,1.4,0,2.7-.1,4.1-.2l7.2-.3,4.3-.3c2.3-.2,4.6-.3,6.9-.5,1.8-.2,3.7-.3,5.5-.5l2.2-.2c3-.3,5.9-.6,8.9-1l1.3-.2c2.6-.3,5.2-.7,7.7-1.1l2.3-.4,5-.9c1.9-.3,3.7-.7,5.6-1l4.5-.9c1.8-.4,3.6-.8,5.5-1.2,1.5-.3,3-.7,4.5-1,1.8-.4,3.6-.9,5.3-1.3,1.5-.4,3-.8,4.4-1.2l5.2-1.5c1.8-.5,3.6-1.1,5.5-1.6l1.9-.6c2.7-.9,5.3-1.7,7.9-2.7l1.4-.5c2.2-.8,4.4-1.6,6.5-2.5l2-.8c2.5-1,5-2.1,7.5-3.1l.9-.4c2.2-1,4.4-2,6.5-3l1.9-.9c2.4-1.2,4.8-2.4,7.1-3.6l1-.6,2.2-1.2,2-1.1.1-.1,1.7-1,1.9-1.1.4-.2,1.4-.9,1.8-1.2.2-.1,1.5-1,1.8-1.2.3-.2,1.4-.9,1.7-1.2.1-.1,1.5-1,1.6-1.2.1-.1,1.4-1.1,1.6-1.2h.1l1.4-1.1,1.5-1.3h0l1.3-1.1c1.1-.9,2.2-1.9,3.2-2.8l.8-.8c.8-.7,1.6-1.5,2.3-2.2l.4-.4.6-.6c.8-.8,1.5-1.5,2.2-2.3l.7-.7,2.6-2.9.8-1a21,21,0,0,0,1.4-1.7l.7-.9.3-.3c.4-.5.8-1.1,1.2-1.6l.4-.6.5-.6a15.26,15.26,0,0,0,1.1-1.6l.2-.4.6-.9a17.4,17.4,0,0,0,1.1-1.8h0c.2-.4.5-.7.7-1.1s.6-1,.9-1.5h0c.2-.4.5-.8.7-1.2a11.87,11.87,0,0,0,.8-1.6,6.66,6.66,0,0,1,.6-1.2c.2-.4.5-1.1.8-1.6a5.36,5.36,0,0,0,.5-1.1l.1-.2a10.09,10.09,0,0,1,.7-1.5c.1-.3.3-.6.4-.9s.3-.7.4-1l.3-.8a6.9,6.9,0,0,1,.4-1,4.88,4.88,0,0,0,.4-1.1l.3-.9a2.54,2.54,0,0,0,.2-.7,5.64,5.64,0,0,1,.4-1.2,6.58,6.58,0,0,0,.4-1.5c.1-.5.2-.8.3-1.3s.3-1,.4-1.5.2-.8.3-1.3l.3-1.5c.1-.5.1-.8.2-1.3h0c.1-.5.1-1,.2-1.5a5.28,5.28,0,0,0,.1-1.2v-.2c0-.5.1-.9.1-1.4s.1-.8.1-1.2v-.3a5.7,5.7,0,0,1,.1-1.3,4,4,0,0,0,0-1.1C428.1,299.2,407,269.1,364.4,246.2Z" transform="translate(0)" fill="#16204b"/>
  <path d="M77.8,326.4l.1-20c-.1,27.2,16.4,54.4,49.5,75.5,52,33.1,129.8,41.4,194.4,24.4v20c-64.6,17-142.4,8.8-194.4-24.4-33.2-21.1-49.6-48.3-49.6-75.5" transform="translate(0)" fill="#1960a7"/>
  <path d="M321.7,426.3v-20a202.73,202.73,0,0,0,52.8-21.7c35.2-21,52.9-49.1,53-77.1v20c-.1,28.1-17.8,56.1-53,77.1a209.87,209.87,0,0,1-52.8,21.7" transform="translate(0)" fill="#d6edfb"/>
  <path d="M130.9,229.3C200.1,187.9,310.8,189,378,232s65.7,111.2-3.5,152.7S194.6,424.9,127.3,382,61.6,270.7,130.9,229.3Zm19.5,12.4c-58.1,34.8-59.4,92.2-2.9,128.2s149.4,37,207.6,2.2,59.5-92.1,3-128.1-149.6-37-207.7-2.3" transform="translate(0)" fill="#fff"/>
  <path d="M150.4,222.1c58.1-34.8,151.1-33.8,207.6,2.2s55.2,93.4-3,128.2-151,33.8-207.6-2.2S92.3,256.9,150.4,222.1ZM170,234.5c-47,28.1-48.1,74.6-2.4,103.7s120.9,30,167.9,1.8,48.1-74.6,2.4-103.7S217,206.4,170,234.5" transform="translate(0)" fill="#fff"/>
  <path d="M57.8,436.8,35.7,424c6.5-4.4,6.2-11-1-15.1-7.5-4.4-19.7-4.4-27.2,0s-7.4,11.3.1,15.7,19.3,4.4,26.8.3L57.1,438a.69.69,0,0,0,.9-.3.9.9,0,0,0-.2-.9Z" transform="translate(0)" fill="#16204b"/>
  <circle cx="57.2" cy="381.9" r="22" fill="#009983"/>
  <line x1="57.5" y1="377.2" x2="57.5" y2="437.1" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
  <line x1="57.5" y1="396.7" x2="67.8" y2="387.6" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
  <path d="M57.5,437.1" transform="translate(0)" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.4"/>
  <path d="M57.5,377.2" transform="translate(0)" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.4"/>
  <path d="M50.8,383.8" transform="translate(0)" fill="none" stroke="#6f58a2" stroke-miterlimit="10" stroke-width="1.4"/>
  <line x1="52.6" y1="388" x2="57.5" y2="391.1" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
  <line x1="62.3" y1="378.5" x2="57.5" y2="381.6" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
  <ellipse cx="108.1" cy="413.9" rx="3.8" ry="4.7" fill="#1960a7"/>
  <path d="M113,424.7c0-3.3-2.2-4.1-4.9-4.1s-4.9.9-4.9,4.1V439H113Z" transform="translate(0)" fill="#1960a7"/>
  <path d="M104.2,429.5l11.2,8.7a.63.63,0,0,0,.9-.1.48.48,0,0,0,0-.7l-7.2-12Z" transform="translate(0)" fill="#1960a7"/>
  <path d="M109.1,447.7a2.35,2.35,0,0,0-1.7,2.9l.5,1,5.7,12.2a.65.65,0,0,0,.9.2.78.78,0,0,0,.3-.7L112.2,450l-.2-.9A2.45,2.45,0,0,0,109.1,447.7Z" transform="translate(0)" fill="#1960a7"/>
  <path d="M105.4,439l1.9,11.1.2,1.1a2.44,2.44,0,0,0,4.6-.3l.1-1L113,439Z" transform="translate(0)" fill="#1960a7"/>
  <path d="M110.4,447.8a2.54,2.54,0,0,0-3.1,1.4l-.2,1.1-2.2,13.3a.73.73,0,0,0,.6.7.55.55,0,0,0,.6-.4l5.4-12.4.4-.9A2.26,2.26,0,0,0,110.4,447.8Z" transform="translate(0)" fill="#1960a7"/>
  <path d="M110.7,438.2h0v-.1a3.79,3.79,0,0,0-7.4,1.6v.1h0a3.55,3.55,0,0,0,.4.9l3.7,9.5.4,1a2.52,2.52,0,0,0,3.4.7,2.65,2.65,0,0,0,1.1-1.8l-.1-.9-1.3-10.5v-.2Z" transform="translate(0)" fill="#1960a7"/>
  <path d="M378,231.9c-49.8-31.8-123.5-40.6-186.4-26.3l-3.8,19.9C243.4,208.7,312.5,215,358,244c56.5,36,55.2,93.4-3,128.2S203.9,406,147.4,370c-.7-.4-1.3-.9-2-1.3l-20.3,11.9c.7.5,1.5,1,2.2,1.5,67.3,42.9,178,44.1,247.2,2.7S445.3,274.8,378,231.9Z" transform="translate(0)" fill="#fff"/>
  <path d="M150.4,241.7a165.37,165.37,0,0,1,37.4-16.2l3.8-19.9c-22.1,5-42.8,12.9-60.8,23.6-68.4,41-70.7,108.2-5.7,151.2l20.3-11.9C91,332.5,92.9,276.1,150.4,241.7Z" transform="translate(0)" fill="#cfd5db"/>
  <path d="M105.9,306.5l.1-19.6c-.1,22.8,13.7,45.7,41.5,63.4,43.6,27.8,109,34.7,163.3,20.5v19.6c-54.3,14.3-119.6,7.4-163.3-20.5-27.9-17.7-41.7-40.6-41.6-63.4" transform="translate(0)" fill="#1960a7"/>
  <path d="M310.7,390.4V370.8A174.57,174.57,0,0,0,355,352.6c29.5-17.7,44.4-41.2,44.5-64.8v19.6c-.1,23.6-14.9,47.1-44.5,64.8a174.58,174.58,0,0,1-44.3,18.2" transform="translate(0)" fill="#d6edfb"/>
  <path d="M134,287l.1-19.6c0,18.5,11.1,37,33.6,51.3,35.3,22.5,88.2,28.1,132.1,16.5v19.6c-43.9,11.6-96.8,6-132.1-16.5-22.6-14.4-33.8-32.9-33.7-51.3" transform="translate(0)" fill="#1960a7"/>
  <path d="M299.7,354.8V335.2a142.11,142.11,0,0,0,35.9-14.7c23.9-14.3,35.9-33.3,36-52.4v19.6c0,19.1-12.1,38.1-36,52.4a143.63,143.63,0,0,1-35.9,14.7" transform="translate(0)" fill="#d6edfb"/>
  <path d="M170,218.1c47-28.1,122.2-27.3,167.9,1.8s44.6,75.6-2.4,103.7-122.2,27.3-167.9-1.8S123,246.2,170,218.1Z" transform="translate(0)" fill="#fff"/>
  <path d="M358,224.3c-34.8-22.2-83.5-31.1-129.3-26.5l2.7,17.3c37.6-4.2,77.9,3,106.5,21.2,45.7,29.1,44.6,75.6-2.4,103.7-37.7,22.6-93.6,26.5-137.8,12.1l-21.3,12.4C232.2,385.6,306,381.8,355,352.4,413.2,317.7,414.5,260.3,358,224.3Z" transform="translate(0)" fill="#fff"/>
  <path d="M167.6,338.3c-45.7-29.1-44.7-75.6,2.4-103.7,17.5-10.5,39-17,61.4-19.4l-2.7-17.3c-28.5,2.9-56,10.9-78.3,24.3-58.1,34.8-59.4,92.2-3,128.2a161.86,161.86,0,0,0,28.9,14.3l21.3-12.4A138.88,138.88,0,0,1,167.6,338.3Z" transform="translate(0)" fill="#cfd5db"/>
  <path d="M339,222c-18.9-12.1-42.9-19.3-67.8-21.6l17.1,110.8-57.8,33.6c37,4.6,76.9-1.7,106.1-19.1C383.7,297.6,384.7,251.2,339,222Z" transform="translate(0)" fill="#fff"/>
  <path d="M271.2,200.5c-35.3-3.3-72.6,3.3-100.1,19.8-47,28.1-48.1,74.6-2.4,103.7,17.4,11.1,39.1,18.1,61.9,20.9l57.8-33.6Z" transform="translate(0)" fill="#cfd5db"/>
  <g id="right-cloud">
    <path id="frame" d="M380.9,95.6l.7-.4a30.84,30.84,0,0,1-.8-6.8c0-17.6,12.4-39.1,27.9-47.9,7.8-4.5,14.8-4.9,19.9-1.9l16.7,9.6c-5.1-2.9-12.1-2.5-19.9,1.9C410,58.9,397.5,80.4,397.6,98a25.83,25.83,0,0,0,.8,6.8l-.7.4c-11.9,6.8-21.5,23.4-21.5,37,0,6.8,2.4,11.5,6.3,13.7l-16.7-9.6c-3.9-2.2-6.3-7-6.3-13.7C359.4,119,369,102.4,380.9,95.6Z" transform="translate(0)" fill="#1960a7"/>
    <path id="cloud" d="M425.4,50.1C410,58.9,397.5,80.4,397.6,98a25.83,25.83,0,0,0,.8,6.8l-.7.4c-11.9,6.8-21.5,23.4-21.5,37s9.7,19.1,21.6,12.2l54.1-30.9c14-8,25.3-27.5,25.3-43.5,0-15.3-10.5-21.9-23.7-15.3C452.8,48,440.5,41.4,425.4,50.1Z" transform="translate(0)" fill="#fff"/>
    <path id="a" d="M411,114l1.9-6.7,1.9,4.5L411,114m3.6-12.6-3.5,2L405,124l3.9-2.3,1-3.5,6.2-3.6,1,2.3,3.9-2.3-6.4-13.2" transform="translate(0)" fill="#1960a7"/>
    <polyline id="w" points="441.2 86 437.4 88.2 434.6 100.3 431.5 91.6 428.5 93.4 425.4 105.6 422.6 96.7 418.8 99 423.6 113.2 427.1 111.2 430 99.1 433 107.8 436.5 105.7 441.2 86" fill="#1960a7"/>
    <path id="s" d="M448.8,81.3c-3.4,2-5.8,5.5-5.8,8.9,0,2.7,1.4,3.5,4.3,2.5l2.4-.8c1.3-.4,1.6,0,1.6.5,0,.9-.7,1.9-2.1,2.7-1.7,1-2.8.8-3.1-.6l-3.6,2.1c.3,4,3.3,4,6.6,2.2,3.7-2.1,6.1-5.7,6.1-9,0-2.9-1.8-3.1-4.5-2.3l-2,.6c-1.1.4-1.7.3-1.7-.6a3.58,3.58,0,0,1,2.1-2.9c1.5-.8,2.2-.4,2.4.8l3.7-2.1c-.5-4.4-4-3.4-6.4-2" transform="translate(0)" fill="#1960a7"/>
  </g>
  <g id="logo">
    <polygon points="288.3 311.2 179.8 248.1 179.8 222.4 288.3 285.4 288.3 311.2" fill="#1960a7"/>
    <g>
      <path d="M288.3,285.4,179.8,222.3c14.3-8.2,27.6-20.2,39.3-34.4L327.6,251c-11.7,14.2-25.1,26.2-39.3,34.4" transform="translate(0)" fill="#fff"/>
      <path d="M327.5,251.1,219,188a201.29,201.29,0,0,0,18.3-26.5c15-26,24.2-55.4,24.1-82-.1-26.2-9.2-44.7-23.9-53.6L346,89c14.7,8.8,23.8,27.3,23.9,53.6.1,26.6-9.1,56-24.1,82a201.29,201.29,0,0,1-18.3,26.5" transform="translate(0)" fill="#1960a7"/>
    </g>
    <g>
      <path d="M252.2,253.1,143.7,190c5.7,3.3,12.7,4.2,20.6,2.6l108.5,63.1c-7.8,1.6-14.9.7-20.6-2.6" transform="translate(0)" fill="#1960a7"/>
      <path d="M272.8,255.7,164.3,192.6a53.85,53.85,0,0,0,15.4-6.1l108.5,63.1a53.85,53.85,0,0,1-15.4,6.1" transform="translate(0)" fill="#fff"/>
    </g>
    <path d="M324,211.7,215.5,148.6c9.4-16.3,15.2-34.8,15.2-51.4,0-16.3-5.7-27.9-14.8-33.4l-.4-.3,108.5,63,.4.3c9.1,5.5,14.8,17.1,14.8,33.4,0,16.8-5.8,35.2-15.2,51.5" transform="translate(0)" fill="#1960a7"/>
    <path d="M360.8,63c18.8,11,30.4,34.5,30.5,67.8s-11.4,70.2-30,102.8-44.4,61.1-73,77.5l-.1-25.8c22.5-13,42.8-35.3,57.5-60.9,15-26,24.2-55.4,24.1-82-.1-26.2-9.2-44.7-23.9-53.5Z" transform="translate(0)" fill="#d6edfb"/>
    <polygon points="346 89 237.5 26 252.3 0 360.8 63 346 89" fill="#fff"/>
    <g>
      <path d="M258.5,241.6,150,178.6c-7.7-4.5-12.4-14-12.5-27.6,0-13.3,4.5-28.1,11.9-41.2a101.43,101.43,0,0,1,9.8-14.4l108.5,63.1a93.7,93.7,0,0,0-9.8,14.4C250.5,186,246,200.7,246,214c0,13.6,4.8,23.1,12.5,27.6" transform="translate(0)" fill="#1960a7"/>
      <path d="M267.8,158.4l-108.5-63a75.2,75.2,0,0,1,20.1-17.6l108.5,63.1a74.84,74.84,0,0,0-20.1,17.5" transform="translate(0)" fill="#fff"/>
    </g>
    <path d="M287.9,165.9c-5.8,3.4-11,9.2-14.8,15.9-3.5,6.3-5.7,13.4-5.6,19.8,0,6.8,2.5,11.5,6.4,13.6l-15.1,26.5c-7.8-4.4-12.7-14-12.7-27.7,0-13.3,4.5-28.1,11.9-41.2,7.6-13.5,18.2-25.2,29.9-32Z" transform="translate(0)" fill="#d6edfb"/>
    <g>
      <path d="M287.7,105,179.2,42A74.82,74.82,0,0,1,201,33.3l108.5,63a78.92,78.92,0,0,0-21.8,8.7" transform="translate(0)" fill="#fff"/>
      <path d="M309.5,96.3,201,33.3c11.2-2.4,21.3-1,29.5,3.8l108.5,63c-8.2-4.7-18.2-6.2-29.5-3.8" transform="translate(0)" fill="#1960a7"/>
    </g>
    <path d="M339.2,220.7l-15.2-9c9.4-16.3,15.2-34.8,15.2-51.4,0-16.3-5.7-27.9-14.8-33.4-9.3-5.7-22.3-5-36.5,3.2l-.1-25c20.2-11.7,38.5-12.6,51.7-4.6,13,7.8,21.1,24.3,21.1,47.5C360.7,171.5,352.5,197.6,339.2,220.7Z" transform="translate(0)" fill="#d6edfb"/>
    <polygon points="287.8 130.1 179.3 67 179.2 42 287.7 105 287.8 130.1" fill="#1960a7"/>
    <path d="M237.1,279.6,128.6,216.5c-13.4-7.7-21.7-24.3-21.8-47.9-.1-23.2,7.9-48.9,20.8-71.7L236.1,160c-12.9,22.8-20.9,48.5-20.8,71.7.1,23.7,8.4,40.2,21.8,47.9" transform="translate(0)" fill="#1960a7"/>
    <path d="M288.1,249.6l.1,25.1c-19.9,11.5-38,12.5-51.1,5-13.4-7.7-21.7-24.3-21.8-47.9-.1-23.2,7.9-48.9,20.8-71.7l15.2,9c-9.1,16-14.6,34-14.6,50.4,0,16.7,6,28.4,15.4,33.8C261.5,258.4,274.2,257.6,288.1,249.6Z" transform="translate(0)" fill="#d6edfb"/>
    <path d="M229.6,156.2c-14.6,25.7-23.6,54.7-23.5,80.9.1,26.6,9.4,45.3,24.5,53.9l-15.4,27c-18.8-11-30.4-34.5-30.5-67.8s11.4-70.2,30-102.8,44.4-61,73-77.5l.1,24.3C265,107.4,244.4,130.1,229.6,156.2Z" transform="translate(0)" fill="#d6edfb"/>
    <g>
      <path d="M215.2,318c-18.8-11-30.4-34.5-30.5-67.8s11.4-70.2,30-102.8a256,256,0,0,1,23.5-34.2l-108.5-63a243.77,243.77,0,0,0-23.5,34.2c-18.6,32.6-30.1,69.5-30,102.8S87.9,244,106.7,255l108.5,63" transform="translate(0)" fill="#1960a7"/>
      <path d="M238.1,113.3,129.6,50.2c14.7-17.9,31.6-33,49.5-43.4L287.6,69.9c-18,10.4-34.8,25.5-49.5,43.4" transform="translate(0)" fill="#fff"/>
    </g>
  </g>
  <path d="M366.2,467.7,344.1,455c6.5-4.4,6.2-11-1-15.1-7.5-4.4-19.7-4.4-27.2,0s-7.4,11.3.1,15.7,19.3,4.4,26.8.3L365.5,469a.69.69,0,0,0,.9-.3.78.78,0,0,0-.2-1Z" transform="translate(0)" fill="#16204b"/>
  <path d="M384.2,427.5a18.6,18.6,0,0,1-37.2,0v-29a18.6,18.6,0,0,1,37.2,0Z" transform="translate(0)" fill="#009983"/>
  <line x1="365.9" y1="419.4" x2="365.9" y2="468.1" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
  <line x1="365.9" y1="438.9" x2="376.3" y2="429.8" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
  <path d="M365.9,419.4" transform="translate(0)" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.4"/>
  <path d="M359.2,426" transform="translate(0)" fill="none" stroke="#6f58a2" stroke-miterlimit="10" stroke-width="1.4"/>
  <line x1="361.1" y1="430.2" x2="365.9" y2="433.3" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
  <line x1="370.7" y1="420.7" x2="365.9" y2="423.8" fill="none" stroke="#6f58a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
  <ellipse cx="366.3" cy="284.5" rx="3.8" ry="4.7" fill="#1960a7"/>
  <path d="M361.4,295.3c0-3.3,2.2-4.1,4.9-4.1s4.9.9,4.9,4.1v14.3h-9.8Z" transform="translate(0)" fill="#1960a7"/>
  <path d="M370.1,300.1l-11.2,8.7a.65.65,0,0,1-.9-.2.64.64,0,0,1,0-.6l7.2-12Z" transform="translate(0)" fill="#1960a7"/>
  <path d="M365.2,318.4a2.35,2.35,0,0,1,1.7,2.9l-.5,1-5.7,12.2a.65.65,0,0,1-.9.2.78.78,0,0,1-.3-.7l2.5-13.3.2-.9A2.52,2.52,0,0,1,365.2,318.4Z" transform="translate(0)" fill="#1960a7"/>
  <path d="M369,309.6l-1.9,11.1-.2,1.1a2.44,2.44,0,0,1-4.6-.3l-.1-1-.9-10.9Z" transform="translate(0)" fill="#1960a7"/>
  <path d="M363.9,318.5a2.38,2.38,0,0,1,3,1.4l.2,1.1,2.3,13.3a.73.73,0,0,1-.6.7.55.55,0,0,1-.6-.4l-5.5-12.4-.4-.9A2.36,2.36,0,0,1,363.9,318.5Z" transform="translate(0)" fill="#1960a7"/>
  <path d="M363.6,308.8h0v-.1a3.66,3.66,0,0,1,4.5-2.9,3.7,3.7,0,0,1,2.9,4.5v.1h0a3.55,3.55,0,0,1-.4.9l-3.7,9.5-.4,1a2.43,2.43,0,0,1-3.4.6,2.23,2.23,0,0,1-1-1.8l.1-.9,1.3-10.5V309Z" transform="translate(0)" fill="#1960a7"/>
  <g id="left-cloud">
    <path id="frame-2" data-name="frame" d="M54.5,233.2l.8-.4a33.59,33.59,0,0,1-.8-7.1c0-18.4,12.9-40.7,29-49.9,8.1-4.6,15.4-5.1,20.7-2l17.4,10c-5.3-3.1-12.6-2.6-20.7,2-16.1,9.2-29.1,31.6-29,49.9a33.59,33.59,0,0,0,.8,7.1l-.8.4c-12.4,7.1-22.4,24.4-22.4,38.5,0,7,2.5,12,6.5,14.3L38.6,286c-4-2.3-6.5-7.3-6.5-14.3C32.1,257.5,42.1,240.3,54.5,233.2Z" transform="translate(0)" fill="#1960a7"/>
    <path id="cloud-2" data-name="cloud" d="M100.9,185.7c-16.1,9.2-29.1,31.6-29,49.9a33.59,33.59,0,0,0,.8,7.1l-.8.4c-12.4,7.1-22.4,24.4-22.4,38.5S59.6,301.5,72,294.4l56.4-32.2c14.6-8.4,26.4-28.7,26.3-45.3,0-16-10.9-22.8-24.7-16C129.4,183.6,116.6,176.7,100.9,185.7Z" transform="translate(0)" fill="#fff"/>
    <polygon id="window" points="103.8 255.3 103.8 240.3 121.8 229.9 121.9 247.7 103.8 255.3" fill="#1960a7"/>
    <polygon id="window-2" data-name="window" points="103.7 222.2 121.8 208.9 121.8 226.8 103.8 237.2 103.7 222.2" fill="#1960a7"/>
    <polygon id="window-3" data-name="window" points="87.8 261.9 87.7 249.6 101.1 241.8 101.1 256.4 87.8 261.9" fill="#1960a7"/>
    <polygon id="window-4" data-name="window" points="87.7 246.5 87.7 234.1 101 224.2 101.1 238.8 87.7 246.5" fill="#1960a7"/>
  </g>
  <g id="top-cloud">
    <path id="frame-3" data-name="frame" d="M46.6,80.2l.9-.5a38.37,38.37,0,0,1-.9-8.3c-.1-21.3,15-47.3,33.7-58,9.4-5.4,18-5.9,24.1-2.4l20.2,11.6c-6.2-3.5-14.7-3-24.1,2.3C81.8,35.7,66.7,61.7,66.8,83a38.37,38.37,0,0,0,.9,8.3l-.9.5c-14.4,8.3-26.1,28.3-26,44.8,0,8.2,2.9,13.9,7.6,16.6L28.2,141.6c-4.7-2.7-7.6-8.5-7.6-16.6C20.6,108.5,32.2,88.5,46.6,80.2Z" transform="translate(0)" fill="#1662a8"/>
    <path id="cloud-3" data-name="cloud" d="M100.5,25C81.8,35.7,66.7,61.7,66.8,83a38.37,38.37,0,0,0,.9,8.3l-.9.5c-14.4,8.3-26.1,28.3-26,44.8S52.6,159.8,67,151.5l65.5-37.4c17-9.7,30.7-33.3,30.6-52.7-.1-18.5-12.7-26.5-28.7-18.6C133.7,22.5,118.8,14.6,100.5,25Z" transform="translate(0)" fill="#fff"/>
    <path id="window-5" data-name="window" d="M98.3,87A17,17,0,0,1,106,73.7c4.3-2.5,7.7-.5,7.8,4.4A16.86,16.86,0,0,1,106,91.4c-4.3,2.5-7.7.5-7.7-4.4m16.3-26.8L97.2,70.3,88.6,92.4l8.9,12.2,17.3-10,8.4-22.1-8.6-12.3" transform="translate(0)" fill="#d6edfb"/>
    <path id="window-6" data-name="window" d="M88.6,92.4l8.6-22.1,17.4-10.1,8.6,12.3-8.4,22.1-17.3,10L88.6,92.4m29.6-45.6L93.7,60.9A8.89,8.89,0,0,0,90.4,65L83.3,83.3,78.2,96.4c-.6,1.6-.6,3.5,0,4.4l12.3,17.3c.6.9,2.1,1,3.3.3l24.4-14.1a9.43,9.43,0,0,0,3.3-4.1l12.2-31.4c.6-1.6.6-3.5,0-4.4l-7.3-10.3-5-7a2.44,2.44,0,0,0-3.2-.3" transform="translate(0)" fill="#1a60a7"/>
    <path id="cloud-4" data-name="cloud" d="M106,73.7A16.91,16.91,0,0,0,98.3,87c0,4.9,3.5,6.9,7.8,4.4a17.18,17.18,0,0,0,7.7-13.3c-.1-4.9-3.6-6.9-7.8-4.4" transform="translate(0)" fill="#fff"/>
  </g>
</svg>
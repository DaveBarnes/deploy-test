<?php
if ( $hs_form['hs_id'] ):
	$form = new HubSpotForm( $hs_form['hs_id'], $hs_form['title'], $hs_form['redirect'], $hs_form['submit_text'], $hs_form['hidden_variables'] );

	$form_title = (strpos($hs_form['title'], ' - ') === FALSE ? $hs_form['title'] : substr($hs_form['title'],strpos($hs_form['title'], ' - ')+3));

	$form_action = $override_eventcategory ? $override_eventcategory : 'Form Submission';
	$form_title = $override_action ? $override_action : $form_title;
	$form_label = $override_form_label ? $override_form_label : get_the_title();

	?>
	<div class="conversion-form-mask js-conversion-form-mask is-hidden" data-form-id="<?php echo $hs_form['uid']; ?>"></div>
	<section class="conversion-form js-conversion-form is-hidden" id="<?php echo $hs_form['uid']; ?>">
		<div class="ptb-xl conversion-form__background js-conversion-form-background">
			<form action="/api/" class="section-wrap section-wrap--restricted conversion-form__form form js-analytics-form" method="post" enctype="application/x-www-form-urlencoded" name="conversion-form" data-parsley-validate>

				<div class="inner-wrap-@-sm">

					<div class="conversion-form__header">
						<h2 class="conversion-form__heading fs-xl fw-semibold mb-<?php echo( $hs_form['intro'] ? 'm' : 'l' ); ?>"><?php echo $form_title; ?></h2>
						<a href="#" class="conversion-form__close-button fc-white fs-s js-conversion-form-close-button">
							<span>CLOSE</span>
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="612px" height="792px" viewBox="0 0 612 792" enable-background="new 0 0 612 792" xml:space="preserve">
							<rect x="30.922" y="309.339" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -152.0917 316.6652)" fill="#0f285a" width="550.562" height="65.168"/>
								<rect x="30.922" y="309.338" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 280.9468 800.2171)" fill="#0f285a" width="550.562" height="65.168"/>
						</svg>
						</a>
						<?php if ( $hs_form['intro'] ): ?>
							<div class="mb-l">
								<?php echo $hs_form['intro']; ?>
							</div>
						<?php endif; ?>
					</div>

					<?php
					if ( $form->formData['fields'] ):
						$text_field_count = 0;
						$field_count      = count( $form->formData['fields'] );
						foreach ( $form->formData['fields'] as $f ) {
							if ( $f['type'] == 'text' ) {
								$text_field_count ++;
							}
						}

					endif;
					?>
					<div class="form-layout <?php echo 'form-layout--' . $field_count . '-items form-layout--' . $text_field_count . '-text-fields'; ?>">
						<!-- fields -->
						<?php if ( $form->formData['fields'] ):

							?>
							<?php foreach ( $form->formData['fields'] as $field ):
							if ( $field['type'] != 'hidden' && $field['type'] != 'textarea' ): ?>
								<div class="form-layout__field <?php if ( $field['type'] == 'textarea' ): ?>form-layout__field--textarea <?php endif; ?>">
									<label class="form__label fc-white" for="<?php print $field['id']; ?>"><?php print $field['label']; ?>:</label>
									<?php print $field['markup']; ?>
								</div>
							<?php endif;
						endforeach; ?>
						<?php endif; ?>

						<div class="form-layout__field form-layout__field--empty">

						</div>

						<?php foreach ( $form->formData['fields'] as $field ):
							if ( $field['type'] == 'textarea' ): ?>
								<div class="form-layout__field <?php if ( $field['type'] == 'textarea' ): ?>form-layout__field--textarea <?php endif; ?>">
									<label class="form__label fc-white" for="<?php print $field['id']; ?>"><?php print $field['label']; ?>:</label>
									<?php print $field['markup']; ?>
								</div>
							<?php endif;
						endforeach; ?>

						<?php foreach ( $form->formData['fields'] as $field ):
							if ( $field['type'] == 'hidden' ):
								print $field['markup'];
							endif;
						endforeach;
						print $form->formData['hiddenFields'];
						?>
						<input type="hidden" name="eventcategory" class="js-eventcategory" value="<?php echo $form_action; ?>">
						<input type="hidden" name="eventaction" class="js-eventaction" value="<?php echo $form_title; ?>">
						<input type="hidden" name="eventlabel" class="js-eventlabel" value="<?php the_title(); ?>">

						<div class="form-layout__button">
							<button type="submit" class="cta cta--large cta--secondary mt-l js-conversion-form-submit">
								<span class="line-height-adjust"><?php echo $hs_form['submit_text']; ?></span>
							</button>
						</div>

					</div>

				</div>

			</form>
		</div>
	</section>
	<?php
endif;

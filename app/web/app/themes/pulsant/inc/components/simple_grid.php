<section class="<?php echo get_sub_field( 'grid_items_bg_colour' ); ?>">
	<?php

	$heading_class = get_sub_field( 'grid_items_header_modifiers' );
	if ( ! $heading_class ) {
		$heading_class = 'fc-dark-blue fw-semibold fs-l';
	}

	if ( get_sub_field( 'title' ) ): ?>
		<div class="section-wrap">
			<h1 class="bgc-white ta-center pt-l fc-dark-blue fw-semibold fs-xl"><?php the_sub_field( 'title' ); ?></h1>
		</div>
	<?php endif;

	if ( have_rows( 'grid_items_items' ) ) : ?>
		<div class="bgc-white pt-l">
			<div class="section-wrap section-wrap--restricted">
				<div class="simple-grid <?php echo( is_page_template( 'page-templates/page-value-proposition.php' ) ? 'simple-grid--val-prop' : '' ); ?>">
					<?php while ( have_rows( 'grid_items_items' ) ) :
						the_row();
						$image        = get_sub_field( 'grid_item_image' );
						$link         = get_sub_field( 'grid_item_link' );
						$copy         = get_sub_field( 'grid_item_copy' );
						$motif        = get_sub_field( 'grid_item_heading_motif' );
						$main_heading = get_sub_field( 'grid_item_main_heading' );
						$newtab       = get_sub_field( 'grid_item_newtab' );

						$is_job      = get_sub_field( 'grid_item_is_job', $pid );
						$expiry_date = get_sub_field( 'grid_item_expiry_date', $pid );

						if ( ! $is_job || ( $expiry_date && date( 'Ymd' ) < $expiry_date ) ):
							?>
							<div class="simple-grid__item">
								<div class="simple-grid__item-spacing">
									<div class="inner-wrap-@-sm">
										<article class="<?php print strpos( $heading_class, 'arrow' ) !== FALSE ? 'ta-center' : 'article-attract'; ?>">

											<?php if ( $image ) { ?>
												<a href="<?php print $link['url']; ?>"><img src="<?php print $image['url']; ?>" alt="<?php print $image['alt']; ?>" class="responsive-img <?php print strpos( $heading_class, 'arrow' ) !== FALSE ? '' : 'article-attract__img'; ?>"></a>
											<?php }

											if ( $motif ) : ?>
												<div class="service-text-collection__intro-heading">
													<h1 class="intro-heading intro-heading--motif intro-heading--retain-small"><?php print $motif; ?></h1>
												</div>
											<?php endif; ?>
											<div class="<?php print strpos( $heading_class, 'arrow' ) !== FALSE ? '' : 'article-attract__body'; ?>">
												<div class="<?php print strpos( $heading_class, 'arrow' ) !== FALSE ? '' : 'article-attract__main-heading'; ?>">
													<h2 class="<?php print $heading_class; ?>"><?php print $main_heading; ?></h2>
												</div>
												<div class="article-attract__copy">
													<?php print $copy; ?>
												</div>
												<div class="article-attract__cta">
													<a href="<?php print $link['url']; ?>" <?php if ( $newtab == 1 ) { ?>target="_blank"<?php } ?> class="cta"><?php print $link['title']; ?></a>
												</div>
											</div>
										</article>
									</div>
								</div>
							</div>
						<?php endif;
					endwhile; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
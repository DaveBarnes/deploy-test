<?php
$bgclass      = ' ';
$sectionclass = ' ';
if ( get_sub_field( 'icons_bg_white' ) ) {
	$bgclass .= ' bgc-white';
}

if ( get_sub_field( 'icons_width' ) ) {
	$sectionclass .= ' section-wrap--' . get_sub_field( 'icons_width' );
}

if ( get_sub_field( 'icons_spacing_classes' ) ) {
	$spacing = get_sub_field( 'icons_spacing_classes' );
} else{
	$spacing = 'ptb-l ptb-xl--@-sm';
}

?>
<section class="<?php print $bgclass; ?>">
	<div class="section-wrap inner-wrap-@-sm<?php print $sectionclass; ?>">
		<div class="<?php echo $spacing; ?>">
			<div class="spotlight">

				<?php if ( get_sub_field( 'icons_heading' ) ): ?>
					<div class="spotlight">
						<div class="spotlight__heading">
							<h1 class="fc-dark-blue fw-semibold fs-xl"><?php the_sub_field( 'icons_heading' ); ?></h1>
						</div>
					</div>
				<?php endif; ?>

				<?php
				$format = get_sub_field( 'icons_format' );
				if ( empty( $format ) ) {
					$format = '';
				}
				?>
				<div class="logo-collection <?php echo $format; ?>">
					<?php if ( have_rows( 'icons_logos' ) ) :
						while ( have_rows( 'icons_logos' ) ) : the_row();
							$post_object = get_sub_field( 'logo' );
							if ( $post_object ):
								$post = $post_object;
								setup_postdata( $post );
								$size    = get_field( 'size', $post->ID );
								$icon    = get_field( 'image', $post->ID );
								$company = 'logo-collection__item--' . preg_replace( '/[^\da-z]/i', '', trim( strtolower( get_the_title() ) ) ); ?>
								<div class="logo-collection__item <?php print $company; ?> logo-collection__item--<?php print $size; ?> js-in-view-item"><img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>"/></div>
								<?php wp_reset_postdata();
							endif;
						endwhile;
					endif; ?>
				</div>

				<?php
				$link = get_sub_field( 'icons_cta_link' );
				if ( ! $link ) {
					$cta = get_sub_field( 'icons_cta_list' );
					if ( $cta ) {
						switch ( $cta ) {
							case 'partners':
								$link['url']   = '/why-us/vendor-partners/';
								$link['title'] = 'View our partners';
								break;
							case 'accreditations':
								$link['url']   = '/why-us/accreditations/';
								$link['title'] = 'See our accreditations';
								break;
							case 'customers':
								$link['url']   = '/knowledge-hub/casestudy/';
								$link['title'] = 'See our case studies';
								break;
						};
					}
				}

				if ( $link && isset($link['title']) ): ?>
					<div class="spotlight__cta ta-center pt-l">
						<a href="<?php print $link['url']; ?>" class="cta"><?php print $link['title']; ?></a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
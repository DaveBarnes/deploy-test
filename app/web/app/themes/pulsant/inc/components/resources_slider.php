<section class="ptb-xl bgc-white inner-wrap-@-sm">
	<div class="section-wrap section-wrap--restricted">
		<?php if ( get_sub_field( 'resources_heading' ) ): ?>
		<h1 class="motif fs-xl fw-semibold fc-dark-blue pb-l"><?php the_sub_field( 'resources_heading' ); ?></h1>
		<?php endif; ?>

		<div class="case-study-collection">
			<?php if ( have_rows( 'resources_resources' ) ) : ?>
				<div class="case-study-collection__items js-case-study-collection">
					<?php
					while ( have_rows( 'resources_resources' ) ) : the_row();
						$post_object = get_sub_field( 'resource_item' );
						$custom_url = get_sub_field( 'custom_url' );
						if ( $post_object ) :

							$post = $post_object;
							setup_postdata( $post );

							$resource_type = get_field( 'type' );
							$img           = get_the_post_thumbnail( );
							$link          = get_permalink();
							$tagline       = get_field( 'slider_synopsis' );
							$slide_title   = get_field( 'slider_title' );
							$cta_text      = 'View ' . $resource_type['label'];
							if ( ! $slide_title ) {
								$slide_title = get_the_title();
							}

							if($custom_url != '') {
								$link = $custom_url;
								$resource_type['label'] = '';
								$cta_text = 'Read More';
							}

							?>
							<div class="case-study-collection__item">

								<article class="case-study">
									<div class="case-study__image" style="background-image: url(<?php the_post_thumbnail_url( 'resource-thumb'); ?>)">
										
									</div>
									<div class="case-study__copy">
										<span class="case-study__intro-heading"><?php echo $resource_type['label']; ?></span>
										<h1 class="case-study__main-heading fs-xl fw-semibold"><?php echo $slide_title; ?></h1>
										<div class="case-study__body"><?php echo $tagline; ?></div>
										<div class="case-study__link">
											<a class="cta" href="<?php echo $link; ?>"><?php echo $cta_text; ?></a>
										</div>
									</div>
								</article>

							</div>
							<?php

							wp_reset_postdata();
						endif;
					endwhile;
					?>
				</div>

				<div class="case-study-collection__controls js-case-study-collection-controls">
				</div>
			<?php endif; ?>
		</div>
		<?php
		$pl         = get_permalink( 609 );
		$categories = get_the_category();
		$cat_url    = '';
		if ( $categories ) {
			$category_id = $categories[0]->cat_ID;
			$cat_url     = '?hubcat=' . $categories[0]->slug;
			$cat_name    = $categories[0]->name;
		}
		?>
		<div class="ta-center mt-m">
			<div class="attention-bar__cta">
				<a class="cta" href="<?php echo $pl . $cat_url; ?>">Explore All <?php echo $cat_name; ?> Resources</a>
			</div>
		</div>
	</div>
</section>

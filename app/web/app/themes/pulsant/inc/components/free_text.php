<section class="ptb-xl <?php echo( ! get_sub_field( 'text_bg_colour' ) ? 'bgc-white' : get_sub_field( 'text_bg_colour' ) ); ?>">
	
	<?php if ( get_sub_field( 'text_headline' ) ) { ?>
	<div class="section-wrap section-wrap--narrow inner-wrap-@-sm">
		<h1 class="motif fw-semibold fs-xl fc-dark-blue pb-l"><?php the_sub_field( 'text_headline' ); ?></h1>
	</div>
	<?php } ?>

	<div class="section-wrap section-wrap--restricted inner-wrap-@-sm">
		<?php the_sub_field( 'text_body' ); ?>
	</div>

	<?php if ( get_field( 'brochure_button' ) == 1 ) { ?>
		<div class="ta-center pt-l">
			<a href="#" class="cta cta--large cta--primary">
				<span class="line-height-adjust">Download brochure</span>
			</a>
		</div>
	<?php } ?>

</section>
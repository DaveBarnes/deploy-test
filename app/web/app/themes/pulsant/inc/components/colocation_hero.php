<?php
$nav_items = wp_get_nav_menu_items( 'header-menu' );

foreach ( $nav_items as $item ) {
	if ( $item->object_id == $post->ID ) {
		$location_menu_id = $item->menu_item_parent;
		$parent_id        = get_post_meta( $item->menu_item_parent, '_menu_item_object_id', TRUE );
		if ( $parent_id ) {
			$parent       = get_post( $parent_id );
			$grand_parent = get_post( $parent->post_parent );
		}
		break;
	}
}

$locations = [];

foreach ( $nav_items as $item ) {
	if ( $item->menu_item_parent == $location_menu_id ) {
		$locations[] = array( 'title' => $item->title, 'url' => $item->url );
	}
}

$cta_link = get_sub_field( 'cta_url_picker' );

if ( have_rows( 'item_cta' ) ):
	while ( have_rows( 'item_cta' ) ) : the_row();
		$form                        = get_sub_field( 'form' );
		$cta_text                    = get_sub_field( 'cta_text' );
		$id                          = $form->ID;
		$hs_form['title']            = get_the_title( ) . ' - ' . ucwords(strtolower(get_the_title( $id )));
		$hs_form['hs_id']            = get_field( 'form_id', $id );
		$hs_form['uid']              = substr( $hs_form['hs_id'], 0, 5 ) . rand( 9999, 99999 );
		$hs_form['intro']            = get_field( 'form_intro', $id );
		$hs_form['class']            = get_field( 'js_class', $id );
		$hs_form['redirect']         = get_field( 'redirect_url', $id );
		$hs_form['submit_text']      = get_field( 'submit_text', $id );
		$hs_form['hidden_variables'] = array( 'tour_location' => get_the_title() );

		$cta_link['title'] = get_sub_field( 'cta_text' );
		$cta_link['url']   = '';

	endwhile;
	include realpath( dirname( __FILE__ ) ) . '/../partials/conversion-form.php';
endif;


?>

<div class="hero-banner-shift"></div>
<section class="colocation-hero-banner js-hero-banner">
	<div class="colocation-hero-banner__wrap">
		<div class="colocation-hero-banner__main">
			<div class="colocation-hero-banner__main-spacing">
				<div class="colocation-hero-banner__main-wrap inner-wrap-@-sm">
					<div class="colocation-hero-banner__breadcrumb">
						<ul class="breadcrumb fs-s fc-white">
							<?php if ( $grand_parent ): ?>
								<li class="breadcrumb__item">
									<a class="breadcrumb__link" href="<?php echo get_permalink( $grand_parent ); ?>"><?php echo $grand_parent->post_title; ?></a>
								</li>
							<?php endif; ?>
							<li class="breadcrumb__item">
								<a class="breadcrumb__link" href="<?php echo get_permalink( $parent ); ?>"><?php echo $parent->post_title; ?></a>
							</li>
							<li class="breadcrumb__item">
								<a class="breadcrumb__link"><?php echo $post->post_title; ?></a>
							</li>
						</ul>
					</div>

					<div class="colocation-hero-banner__information">
						<h1 class="colocation-hero-banner__heading fc-white fs-xl"><?php the_sub_field( 'item_heading' ); ?></h1>

						<div class="colocation-hero-banner__copy fc-white"><?php the_sub_field( 'item_copy' ); ?></div>

						<div class="colocation-hero-banner__ctas">
							<?php $item_cta = get_sub_field( 'item_cta' ); ?>
							<a href="<?php echo $cta_link['url']; ?>" data-form-id="<?php echo $hs_form['uid']; ?>" class="cta cta--large cta--secondary js-open-conversion-form">
								<span class="line-height-adjust">
									<?php echo $cta_link['title']; ?>
								</span>
							</a>

							<?php if ( $locations ): ?>
								<div class="cta cta--large cta--hollow cta--hollow-light cta--select cta--select-wrap">
									<select class="js-select-goto-href" name="" id="">
										<option value="">Choose a new location</option>
										<?php foreach ( $locations as $location ) { ?>
											<option value="<?php echo $location['url']; ?>"><?php echo $location['title']; ?></option>
										<?php } ?>
									</select>
								</div>
							<?php endif; ?>
						</div>
					</div>

				</div>
				<div class="hero-banner-mask js-hero-banner-mask"></div>
			</div>
		</div>

		<div class="colocation-hero-banner__aside">
			<div class="colocation-hero-banner__image">
				<?php
				$image = get_sub_field( 'item_image' );
				$thumb = wp_get_attachment_image_src( $image['ID'], array(920,668) );
				$thumb = array_shift( $thumb );
				?>
				<img src="<?php echo $thumb; ?>" class="responsive-img" alt="">
			</div>
			<div class="colocation-hero-banner__address">
				<div class="inner-wrap-@-sm">
					<p class="mb-m">
						<?php the_sub_field( 'item_address' ); ?>
					</p>

					<a href="<?php the_sub_field( 'item_map_link' ); ?>" class="cta" target="_blank">View Map</a>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="bgc-<?php print get_field( 'cta_bg_colour', 'option' ); ?> ptb-<?php print get_field( 'cta_padding', 'option' ); ?>">
	<section class="attention-bar <?php if(get_field( 'cta_bg_colour', 'option' ) == 'white') print 'attention-bar--white';?> attention-bar--single">
		<div class="section-wrap inner-wrap-@-sm">
			<div class="attention-bar__wrap">
				<div class="attention-bar__primary">
					<div class="fc-dark-blue fw-semibold fs-l"><?php the_field( 'cta_text', 'option' ); ?></div>
					<div class="attention-bar__cta">
						<?php $cta_link = get_field( 'cta_cta_link', 'option' ); ?>
						<a class="cta cta--large cta--secondary" href="<?php echo $cta_link['url']; ?>"><?php echo $cta_link['title']; ?></a>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<section class="video-feature js-home-video-feature">
	<div class="video-feature__video-wrap">
		<video class="video-feature__video js-home-video" width="1920" height="800" controls="" controlslist="nodownload" autobuffer="" style="opacity: 0; pointer-events: none;">
			<?php if ( get_sub_field( 'video_video_mp4' ) ) { ?>
				<source src="<?php the_sub_field( 'video_video_mp4' ); ?>">
			<?php }
			if ( get_sub_field( 'video_video_webm' ) ) { ?>
				<source src="<?php the_sub_field( 'video_video_webm' ); ?>" type="video/webm">
			<?php } ?>
		</video>
		<?php if ( get_sub_field( 'video_overlay' ) ) { ?>
			<img src="<?php the_sub_field( 'video_overlay' ); ?>" alt="Video Preview" class="video-feature__img responsive-img">
		<?php } ?>

		<div class="video-feature__overlay"></div>

		<button class="video-feature__play-button js-home-video-start-button">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 115 60" enable-background="new 0 0 115 60" xml:space="preserve" class="video-feature__play-button-icon">
                            <polygon opacity="0.5" fill="#FFFFFF" points="38.7,54.6 38.7,6.8 78,30.7 "></polygon>
                        </svg>
		</button>

		<button class="video-feature__close-button js-home-video-close-button">
			<i><span class="cross"></span></i>
		</button>
	</div>

	<div class="video-feature__copy-wrap ptb-l ptb-n--@-lg js-copy">
		<div class="section-wrap section-wrap--restricted inner-wrap-@-sm">
			<div class="video-feature__copy fc-white">
				<div class="video-feature__intro-heading">
					<h1 class="motif fs-s"><?php the_sub_field( 'video_intro_heading' ); ?></h1>
				</div>
				<div class="video-feature__main-heading">
					<h2 class="fw-semibold fs-xl"><?php the_sub_field( 'video_main_heading' ); ?></h2>
				</div>
				<p><?php the_sub_field( 'video_text' ); ?></p>
			</div>
		</div>
	</div>
</section>
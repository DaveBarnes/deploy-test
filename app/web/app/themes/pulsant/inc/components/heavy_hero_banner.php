<?php
$cta_count = get_sub_field( 'heavy_hero_banner_heavy_cta_count' );


if ( is_array( $cta_count ) ) {
	$cta_count = $cta_count[0];
}
$is_cta_form = get_sub_field( 'heavy_hero_banner_heavy_cta_form_cta' );
$cta_link    = get_sub_field( 'heavy_hero_banner_heavy_cta_link' );
$second_cta  = get_sub_field( 'heavy_hero_banner_heavy_cta_second_cta' );

if ( $cta_count > 0 && $is_cta_form ):
	$form                   = get_sub_field( 'heavy_hero_banner_heavy_cta_form' );
	$id                     = $form->ID;
	$hs_form['title']       = get_the_title( ) . ' - ' . ucwords(strtolower(get_the_title( $id )));
	$hs_form['hs_id']       = get_field( 'form_id', $id );
	$hs_form['uid']         = substr( $hs_form['hs_id'], 0, 5 ) . rand( 9999, 99999 );
	$hs_form['intro']       = get_field( 'form_intro', $id );
	$hs_form['class']       = get_field( 'js_class', $id );
	$hs_form['redirect']    = get_field( 'redirect_url', $id );
	$hs_form['submit_text'] = get_field( 'submit_text', $id );

	$cta_link['title']  = get_sub_field( 'heavy_hero_banner_heavy_cta_text' );
	$cta_link['url']    = '';
	$cta_link['target'] = '';

	include realpath( dirname( __FILE__ ) ) . '/../partials/conversion-form.php';
else:

endif;
?>
<section class="heavy-hero-banner js-hero-banner <?php echo get_sub_field( 'heavy_hero_banner_extra_modifiers' ); ?>">
	<div class="section-wrap section-wrap--restricted inner-wrap-@-sm">
		<?php
		if ( $modifiers = get_sub_field( 'heavy_hero_banner_modifiers' ) ) {
			$modifiers = ' ' . implode( ' ', $modifiers );
		}

		if ( get_sub_field( 'heavy_hero_banner_balance_modifier' ) ) {
			$balance_modifiers = get_sub_field( 'heavy_hero_banner_balance_modifier' );
		} else {
			$balance_modifiers = "hero-feature--text-focus";
		}
		?>
		<div class="hero-feature hero-feature--heavy-banner hero-feature--dark <?php echo $modifiers; ?> <?php echo $balance_modifiers; ?>">
			<div class="hero-feature__aside">
				<?php $post_object = get_sub_field( 'heavy_hero_banner_aside' );
				if ( $post_object ) {
					$post = $post_object;
					setup_postdata( $post );
					the_content();
					wp_reset_postdata();
				} else {
					if ( $img = get_sub_field( 'heavy_hero_banner_graphic' ) ) { ?>
						<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">
					<?php }
				} ?>
			</div>
			<div class="hero-feature__text">
				<div class="service-text-collection">
					<h1 class="service-text-collection__mini-heading motif">
						<?php the_sub_field( 'heavy_hero_banner_mini_heading' ); ?>
					</h1>

					<div class="service-text-collection__main-heading">
						<h2 class="heavy-hero-banner__heading fc-white">
							<?php the_sub_field( 'heavy_hero_banner_main_heading' ); ?>
						</h2>
					</div>

					<div class="service-text-collection__copy">
						<?php if ( get_sub_field( 'heavy_hero_banner_copy' ) ): ?>
							<?php the_sub_field( 'heavy_hero_banner_copy' ); ?>
						<?php endif; ?>
						<?php if ( $cta_count > 0 ): ?>
							<div class="heavy-hero-banner__cta">
								<a href="<?php echo $cta_link['url']; ?>" target="<?php echo $cta_link['target']; ?>" data-form-id="<?php echo $hs_form['uid']; ?>" class="cta cta--large cta--primary <?php echo $is_cta_form ? 'js-open-conversion-form' : ''; ?>">
										<span class="line-height-adjust">
											<?php echo $cta_link['title']; ?>
										</span>
								</a>
								<?php if ( $cta_count > 1 ): ?>
									<a href="<?php echo $second_cta['url']; ?>" target="<?php echo $second_cta['target']; ?>" class="cta cta--large cta--hollow">
										<span class="line-height-adjust">
											<?php echo $second_cta['title']; ?>
										</span>
									</a>
								<?php endif; ?>
							</div>
						<?php endif; ?>

					</div>
				</div>

			</div>
		</div>

	</div>
	<div class="hero-banner-mask js-hero-banner-mask"></div>
</section>
<?php
if ( get_sub_field( 'attention_bar_link_to_form' ) == 1 ):
	$form_field = get_sub_field( 'attention_bar_pick_a_form' );
	if ( $form_field ):
		$id                  = $form_field->ID;
		$hs_form['title']    = get_the_title( ) . ' - ' . ucwords(strtolower(get_the_title( $id )));
		$hs_form['hs_id']    = get_field( 'form_id', $id );
		$hs_form['uid']      = substr($hs_form['hs_id'],0,5) . rand(9999,99999);
		$hs_form['intro']    = get_field( 'form_intro', $id );
		$hs_form['class']    = get_field( 'js_class', $id );
		$hs_form['redirect'] = get_field( 'redirect_url', $id );
		$hs_form['submit_text'] = get_field( 'submit_text', $id );
		include realpath(dirname(__FILE__)) . '/../partials/conversion-form.php';

	endif;
endif;

?>
<div class="bgc-<?php print get_sub_field( 'attention_bar_bg_colour' ); ?> ptb-<?php print get_sub_field( 'attention_bar_padding' ); ?>">
	<section class="attention-bar <?php if ( get_sub_field( 'attention_bar_bg_colour' ) == 'white' ) {
		print 'attention-bar--white';
	} ?> attention-bar--single">
		<div class="section-wrap inner-wrap-@-sm">
			<div class="attention-bar__wrap">
				<div class="attention-bar__primary">
					<div class="fc-dark-blue fw-semibold fs-l"><?php the_sub_field( 'attention_bar_text' ); ?></div>
					<div class="attention-bar__cta">
						<?php if ( get_sub_field( 'attention_bar_link_to_form' ) == 1 ):
							?>
							<a class="cta cta--large cta--secondary js-open-conversion-form" data-form-id="<?php echo $hs_form['uid']; ?>" href="/contact-us/">
								<span class="line-height-adjust">
									<?php print get_sub_field( 'attention_bar_form_button_copy' ); ?>
								</span>
							</a>
							<?php
						else: ?>
							<?php $cta_link = get_sub_field( 'attention_bar_cta_link' ); ?>
							<a class="cta cta--large cta--secondary" href="<?php echo $cta_link['url']; ?>">
								<span class="line-height-adjust">
									<?php echo $cta_link['title']; ?>
								</span>
							</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
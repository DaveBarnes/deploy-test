<?php
$column_count   = get_sub_field( 'key_points_columns' );
$bg_colour = 'white';

switch(get_sub_field( 'key_points_background_colour' )){

	case 1:
		$bg_colour = 'dark-blue';
		break;
	case 2:
		$bg_colour = 'grey';
		break;
}

if ( get_sub_field( 'key_points_section_width' ) ) {
	$sectionclass .= ' section-wrap--' . get_sub_field( 'key_points_section_width' );
}

$heading_colour = $bg_colour == 'dark-blue' ? 'white' : 'dark-blue';
$points_heading_colour = $bg_colour == 'dark-blue' ? 'brand-blue' : 'dark-blue';
$hide_dots = get_sub_field( 'key_points_hide_dots' );
$align_body_and_bullets = get_sub_field( 'key_points_align_body_and_bullets' );

if ( $column_count == 1 ) {
	?>
	<section class="key-points-explanation bgc-<?php echo $bg_colour; ?> ptb-xl">
		<div class="section-wrap inner-wrap-@-sm<?php print $sectionclass; ?>">
			
			<?php if ($align_body_and_bullets): ?>
			<div class="key-points-explanation__header">
				<div class="service-text-collection__main-heading">
					<h2 class="motif fw-semibold fs-xl fc-<?php echo $heading_colour; ?>"><?php the_sub_field( 'key_points_heading' ); ?></h2>
				</div>
			</div>
			<?php endif; ?>

			<div class="key-points-explanation__wrap">
				
				<?php if ( !$hide_dots ): ?>
				<div class="key-points-explanation__decoration">
					<svg class="js-decoration" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 386.61 579.21">
						<title>dots</title>
						<path d="M282.4,190.1a9,9,0,0,1-9.2,8.8,9,9,0,1,1,9.2-8.8Z" fill="#1960a7"/>
						<circle cx="141.21" cy="539.21" r="10" transform="translate(-401.51 666.26) rotate(-88.5)" fill="#1960a7"/>
						<path d="M93.5,274.6a7,7,0,1,1-7.1,6.8A6.81,6.81,0,0,1,93.5,274.6Z" fill="#d6edfb"/>
						<path d="M223.8,202.2a7.8,7.8,0,1,1-7.6-8A7.69,7.69,0,0,1,223.8,202.2Z" fill="#d6edfb"/>
						<path d="M211.9,284.1a7.4,7.4,0,1,1-7.2-7.6A7.51,7.51,0,0,1,211.9,284.1Z" fill="#d6edfb"/>
						<path d="M263.8,343.4a8.4,8.4,0,1,1-16.8-.5,8.4,8.4,0,1,1,16.8.5Z" fill="#d6edfb"/>
						<path d="M39.3,399.2a9.55,9.55,0,1,1,9.8-9.3A9.46,9.46,0,0,1,39.3,399.2Z" fill="#d6edfb"/>
						<path d="M183.6,126.3a10.4,10.4,0,1,1-.5,20.8,10.4,10.4,0,0,1,.5-20.8Z" fill="#1d9fda"/>
						<path d="M132.3,330.4a6.45,6.45,0,1,1-6.6,6.3A6.47,6.47,0,0,1,132.3,330.4Z" fill="#1d9fda"/>
						<path d="M325.6,250.3a6,6,0,1,1-5.8-6.2A6,6,0,0,1,325.6,250.3Z" fill="#1d9fda"/>
						<path d="M141.1,236.6a6,6,0,1,1-5.8-6.2A6.08,6.08,0,0,1,141.1,236.6Z" fill="#1d9fda"/>
						<path d="M356.6,120.1a7.8,7.8,0,1,1-7.6-8A7.81,7.81,0,0,1,356.6,120.1Z" fill="#1960a7"/>
						<path d="M70.5,39.2a8.88,8.88,0,0,1-9.2,8.7,9,9,0,0,1,.4-18A9.18,9.18,0,0,1,70.5,39.2Z" fill="#1d9fda"/>
						<path d="M273,294a7.8,7.8,0,1,1-7.6-8A7.81,7.81,0,0,1,273,294Z" fill="#1d9fda"/>
						<path d="M222.7,309.9a9.05,9.05,0,1,1-8.8-9.3A9.05,9.05,0,0,1,222.7,309.9Z" fill="#1960a7"/>
						<path d="M254.5,80.6a8.65,8.65,0,1,1-8.4-8.9A8.76,8.76,0,0,1,254.5,80.6Z" fill="#d6edfb"/>
						<path d="M151.1,84.5a4.69,4.69,0,0,1,4.8-4.6,4.78,4.78,0,0,1,4.6,4.9,4.69,4.69,0,0,1-4.8,4.6A4.78,4.78,0,0,1,151.1,84.5Z" fill="#1d9fda"/>
					</svg>
				</div>
				<?php endif; ?>

				<div class="key-points-explanation__body">

					<div class="service-text-collection">
						<?php if (!$align_body_and_bullets): ?>
						<div class="service-text-collection__main-heading">
							<h2 class="motif fw-semibold fs-xl fc-<?php echo $heading_colour; ?>"><?php the_sub_field( 'key_points_heading' ); ?></h2>
						</div>
						<?php endif; ?>

						<div class="service-text-collection__copy fc-<?php echo $heading_colour; ?>"><?php the_sub_field( 'key_points_copy' ); ?></div>
					</div>

				</div>
				<?php if ( have_rows( 'key_points_points' ) ) : ?>
				<div class="key-points-explanation__points">
					<?php while ( have_rows( 'key_points_points' ) ) : the_row(); ?>
						<div class="key-points-explanation__point-item">
							<div class="key-points-explanation__point-item-spacing">
								<div class="check-article">
									<div class="check-article__icon">
                                        <span class="tick-icon">
                                            <span class="tick-icon__tick"></span>
                                        </span>
									</div>
									<?php if ( get_sub_field( 'heading' ) ): ?>
										<h2 class="check-article__heading fc-<?php echo $points_heading_colour; ?> fw-semibold fs-l">
											<?php the_sub_field( 'heading' ); ?>
										</h2>
									<?php endif; ?>
									<?php if ( get_sub_field( 'copy' ) ): ?>
										<div class="check-article__copy fc-<?php echo $heading_colour; ?>">
											<p><?php the_sub_field( 'copy' ); ?></p>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<?php
} else {
	?>
	<section class="bgc-<?php echo $bg_colour; ?> ptb-l ptb-xl--@-sm">
		<div class="section-wrap inner-wrap-@-sm<?php print $sectionclass; ?>">
			<div class="key-points <?php echo $column_count == 2 ? 'key-points--pushed-two-cols' : ''; ?>">
				<div class="key-points__heading">
					<h1 class="motif fw-semibold fs-xl fc-<?php echo $heading_colour; ?>"><?php the_sub_field( 'key_points_heading' ); ?></h1>
				</div>

				<div class="key-points__items">
					<?php if ( !$hide_dots ): ?>
					<i class="key-points__decoration">
						<svg class="js-decoration" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 386.61 579.21">
							<title>dots</title>
							<path d="M282.4,190.1a9,9,0,0,1-9.2,8.8,9,9,0,1,1,9.2-8.8Z" fill="#1960a7" data-svg-origin="264.4021911621094 180.9022674560547" transform="matrix(1,0,0,1,-22.887158556870748,30.33949277246286)"></path>
							<circle cx="141.21" cy="539.21" r="10" transform="matrix(0.02617,-0.99966,0.99966,0.02617,-392.35517137223934,626.6494206200883)" fill="#1960a7" data-svg-origin="131.2100067138672 529.2100219726562"></circle>
							<path d="M93.5,274.6a7,7,0,1,1-7.1,6.8A6.81,6.81,0,0,1,93.5,274.6Z" fill="#d6edfb" data-svg-origin="86.39717102050781 274.5938415527344" transform="matrix(1,0,0,1,-0.1137083176346972,-0.6551736538562809)"></path>
							<path d="M223.8,202.2a7.8,7.8,0,1,1-7.6-8A7.69,7.69,0,0,1,223.8,202.2Z" fill="#d6edfb" data-svg-origin="208.20263671875 194.19752502441406" transform="matrix(1,0,0,1,27.34341259867552,30.2921474219005)"></path>
							<path d="M211.9,284.1a7.4,7.4,0,1,1-7.2-7.6A7.51,7.51,0,0,1,211.9,284.1Z" fill="#d6edfb" data-svg-origin="197.102783203125 276.49737548828125" transform="matrix(1,0,0,1,-0.1412757111333871,0.9999125857132131)"></path>
							<path d="M263.8,343.4a8.4,8.4,0,1,1-16.8-.5,8.4,8.4,0,1,1,16.8.5Z" fill="#d6edfb" data-svg-origin="246.99627685546875 334.74627685546875" transform="matrix(1,0,0,1,2.4100615076174465,-2.10442036669994)"></path>
							<path d="M39.3,399.2a9.55,9.55,0,1,1,9.8-9.3A9.46,9.46,0,0,1,39.3,399.2Z" fill="#d6edfb" data-svg-origin="30.003183364868164 380.1034240722656" transform="matrix(1,0,0,1,0.6814045034626637,-30.172680776836657)"></path>
							<path d="M183.6,126.3a10.4,10.4,0,1,1-.5,20.8,10.4,10.4,0,0,1,.5-20.8Z" fill="#1d9fda" data-svg-origin="172.95318603515625 126.29714965820312" transform="matrix(1,0,0,1,-4.498976900821138,-4.442422840602338)"></path>
							<path d="M132.3,330.4a6.45,6.45,0,1,1-6.6,6.3A6.47,6.47,0,0,1,132.3,330.4Z" fill="#1d9fda" data-svg-origin="125.69828796386719 330.3986511230469" transform="matrix(1,0,0,1,-0.6234519633093079,0.14418516196045986)"></path>
							<path d="M325.6,250.3a6,6,0,1,1-5.8-6.2A6,6,0,0,1,325.6,250.3Z" fill="#1d9fda" data-svg-origin="313.60345458984375 244.09678649902344" transform="matrix(1,0,0,1,23.33371714288647,-15.627867303365267)"></path>
							<path d="M141.1,236.6a6,6,0,1,1-5.8-6.2A6.08,6.08,0,0,1,141.1,236.6Z" fill="#1d9fda" data-svg-origin="129.10345458984375 230.39678955078125" transform="matrix(1,0,0,1,-5.216908351647085,14.04564217350467)"></path>
							<path d="M356.6,120.1a7.8,7.8,0,1,1-7.6-8A7.81,7.81,0,0,1,356.6,120.1Z" fill="#1960a7" data-svg-origin="341.0025939941406 112.09750366210938" transform="matrix(1,0,0,1,0.7353631107102357,-1.0114014780300231)"></path>
							<path d="M70.5,39.2a8.88,8.88,0,0,1-9.2,8.7,9,9,0,0,1,.4-18A9.18,9.18,0,0,1,70.5,39.2Z" fill="#1d9fda" data-svg-origin="52.50088119506836 29.89784812927246" transform="matrix(1,0,0,1,-0.6521776259755044,-0.8586875607804105)"></path>
							<path d="M273,294a7.8,7.8,0,1,1-7.6-8A7.81,7.81,0,0,1,273,294Z" fill="#1d9fda" data-svg-origin="257.402587890625 285.99749755859375" transform="matrix(1,0,0,1,10.213104246175032,39.0312383793088)"></path>
							<path d="M222.7,309.9a9.05,9.05,0,1,1-8.8-9.3A9.05,9.05,0,0,1,222.7,309.9Z" fill="#1960a7" data-svg-origin="204.6035614013672 300.59661865234375" transform="matrix(1,0,0,1,-10.379732261197674,3.9514356261376284)"></path>
							<path d="M254.5,80.6a8.65,8.65,0,1,1-8.4-8.9A8.76,8.76,0,0,1,254.5,80.6Z" fill="#d6edfb" data-svg-origin="237.20372009277344 71.69649505615234" transform="matrix(1,0,0,1,1.5367051993701053,38.163949412381406)"></path>
							<path d="M151.1,84.5a4.69,4.69,0,0,1,4.8-4.6,4.78,4.78,0,0,1,4.6,4.9,4.69,4.69,0,0,1-4.8,4.6A4.78,4.78,0,0,1,151.1,84.5Z" fill="#1d9fda" data-svg-origin="151.0984344482422 79.89868927001953" transform="matrix(1,0,0,1,-4.335583974867276,-4.083076280835085)"></path>
						</svg>
					</i>
					<?php endif; ?>

					<?php if ( have_rows( 'key_points_points' ) ) :
						while ( have_rows( 'key_points_points' ) ) : the_row(); ?>
							<div class="key-points__item">
								<div class="key-points__item-spacing">
									<div class="check-article">
										<div class="check-article__icon">
	                                        <span class="tick-icon">
	                                            <span class="tick-icon__tick"></span>
	                                        </span>
										</div>
										<h2 class="check-article__heading fw-semibold fc-<?php echo $heading_colour == 'white' ? 'white' : $heading_colour; ?> <?php echo $column_count == 2 ? 'fw-semibold' : ''; ?> fs-l"><?php the_sub_field( 'heading' ); ?></h2>
										<div class="check-article__copy <?php if ( $heading_colour == 'white' ) {
											echo 'fc-white';
										} ?>">
											<p><?php the_sub_field( 'copy' ); ?></p>
										</div>
										<?php $cta_link = get_sub_field( 'cta_link' ); ?>
										<a href="<?php echo $cta_link['url']; ?>" class="cta"><?php echo $cta_link['title']; ?></a>
									</div>
								</div>
							</div>
						<?php endwhile;
					endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php } ?>
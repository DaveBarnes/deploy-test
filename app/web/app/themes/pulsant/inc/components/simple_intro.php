<div class="<?php the_sub_field('simple_intro_spacing_classes'); ?>">
	<div class="<?php the_sub_field('simple_intro_custom_classes'); ?> ta-center">
		<div class="inner-wrap-@-sm">
			<h1 class="simple-intro__heading fs-xl"><?php the_sub_field('simple_intro_heading'); ?></h1>

			<div class="simple-body"><?php the_sub_field('simple_intro_intro'); ?></div>
		</div>
	</div>
</div>
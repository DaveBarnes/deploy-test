<section class="<?php the_sub_field( 'bg_colour'); ?>">
	<div class="section-wrap">
		<h1 class="bgc-white ta-center pt-l fc-dark-blue fw-semibold fs-xl"><?php the_sub_field( 'title' ); ?></h1>
	</div>
	<?php if ( have_rows( 'ctas' ) ) : ?>
		<div class="bgc-white pt-l">
			<div class="section-wrap section-wrap--restricted">
				<div class="simple-grid">
					<?php while ( have_rows( 'ctas' ) ) :
						the_row();

						$post_object = get_sub_field( 'cta_item' );
						$pid         = $post_object->ID;

						$image        = get_field( 'image', $pid );
						$link         = get_field( 'link', $pid );
						$copy         = get_field( 'copy', $pid );
						$motif        = get_field( 'heading_motif', $pid );
						$main_heading = get_field( 'main_heading', $pid );
						$newtab		  = get_field( 'newtab', $pid );
						?>
						<div class="simple-grid__item">
							<div class="simple-grid__item-spacing">
								<div class="inner-wrap-@-sm">
									<article class="article-attract">
										<a href="<?php print $link['url']; ?>" <?php if ( $newtab == 1 ) { ?>target="_blank"<?php } ?>><img src="<?php print $image['url']; ?>" alt="<?php print $image['alt']; ?>" class="responsive-img article-attract__img"></a>
										<?php if ( $motif ) : ?>
											<div class="service-text-collection__intro-heading">
												<h1 class="intro-heading intro-heading--motif intro-heading--retain-small"><?php print $motif; ?></h1>
											</div>
										<?php endif; ?>
										<div class="article-attract__body">
											<div class="article-attract__main-heading">
												<h2 class="fc-dark-blue fw-semibold fs-l"><?php print $main_heading; ?></h2>
											</div>
											<div class="article-attract__copy">
												<?php print $copy; ?>
											</div>
											<div class="article-attract__cta">
												<a href="<?php print $link['url']; ?>" <?php if ( $newtab == 1 ) { ?>target="_blank"<?php } ?> class="cta"><?php print $link['title']; ?></a>
											</div>
										</div>
									</article>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
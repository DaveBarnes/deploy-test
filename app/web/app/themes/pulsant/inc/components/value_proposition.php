<section class="value-proposition js-value-proposition ptb-xl ptb-xxl--@-lg bgc-white">
    <div class="section-wrap inner-wrap-@-sm section-wrap--narrow">
        <div class="value-proposition__wrap js-wrap" id="value-proposition">
            
            <section class="value-proposition__svg js-svg-wrap">
                <div class="value-proposition__split js-split"></div>
                <div class="value-proposition__svg-wrap">
                    <?php
                        echo file_get_contents( get_theme_file_path() . "/inc/partials/value-proposition.svg.php" );
                    ?>
                </div>
            </section>

            <section class="value-proposition__scene value-proposition__scene--start js-scene" data-scene="start">  
                <div class="value-proposition__header">
                    <div class="simple-intro simple-intro--wide ta-center">
                        <h2 class="simple-intro__heading">
                            <span class="value-proposition__header-heading">Blending the best of people and technology through infrastructure, IT transformation and multi-cloud</span>
                        </h2>
                    </div>
                </div>
                <div class="value-proposition__svg-placeholder">
                        
                </div>
                <div class="value-proposition__footer">

                    <div class="simple-intro simple-intro--wide ta-center js-start-intro">
                        <div class="inner-wrap-@-sm">
                            <h3 class="value-proposition__subhead fs-l mb-m">Discover the ideal hybrid IT solution for your organisation</h3>
                            <button type="button" class="cta cta--large cta--primary cta--rounded js-progression-button" data-to-scene="choice" data-link-for-smaller-screens="/why-us/value-proposition-story/">
                                <span class="line-height-adjust">Explore</span>
                            </button>
                        </div>
                    </div>

                </div>
            </section>

            <section class="value-proposition__scene value-proposition__scene--choice js-scene" data-scene="choice">                    
                <div class="value-proposition__header">
                    <div class="simple-intro simple-intro--wide ta-center">
                        <div class="inner-wrap-@-sm">
                            <h2 class="simple-intro__heading">
                                <span class="value-proposition__header-heading">Blending the best of people and technology through infrastructure, IT transformation and multi-cloud</span>
                            </h2>
                        </div>
                    </div>
                </div>
                
                <div class="value-proposition__choice-sections">
                    
                    <div class="value-proposition__choice value-proposition__choice--services">
                        <h3 class="value-proposition__choice-heading">Services</h3>
                        <p class="value-proposition__choice-copy">Delivered by our expert team</p>
                        <button type="button" class="cta cta--large cta--primary cta--rounded js-to-services-button js-progression-button" data-to-scene="services">
                            <span class="line-height-adjust">Explore</span>
                        </button>
                    </div>

                    <div class="value-proposition__choice value-proposition__choice--technology">
                        <h3 class="value-proposition__choice-heading">Technology</h3>
                        <p class="value-proposition__choice-copy">Capacity to fulfill your business needs</p>
                        <button type="button" class="cta cta--large cta--primary cta--rounded js-to-services-button js-progression-button" data-to-scene="technology">
                            <span class="line-height-adjust">Explore</span>
                        </button>
                    </div>
                </div>
                
                <div class="value-proposition__footer">
                    <div class="value-proposition__back-to-start ta-center js-back-to-start">
                        <button class="cta js-progression-button" data-to-scene="start">&#x2190; Back to start</button>
                    </div>
                </div>
            </section>

            <section class="value-proposition__scene value-proposition__scene--technology js-scene" data-scene="technology" data-end-point="true">
                <div class="value-proposition__header">
                    <div class="simple-intro simple-intro--wide ta-center">
                        <div class="inner-wrap-@-sm">
                            <h2 class="simple-intro__heading">
                                <span class="value-proposition__end-scene-heading">Technology</span>
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="value-proposition__lists">
                    <div class="value-proposition-lists value-proposition-lists--in-val-prop-feature ">
                        <div class="simple-grid simple-grid--nowrap simple-grid--less-space-between-items">           
                            <div class="simple-grid__item">
                                <div class="simple-grid__item-spacing">
                                    <div class="inner-wrap-@-sm">
                                        <h4 class="fs-l mb-s mb-m--@-md fw-semibold">Manage</h4>

                                        <ul class="pl-m mb-m mb-l--@-md mb-n--@-lg">
                                            <li>Hybrid IT management</li>
                                            <li>End-to-end support &amp; SLAs</li>
                                            <li>User to cloud support</li>
                                            <li>Continuous improvement</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="simple-grid__item">
                                <div class="simple-grid__item-spacing">
                                    <div class="inner-wrap-@-sm">
                                        <h4 class="fs-l mb-s mb-m--@-md fw-semibold">Transform</h4>

                                        <ul class="pl-m mb-m mb-l--@-md mb-n--@-lg">
                                            <li>IT-led change</li>
                                            <li>Cloud benefits realisation</li>
                                            <li>Solution design</li>
                                            <li>Cloud adoption</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="simple-grid__item">
                                <div class="simple-grid__item-spacing">
                                    <div class="inner-wrap-@-sm">
                                        <h4 class="fs-l mb-s mb-m--@-md fw-semibold">Secure</h4>

                                        <ul class="pl-m mb-m mb-l--@-md mb-n--@-lg">
                                            <li>Security analytics</li>
                                            <li>Threat management</li>
                                            <li>Edge to core protection</li>
                                            <li>Customer protection</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="simple-grid__item">
                                <div class="simple-grid__item-spacing">
                                    <div class="inner-wrap-@-sm">
                                        <h4 class="fs-l mb-s mb-m--@-md fw-semibold">Comply</h4>

                                        <ul class="pl-m mb-m mb-l--@-md mb-n--@-lg">
                                            <li>Regulatory IT compliance</li>
                                            <li>Compliance dashboard</li>
                                            <li>End-to-end IT monitoring</li>
                                            <li>Hybrid operational analytics</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
                <div class="value-proposition__footer">
                    <div class="value-proposition__back-options ta-center">
                        <button class="cta cta--large cta--primary cta--rounded js-progression-button" data-to-scene="services">
                            <span class="line-height-adjust">Services</span>
                        </button>
                        <button class="cta cta--large cta--hollow cta--hollow-primary cta--rounded js-progression-button" data-to-scene="choice">
                            <span class="line-height-adjust">Back</span>
                        </button>
                    </div>

                    <div class="value-proposition__back-to-start ta-center js-back-to-start">
                        <button class="cta js-progression-button" data-to-scene="start">&#x2190; Back to start</button>
                    </div>
                </div>
            </section>

            <section class="value-proposition__scene value-proposition__scene--services js-scene" data-scene="services" data-end-point="true">
                    
                <div class="value-proposition__header">
                    <div class="simple-intro simple-intro--wide ta-center">
                        <h2 class="simple-intro__heading">
                            <span class="value-proposition__end-scene-heading">Services</span>
                        </h2>
                    </div>
                </div>

                <div class="value-proposition__lists">
                    <div class="value-proposition-lists value-proposition-lists--in-val-prop-feature">
                        <div class="simple-grid">
                            <div class="simple-grid__item">
                                <div class="simple-grid__item-spacing">
                                    <div class="inner-wrap-@-sm">
                                        <h4 class="fs-l mb-s mb-m--@-md fw-semibold">Cloud platforms &amp; sourcing</h4>

                                        <ul class="pl-m mb-m mb-l--@-md mb-n--@-lg">
                                            <li>Microsoft Azure &amp; O365</li>
                                            <li>Amazon Web Services</li>
                                            <li>Pulsant UK Cloud</li>
                                            <li>Private, public &amp; hybrid</li>
                                            <li>Connectivity &amp; automation</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="simple-grid__item">
                                <div class="simple-grid__item-spacing">
                                    <div class="inner-wrap-@-sm">
                                        <h4 class="fs-l mb-s mb-m--@-md fw-semibold">Capacity &amp; resources</h4>

                                        <ul class="pl-m mb-m mb-l--@-md mb-n--@-lg">
                                            <li>Colocation</li>
                                            <li>Infrastructure services</li>
                                            <li>Networking</li>
                                            <li>Workplace recovery</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="simple-grid__item">
                                <div class="simple-grid__item-spacing">
                                    <div class="inner-wrap-@-sm">
                                        <h4 class="fs-l mb-s mb-m--@-md fw-semibold">IT consultancy &amp; analytics</h4>

                                        <ul class="pl-m mb-m mb-l--@-md mb-n--@-lg">
                                            <li>Project management</li>
                                            <li>Proof of concept</li>
                                            <li>Business case development</li>
                                            <li>Transformation roadmap</li>
                                            <li>ROI analysis</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

                <div class="value-proposition__footer">
                    <div class="value-proposition__back-options ta-center">
                        <button class="cta cta--large cta--primary cta--rounded js-progression-button" data-to-scene="technology">
                            <span class="line-height-adjust">Technology</span>
                        </button>
                        <button class="cta cta--large cta--hollow cta--hollow-primary cta--rounded js-progression-button" data-to-scene="choice">
                            <span class="line-height-adjust">Back</span>
                        </button>
                    </div>

                    <div class="value-proposition__back-to-start ta-center js-back-to-start">
                        <button class="cta js-progression-button" data-to-scene="start">&#x2190; Back to start</button>
                    </div>
                </div>

            </section>

        </div>
    </div>
</section>
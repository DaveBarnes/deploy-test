<?php
if ( get_row_layout() == 'gallery' ) :
	if ( have_rows( 'gallery_images' ) ) :
		$images = [];
		while ( have_rows( 'gallery_images' ) ) :
			the_row();
			$image = get_sub_field( 'image' );
			if ( $image ) {
				$x1    = wp_get_attachment_image_src( $image['ID'], 'gallery-image' );
				$x2    = wp_get_attachment_image_src( $image['ID'], 'gallery-image-2x' );
				$thumb = wp_get_attachment_image_src( $image['ID'], 'thumb-button' );

				$images[] = array( $x1[0], $x2[0], $thumb[0] );
			}
		endwhile;

		$img_count = count( $images );

	endif;
endif;
?>
<section class="bgc-white ptb-l">
	<div class="section-wrap inner-wrap-@-sm">
		<?php if ( $img_count > 2 ) { ?>
			<div class="gallery js-gallery">
				<div class="gallery__stage">
					<img
						src="<?php echo $images[0][0]; ?>"
						srcset="<?php echo $images[0][0]; ?>, <?php echo $images[0][1]; ?> 2x"
						class="responsive-img js-stage-image"
						alt=""
						<?php foreach ( $images as $k => $i ) { ?>
							data-img-<?php echo $k + 1; ?>-src="<?php echo $images[ $k ][0]; ?>"
							data-img-<?php echo $k + 1; ?>-srcset="<?php echo $images[ $k ][0]; ?>, <?php echo $images[ $k ][1]; ?> 2x"
						<?php } ?>
					>
				</div>
				<ul class="no-list gallery__navigation">
					<?php foreach ( $images as $k => $i ) { ?>
						<li class="gallery__nav-item <?php if ( $k == 0 ) {
							echo 'is-active';
						} ?>">
							<button class="gallery__nav-button js-thumb-button" data-show-id="<?php echo $k + 1; ?>">
								<img src="<?php echo $images[ $k ][2]; ?>" class="responsive-img" alt="">
							</button>
						</li>
					<?php } ?>
				</ul>
			</div>
		<?php } else { ?>
			<div class="gallery">
				<div class="gallery__stage">
					<img src="<?php echo $images[0][0]; ?>" srcset="<?php echo $images[0][0]; ?>, <?php echo $images[0][1]; ?> 2x" class="responsive-img" alt="">
				</div>
				<div class="gallery__stage">
					<img src="<?php echo $images[1][0]; ?>" srcset="<?php echo $images[1][0]; ?>, <?php echo $images[1][1]; ?> 2x" class="responsive-img" alt="">
				</div>
			</div>
		<?php } ?>
	</div>
</section>


<?php
if ( get_sub_field( 'hero_width' ) ) {
	$sectionclasses = get_sub_field( 'hero_width' );
	if(is_array($sectionclasses)){
		$sectionclasses = $sectionclasses[0];
	}
	$sectionclass = ' section-wrap--' . $sectionclasses;
} else {
	$sectionclass = ' section-wrap--narrow';
}

if ( get_sub_field( 'hero_spacing_classes' ) ) {
	$spacing = get_sub_field( 'hero_spacing_classes' );
} else{
	$spacing = 'ptb-l ptb-xl--@-sm';
}
?>
<div class="<?php the_sub_field( 'hero_bg_colour' ); ?>">
	<section class="section-wrap inner-wrap-@-sm<?php echo $sectionclass; ?>">
		<div class="<?php echo $spacing; ?>">
			<div class="hero-feature <?php the_sub_field( 'hero_classes' ); ?>">
				<div class="hero-feature__aside <?php the_sub_field( 'hero_aside_classes' ); ?>">
					<?php $post_object = get_sub_field( 'hero_aside' );
					if ( $post_object ) {
						$post = $post_object;
						setup_postdata( $post );
						echo $post->post_content;
						wp_reset_postdata();
					} else {
						if ( $img = get_sub_field( 'hero_graphic' ) ) { ?>
							<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">
						<?php }
					} ?>
				</div>
				<div class="hero-feature__text">
					<div class="service-text-collection">
						<div class="service-text-collection__main-heading">
							<h2 class="motif fw-semibold fs-xl fc-dark-blue"><?php the_sub_field( 'hero_main_heading' ); ?></h2>
						</div>

						<div class="service-text-collection__copy">
							<?php the_sub_field( 'hero_copy' ); ?>
							<div class="fc-dark-blue fw-semibold fs-l"><?php the_sub_field( 'hero_cta' ); ?></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>
</div>
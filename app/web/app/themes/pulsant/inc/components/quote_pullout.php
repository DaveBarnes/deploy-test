<?php
	$title   = get_sub_field( 'quote_pullout_title' );
	$body   = get_sub_field( 'quote_pullout_body' );
	$attribution   = get_sub_field( 'quote_pullout_attribution' );
?>
<section class="quote-pullout">
	<div class="section-wrap section-wrap--narrow inner-wrap-@-sm">
		<?php if ( $title ): ?>
			<p class="quote-pullout__title">
				<?php echo $title; ?>
			</p>
		<?php endif; ?>
		<div class="quote-pullout__body">
			<?php echo $body; ?>
		</div>
		<?php if ( $attribution ): ?>
			<p class="quote-pullout__attribution">
				<?php echo $attribution; ?>
			</p>
		<?php endif; ?>
	</div>
</section>
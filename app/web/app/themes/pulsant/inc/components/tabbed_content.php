<?php
$is_value_prop = is_page_template( 'page-templates/page-value-proposition.php' );

while ( have_rows( 'tabs_tabs' ) ) : the_row();

	$post_object = get_sub_field( 'tab_content_aside' );
	if ( $post_object ) {
		$post = $post_object;
		setup_postdata( $post );
		$svg = get_the_content();
		wp_reset_postdata();
	} else {
		if ( $img = get_sub_field( 'tab_content_graphic' ) ) {
			$svg = '<img src="' . $img['url'] . '" alt="' . $img['alt'] . '">';
		}
	}

	$rows[] = array(
		'title'   => get_sub_field( 'tab_content_main_heading' ),
		'copy'    => get_sub_field( 'tab_content_copy' ),
		'cta'     => get_sub_field( 'tab_content_cta' ),
		'classes' => get_sub_field( 'tab_content_classes' ),
		'svg'     => $svg,
	);

	$row_count = count( $rows );

endwhile;

if ( $is_value_prop ):
	?>
	<div class="<?php the_sub_field( 'tabs_bg_colour' ) ?>">
		<div class="simple-intro simple-intro--widest pb-l">
			<div class="hero-feature-tabs js-hero-feature-tabs hero-feature-tabs--2-items">

				<div class="hero-feature-tabs__select ta-center mb-l">
					<select name="" id="" class="js-feature-tabs-select">
						<?php foreach ( $rows as $k => $row ) { ?>
							<option value="<?php print $k + 1; ?>"><?php print $row['title']; ?></option>
						<?php } ?>
					</select>
				</div>

				<ul class="no-list hero-feature-tabs__tabs">
					<?php foreach ( $rows as $k => $row ) { ?>
						<li class="hero-feature-tabs__tab-item">
							<div class="hero-feature-tabs__tab-item-spacer">
								<button class="hero-feature-tabs__button js-feature-tabs-button <?php if ( $k == 0 ) {
									print 'no-button is-active';
								} ?>" data-feature-to-show="<?php print $k + 1; ?>">
									<span class="line-height-adjust"><?php print $row['title']; ?></span>
									<i class="hero-feature-tabs__triangle"></i>
								</button>
							</div>
						</li>
					<?php } ?>
				</ul>

				<div class="hero-feature-tabs__features js-feature-tabs-features">
					<?php foreach ( $rows as $k => $row ): ?>
						<div class="hero-feature-tabs__feature-item js-feature-item <?php if ( $k == 0 ) {
							print 'is-active" style="opacity: 1;';
						} ?>" data-feature-id="<?php print $k + 1; ?>">

							<div class="inner-wrap-@-sm">
								<h3 class="fs-xl ta-center fw-semibold mb-l"><?php echo strip_tags( $row['cta'] ); ?></h3>
							</div>
							<?php print $row['copy']; ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
<?php else : ?>

	<div class="<?php the_sub_field( 'tabs_bg_colour' ) ?>">
		<section class="section-wrap section-wrap--narrow inner-wrap-@-sm">
			<div class="pt-l pb-m pt-xl--@-sm pb-l--@-sm">

				<h1 class="ta-center fc-dark-blue fw-semibold fs-xl mb-s mb-l--@-md"><?php the_sub_field( 'tabs_title' ); ?></h1>
				<?php ?>
				<div class="hero-feature-tabs js-hero-feature-tabs hero-feature-tabs--<?php echo $row_count; ?>-items">

					<div class="hero-feature-tabs__select ta-center mb-l">
						<select name="" id="" class="js-feature-tabs-select">
							<?php foreach ( $rows as $k => $row ) { ?>
								<option value="<?php print $k + 1; ?>"><?php print $row['title']; ?></option>
							<?php } ?>
						</select>
					</div>

					<ul class="no-list hero-feature-tabs__tabs">
						<?php foreach ( $rows as $k => $row ) { ?>
							<li class="hero-feature-tabs__tab-item">
								<div class="hero-feature-tabs__tab-item-spacer">
									<button class="hero-feature-tabs__button js-feature-tabs-button <?php if ( $k == 0 ) {
										print 'no-button is-active';
									} ?>" data-feature-to-show="<?php print $k + 1; ?>">
										<span class="line-height-adjust"><?php print $row['title']; ?></span>
										<i class="hero-feature-tabs__triangle"></i>
									</button>
								</div>
							</li>
						<?php } ?>
					</ul>

					<div class="hero-feature-tabs__features js-feature-tabs-features">

						<?php foreach ( $rows as $k => $row ) { ?>
							<div class="hero-feature-tabs__feature-item js-feature-item <?php if ( $k == 0 ) {
								print 'is-active';
							} ?>" data-feature-id="<?php print $k + 1; ?>">

								<div class="hero-feature hero-feature--vert-centre <?php echo $row['classes']; ?>">
									<div class="hero-feature__aside">
										<?php print $row['svg']; ?>
									</div>
									<div class="hero-feature__text">
										<div class="service-text-collection">
											<div class="service-text-collection__main-heading">
												<h2 class="motif fw-semibold fs-xl fc-dark-blue"><?php print $row['title']; ?></h2>
											</div>

											<div class="service-text-collection__copy">
												<?php print $row['copy']; ?>
												<?php print $row['cta']; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>

			<?php
			$logo_collection = get_sub_field( 'tabs_logo_collection' );
			if ( $logo_collection ) {
				if ( $cid = $logo_collection->ID ) {
					?>
					<div class="pb-xl--@-sm">
						<div class="logo-collection <?php the_sub_field( 'tabs_format' ); ?>">
							<?php
							$logos = get_field( 'logos', $cid );
							if ( $logos['icons'] ) {
								foreach ( $logos['icons'] as $logo ) {
									$width = $logo['width'];
									?>
									<div class="logo-collection__item logo-collection__item--<?php print $width; ?>">
										<img src="<?php print $logo['icon']['url']; ?>">
									</div>
									<?php
								}
							}
							?>
						</div>
					</div>
					<?php
				}

			}
			?>
		</section>
	</div>

<?php endif; ?>

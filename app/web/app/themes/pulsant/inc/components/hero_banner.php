<?php
$image        = get_sub_field( 'banner_image' );
$decorated    = get_sub_field( 'banner_decoration' ) == '1';
$mini_heading = get_sub_field( 'banner_mini_heading' );
$split        = get_sub_field( 'banner_split' );
$form_cta     = get_sub_field( 'banner_cta' );

if ( have_rows( 'banner_cta' ) ):
	while ( have_rows( 'banner_cta' ) ) : the_row();
		$form                   = get_sub_field( 'form' );
		$id                     = $form->ID;
		$hs_form['title']       = get_the_title( ) . ' - ' . get_the_title( $id );
		$hs_form['hs_id']       = get_field( 'form_id', $id );
		$hs_form['uid']         = substr($hs_form['hs_id'],0,5) . rand(9999,99999);
		$hs_form['intro']       = get_field( 'form_intro', $id );
		$hs_form['class']       = get_field( 'js_class', $id );
		$hs_form['redirect']    = get_field( 'redirect_url', $id );
		$hs_form['submit_text'] = get_field( 'submit_text', $id );

		$cta_text       = get_sub_field( 'cta_text' );
		$cta_url_picker = get_field( 'cta_url_picker' );
		$cta_url        = get_field( 'cta_url' );

	endwhile;
	include realpath( dirname( __FILE__ ) ) . '/../partials/conversion-form.php';
endif;


if ( ! $split ):
	?>
	<div class="hero-banner-shift"></div>
	<section class="hero-banner js-hero-banner">
		<div class="hero-banner__wrap">
			<div class="hero-banner__shift">
				<img class="hero-banner__image responsive-img" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
				<?php if ( $decorated ) { ?>
					<div class="hero-banner__decoration">
						<svg class="js-decoration" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1146.75 470.45"><title>dots2</title>
							<path d="M115,52.54a6.33,6.33,0,1,1,5-7.44,6.33,6.33,0,0,1-5,7.44Z" fill="#1d9fda" data-svg-origin="107.45999145507812 39.99671936035156" transform="matrix(1,0,0,1,11.02352509304376,-19.865073660252772)"></path>
							<path d="M172.15,356.08a5.16,5.16,0,1,1-6.11-4h0A5.16,5.16,0,0,1,172.15,356.08Z" fill="#1960a7" data-svg-origin="161.93801879882812 351.9703674316406" transform="matrix(1,0,0,1,-25.820305234364607,38.0979339724364)"></path>
							<path d="M763.29,405.94a7.1,7.1,0,1,1-8.35-5.58h0a7.1,7.1,0,0,1,8.35,5.58Z" fill="#1960a7" data-svg-origin="749.2261352539062 400.2234191894531" transform="matrix(1,0,0,1,14.246177148338036,-22.216081434400614)"></path>
							<path d="M1080.8,258a7.75,7.75,0,1,1-9.12-6.08h0a7.75,7.75,0,0,1,9.12,6.08Z" fill="#1960a7" data-svg-origin="1065.450439453125 251.76939392089844" transform="matrix(1,0,0,1,24.138183689633014,-3.1815182505697646)"></path>
							<path d="M1086.36,45.06a6.28,6.28,0,1,1-7.38-4.94h0a6.27,6.27,0,0,1,7.38,4.92Z" fill="#1d9fda" data-svg-origin="1073.9195556640625 39.997894287109375" transform="matrix(1,0,0,1,29.944907081188294,33.41708624119521)"></path>
							<path d="M1067.3,152.08a4.87,4.87,0,1,1-5.73-3.82h0A4.89,4.89,0,0,1,1067.3,152.08Z" fill="#d6edfb" data-svg-origin="1057.65478515625 148.16900634765625" transform="matrix(1,0,0,1,21.392344886152113,-9.420170838093417)"></path>
							<path d="M167,121.64a4.87,4.87,0,1,1-5.73-3.82h0A4.89,4.89,0,0,1,167,121.64Z" fill="#d6edfb" data-svg-origin="157.3546600341797 117.72899627685547" transform="matrix(1,0,0,1,2.0986156164741523,28.24595319149782)"></path>
							<path d="M101.9,297.41A5,5,0,1,1,96,293.48a5,5,0,0,1,5.88,3.93Z" fill="#d6edfb" data-svg-origin="91.9949951171875 293.3839111328125" transform="matrix(1,0,0,1,1.406503784852304,-24.73723194727547)"></path>
							<path d="M127.14,205.66a5,5,0,1,1-5.88-3.93,5,5,0,0,1,5.88,3.93Z" fill="#d6edfb" data-svg-origin="117.23574829101562 201.63389587402344" transform="matrix(1,0,0,1,9.955924638347211,30.712079186881063)"></path>
							<path d="M720.29,249.09a7,7,0,1,1,5.5-8.24,7,7,0,0,1-5.5,8.24Z" fill="#1d9fda" data-svg-origin="711.9262084960938 235.22413635253906" transform="matrix(1,0,0,1,-15.168169121309447,-34.483236081057576)"></path>
							<path d="M1040.84,303.5a5.88,5.88,0,1,1-4.6,6.93h0a5.88,5.88,0,0,1,4.6-6.93Z" fill="#1d9fda" data-svg-origin="1036.1231689453125 303.3837585449219" transform="matrix(1,0,0,1,-0.46292141100821566,-0.44546262739558706)"></path>
							<path d="M777,86.72a6.34,6.34,0,1,1-7.45-5,6.34,6.34,0,0,1,7.45,5Z" fill="#1d9fda" data-svg-origin="764.4383544921875 81.59983825683594" transform="matrix(1,0,0,1,10.927118706406011,-29.51502221838331)"></path>
							<path d="M52.56,328.31a6.34,6.34,0,1,1-7.45-5,6.34,6.34,0,0,1,7.45,5Z" fill="#1d9fda" data-svg-origin="39.99846267700195 323.1898498535156" transform="matrix(1,0,0,1,0.22694866856048582,-0.46570582308986935)"></path>
							<path d="M729.19,339.56a5.18,5.18,0,1,1-6.09-4.07h0a5.17,5.17,0,0,1,6.09,4h0Z" fill="#d6edfb" data-svg-origin="718.9295043945312 335.3861083984375" transform="matrix(1,0,0,1,22.49312986348864,26.363888801895477)"></path>
							<path d="M90.27,161.12a5.18,5.18,0,1,1-6.09-4.07h0a5.17,5.17,0,0,1,6.09,4h0Z" fill="#d6edfb" data-svg-origin="80.00955200195312 156.9461212158203" transform="matrix(1,0,0,1,-35.63515264935008,21.265226692484045)"></path>
							<circle cx="1100.82" cy="408.78" r="5.93" transform="matrix(0.98085,-0.19474,0.19474,0.98085,-80.60586474925086,247.6050960652446)" fill="#1d9fda" data-svg-origin="1094.889892578125 402.8500061035156"></circle>
							<path d="M1026.2,197.92a5.67,5.67,0,1,1-6.67-4.45h0A5.69,5.69,0,0,1,1026.2,197.92Z" fill="#d6edfb" data-svg-origin="1014.9697265625 193.36383056640625" transform="matrix(1,0,0,1,4.438274395078539,-7.472207698588978)"></path>
							<path d="M867.64,415.83A7.5,7.5,0,1,1,858.8,410h0A7.5,7.5,0,0,1,867.64,415.83Z" fill="#d6edfb" data-svg-origin="852.795654296875 409.849365234375" transform="matrix(1,0,0,1,0.6158427207010351,0.5216956989543909)"></path>
							<path d="M104.35,410a7.5,7.5,0,1,1-8.84-5.86h0A7.5,7.5,0,0,1,104.35,410Z" fill="#d6edfb" data-svg-origin="89.49961853027344 403.9906005859375" transform="matrix(1,0,0,1,-17.989090091314036,-13.869240227774407)"></path>
							<rect width="1146.75" height="470.45" fill="none"></rect>
						</svg>
					</div>
				<?php } ?>
				<div class="hero-banner__copy">
					<div class="section-wrap inner-wrap-@-sm section-wrap--<?php print $decorated ? 'narrow' : 'restricted'; ?>">

						<div class="hero-banner__heading">
							<h1 class="intro-heading intro-heading--motif mb-m fc-white"><?php the_sub_field( 'banner_tag_line' ); ?></h1>
						</div>
						<div class="hero-banner__sub-heading <?php if ( $mini_heading ) {
							print 'mb-m';
						} ?>">
							<div class="fc-white"><?php the_sub_field( 'banner_text' ); ?></div>
						</div>
						<?php if ( $mini_heading ): ?>
							<div class="hero-banner__mini-heading fc-white">
								<?php print $mini_heading; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="hero-banner-mask js-hero-banner-mask"></div>
			</div>
		</div>
	</section>
<?php else:

	$nav_items = wp_get_nav_menu_items( 'header-menu' );

	foreach ( $nav_items as $item ) {
		if ( $item->object_id == $post->ID ) {
			$location_menu_id = $item->menu_item_parent;
			$parent_id        = get_post_meta( $item->menu_item_parent, '_menu_item_object_id', TRUE );
			if ( $parent_id ) {
				$parent       = get_post( $parent_id );
				$grand_parent = get_post( $parent->post_parent );
				if($grand_parent->ID == $post->ID){
					unset($grand_parent);
				}
			}
			break;
		}
	}

	?>

	<div class="hero-banner-shift"></div>
	<section class="split-hero-banner">
		<section class="section-wrap">
			<div class="split-hero-banner__wrap js-hero-banner">
				<div class="split-hero-banner__copy">
					<div class="split-hero-banner__copy-spacing">
						<div class="split-hero-banner__inner-wrap inner-wrap-@-sm">
							<div class="split-hero-banner__breadcrumb">
								<ul class="breadcrumb fs-xs fc-white">
									<?php if ( $grand_parent ): ?>
										<li class="breadcrumb__item">
											<a class="breadcrumb__link" href="<?php echo get_permalink( $grand_parent ); ?>"><?php print $grand_parent->post_title; ?></a>
										</li>
									<?php endif; ?>
									<?php if ( $parent ): ?>
										<li class="breadcrumb__item">
											<a class="breadcrumb__link" href="<?php echo get_permalink( $parent ); ?>"><?php print $parent->post_title; ?></a>
										</li>
									<?php endif; ?>
									<li class="breadcrumb__item">
										<a class="breadcrumb__link"><?php the_title(); ?></a>
									</li>
								</ul>
							</div>

							<?php
							$subheading = ( get_sub_field( 'banner_tag_line' ) ? get_sub_field( 'banner_tag_line' ) : get_the_title() );
							?>

							<div class="split-hero-banner__content">

								<h1 class="split-hero-banner__heading simple-motif simple-motif--white simple-motif--fixed-length alt-font fc-white mb-m">
									<span class="line-height-adjust"><?php echo $subheading; ?></span>
								</h1>
								<p class="split-hero-banner__intro fc-white"><?php echo strip_tags( get_sub_field( 'banner_text' ) ); ?></p>

								<?php

								if ( $cta_text ):

									$url = ( $cta_url ) ? $cta_url : $cta_url_picker;
									?>
									<div class="split-hero-banner__cta">
										<a href="<?php echo $url; ?>" data-form-id="<?php echo $hs_form['uid']; ?>" class="cta cta--large cta--secondary js-open-conversion-form">
											<span class="line-height-adjust">
												<?php echo $cta_text; ?>
											</span>
										</a>
									</div>
								<?php endif;

								if ( ! $cta_text && $grand_parent->ID == 1113 ):
									$id = 4519;
									$hs_form['title'] = get_the_title( ) . ' - ' . ucwords(strtolower(get_the_title( $id )));
									$hs_form['hs_id'] = get_field( 'form_id', $id );
									$hs_form['uid']   = substr($hs_form['hs_id'],0,5) . rand(9999,99999);
									$hs_form['intro'] = get_field( 'form_intro', $id );
									$hs_form['class'] = get_field( 'js_class', $id );
									$hs_form['redirect'] = get_field( 'redirect_url', $id );
									$hs_form['submit_text'] = get_field( 'submit_text', $id );
									$append_form = TRUE;

									?>
									<div class="split-hero-banner__cta">
										<a href="" data-form-id="<?php echo $hs_form['uid']; ?>" class="cta cta--large cta--secondary js-open-conversion-form">
											<span class="line-height-adjust">
												Get A Quote
											</span>
										</a>
									</div>
									<?php
								endif;

								?>

							</div>
						</div>
					</div>
				</div>
				<div class="split-hero-banner__image">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="responsive-img">
				</div>
				<div class="hero-banner-mask js-hero-banner-mask"></div>
			</div>

		</section>
	</section>
	<?php if ( $append_form ) {
	include realpath( dirname( __FILE__ ) ) . '/../partials/conversion-form.php';
}

endif; ?>

<section class="ptb-xl ta-center <?php the_sub_field( 'faqs_bg_colour' ); ?>">
	<div class="section-wrap section-wrap--narrow">

		<div class="spotlight">
			<div class="spotlight__heading">
				<h1 class="intro-heading intro-heading--motif intro-heading--motif-to-underline js-in-view" data-inview="simpleClass"><?php the_sub_field( 'faqs_heading' ); ?></h1>
			</div>
		</div>

		<?php if ( have_rows( 'faqs_faqs' ) ) : ?>
		<div class="questions-answered owl-carousel-pulsant-theme">
			<div class="questions-answered__carousel-wrap">
				<div class="questions-answered__items js-questions-answered">

					<?php while ( have_rows( 'faqs_faqs' ) ) :
					the_row();

					$post_object = get_sub_field( 'faq_item' );
					if ( $post_object ):
						$post = $post_object;
						setup_postdata( $post ); ?>
						<div class="questions-answered__item">
							<h2 class="questions-answered__question fs-l fw-semibold pb-s">
								<span class="fc-brand-blue">Q:</span>
								<?php the_title(); ?>
							</h2>
							<h3 class="questions-answered__answer">
								<span class="fw-semibold">A:</span>
								<?php print get_the_content(); ?>
							</h3>
						</div>
						<?php wp_reset_postdata(); ?>
					<?php endif;
					endwhile; ?>

				</div>
				<div class="questions-answered__controls js-questions-answered-controls">
					<!-- controls added with JS -->
				</div>
			</div>
			<div class="owl-theme">
				<div class="questions-answered__dots owl-dots js-questions-answered-dots">
					<!-- controls added with JS -->
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
  <div class="ta-center mt-m">
    <div class="attention-bar__cta">
      <a class="cta" href="/faqs">Explore All FAQs</a>
    </div>
  </div>
</section>
<?php
// Register Custom Post Type
function form_post_type() {

	$labels = array(
		'name'                  => _x( 'Hubspot Forms', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Hubspot Form', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Forms', 'text_domain' ),
		'name_admin_bar'        => __( 'Form', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Form', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args   = array(
		'label'               => __( 'Form', 'text_domain' ),
		'description'         => __( 'Forms', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', ),
		'hierarchical'        => FALSE,
		'public'              => TRUE,
		'show_ui'             => TRUE,
		'show_in_menu'        => TRUE,
		'menu_position'       => 11,
		'menu_icon'           => 'dashicons-feedback',
		'show_in_admin_bar'   => TRUE,
		'show_in_nav_menus'   => TRUE,
		'can_export'          => TRUE,
		'has_archive'         => FALSE,
		'exclude_from_search' => TRUE,
		'publicly_queryable'  => TRUE,
		'rewrite'             => FALSE,
		'capability_type'     => 'page',
	);

	register_post_type( 'hs_form', $args );
}

add_action( 'init', 'form_post_type', 0 );

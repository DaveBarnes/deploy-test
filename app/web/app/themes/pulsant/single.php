<?php
$extend_header = true;

$post_type = current( get_field( 'type' ) );
if ( ! $post_type ) {
	$post_type = 'blog';
}

$cats = implode( ',', wp_list_pluck( get_the_category(), 'term_id' ) );

// Don't use uncategorised!
if ( $cats == '1' ) {
	$cats = 0;
}

$hub_home = get_permalink( 609 );
$type     = get_field( 'type' );

$next_post = get_posts( array(
	'numberposts' => 1,
	'post_type'   => 'post',
	'category'    => $cats,
	'date_query'  => array(
		'after' => $post->post_date,
	),
	'orderby'     => 'date',
	'order'       => 'ASC',
) );

$prev_post = get_posts( array(
	'numberposts' => 1,
	'category'    => $catID,
	'post_type'   => 'post',
	'date_query'  => array(
		'before' => $post->post_date,
	),
	'orderby'     => 'date',
	'order'       => 'DESC',
) );


get_template_part( 'inc/partials/header' );

// Use this instead of get_template_part to make above variables available :)
include( locate_template( 'inc/hub/' . $post_type . '.php' ) );

get_template_part( 'inc/partials/footer' );

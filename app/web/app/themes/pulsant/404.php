<?php
/**
 * Template Name: Contact Page
 *
 * @package WordPress
 */

get_template_part( 'inc/partials/header' );
$image = get_field( 'image' );

$form = new HubSpotForm( 'cfd5818a-1b63-488d-a1c0-a555ca1ab3d4', 'Send an Enquiry', '/contact-us-thank-you/', 'Submit', array( 'enquiry_source' => 'Send an Enquiry' ) );

?>

<section class="heavy-hero-banner js-hero-banner ">
<div class="section-wrap section-wrap--restricted inner-wrap-@-sm">
<div class="hero-feature hero-feature--heavy-banner hero-feature--dark  hero-feature--align-centre-at-tablet hero-feature--text-focus">
<div class="hero-feature__aside">
<img src="https://www.pulsant.com/wp-content/uploads/2017/09/Thank-you.svg" alt="">
</div>
<div class="hero-feature__text">
<div class="service-text-collection">
<h1 class="service-text-collection__mini-heading motif">
</h1>
<div class="service-text-collection__main-heading">
<h2 class="heavy-hero-banner__heading fc-white">
404 Error</h2>
</div>
<div class="service-text-collection__copy">
<p>Page not found.</p>
</div>
</div>
</div>
</div>
</div>
<div class="hero-banner-mask js-hero-banner-mask"></div>
</section>

<?php
get_template_part( 'inc/partials/footer' );

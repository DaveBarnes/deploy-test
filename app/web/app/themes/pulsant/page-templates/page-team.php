<?php
/**
 * Template Name: Team Page
 *
 * @package WordPress
 */

get_template_part( 'inc/partials/header' );

$global_cta = get_field( 'cta-active', 'option' );
if ( $global_cta ) {
	$global_cta_location = get_field( 'cta-location', 'option' );
}
?>
	<div class="site-central-wrap">

		<div class="inner-wrap-@-sm ptb-l">
			<div class="simple-intro simple-intro--wide ta-center">
				<h1 class="simple-intro__heading fs-xl"><?php the_title(); ?></h1>

				<div class="simple-body">
					<?php the_content(); ?>
				</div>
			</div>
		</div>

		<div class="site-central-wrap__inner-restrict pb-l">
			<?php if ( have_rows( 'team_list' ) ) :
				$idx = 0;
				while ( have_rows( 'team_list' ) ) : the_row();
					$idx ++;
					$image = get_sub_field( 'image' );
					?>
					<div class="team-member js-team-member <?php echo( $idx % 2 == 0 ? 'team-member--reverse' : '' ); ?>">
						<div class="inner-wrap-@-sm team-member__wrap">
							<div class="team-member__photo" style="background-image: url('<?php echo $image['url']; ?>');">

							</div>
							<div class="team-member__about">
								<div class="team-member__intro js-intro">
									<p class="team-member__role">
										<?php the_sub_field( 'job_title' ); ?>
									</p>
									<p class="team-member__name">
										<?php the_sub_field( 'name' ); ?>
									</p>
									<p class="team-member__quote">
										<?php the_sub_field( 'quote' ); ?>
									</p>
									<button class="team-member__bio-button js-view-bio">
										<span class="team-member__bio-button-underline">Read More</span>
									</button>
								</div>
								<div class="team-member__bio is-hidden js-bio">
									<?php the_sub_field( 'bio' ); ?>
									<button class="team-member__bio-button js-close-bio">
										<span class="team-member__bio-button-underline">Back</span>
									</button>
								</div>
							</div>
						</div>
					</div>

				<?php endwhile; ?>
			<?php endif; ?>

		</div>
		<?php

		if ( have_rows( 'elements' ) ) :
			$global_cta_printed = FALSE;
			while ( have_rows( 'elements' ) ) : the_row();
				$layout = get_row_layout();
				get_template_part( 'inc/components/' . $layout );
				if ( $global_cta && ! $global_cta_printed && $global_cta_location == 'all' && strpos( $layout, 'hero' ) !== FALSE ) {
					get_template_part( 'inc/components/global_cta' );
					$global_cta_printed = TRUE;
				}
			endwhile;
		endif;

		?>
	</div>
<?php
get_template_part( 'inc/partials/footer' );

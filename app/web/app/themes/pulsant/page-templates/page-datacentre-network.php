<?php
/**
 * Template Name: Datacentre Network Page
 *
 * @package WordPress
 */

get_template_part( 'inc/partials/header' );
$image = get_field( 'image' );


?>

	<section class="datacentre-network-hero-banner js-hero-banner">
		<div class="section-wrap inner-wrap-@-sm">

			<div class="datacentre-network-hero-banner__intro">
				<div class="datacentre-network-hero-banner__mini-heading">
					<div class="service-text-collection service-text-collection--dark">
						<h1 class="service-text-collection__mini-heading motif">Datacentre network</h1>
					</div>
				</div>
				<div class="datacentre-network-hero-banner__main-heading">
					<div class="service-text-collection service-text-collection--dark">
						<div class="service-text-collection__main-heading">
							<h2 class="datacentre-network-hero-banner__heading fs-xl fw-semibold">
								Reliable, fluid, connected datacentre network
							</h2>
						</div>	
					</div>
				</div>
				<div class="datacentre-network-hero-banner__body">
					<p>Growing business and operations can create increased strain on IT and bandwidth requirements. Our datacentre network provides you with a range of secure, diverse, high capacity network interconnections between our own sites across the UK, internet transit providers, cloud partners and international networks.</p>

					<a href="/services/infrastructure/datacentres/">View all datacentres</a>
				</div>
			</div>
			<div class="datacentre-network-hero-banner__map">
				<img src="<?php print get_template_directory_uri() . "/images/datacentre-network.svg"; ?>" alt="Datacentre Network Map" class="responsive-img">
			</div>
		</div>
		<div class="hero-banner-mask js-hero-banner-mask"></div>
	</section>

	
<?php

if ( have_rows( 'elements' ) ) :
	$global_cta_printed = false;
	while ( have_rows( 'elements' ) ) : the_row();
		$layout = get_row_layout();
		get_template_part( 'inc/components/' . $layout );
		if ( $global_cta && ! $global_cta_printed && $global_cta_location == 'all' && strpos($layout, 'hero') !== false ) {
			get_template_part( 'inc/components/global_cta' );
			$global_cta_printed = true;
		}
	endwhile;
endif;

get_template_part( 'inc/partials/footer' );


<?php
/**
 * Template Name: Contact Page
 *
 * @package WordPress
 */

get_template_part( 'inc/partials/header' );
$image = get_field( 'image' );

$form = new HubSpotForm( 'cfd5818a-1b63-488d-a1c0-a555ca1ab3d4', 'Send an Enquiry', '/contact-us-thank-you/', 'Submit', array( 'enquiry_source' => 'Send an Enquiry' ) );

?>

	<section class="simple-hero-banner bgc-dark-blue js-hero-banner">
		<div class="section-wrap--restricted">
			<div class="simple-hero-banner__wrap inner-wrap-@-sm">
				<div class="simple-hero-banner__copy">
					<h1 class="fw-semibold fs-xl pb-s fc-white"><?php the_field( 'intro_text' ); ?></h1>
				</div>
				<div class="simple-hero-banner__image">
					<?php $image = get_field( 'image' );
					if ( $image ) { ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="hero-banner-mask js-hero-banner-mask"></div>
	</section>

	<section class="ptb-l ptb-xl--@-lg bgc-white">
		<?php if ( have_rows( 'contact_numbers' ) ) : ?>
			<div class="definition-grid">
				<div class="definition-grid__wrap section-wrap--restricted inner-wrap-@-sm">
					<div class="definition-grid__heading">
						<h2 class="fw-semibold fc-dark-blue fs-l left-motif">Talk to us</h2>
					</div>

					<div class="definition-grid__content">
						<?php while ( have_rows( 'contact_numbers' ) ) : the_row();

							$contact_link = get_sub_field( 'email' );
							if(strpos($contact_link,'@') !== FALSE ){
								$contact_link = 'mailto:' . $contact_link;
							} else {
								$contact_link = '//' . $contact_link;
							}
							?>
							<div class="contact-summary">
								<h3 class="contact-summary__heading"><?php the_sub_field( 'department' ); ?></h3>
								<p class="contact-summary__phone"><?php the_sub_field( 'phone' ); ?></p>
								<p><a class="contact-summary__email" href="<?php echo $contact_link; ?>"><?php the_sub_field( 'email' ); ?></a></p>
							</div>

						<?php endwhile; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<div class="definition-grid bgc-lightest-blue">
			<form class="form form--contact" action="/api/" method="post" enctype="application/x-www-form-urlencoded" data-parsley-validate>
				<div class="definition-grid__wrap section-wrap--restricted inner-wrap-@-sm">
					<div class="definition-grid__heading">
						<h2 class="fw-semibold fc-dark-blue fs-l left-motif">Send an enquiry</h2>
					</div>
					<div class="definition-grid__content">
						<div class="form-layout form-layout--two-columns">

							<?php
							foreach ( $form->formData['fields'] as $field ) {
								if ( $field['type'] == 'text' ) {
									?>
									<div class="form-layout__field <?php if ( $field['type'] == 'textarea' ): ?>form-layout__field--textarea <?php endif; ?>">
										<label class="form__label" for="<?php echo $field['id']; ?>"><?php echo $field['label']; ?></label>
										<?php echo $field['markup']; ?>
									</div>
							<?php
								}
							}
							?>

							<?php
							foreach ( $form->formData['fields'] as $field ) {
								if ( $field['type'] != 'text' ) {
									?>
									<div class="form-layout__field <?php if ( $field['type'] == 'textarea' ): ?>form-layout__field--textarea <?php endif; ?>">
									<label class="form__label" for="<?php echo $field['id']; ?>"><?php echo $field['label']; ?></label>
									<?php echo $field['markup']; ?>
									</div>
							<?php
								}
							}
							?>

							<?php echo $form->formData['hiddenFields']; ?>
							<button class="cta cta--large cta--primary">
								<span class="line-height-adjust">Submit</span>
							</button>
						</div>
					</div>
				</div>
				<input type="hidden" name="eventcategory" class="js-eventcategory" value="Form Submission">
				<input type="hidden" name="eventaction" class="js-eventaction" value="Contact Form">
				<input type="hidden" name="eventlabel" class="js-eventlabel" value="<?php the_title(); ?>">
			</form>
		</div>

		<?php if ( have_rows( 'contact_addresses' ) ) : ?>
			<div class="definition-grid">
				<div class="definition-grid__wrap section-wrap--restricted inner-wrap-@-sm">
					<div class="definition-grid__heading">
						<h2 class="fw-semibold fc-dark-blue fs-l left-motif">Find us</h2>
					</div>
					<div class="definition-grid__content">
						<?php while ( have_rows( 'contact_addresses' ) ) :
							the_row(); ?>
							<div class="contact-summary contact-summary<?php print ( get_sub_field( 'important' ) == 1 ? '--important' : '' ); ?>">
								<h3 class="contact-summary__heading"><?php the_sub_field( 'name' ); ?></h3>
								<p class="contact-summary__phone"><?php the_sub_field( 'address' ); ?></p>
								<?php if ( get_sub_field( 'registered_number' ) ): ?>
									<p class="contact-summary__break"><span class="contact-summary__small">Registered Number<br> <?php the_sub_field( 'registered_number' ); ?></span></p>
								<?php endif; ?>
								<p><a class="contact-summary__link" href="<?php the_sub_field( 'map_link' ); ?>">view map</a></p>
								<?php if ( get_sub_field( 'page_link' ) ):
									$page_link = get_sub_field( 'page_link' );
									?>
									<p><a class="contact-summary__link" href="<?php echo $page_link['url']; ?>" target="<?php echo $page_link['target']; ?>"><?php echo $page_link['title']; ?></a></p>
									<?php
								endif;
								?>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		<?php endif;

		if ( have_rows( 'other_addresses' ) ) : ?>

		<div class="js-other-addresses">

			<div class="definition-grid js-address-content">
				<div class="definition-grid__wrap section-wrap--restricted inner-wrap-@-sm">
					<div class="definition-grid__heading">
						<h2 class="fw-semibold fc-dark-blue fs-l left-motif">Datacentres</h2>
					</div>
					<div class="definition-grid__content">

						<?php while ( have_rows( 'other_addresses' ) ) :
							the_row(); ?>
							<div class="contact-summary contact-summary<?php print ( get_sub_field( 'important' ) == 1 ? '--important' : '' ); ?>">
								<h3 class="contact-summary__heading"><?php the_sub_field( 'name' ); ?></h3>
								<p class="contact-summary__phone"><?php the_sub_field( 'address' ); ?></p>
								<?php if ( get_sub_field( 'registered_number' ) ): ?>
									<p class="contact-summary__break"><span class="contact-summary__small">Registered Number<br> <?php the_sub_field( 'registered_number' ); ?></span></p>
								<?php endif; ?>
								<p><a class="contact-summary__link" href="<?php the_sub_field( 'map_link' ); ?>">view map</a></p>
								<?php if ( get_sub_field( 'page_link' ) ):
									$page_link = get_sub_field( 'page_link' );
									?>
									<p><a class="contact-summary__link" href="<?php echo $page_link['url']; ?>" target="<?php echo $page_link['target']; ?>"><?php echo $page_link['title']; ?></a></p>
									<?php
								endif;
								?>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>

			<div class="definition-grid definition-grid--button">
				<div class="definition-grid__wrap section-wrap--restricted inner-wrap-@-sm">
					<div class="definition-grid__heading">

					</div>
					<div class="definition-grid__content">
						<div class="definition-grid__cta">
							<button class="cta cta--large cta--hollow cta--hollow-primary js-hide-show-button" data-alt-text="Hide  datacentre locations">
								<span class="line-height-adjust js-hide-show-button-text">View datacentre locations</span>
							</button>
						</div>
					</div>
				</div>
			</div>

		</div>
		<?php endif; ?>
	</section>
<?php
get_template_part( 'inc/partials/footer' );

<?php
/**
 * Template Name: Timeline Template
 *
 * @package WordPress
 */
get_template_part( 'inc/partials/header' );

the_content();
?>
	<section class="section-wrap section-wrap--restricted inner-wrap-@-sm">
		<div class="ptb-n">
			<?php if ( have_rows( 'entries' ) ) :
				$row_index = 0; ?>
				<div id="cd-timeline" class="cd-container">
					<?php while ( have_rows( 'entries' ) ) : the_row();
						$row_index ++; ?>
						<div class="cd-timeline-block">
							<div class="cd-timeline-img cd-<?php print ( $row_index % 3 ) + 1; ?>">
							</div>

							<div class="cd-timeline-content">
								<?php the_sub_field( 'text' ); ?>
								<span class="cd-date"><?php the_sub_field( 'date' ); ?></span>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif;
			?>
		</div>
	</section>
<?php

get_template_part( 'inc/partials/footer' );

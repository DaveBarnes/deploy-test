<?php
/**
 * Template Name: Onyx
 *
 * @package WordPress
 */

get_template_part( 'inc/partials/header' );


the_content();

if ( have_rows( 'elements' ) ) :
	$global_cta_printed = false;
	while ( have_rows( 'elements' ) ) : the_row();
		$layout = get_row_layout();
		get_template_part( 'inc/components/' . $layout );
		if ( $global_cta && ! $global_cta_printed && $global_cta_location == 'all' && strpos($layout, 'hero') !== false ) {
			get_template_part( 'inc/components/global_cta' );
			$global_cta_printed = true;
		}
	endwhile;
endif;

?>
<section class="heavy-hero-banner js-hero-banner ">
	<div class="section-wrap section-wrap--restricted inner-wrap-@-sm">
		<div class="hero-feature hero-feature--heavy-banner hero-feature--dark hero-feature--text-focus hero-feature--align-centre-at-tablet">
			<div class="hero-feature__aside hero-feature__aside--img-width-80-percent-at-tablet">
				<img class="icon-snippet-collection__img icon-snippet-collection__img--support" src="<?php print get_template_directory_uri() . "/images/onyx/onyx-logo.svg"; ?>" alt="Onyx Group">
			</div>
			<div class="hero-feature__text">
				<div class="service-text-collection">
					<h1 class="service-text-collection__mini-heading motif">
					How can we help? </h1>
					<div class="service-text-collection__main-heading">
						<h2 class="heavy-hero-banner__heading fc-white">
						Access our Customer Support facilities and see the status of the Onyx Network here. </h2>
					</div>
					<div class="service-text-collection__copy">
						<div class="heavy-hero-banner__cta">
							<a href="" target="" data-form-id="0b99176406" class="cta cta--large cta--primary js-open-conversion-form">
								<span class="line-height-adjust">
								Send an enquiry </span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="hero-banner-mask js-hero-banner-mask"></div>
</section>

<section class="bgc-white">
	<div class="section-wrap inner-wrap-@-sm  section-wrap--narrow">
		<div class="ptb-l ptb-xl--@-sm">
			<div class="spotlight">

				<div class="spotlight">
					<div class="spotlight__heading">
						<h1 class="fc-dark-blue fw-semibold fs-xl">Customer Support Facilities</h1>
					</div>
				</div>

				<div class="icon-snippet-collection">
					<div class="icon-snippet-collection__item icon-snippet-collection__item--support">
						<img class="icon-snippet-collection__img icon-snippet-collection__img--support" src="<?php print get_template_directory_uri() . "/images/onyx/icon-onyx-support.png"; ?>" alt="">
						<h2 class="icon-snippet-collection__heading">Onyx Support</h2>
						<a href="mailto:support@onyx.net" class="icon-snippet-collection__link">support@onyx.net</a>
						<h3 class="icon-snippet-collection__number">03333 440 440</h3>
					</div>

					<div class="icon-snippet-collection__item icon-snippet-collection__item--webmail">
						<img class="icon-snippet-collection__img icon-snippet-collection__img--webmail" src="<?php print get_template_directory_uri() . "/images/onyx/icon-onyx-webmail.png"; ?>" alt="">
						<h2 class="icon-snippet-collection__heading">Webmail</h2>
						<a href="https://webmail.onyx.net/" class="icon-snippet-collection__link">Access Webmail</a>
					</div>

					<div class="icon-snippet-collection__item icon-snippet-collection__item--usage">
						<img class="icon-snippet-collection__img icon-snippet-collection__img--usage" src="<?php print get_template_directory_uri() . "/images/onyx/icon-onyx-usage.png"; ?>" alt="">
						<h2 class="icon-snippet-collection__heading">Usage</h2>
						<a href="https://monitoring.onyx.net/" class="icon-snippet-collection__link">Monitoring</a>
					</div>

					<div class="icon-snippet-collection__item icon-snippet-collection__item--remote">
						<img class="icon-snippet-collection__img icon-snippet-collection__img--remote" src="<?php print get_template_directory_uri() . "/images/onyx/icon-onyx-remote.png"; ?>" alt="">
						<h2 class="icon-snippet-collection__heading">Remote Support</h2>
						<a href="http://www.gotoassist.com/ph/onyxgroup" class="icon-snippet-collection__link">Remote Support</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
get_template_part( 'inc/partials/footer' );
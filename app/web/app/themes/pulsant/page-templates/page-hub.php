<?php
/**
 * Template Name: Hub List
 *
 * @package WordPress
 */

global $post;
$post_slug   = $post->post_name;
$filter_type = get_query_var( 'type', NULL );
$cat         = get_query_var( 'hubcat', NULL );

$type_list     = get_field_object( 'field_599fff5dc04db' );
$type_list     = $type_list['choices'];
$extend_header = TRUE;

get_template_part( 'inc/partials/header' );

?>
	<section class="knowledge-hub-intro">
		<div class="knowledge-hub-intro__wrap section-wrap section-wrap--smaller inner-wrap-@-sm">
			<div class="knowledge-hub-intro__copy">
				<h1 class="fs-xl fc-dark-blue fw-semibold simple-motif mb-s mb-m--@-sm"><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
			<div class="knowledge-hub-intro__nav">
				<label class="fw-semibold mb-s" for="hub-category">Choose a category</label>
				<select name="hub-category" id="hub-category" class="js-jump-menu">
					<option value="/<?php echo $post_slug; ?>">- Latest Posts -</option>
					<?php foreach ( $type_list as $k => $type ):

						if ( substr( $type, -1 ) == 'y' ) {
							$type = substr( $type, 0, -1 ) . 'ies';
						} else {
							$type .= 's';
						}

						?>
						<option value="/<?php echo $post_slug . '/' . $k; ?>" <?php echo( get_query_var( 'type' ) == $k ? 'selected="selected"' : '' ); ?>><?php echo $type; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
	</section>

	<div class="knowledge-hub-sign-up-form ptb-m">
		<div class="section-wrap section-wrap--smaller">
			<div class="inner-wrap-@-sm">
				<div class="knowledge-hub-sign-up-form__wrap">
					<p class="knowledge-hub-sign-up-form__heading fc-white">
						<span class="line-height-adjust">Register for updates</span>
					</p>
					<?php
					$hubspot = new HubSpotForm( 'fe2d7f52-441c-49e3-b6ec-4f15de911efb', get_the_title(), '/thank-you-signup/' );
					?>
					<form class="js-analytics-form js-nocaptcha form" action="/api/" method="post" enctype="application/x-www-form-urlencoded" data-parsley-validate data-parsley-errors-container="#form-newsletter-kh-errors">
					<div class="knowledge-hub-sign-up-form__input">
						<div class="form form--newsletter pb-m pb-n--@-md">
							<div class="form__input-and-button">
									<?php print str_replace('<input ','<input placeholder="Email address" ',$hubspot->formData['fields']['email']['markup']);
									print $hubspot->formData['hiddenFields']; ?>
									<button class="form__button bgc-dark-blue">Submit</button>
							</div>
						</div>
					</div>
					<div id="form-newsletter-kh-errors"></div>
						<input type="hidden" name="eventcategory" class="js-eventcategory" value="Form Submission">
						<input type="hidden" name="eventaction" class="js-eventaction" value="Register For Updates">
						<input type="hidden" name="eventlabel" class="js-eventlabel" value="<?php the_title(); ?>">
					</form>
					<a href="/privacy-cookie-policy/" class="knowledge-hub-sign-up-form__link fs-s">
						Privacy policy
					</a>
				</div>
			</div>
		</div>
	</div>
<?php
$post_types = NULL;
$paged      = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

if ( $filter_type ) {
	$post_types = array(
		'key'     => 'type',
		'value'   => array( $filter_type ),
		'compare' => 'IN',
	);
}

$args = array(
	'numberposts'    => - 1,
	'posts_per_page' => 20,
	'paged'          => $paged,
	'post_type'      => 'post',
	'meta_query'     => array(
		'relation' => 'AND',
		$post_types,
	),
);

if ( $cat ) {
	$args['category_name'] = $cat;
}

$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ): ?>
	<section class="section-wrap section-wrap--smaller">
		<div class="inner-wrap-@-sm">
			<div class="knowledge-hub-grid">
				<?php while ( $the_query->have_posts() ) : $the_query->the_post();

					$type       = get_field( 'type' );
					$modifier   = get_field( 'teaser_modifier');
					?>
					<div class="knowledge-hub-grid__item">

						<a class="knowledge-hub-article-teaser <?php echo $modifier; ?>" href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail( 'hub-thumb', array( 'class' => 'knowledge-hub-article-teaser__img responsive-img' ) ); ?>

							<div class="knowledge-hub-article-teaser__info">
								<h1 class="knowledge-hub-article-teaser__heading fw-semibold"><?php the_title(); ?></h1>
								<span class="knowledge-hub-article-teaser__cta">
                                        <span class="line-height-adjust">Read more</span>
                                    </span>
								<span class="knowledge-hub-article-teaser__date">
                                        <span class="line-height-adjust"><?php if ( get_field( 'event_date' ) ) {
		                                        the_field( 'event_date' );
	                                        } else {
		                                        the_time( 'j M Y' );
	                                        } ?></span>
                                    </span>
								<span class="knowledge-hub-article-teaser__label">
                                        <span class="line-height-adjust">
                                            <?php echo $type['label']; ?>
                                        </span>
                                    </span>
							</div>
						</a>

					</div>

				<?php endwhile; ?>
			</div>

			<?php if ( $the_query->max_num_pages > 1 ) : ?>
				<nav class="pagination">
					<div class="pagination__prev">
						<?php echo get_previous_posts_link( '&#8592; Previous' ); ?>
					</div>
					<div class="pagination__numbers">
						<?php
						if (function_exists(custom_pagination)) {
							custom_pagination($the_query->max_num_pages,3,$paged);
						}
						?>
					</div>
					<div class="pagination__next">
						<?php echo get_next_posts_link( 'Next &#8594;', $the_query->max_num_pages ); ?>

					</div>
				</nav>
			<?php endif; ?>
	</section>

<?php else: ?>
	<section class="section-wrap section-wrap--smaller">
		<div class="inner-wrap-@-sm ptb-xl ta-center">
			<p>Sorry, no articles were found. Please try a different category.</p>
		</div>
	</section>
<?php endif;

wp_reset_query();     // Restore global post data stomped by the_post().
get_template_part( 'inc/partials/footer' );

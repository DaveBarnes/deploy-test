<?php
/**
 * Template Name: Simple Page
 *
 * @package WordPress
 */

get_template_part( 'inc/partials/header' );
$image = get_field( 'image' );
?>
	<section class="simple-hero-banner js-hero-banner">
		<div class="section-wrap--narrow">
			<div class="simple-hero-banner__wrap inner-wrap-@-sm">
				<div class="simple-hero-banner__copy">
					<h1 class="fw-semibold fs-xl pb-s"><?php the_title(); ?></h1>
					<?php the_field( 'intro_text' ); ?>
				</div>
				<div class="simple-hero-banner__image">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			</div>
		</div>
	</section>

<?php if ( have_rows( 'text_group' ) ): ?>
	<?php while ( have_rows( 'text_group' ) ) : the_row();
		if ( get_row_layout() == 'document_group' ) : ?>
			<section class="simple-text wp-content">
				<div class="inner-wrap-@-sm">
					<div class="simple-text__group">
						<h2><?php the_sub_field( 'heading' ); ?></h2>
						<?php if ( have_rows( 'links' ) ) :
							while ( have_rows( 'links' ) ) : the_row();
								$file = get_sub_field( 'file' );
								if ( $file ) { ?>
									<a class="no-underline" href="<?php echo $file['url']; ?>" title="<?php the_sub_field( 'description' ); ?>"><?php the_sub_field( 'description' ); ?></a>
								<?php }
							endwhile;
						endif; ?>
					</div>
				</div>
			</section>
		<?php elseif ( get_row_layout() == 'text_content' ) : ?>
			<section class="simple-text wp-content">
				<div class="inner-wrap-@-sm">
					<?php the_sub_field( 'content' ); ?>
				</div>
			</section>
		<?php endif;
	endwhile;
endif;

get_template_part( 'inc/partials/footer' );

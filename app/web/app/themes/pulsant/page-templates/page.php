<?php
get_template_part( 'inc/partials/header' );

$global_cta = get_field( 'cta-active', 'option' );
if ( $global_cta ) {
	$global_cta_location = get_field( 'cta-location', 'option' );
}

if ( is_front_page() ):
	?>
	<section class="site-intro-banner js-site-intro-banner js-hero-banner owl-carousel-pulsant-theme-alt">
		<div class="site-intro-banner__wrap">
			
			<div class="site-intro-banner__carousel js-carousel-wrap">
				<div class="site-intro-banner__carousel-item">
					<div class="section-wrap inner-wrap-@-sm">
						<div class="site-intro-banner__carousel-item-wrap">
							<img src="<?php print get_template_directory_uri() . "/images/site-intro-banner-slide-1-illustration.svg"; ?>" alt="" class="site-intro-banner__carousel-image">
							<div class="site-intro-banner__carousel-text-wrap">
								<h1 class="site-intro-banner__carousel-heading site-intro-banner__carousel-heading--slide-1 fw-semibold fc-white">
									Experts in compliant business cloud platforms
								</h1>
								<p class="site-intro-banner__carousel-copy fc-white">We are the UK’s leading secure hybrid IT provider helping you realise the benefits of the cloud.</p>
							</div>
						</div>
					</div>
				</div>

				<div class="site-intro-banner__carousel-item">
					<div class="section-wrap inner-wrap-@-sm">
						<div class="site-intro-banner__carousel-item-wrap">
							<div class="site-intro-banner__carousel-image">
								<?php require get_theme_file_path() ."/inc/partials/individual-circle.svg.php"; ?>
							</div>
							<div class="site-intro-banner__carousel-text-wrap">
								<h2 class="site-intro-banner__carousel-heading fw-semibold fc-white">
									Reliability
								</h2>
								<p class="site-intro-banner__carousel-copy fc-white">We help you realise the full capability of cloud through intelligent hybrid IT platforms. Our managed cloud solutions, optimise IT to meet continuous demand and drive your business&nbsp;forward. </p>
							</div>
						</div>
					</div>
				</div>

				<div class="site-intro-banner__carousel-item">
					<div class="section-wrap inner-wrap-@-sm">
						<div class="site-intro-banner__carousel-item-wrap">
							<div class="site-intro-banner__carousel-image">
								<?php require get_theme_file_path() ."/inc/partials/individual-cloud.svg.php"; ?>
							</div>
							<div class="site-intro-banner__carousel-text-wrap">
								<h2 class="site-intro-banner__carousel-heading fw-semibold fc-white">
									Fluidity
								</h2>
								<p class="site-intro-banner__carousel-copy fc-white">Businesses are in a constant state of flux and need flexible IT to help them adapt, thrive and meet customer demand. Rely on our expert cloud consultancy and specialist resources to navigate this&nbsp;change.</p>
							</div>
						</div>
					</div>
				</div>

				<div class="site-intro-banner__carousel-item">
					<div class="section-wrap inner-wrap-@-sm">
						<div class="site-intro-banner__carousel-item-wrap">
							<div class="site-intro-banner__carousel-image">
								<?php require get_theme_file_path() ."/inc/partials/individual-shield.svg.php"; ?>
							</div>
							<div class="site-intro-banner__carousel-text-wrap">
								<h2 class="site-intro-banner__carousel-heading fw-semibold fc-white">
									Assurability
								</h2>
								<p class="site-intro-banner__carousel-copy fc-white">Our practised approach to cyber security and threat mitigation empowers you to determine, deploy and maintain the right levels of protection to keep your business secure, as the threat landscape&nbsp;changes.</p>
							</div>
						</div>
					</div>
				</div>

				<div class="site-intro-banner__carousel-item">
					<div class="section-wrap inner-wrap-@-sm">
						<div class="site-intro-banner__carousel-item-wrap">
							<div class="site-intro-banner__carousel-image">
								<?php require get_theme_file_path() ."/inc/partials/individual-tick.svg.php"; ?>
							</div>
							<div class="site-intro-banner__carousel-text-wrap">
								<h2 class="site-intro-banner__carousel-heading fw-semibold fc-white">
									Credibility
								</h2>
								<p class="site-intro-banner__carousel-copy fc-white">Operating in a regulated environment is challenging. We offer end-to-end compliance of hybrid services, driven by IT analytics to continuously monitor, audit and demonstrate&nbsp;compliance.</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="site-intro-banner__titles">
				<div class="site-intro-banner__titles-wrap">

					<div class="site-intro-banner__canvas js-canvas-wrap">
						<canvas id="canvas"></canvas>
					</div>

					<div class="site-intro-banner__text-wrap site-intro-banner__text-wrap--slide-1 js-text-wrap" data-index="0">
						<div class="hero-feature hero-feature--dark hero-feature--align-centre-at-tablet hero-feature--text-focus">
							<div class="hero-feature__aside js-illustration">
								<?php require get_theme_file_path() ."/inc/partials/individual-slide-one-illustration.svg.php"; ?>
							</div>
							<div class="hero-feature__text">
								<div class="service-text-collection">
									<div class="service-text-collection__main-heading">
										<h1 class="site-intro-banner__slide-1-heading fw-semibold fc-white">
											Experts in compliant<br>business cloud platforms
										</h1>
									</div>

									<div class="service-text-collection__copy">
										<p class="site-intro-banner__copy site-intro-banner__copy--slide-1 motif">We are the UK’s leading secure hybrid IT and cloud hosting provider helping you realise the benefits of the cloud.</p>
									</div>

									<div class="service-text-collection__cta">
										<a href="#value-proposition" class="cta cta--large cta--primary js-scroll-to">
											<span class="line-height-adjust">What do pulsant do?</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="site-intro-banner__text-wrap js-text-wrap is-hidden" data-index="1">
						<div class="site-intro-banner__svg js-svgs" data-index="1">
							<?php require get_theme_file_path() ."/inc/partials/individual-circle.svg.php"; ?>
						</div>
						<div class="site-intro-banner__text-container">
							<h2 class="site-intro-banner__heading fc-white fw-semibold">
								<span class="site-intro-banner__heading-inner js-heading-inner" data-index="1">Reliability</span>
							</h2>
							<p class="site-intro-banner__copy motif fc-white js-copy" data-index="1">We help you realise the full capability of cloud through intelligent hybrid IT platforms. Our managed cloud solutions, optimise IT to meet continuous demand and drive your business forward. </p>
						</div>
					</div>

					<div class="site-intro-banner__text-wrap js-text-wrap is-hidden" data-index="2">
						<div class="site-intro-banner__svg js-svgs" data-index="2">
							<?php require get_theme_file_path() ."/inc/partials/individual-cloud.svg.php"; ?>
						</div>
						<div class="site-intro-banner__text-container">
							<h2 class="site-intro-banner__heading fc-white fw-semibold">
								<span class="site-intro-banner__heading-inner js-heading-inner" data-index="2">Fluidity</span>
							</h2>
							<p class="site-intro-banner__copy motif fc-white js-copy" data-index="2">Businesses are in a constant state of flux and need flexible IT to help them adapt, thrive and meet customer demand. Rely on our expert cloud consultancy and specialist resources to navigate this change.</p>
						</div>
					</div>

					<div class="site-intro-banner__text-wrap js-text-wrap is-hidden" data-index="3">
						<div class="site-intro-banner__svg js-svgs" data-index="3">
							<?php require get_theme_file_path() ."/inc/partials/individual-shield.svg.php"; ?>
						</div>
						<div class="site-intro-banner__text-container">
							<h2 class="site-intro-banner__heading fc-white fw-semibold">
								<span class="site-intro-banner__heading-inner js-heading-inner" data-index="3">Assurability</span>
							</h2>
							<p class="site-intro-banner__copy motif fc-white js-copy" data-index="3">Our practised approach to cyber security and threat mitigation empowers you to determine, deploy and maintain the right levels of protection to keep your business secure, as the threat landscape changes.</p>
						</div>
					</div>

					<div class="site-intro-banner__text-wrap js-text-wrap is-hidden" data-index="4">
						<div class="site-intro-banner__svg js-svgs" data-index="4">
							<?php require get_theme_file_path() ."/inc/partials/individual-tick.svg.php"; ?>
						</div>
						<div class="site-intro-banner__text-container">
							<h2 class="site-intro-banner__heading fc-white fw-semibold">
								<span class="site-intro-banner__heading-inner js-heading-inner" data-index="4">Credibility</span>
							</h2>
							<p class="site-intro-banner__copy motif fc-white js-copy" data-index="4">Operating in a regulated environment is challenging. We offer end-to-end compliance of hybrid services, driven by IT analytics to continuously monitor, audit and demonstrate compliance.</p>
						</div>
					</div>
				</div>
			</div>

			<div class="site-intro-banner__controls js-site-intro-banner-controls">
				<!-- controls added with JS -->
			</div>

			<div class="owl-theme">
				<div class="site-intro-banner__dots owl-dots js-site-intro-banner-dots">
					<!-- dots added with JS -->
				</div>
			</div>
		</div>
		<div class="hero-banner-mask js-hero-banner-mask"></div>
	</section>
<?php

	if ( $global_cta ) {
		get_template_part( 'inc/components/global_cta' );
	}

endif;

the_content();

if ( have_rows( 'elements' ) ) :
	$global_cta_printed = false;
	while ( have_rows( 'elements' ) ) : the_row();
		$layout = get_row_layout();
		get_template_part( 'inc/components/' . $layout );
		if ( $global_cta && ! $global_cta_printed && $global_cta_location == 'all' && strpos($layout, 'hero') !== false ) {
			get_template_part( 'inc/components/global_cta' );
			$global_cta_printed = true;
		}
	endwhile;
endif;

get_template_part( 'inc/partials/footer' );

<?php
/**
 * Template Name: Value Proposition
 *
 * @package WordPress
 */

get_template_part( 'inc/partials/header' );

?>
	<div class="site-central-wrap">
		<?php
		if ( have_rows( 'elements' ) ) :
			$global_cta_printed = FALSE;
			while ( have_rows( 'elements' ) ) : the_row();
				$layout = get_row_layout();
				get_template_part( 'inc/components/' . $layout );
				if ( $global_cta && ! $global_cta_printed && $global_cta_location == 'all' && strpos( $layout, 'hero' ) !== FALSE ) {
					get_template_part( 'inc/components/global_cta' );
					$global_cta_printed = TRUE;
				}
			endwhile;
		endif;
		?>
	</div>
<?php
get_template_part( 'inc/partials/footer' );


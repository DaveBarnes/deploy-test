<?php
/**
 * Template Name: Datacentres
 *
 * @package WordPress
 */

get_template_part( 'inc/partials/header' );
$image = get_field( 'image' );


?>
<section class="bgc-dark-blue datacentre-map js-datacentre-map js-hero-banner">
	<div class="section-wrap section-wrap--restricted inner-wrap-@-sm datacentre-map__wrap">
		<div class="datacentre-map__details">
			<div class="service-text-collection">
				<h1 class="service-text-collection__mini-heading motif">
					Datacentres
				</h1>
				<h2 class="datacentre-map__heading fs-xl fw-semibold">
					Enterprise-class global datacentre infrastructure
				</h2>
				<div class="service-text-collection__copy">
					<p>
						We own and operate a network of UK enterprise-class datacentres, and have a strong global presence in the US, Asia and Europe.
					</p>
					<p>
						With secure sites across the UK, connected by a high-performing network, your organisation has a choice of locations to suit your resilience requirements.
					</p>
					
					<div class="datacentre-map__info-button js-workplace-recovery-link" style="display: none;">
						<a href="/services/business-continuity/workplace-recovery/" class="cta cta--primary cta--large">
							<span class="line-height-adjust">Explore Workplace Recovery</span>
						</a>
					</div>

					<div class="datacentre-map__info-button js-offices-link" style="display: none;">
						<a href="/contact-us/" class="cta cta--primary cta--large">
							<span class="line-height-adjust">Contact Us</span>
						</a>
					</div>

					<div class="datacentre-map__datacentre-list js-datacentre-list">
						<p>
							Select a datacentre:
						</p>
						<ul class="location-list location-list--no-left-space location-list--with-buttons location-list--two">
							<li>
								<button class="location-list__button js-datacentre-list-button" data-datacentre="Edinburgh South Gyle">South Gyle</button>
								<a class="location-list__link" href="/services/colocation/south-gyle-datacentre/">South Gyle</a>
							</li>
							<li>
								<button class="location-list__button js-datacentre-list-button" data-datacentre="South London">South London</button>
								<a class="location-list__link" href="/services/colocation/south-london-datacentre/">South London</a>
							</li>
							<li>
								<button class="location-list__button js-datacentre-list-button" data-datacentre="Reading East">Reading East</button>
								<a class="location-list__link" href="/services/colocation/reading-east-datacentre/">Reading East</a>
							</li>
							<li>
								<button class="location-list__button js-datacentre-list-button" data-datacentre="Maidenhead">Maidenhead</button>
								<a class="location-list__link" href="/services/colocation/maidenhead-datacentre/">Maidenhead</a>
							</li>
							<li>
								<button class="location-list__button js-datacentre-list-button" data-datacentre="Milton Keynes">Milton Keynes</button>
								<a class="location-list__link" href="/services/colocation/milton-keynes-datacentre/">Milton Keynes</a>
							</li>
							<li>
								<button class="location-list__button js-datacentre-list-button" data-datacentre="Edinburgh New Bridge">Newbridge</button>
								<a class="location-list__link" href="/services/colocation/newbridge-datacentre/">Newbridge</a>
							</li>
							<li>
								<button class="location-list__button js-datacentre-list-button" data-datacentre="Newcastle East">Newcastle East</button>
								<a class="location-list__link" href="/services/colocation/newcastle-east-datacentre/">Newcastle East</a>
							</li>
							<li>
								<button class="location-list__button js-datacentre-list-button" data-datacentre="Newcastle Central">Newcastle Central</button>
								<a class="location-list__link" href="/services/colocation/newcastle-central-datacentre/">Newcastle Central</a>
							</li>
							<li>
								<button class="location-list__button js-datacentre-list-button" data-datacentre="Edinburgh">Edinburgh</button>
								<a class="location-list__link" href="/services/colocation/edinburgh-datacentre/">Edinburgh</a>
							</li>
							<li>
								<button class="location-list__button js-datacentre-list-button" data-datacentre="Sheffield">Sheffield</button>
								<a class="location-list__link" href="/services/colocation/sheffield-datacentre/">Sheffield</a>
							</li>
							<li>
								<button class="location-list__button js-datacentre-list-button" data-datacentre="Glasgow North">Glasgow North</button>
								<a class="location-list__link" href="/services/colocation/glasgow-north-datacentre/">Glasgow North</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="datacentre-map__map">
			<div class="datacentre-map__map-wrap">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 451.9 741" enable-background="new 0 0 451.9 741" xml:space="preserve" id="datacentre-map">
				<g id="Map">
					<g>
						<path fill="#FFFFFF" d="M218.2,3l-10.8-3c0,0-6.5,7.4-9.1,5.6c-2.6-1.7-4.3-1.3-6.1-0.9c-1.7,0.4-5.6,5.2-9.5,4.3
							c-3.9-0.9-20.8-2.2-20.8-2.2l-6.9,3.5l-2.2-3.5l-5.2-0.4c0,0-0.4,3.9-1.7,3.9c-1.3,0-3.9-6.1-6.9-6.9c-3-0.9-9.1-0.9-9.1-0.9
							s-6.9,9.1-6.9,10.8s0.4,6.5,0.4,6.5s-7.4,2.2-6.1,4.3c1.3,2.2,0.9,8.7,0.9,8.7h-6.1l-2.2,4.3l0.4,5.2l-1.7,0.4l-2.6,5.2l3,8.2
							l7.4,3.5l2.2,4.8l-5.6-3.5c0,0-3-3.5-3.9-2.2c-0.9,1.3-6.1,3.5-6.1,3.5l-3.9-0.9l-2.2,2.6l2.2,8.2l3.9,3.9l2.2,3.5
							c0,0-4.8,0.9-6.9-1.3c-2.2-2.2-4.3-10.8-4.3-10.8s-1.3-3-3-3s-3.9,3-3.9,3l2.2,8.7c0,0,2.2,9.1,3.5,10c1.3,0.9,3.5,5.6,3.5,5.6
							s-1.7,3.9-3,2.6c-1.3-1.3-1.7-3.5-3.9-3.9c-2.2-0.4-5.2,8.7-6.1,10.8c-0.9,2.2,3.5,8.7,3.5,8.7l7.8-2.6l-1.3,4.8l1.3,3.5
							c0,0,5.6-0.4,4.3,0.4c-1.3,0.9-4.3,2.2-6.1,4.3c-1.7,2.2-3,6.5-3,6.5l4.8,2.2l-0.4,1.7l-4.3-1.3l-2.6,3.9l5.2,4.3l-1.3,2.6
							c0,0-3-2.2-5.2-1.3c-2.2,0.9-5.6,9.5-5.6,9.5l0.9,3.9c0,0-2.6,2.6-4.8,2.2c-2.2-0.4-6.9-0.9-8.2-0.4c-1.3,0.4-4.8,7.8-4.8,7.8
							s6.9,2.6,8.7,1.3c1.7-1.3,10.8-2.2,10.8-2.2L85,163c0,0,0.4,3.5-3,3.5s-6.5,0.9-6.5,0.9l-3.9-0.9c0,0,3,4.8,4.3,6.1
							c1.3,1.3,4.3,2.2,5.6,3c1.3,0.9,6.5,3.5,6.5,3.5l10-7.8h3.9c0,0-3.5,0.9-3.5,2.2c0,1.3,2.2,5.2-0.9,5.2c-3,0-3,0-3,0
							s-5.2,1.7-3,2.2c2.2,0.4,7.8,0.4,5.2,2.2c-2.6,1.7-6.5,4.3-7.4,5.6c-0.9,1.3,0,6.1-0.4,9.1c-0.4,3-1.7,12.1-3.9,17.3
							c-2.2,5.2-4.3,18.6-4.3,18.6l3.5,6.9l-1.7,3.5c0,0-6.1,22.5-6.9,23.8c-0.9,1.3-1.3,5.6-2.6,7.4c-1.3,1.7-4.3,3-1.7,3.9
							c2.6,0.9,5.6,2.2,6.9,1.7c1.3-0.4,6.1-5.6,6.1-5.6s0-4.8,1.7-6.5c1.7-1.7,5.2-10,5.6-13.9c0.4-3.9,0.4-5.6,1.3-7.4
							c0.9-1.7,4.3-6.9,4.3-6.9s-6.5-11.3-6.1-13.4c0.4-2.2,0.9-6.5,3.9-6.9c3-0.4,3,2.6,3,5.2c0,2.6,0.9,10,2.2,9.5
							c1.3-0.4,3.9,0.4,3.9,0.4l5.6,8.2l3.5-3.5c0,0-5.2-4.3-2.6-4.3c2.6,0,5.6-3,5.6-3s2.2-8.2,2.2-10c0-1.7,1.7-7.8,1.7-7.8l4.8,0.4
							c0,0-3.9,7.8-1.7,8.7c2.2,0.9,10.4,7.4,10.4,7.4s-6.1-2.2-8.7-1.3c-2.6,0.9-8.2,15.2-8.2,16.5c0,1.3,1.7,9.1,3,10.4
							c1.3,1.3,5.2,1.7,5.2,1.7s2.6,4.8,3,8.2c0.4,3.5,1.7,6.1-3,9.1c-4.8,3-6.1,2.2-5.2,5.2c0.9,3-1.7,5.6-1.7,5.6s-12.1,16-12.6,17.8
							c-0.4,1.7-1.7,5.2-1.7,5.2s-0.9-2.6-2.6-1.3c-1.7,1.3-3.5,6.5-1.7,10c1.7,3.5,6.1,9.5,6.5,10.8c0.4,1.3,3,11.7,3,11.7l5.2-1.7
							c0,0-3-5.6-2.2-7.4c0.9-1.7,4.3-5.6,4.3-5.6s6.9,3,8.2,4.8c1.3,1.7,7.4,5.6,7.4,5.6s2.2,2.2,3.5,2.2c1.3,0,1.3-0.4,2.6-3.5
							c1.3-3,0.4-10,0.4-10l7.8,2.6c0,0,1.7,2.2,3,2.6c1.3,0.4,3.5,2.2,7.4,2.2c3.9,0,15.6-7.4,18.2-8.7c2.6-1.3,4.3-3,4.3-3l1.3-3.9
							c0,0,3.5,0,5.6,0c2.2,0,7.8,0.4,9.1,0.9c1.3,0.4,9.1-3,3.9,0.9c-5.2,3.9-13.9,8.2-13.9,8.2s-1.7,8.2-4.3,10.8
							c-2.6,2.6-3.5,7.4-3.5,7.4l-3,9.1c0,0,6.1,14.3,7.8,16.9c1.7,2.6,8.2,11.3,8.2,11.3l3.5,0.4c0,0-5.6,6.5-5.6,7.8
							c0,1.3,4.8,7.4,5.6,5.6c0.9-1.7,1.7-3,3.9-4.8c2.2-1.7,6.9-4.8,6.9-4.8s4.3-2.2,6.5-2.6c2.2-0.4,6.1,0,4.3,3.5
							c-1.7,3.5-5.6,7.8-5.6,9.1c0,1.3,0.9,6.9,0.9,6.9l-4.8,4.8c0,0-3.9-1.7-3.9,0c0,1.7-3.5,11.7-0.4,12.6c3,0.9,12.6,0,12.6,0
							s-6.1,3-7.8,3.9c-1.7,0.9-10,9.1-8.2,12.1c1.7,3,7.8,11.7,7.8,11.7s5.2,6.9,7.4,7.8c2.2,0.9,2.2,0.9,2.2,0.9s-7.4,0.9-10.4-2.2
							c-3-3-8.2-8.2-8.2-8.2s-3.5,1.3-3.5,3c0,1.7,4.8,10.4,4.8,10.4s-6.1-5.6-12.6-5.6c-6.5,0-13.9,3.5-17.8,2.6
							c-3.9-0.9-11.7-0.4-11.7-0.4s-8.2,5.6-9.5,6.9c-1.3,1.3-3,2.2-7.8,4.8c-4.8,2.6-8.7,10.8-13,14.3c-4.3,3.5-11.3,5.2-13.9,7.4
							c-2.6,2.2-4.3,11.3-4.3,11.3s7.4,0.9,13-1.7c5.6-2.6,10-4.8,13.9-6.1c3.9-1.3,13-3.9,13-3.9s-2.2,8.2-3.5,10.4
							c-1.3,2.2,0,5.6,0,5.6l3.9,1.3l-3.9,4.8l-0.4,8.2l5.2,1.7c0,0-4.8,10-6.1,10.4c-1.3,0.4-3,6.5-4.3,7.8c-1.3,1.3-0.4,0-1.3,1.3
							c-0.9,1.3-4.8,6.9-9.5,8.7c-4.8,1.7-14.7,6.9-18.2,8.2c-3.5,1.3-5.2,3.9-8.2,5.6c-3,1.7-6.1,1.7-8.7,1.7c-2.6,0-4.3-0.9-4.3-0.9
							l-1.7,3c0,0-4.8,1.3-7.4,3c-2.6,1.7-6.1,4.3-6.1,4.3l10.4-0.4l5.2,6.1c0,0-5.2,5.6-6.1,6.9c-0.9,1.3,0.9,5.6,0.9,5.6l6.9-1.3v1.7
							l-3.5,3l6.9,3.9c0,0,6.1-3.5,9.5-3.9c3.5-0.4,6.5-2.6,6.5-2.6s-0.9-3,0.9-3.5c1.7-0.4,3-0.4,4.3-0.4c1.3,0,8.7-5.2,8.7-5.2
							l3.9,1.3c0,0-2.2,8.7,0.4,9.1c2.6,0.4,0.4-0.4,2.6,0.4c2.2,0.9,4.3,2.6,4.3,2.6l-3.9,1.3v4.8c0,0,4.8,2.2,6.5,1.7
							c1.7-0.4,11.3-0.9,14.3-3.9l4.3,7.4l8.2,4.8l5.6-0.4c0,0-3.9,3.5-1.7,4.3c2.2,0.9,10,3.9,15.2,2.2c5.2-1.7,8.2-3.5,7.8-4.8
							c-0.4-1.3-0.4-3.9-0.4-3.9l3.9-1.3c0,0,12.6-3.5,14.7-4.8c2.2-1.3,3.9-3.9,6.5-4.8c2.6-0.9,9.5-7.4,9.5-7.4s-6.5,8.7-7.8,9.5
							c-1.3,0.9-6.9,6.1-7.8,8.7c-0.9,2.6-7.8,5.6-7.8,5.6l-8.2,8.2l1.7,6.1l-16,6.9c0,0-6.9-6.9-11.7-7.4c-4.8-0.4-8.2-3-9.5-1.7
							c-1.3,1.3-1.3,1.3-1.3,1.3s-12.1,2.6-17.3,3.5c-5.2,0.9-5.2,0.9-5.2,0.9l-5.2,3l1.3,8.2c0,0-10.8,2.2-13.4,2.6
							c-2.6,0.4-6.9,9.1-8.7,12.1c-1.7,3-2.6,0.9-1.7,3c0.9,2.2,1.3,5.6-1.3,8.7c-2.6,3-6.1,6.5-7.8,8.7c-1.7,2.2-4.3,0-5.6,1.3
							c-1.3,1.3-2.2,0.4-6.1,3.9c-3.9,3.5-3,7.8-4.3,10c-1.3,2.2-5.2,5.2-6.9,7.8c-1.7,2.6-6.9,7.8-12.1,9.5c-5.2,1.7-8.7,1.3-12.1,2.6
							c-3.5,1.3-7.8,4.3-9.1,8.2c-1.3,3.9-0.4,6.9,2.2,7.8c2.6,0.9,5.2,1.7,7.4-0.9c2.2-2.6,5.6-7.4,7.8-6.1c2.2,1.3,6.9,0.9,7.4,4.8
							c0.4,3.9,0,9.5,1.7,9.5c1.7,0,10.8-3.9,10.8-3.9s2.6-3,0.9-3.9c-1.7-0.9-2.6-5.6-2.6-5.6l1.7-3l3.9,0.9c0,0,6.5-3,5.2-3
							c-1.3,0,4.8-3.9,4.8-3.9s2.2-8.2,4.3-8.7c2.2-0.4,4.8,2.2,9.5,2.6c4.8,0.4,9.1-1.3,14.7-1.3c5.6,0,5.6,0,5.6,0l0,0
							c0,0,8.2,4.3,9.5,4.8c1.3,0.4,3.9-0.4,3.9-0.4s2.2,3.5,2.6,4.8c0.4,1.3,6.1,3.5,7.4,3.5c1.3,0,6.5-2.6,6.1-3.9
							c-0.4-1.3,0-3.5,0-3.5l11.3-6.1l-0.4-3c0,0-3,0.9-1.3-2.2c1.7-3,0-14.7,5.6-16.5c5.6-1.7,10.4-5.2,12.6-5.6
							c2.2-0.4,11.7-3.5,19.5-0.9c7.8,2.6,14.7,5.6,15.6,9.1c0.9,3.5,2.6,6.9,2.6,6.9h3.9c0,0,0-8.7,2.6-7.4c2.6,1.3,11.3,2.2,17.8,0.9
							c6.5-1.3,4.3-6.9,4.3-6.9s-2.6-3,0.9-2.6c3.5,0.4,11.7-5.2,17.8-3.9c6.1,1.3,12.6-0.4,14.3-1.3c1.7-0.9,0-5.2,4.8-5.6
							c4.8-0.4,7.8-3,10.4,0.4c2.6,3.5,9.1,9.1,12.1,9.5c3,0.4,5.2-2.6,8.7-3.5c3.5-0.9,20.8-1.7,25.1-1.3c4.3,0.4,11.7,0,16.9,3.9
							c5.2,3.9,3.9,2.6,11.3,0c7.4-2.6,8.2-0.9,11.3-4.3c3-3.5,11.3-14.3,16-13c4.8,1.3,8.2,6.9,10.8,4.3c2.6-2.6,6.5-12.1,9.1-13
							c2.6-0.9,10.8-4.3,12.1-6.5c1.3-2.2,2.2-10.4,3-12.1c0.9-1.7,0-4.3,0.9-5.6c0.9-1.3,0.9-1.3,0.9-1.3l-1.7-4.3l-7.8,0.4
							c0,0-12.1-0.9-14.7-0.4c-2.6,0.4-3.5,1.3-7.8,2.2c-4.3,0.9-8.2-2.6-8.2-2.6s4.8-2.6,3.5-3.5c-1.3-0.9-6.1-3-7.4-1.7
							c-1.3,1.3-8.7,5.6-11.3,3.5c-2.6-2.2-3.9-3.9-3.9-3.9s-6.9,0.4-9.1-0.9c-2.2-1.3-3.5-3-3.5-3l5.2,0.9h8.7c0,0,5.2,2.2,6.5,2.2
							c1.3,0,6.5-1.3,10-2.6c3.5-1.3,6.1-5.2,7.4-7.4c1.3-2.2-2.2-3,0.9-3.9c3-0.9,5.2-0.9,5.2-0.9l3-8.2l-9.5,0.9l6.9-3.5
							c0,0,0.9-3.9,3.5-2.6c2.6,1.3,0.4,4.8,6.1,1.7c5.6-3,4.8-13.4,6.5-14.3c1.7-0.9,0,2.6,5.6,0.4c5.6-2.2,11.7-11.3,14.3-15.2
							c2.6-3.9,6.9-18.2,6.9-26c0-7.8-1.7-19.1-2.2-22.5c-0.4-3.5-6.1-10.8-10.4-14.7c-4.3-3.9-10-7.8-13.9-7.8c-3.9,0-13-0.4-18.2-0.4
							c-5.2,0-13.9-0.4-16,0.9c-2.2,1.3-8.7,1.7-8.2,6.1c0.4,4.3-1.7,9.1-1.7,9.1s-6.9-1.3-10-3.9c-3-2.6-6.1-5.2-6.1-5.2l-4.3,1.3
							c0,0,1.7-5.2,4.3-8.7c2.6-3.5,12.6-13.9,13.4-15.2c0.9-1.3-1.7-7.4-3-12.6c-1.3-5.2-7.4-13.9-10-17.8c-2.6-3.9-15.2-7.8-21.7-10.8
							c-6.5-3-4.8-6.1-10.4-5.2c-5.6,0.9-7.8,0-9.1-1.3c-1.3-1.3-2.6-4.8-2.6-4.8l7.8,3l10.8-1.7c0,0,10.4,7.8,13,7.8
							c2.6,0,6.9-0.4,8.2,0.4c1.3,0.9,4.8,3.9,4.8,3.9s0-6.5-0.9-8.7c-0.9-2.2-12.1-20.8-12.1-20.8s-3.5-6.5,0.9-14.7
							c0,0-4.3-1.7-6.1-4.3c-1.7-2.6-5.6-7.4-6.5-8.7c-0.9-1.3-5.4-14.6-10.4-16.5c-5.6-2.2-11.7-6.5-14.7-7.8c-3-1.3-4.3-4.3-12.6-3
							c-8.2,1.3-5.2-1.7-5.2-1.7s2.6-4.3,0.4-12.1c-2.2-7.8-5.6-9.5-5.2-13c0.4-3.5-6.1-18.2-9.1-29.1c-3-10.8-0.9-17.8-3.5-22.5
							c-2.6-4.8-9.5-17.8-12.1-20.4c-2.6-2.6-6.1-0.9-6.9-3.9c-0.9-3,0.4-10-2.6-12.1c-3-2.2-5.2-4.3-6.9-4.8c-1.7-0.4-8.2,3-11.7,0
							c-3.5-3-6.5-6.9-9.5-7.4c-3-0.4-10,4.8-14.7,6.1c-4.8,1.3-12.1,0-14.7,0c-2.6,0-5.6-0.9-8.7-1.3c-3-0.4-9.5-0.4-9.5-0.4l-5.6-5.2
							c0,0,11.7,0.9,19.1-1.7c7.4-2.6,13.9-7.8,16.9-6.5c3,1.3,9.5-0.4,11.7-2.2c2.2-1.7,7.8-3,7.4-6.1c-0.4-3-7.4-6.1-7.4-6.1
							s2.2-5.6-0.9-6.5c-3-0.9-7.8-1.7-8.7-0.4c-0.9,1.3-15.6,6.1-15.6,6.1s7.4-9.5,10.8-10c3.5-0.4,7.4-0.9,11.7-1.3
							c4.3-0.4,10.8,0.9,13.4-5.2c2.6-6.1,2.2-11.7,4.3-13.4c2.2-1.7,10-13,13.9-19.9c3.9-6.9,8.2-15.2,9.1-19.9
							c0.9-4.8,2.6-10.4,5.2-12.6c2.6-2.2,8.2-11.3,9.5-16c1.3-4.8,1.3-5.2-2.2-6.9c-3.5-1.7-1.3,0-5.2-3.5c-3.9-3.5,2.2-7.4-11.3-6.1
							c-13.4,1.3-23.8,0-34.3,0.4c-10.4,0.4-14.7,2.6-20.4,0c-5.6-2.6-6.9-5.6-12.1-2.6c-5.2,3-7.4,4.8-9.1,4.8c-1.7,0-7.4,0.4-9.5,2.2
							c-2.2,1.7-6.1,3.5-9.1,7.4c-3,3.9-10.8,9.1-13.4,14.7c-2.6,5.6-7.4,12.6-7.4,12.6s-3-0.9-2.2-2.2c0.9-1.3,6.1-9.5,8.2-13.9
							c2.2-4.3,1.7-3.5,4.8-7.4c3-3.9,10.4-13.4,10.4-13.4l-10,2.6c0,0,6.1-3,8.2-5.2c2.2-2.2,7.4-3,10.4-5.6c3-2.6,4.8-8.2,4.8-8.2
							l-9.5,2.6l-11.7-3.5l3-3l6.1,1.3c0,0,11.3-11.7,13-13c1.7-1.3,3.9-3.9,6.5-5.2c2.6-1.3,14.7-10.8,20.8-16.5
							c6.1-5.6,9.1-10,10.4-13c1.3-3-0.4-6.1-0.4-8.2c0-2.2,3.5-6.5,3.5-6.5S221.2,5.2,218.2,3z"/>
						<path fill="#FFFFFF" d="M72.7,7.6c-2.7-0.6-6.6,3.5-9.4,6.3c-2.8,2.8-8.1,3.5-8.1,3.5s-5.1,5.8-7.8,6.3c-2.8,0.5-5.6,4.8-5.6,4.8
							l-2,4l-4-4.3L29.5,39c0,0,3.3,3.5,2.3,5.1c-1,1.5-2,7.3-1.3,9.1c0.8,1.8,3.3,3.8,3.3,3.8l-4.6,3.3V63l-3.5,0.8l8.6,11.6l7.6-8.3
							c0,0,2-5.8,3.3-6.3c1.3-0.5,2-1.8,2-1.8l0.3-6.1l3.3-1.5v5.3c0,0,4,0.3,5.8-1.5c1.8-1.8,1.8-5.3,3.3-5.8c1.5-0.5,2-1.5,2-1.5
							l1.3-4.8l-4-0.8c0,0,1.8-1,3.3-2.5c1.5-1.5,3.5-3,3.5-3l5.1-0.8l6.6-5.6l-0.3-1l-6.1,0.3c0,0,4.6-5.6,5.8-9.9
							C78.3,15.5,77,8.6,72.7,7.6z"/>
						<path fill="#FFFFFF" d="M20.8,73.1c-2-0.5-6.6-1.3-8.6,0.3c-2,1.5-5.8,2.8-5.3,4.8c0.5,2,0,4.6,2,4.8c2,0.3,3.8-0.8,4.3,0.3
							c0.5,1,2.3,4.6,2.3,4.6l-3,2.8c0,0,0,7.1,0,7.8c0,0.8,3.3,2,3.3,2s3-1.8,3.3-3c0.3-1.3-1-3.3-1-4.3c0-1,0.3-3.5,0.8-4.3
							c0.5-0.8,2.3,0,3-1c0.8-1,1.3-2.3,1.3-2.3s-2-2.5-1.5-3.3c0.5-0.8,3.8-2.5,4.3-3.3C26.3,78.1,22.8,73.6,20.8,73.1z"/>
						<path fill="#FFFFFF" d="M64.2,77.2c0,0-5.3,0.3-5.8,2c-0.5,1.8,0.3,3.8-0.8,4.3c-1,0.5-0.3,3.5-0.3,3.5s3.5,1.8,3.5,3
							s-1.8,3.8-2.5,3.3c-0.8-0.5-4.6-0.5-5.3-2.8c-0.8-2.3-2.8-4-2.8-4L48,86.8c0,0,0,2.8,0,3.5c0,0.8,0.3,3.5,0.3,3.5L44,91.1
							l-5.6,3.3l4.3,4.5l-1.5,5.1c0,0,2.5,0.8,4.8,1.3c2.3,0.5,6.1-0.8,6.1-0.8s1,7.6,2.3,8.3c1.3,0.8,2.3,1.3,2.3,1.3s1.8,7.8,3.3,8.1
							c1.5,0.3,2.5,0,3.5,0c1,0,3.8,0.8,5.1,1c1.3,0.3,3.8,0.3,3.8,0.3l1-2l2.8,1.8l-0.8,5.8l-2.5,2.5l1,4.5c0,0,4.6-3.3,5.3-4
							c0.8-0.8,3-4,3-4l2.5,0.3l2.5-6.8l3-3.5l-4.3-4c0,0-1.5,1.8-4,1c-2.5-0.8-7.6-4.5-7.6-4.5l1.3-12.6l-2.8-0.5l-1.5,6.8l-2-2.3
							c0,0-0.5-8.1,0.3-10.1c0.8-2-1-3,0.5-3.5c1.5-0.5,1.5-0.5,1.5-0.5s-1.8-3.3-3-5.8C67.3,79.2,64.2,77.2,64.2,77.2z"/>
						<path fill="#FFFFFF" d="M9.6,96.7l-2,1.8c0,0-0.8,7.3-1,8.6c-0.3,1.3,0.8,5.6,0.8,7.1c0,1.5-0.3,7.8-0.3,7.8s4.3,2.3,6.6,1.3
							c2.3-1,2.8-4,2.8-4l-3.3-2.8l1.3-4.8c0,0-2.5-1.3-1.5-2.3c1-1,2.8-2.8,2.8-2.8l-3.3-4l-1-1.8L9.6,96.7z"/>
						<path fill="#FFFFFF" d="M5.8,126.1L1,125.8c0,0-1.5,5.6-0.8,7.6c0.8,2,5.3,2.5,5.8,1C6.5,132.9,5.8,126.1,5.8,126.1z"/>
						<path fill="#FFFFFF" d="M52.5,126.1c0,0-7.8-3.3-10.1,1c-0.6,1.1,1,1,1,1l2,2c0,0,1.8-0.5,2.5-1.3c0.8-0.8,1.8-1.3,2.5-1.3
							C51.3,127.6,52.5,126.1,52.5,126.1z"/>
						<path fill="#FFFFFF" d="M61.6,131.4l-7.3,1l-1.3,3.3c0,0,3,2.5,4.3,3c1.3,0.5,3.5,1.3,3.5,1.3L61.6,131.4z"/>
						<path fill="#FFFFFF" d="M69.7,144c0,0-0.5-3-2-2.5c-1.5,0.5-3,1.5-3,1.5l-2,3.3c0,0,3.3,4.3,4.3,3.5c1-0.8,1.5-3.8,1.5-3.8
							L69.7,144z"/>
						<path fill="#FFFFFF" d="M50.3,162.2c0,0-4-1.5-5.6-1.5c-1.5,0-5.8,1.3-5.8,2.5c0,1.3-0.8,3.3-0.8,3.3s-0.8,1.3-0.3,2
							c0.5,0.8-0.5,3.3,3,2.8c3.5-0.5,7.1-4.5,7.8-5.6C49.5,164.7,50.3,162.2,50.3,162.2z"/>
						<path fill="#FFFFFF" d="M33.6,170.6c0,0-2.8-0.3-4.3-0.3c-1.5,0-5.8,1.8-7.6,3.3c-1.8,1.5-1,3.5-0.3,4.8c0.8,1.3,1,2.8,3.8,1.8
							c2.8-1,5.6-2.5,7.6-4.8C34.8,173.1,34.3,171.1,33.6,170.6z"/>
						<path fill="#FFFFFF" d="M63.4,166.3l-9.4,2.8c0,0,0.8,4,1,5.3c0.3,1.3,1.5,3.3,2.8,3.5c1.3,0.3,6.6-0.5,5.6,2c-1,2.5-2,4.6-2,4.6
							l1.5,2l-3.8,1.5c0,0-3.3-1.3-4-1c-0.8,0.3-3.3,2.3-3.3,2.3s1.8,4.3,2,5.3c0.3,1,2,2.5,3,2.5c1,0,6.3-1.3,8.3-1.3
							c2,0,3.3-0.5,5.1-2c1.8-1.5,4.3-0.8,5.8-1.3c1.5-0.5,6.1-0.8,7.6-2.3c1.5-1.5,2.3-6.1,2.3-6.1s-1.5-3.5-5.1-5.8
							c-3.5-2.3-7.8-3.5-7.8-3.5S64.7,166.3,63.4,166.3z"/>
						<path fill="#FFFFFF" d="M61.6,212l-4.6-3.3c0,0-4.8,5.6-3.8,6.6c1,1,1,1.3,2,1.3C56.3,216.6,61.6,212,61.6,212z"/>
						<path fill="#FFFFFF" d="M83.1,209.5l-2-3c0,0-9.1,6.6-12.1,9.6c-3,3-5.3,7.1-5.3,7.1s-3.5,0-5.3,1.5c-1.8,1.5-5.3,5.1-6.8,4.6
							c-1.5-0.5-4-0.5-4-0.5l-6.6,2.8c0,0-1.5,6.6-2.8,8.3c-1.3,1.8-1.8,4.5-1.8,4.5l4.5,2.3l8.8-5.1l2,1.5c0,0-3.3,5.1-3.8,5.8
							c-0.5,0.8,1,4.3,1,4.3s4.5-1,5.6-1.5c1-0.5,2.3-3.3,3-2.8c0.8,0.5,5.3-1.8,6.1-3.3c0.8-1.5,1-3.8,1-3.8l-2.3-2l0.8-2.8l6.6-3
							l0.8-6.6c0,0,6.6-10.1,8.8-12.6C81.6,212.3,83.1,209.5,83.1,209.5z"/>
						<path fill="#FFFFFF" d="M103.6,245.4l-3.5-0.8l-1,5.6l-1.5,2.8l-1.8,3.5l-1.5,2l-1.3,0.8l-0.3,2.8l1.5,2c0,0,0.3,3.8,0.8,5.1
							c0.5,1.3,3,1.5,4,2.3c1,0.8,3.5,3.3,4.6,3c1-0.3,4.3-1.5,4.8-6.3c0.5-4.8,1.8-7.6,1.8-7.6s0.5-2.8-1-3.8c-1.5-1-3.3-4-3.8-6.3
							C104.9,248.2,103.6,245.4,103.6,245.4z"/>
						<path fill="#FFFFFF" d="M139.2,447.1c-0.3-1-1-2.3-3.5-2c-2.5,0.3-6.8,1.3-8.3,1.3c-1.5,0-3.8,1.3-3.8,1.3s-1,4.3-1,5.3
							c0,1-0.3,2.5-0.3,2.5l-2.8-2h-3.3l4,7.6l4.6,1l3,7.3l3.3,2.3c0,0,6.8-7.6,7.6-7.8c0.8-0.3,3.8-1.3,3.8-1.3l1-2l5.1-3.3
							c0,0-0.5-2.8-3.8-3.5c-3.3-0.8-5.6-1.5-5.6-2.5C139.2,450.1,139.2,447.1,139.2,447.1z"/>
					</g>
				</g>
				<g id="Layer_1">

					<g id="Marker_Datacentres" class="datacentre-map__marker-group datacentre-map__marker-group js-marker-group" data-marker-group="datacentres">

						<g id="Edinburgh" class="datacentre-map__marker datacentre-map__marker--datacentres js-marker" data-heading="Edinburgh" data-address="7 Bankhead Medway, Sighthill Industrial Estate, Edinburgh, EH11 4BY" data-link="/services/colocation/edinburgh-datacentre/">
							<circle cx="203.3" cy="232.7" r="8.2" fill="transparent"/>
							<g>
								<path fill="#1D9FDA" d="M203.3,226.9c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
									C197.5,229.5,200.1,226.9,203.3,226.9 M203.3,224.5c-4.5,0-8.2,3.7-8.2,8.2s3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2
									S207.8,224.5,203.3,224.5L203.3,224.5z"/>
							</g>
							<circle fill="#1D9FDA" cx="203.3" cy="232.7" r="2.7"/>
						</g>
            
            			<g id="Milton_Keynes" class="datacentre-map__marker datacentre-map__marker--datacentres js-marker" data-heading="Milton Keynes" data-address="St Neots House, Rockingham Drive, Linford Wood, Milton Keynes, MK14 6LY" data-link="/services/colocation/milton-keynes-datacentre/">
							<circle cx="303" cy="522.6" r="8.2" fill="transparent"/>
							<g>
								<path fill="#1D9FDA" d="M303,516.7c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
									C297.1,519.3,299.7,516.7,303,516.7 M303,514.4c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2
									C311.2,518,307.5,514.4,303,514.4L303,514.4z"/>
							</g>
							<circle fill="#1D9FDA" cx="303" cy="522.6" r="2.7"/>
						</g>

						<g id="Glasgow_North" class="datacentre-map__marker datacentre-map__marker--datacentres js-marker" data-heading="Glasgow North" data-address="Unit 3, 24 Finlas Street, Cowlairs Industrial Estate, Glasgow, G22 5DT" data-link="/services/colocation/glasgow-north-datacentre/">
							<circle cx="155.3" cy="237.8" r="8.2" fill="transparent"/>
							<g>
								<path fill="#1D9FDA" d="M155.3,232c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8s-5.8-2.6-5.8-5.8
									C149.5,234.6,152.1,232,155.3,232 M155.3,229.6c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2
									C163.5,233.3,159.9,229.6,155.3,229.6L155.3,229.6z"/>
							</g>
							<circle fill="#1D9FDA" cx="155.3" cy="237.8" r="2.7"/>
						</g>

						<g id="Newcastle_East" class="datacentre-map__marker datacentre-map__marker--datacentres js-marker" data-heading="Newcastle East" data-address="New York Way, North Shields, Tyne and Wear, NE27 0QF" data-link="/services/colocation/newcastle-east-datacentre/">
							<circle cx="277.9" cy="290.8" r="8.2" fill="transparent"/>
							<g>
								<path fill="#1D9FDA" d="M277.9,285c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
									C272,287.6,274.6,285,277.9,285 M277.9,282.7c-4.5,0-8.2,3.7-8.2,8.2s3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2S282.4,282.7,277.9,282.7
									L277.9,282.7z"/>
							</g>
							<circle fill="#1D9FDA" cx="277.9" cy="290.8" r="2.7"/>
						</g>

						<g id="Newcastle_Central" class="datacentre-map__marker datacentre-map__marker--datacentres js-marker" data-heading="Newcastle Central" data-address="5 Bridge View, Stepney Lane, Newcastle Upon Tyne, NE1 6PN" data-link="/services/colocation/newcastle-central-datacentre/">
							<circle cx="267.3" cy="306.2" r="8.2" fill="transparent"/>
							<g>
								<path fill="#1D9FDA" d="M267.3,300.3c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
									C261.4,302.9,264,300.3,267.3,300.3 M267.3,298c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2
									C275.5,301.6,271.8,298,267.3,298L267.3,298z"/>
							</g>
							<circle fill="#1D9FDA" cx="267.3" cy="306.2" r="2.7"/>
						</g>
						<g id="Edinburgh_New_Bridge" class="datacentre-map__marker datacentre-map__marker--datacentres js-marker" data-heading="Edinburgh New Bridge" data-address="7 Claylands Roads, Edinburgh, EH28 8LF" data-link="/services/colocation/newbridge-datacentre/">
							<circle cx="187.7" cy="244.5" r="8.2" fill="transparent"/>
							<g>
								<path fill="#1D9FDA" d="M187.7,238.7c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
									C181.8,241.3,184.5,238.7,187.7,238.7 M187.7,236.4c-4.5,0-8.2,3.7-8.2,8.2s3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2
									S192.2,236.4,187.7,236.4L187.7,236.4z"/>
							</g>
							<circle fill="#1D9FDA" cx="187.7" cy="244.5" r="2.7"/>
						</g>
						<g id="Edinburgh_South_Gyle" class="datacentre-map__marker datacentre-map__marker--datacentres js-marker" data-heading="Edinburgh South Gyle" data-address="Sirius House, The Clocktower Estate, Flassches Yard, South Gyle Crescent, Edinburgh, EH12 9LB" data-link="/services/colocation/south-gyle-datacentre/">
							<circle cx="206.6" cy="251.3" r="8.2" fill="transparent"/>
							<g>
								<path fill="#1D9FDA" d="M206.6,245.4c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
									C200.8,248,203.4,245.4,206.6,245.4 M206.6,243.1c-4.5,0-8.2,3.7-8.2,8.2s3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2
									S211.2,243.1,206.6,243.1L206.6,243.1z"/>
							</g>
							<circle fill="#1D9FDA" cx="206.6" cy="251.3" r="2.7"/>
						</g>
						<g id="Maidenhead" class="datacentre-map__marker datacentre-map__marker--datacentres js-marker" data-heading="Maidenhead" data-address="Bluesquare House, Priors Way, Maidenhead, SL6 2HP" data-link="/services/colocation/maidenhead-datacentre/">
							<circle cx="301" cy="599.6" r="8.2" fill="transparent"/>
							<g>
								<path fill="#1D9FDA" d="M301,593.7c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
									C295.1,596.3,297.7,593.7,301,593.7 M301,591.4c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2
									C309.2,595,305.5,591.4,301,591.4L301,591.4z"/>
							</g>
							<circle fill="#1D9FDA" cx="301" cy="599.6" r="2.7"/>
						</g>
						<g id="South_London" class="datacentre-map__marker datacentre-map__marker--datacentres js-marker" data-heading="South London" data-address="Unit 1, 35 Imperial Way, Croydon, London, CR0 4RR" data-link="/services/colocation/south-london-datacentre/">
							<circle  cx="328" cy="629" r="8.2" fill="transparent"/>
							<g>
								<path fill="#1D9FDA" d="M328,623.2c3.2,0,5.8,2.6,5.8,5.8s-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8S324.8,623.2,328,623.2
									 M328,620.8c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2c4.5,0,8.2-3.7,8.2-8.2C336.2,624.5,332.6,620.8,328,620.8L328,620.8z
									"/>
							</g>
							<circle fill="#1D9FDA" cx="328" cy="629" r="2.7"/>
						</g>
						<g id="Reading_East" class="datacentre-map__marker datacentre-map__marker--datacentres js-marker" data-heading="Reading East" data-address="Sutton Business Park, Reading, Berkshire, RG6 1AZ" data-link="/services/colocation/reading-east-datacentre/">
							<circle cx="263.6" cy="618.7" r="8.2" fill="transparent"/>
							<g>
								<path fill="#1D9FDA" d="M263.6,612.9c3.2,0,5.8,2.6,5.8,5.8s-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8S260.4,612.9,263.6,612.9
									 M263.6,610.5c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2C271.8,614.2,268.1,610.5,263.6,610.5L263.6,610.5z
									"/>
							</g>
							<circle fill="#1D9FDA" cx="263.6" cy="618.7" r="2.7"/>
						</g>
						<g id="Sheffield" class="datacentre-map__marker datacentre-map__marker--datacentres js-marker" data-heading="Sheffield" data-address="Unit 1, Pioneer Close, Manvers, Rotherham, S63 7JZ" data-link="/services/colocation/sheffield-datacentre/">
							<circle cx="286" cy="442.6" r="8.2" fill="transparent"/>
							<g>
								<path fill="#1D9FDA" d="M286,436.7c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
									C280.1,439.3,282.7,436.7,286,436.7 M286,434.4c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2
									C294.2,438,290.5,434.4,286,434.4L286,434.4z"/>
							</g>
							<circle fill="#1D9FDA" cx="286" cy="442.6" r="2.7"/>
						</g>
					</g>

					<g id="Marker_Workplace_Recovery" class="datacentre-map__marker-group js-marker-group is-hidden" data-marker-group="workplace-recovery">
						<g id="Glasgow_West" class="datacentre-map__marker datacentre-map__marker--workplace-recovery js-marker" data-heading="Glasgow West" data-address="Trident House, 175 Renfrew Road, Paisley, Renfrewshire, PA3 3EF" data-link="workplace-recovery">
							<circle fill="transparent" cx="140.3" cy="252.8" r="8.2"/>
							<g>
								<path fill="#009983" d="M140.3,247c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8s-5.8-2.6-5.8-5.8
									C134.5,249.6,137.1,247,140.3,247 M140.3,244.6c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2
									C148.5,248.3,144.9,244.6,140.3,244.6L140.3,244.6z"/>
							</g>
							<circle fill="#009983" cx="140.3" cy="252.8" r="2.7"/>
						</g>
						<g id="Edinburgh_South_Gyle" class="datacentre-map__marker datacentre-map__marker--workplace-recovery js-marker" data-heading="Edinburgh South Gyle" data-address="Sirius House, The Clocktower Estate, Flassches Yard, South Gyle Crescent, Edinburgh, EH12 9LB" data-link="workplace-recovery">
							<circle fill="transparent" cx="220.8" cy="239.2" r="8.2"/>
							<g>
								<path fill="#009983" d="M220.8,233.3c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
									C214.9,235.9,217.6,233.3,220.8,233.3 M220.8,231c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2c4.5,0,8.2-3.7,8.2-8.2
									C229,234.6,225.3,231,220.8,231L220.8,231z"/>
							</g>
							<circle fill="#009983" cx="220.8" cy="239.2" r="2.7"/>
						</g>
						<g id="Sunderland" class="datacentre-map__marker datacentre-map__marker--workplace-recovery js-marker" data-heading="Sunderland" data-address="North East Business and Innovation Centre, Wearfield, Enterprise Park East, Sunderland, SR5 2TA" data-link="workplace-recovery">
							<circle fill="transparent" cx="287.8" cy="329.7" r="8.2"/>
							<g>
								<path fill="#009983" d="M287.8,323.9c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
									C282,326.5,284.6,323.9,287.8,323.9 M287.8,321.5c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2c4.5,0,8.2-3.7,8.2-8.2
									C296,325.2,292.3,321.5,287.8,321.5L287.8,321.5z"/>
							</g>
							<circle fill="#009983" cx="287.8" cy="329.7" r="2.7"/>
						</g>
						<g id="Team_Valley" class="datacentre-map__marker datacentre-map__marker--workplace-recovery js-marker" data-heading="Team Valley" data-address="Colmet Court, Queens way South, Team Valley, Gateshead, NE11 0SD" data-link="workplace-recovery">
							<circle fill="transparent" cx="271.4" cy="337.9" r="8.2"/>
							<g>
								<path fill="#009983" d="M271.4,332.1c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
									C265.6,334.7,268.2,332.1,271.4,332.1 M271.4,329.7c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2c4.5,0,8.2-3.7,8.2-8.2
									C279.6,333.4,275.9,329.7,271.4,329.7L271.4,329.7z"/>
							</g>
							<circle fill="#009983" cx="271.4" cy="337.9" r="2.7"/>
						</g>
						<g id="Glasgow_North_2_" class="datacentre-map__marker datacentre-map__marker--workplace-recovery js-marker" data-heading="Glasgow North" data-address="Unit 3, 24 Finlas Street, Cowlairs Industrials Estate, Glasgow, G22 5DT" data-link="workplace-recovery">
							<circle fill="transparent" cx="155.3" cy="237.8" r="8.2"/>
							<g>
								<path fill="#009983" d="M155.3,232c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8s-5.8-2.6-5.8-5.8
									C149.5,234.6,152.1,232,155.3,232 M155.3,229.6c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2
									C163.5,233.3,159.9,229.6,155.3,229.6L155.3,229.6z"/>
							</g>
							<g>
								<circle fill="#009983" cx="155.3" cy="237.8" r="2.7"/>
							</g>
						</g>
						<g id="Stockton_On_Tees_1_" class="datacentre-map__marker datacentre-map__marker--workplace-recovery js-marker" data-heading="Stockton-on-Tees" data-address="9 Cheltnam Road, Portrack Interchange, Business Park, Stockton-on-tees, TS18 2AD" data-link="workplace-recovery">
							<circle fill="transparent" cx="298.7" cy="355.4" r="8.2"/>
							<g>
								<path fill="#009983" d="M298.7,349.6c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
									C292.9,352.2,295.5,349.6,298.7,349.6 M298.7,347.2c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2c4.5,0,8.2-3.7,8.2-8.2
									C306.9,350.9,303.3,347.2,298.7,347.2L298.7,347.2z"/>
							</g>
							<circle fill="#009983" cx="298.7" cy="355.4" r="2.7"/>
						</g>
						<g id="Sheffield_1_" class="datacentre-map__marker datacentre-map__marker--workplace-recovery js-marker" data-heading="Sheffield" data-address="Unit 1, Pioneer Close, Manvers, Rotheram, S63 7JZ" data-link="workplace-recovery">
							<circle fill="transparent" cx="286" cy="442.6" r="8.2"/>
							<g>
								<g>
									<path fill="#009983" d="M286,436.7c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
										C280.1,439.3,282.7,436.7,286,436.7 M286,434.4c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2
										C294.2,438,290.5,434.4,286,434.4L286,434.4z"/>
								</g>
							</g>
							<circle fill="#009983" cx="286" cy="442.6" r="2.7"/>
						</g>
						<g id="Newcastle_East_2_" class="datacentre-map__marker datacentre-map__marker--workplace-recovery js-marker" data-heading="Newcastle East" data-address="New York Way, North Shields, Tyne and Wear, NE27 0QF" data-link="workplace-recovery">
							<circle fill="transparent" cx="277.9" cy="290.8" r="8.2"/>
							<g>
								<g>
									<path fill="#009983" d="M277.9,285c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
										C272,287.6,274.6,285,277.9,285 M277.9,282.7c-4.5,0-8.2,3.7-8.2,8.2s3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2S282.4,282.7,277.9,282.7
										L277.9,282.7z"/>
								</g>
							</g>
							<circle fill="#009983" cx="277.9" cy="290.8" r="2.7"/>
						</g>
					</g>

					<g id="Marker_Offices" class="datacentre-map__marker-group js-marker-group is-hidden" data-marker-group="offices">
						<g id="Edinburgh_South_Gayle_1_" class="datacentre-map__marker datacentre-map__marker--offices js-marker" data-heading="Edinburgh South Gyle" data-address="Sirius House, The Clocktower Estate, Flassches Yard, South Gyle Crescent, Edinburgh, EH12 9LB" data-link="offices">
							<circle fill="transparent" cx="220.8" cy="239.2" r="8.2"/>
							<g>
								<path fill="#7057A1" d="M220.8,233.3c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
									C214.9,235.9,217.6,233.3,220.8,233.3 M220.8,231c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2
									C229,234.6,225.3,231,220.8,231L220.8,231z"/>
							</g>
							<circle fill="#7057A1" cx="220.8" cy="239.2" r="2.7"/>
						</g>
						<g id="Edinburgh_Head_Office" class="datacentre-map__marker datacentre-map__marker--offices js-marker" data-heading="Edinburgh Head Office" data-address="floor 6, Sugar Bond House, 2 Anderson Place, Edinburgh, EH6 5NP" data-link="offices">
							<circle fill="transparent" cx="194.8" cy="236.2" r="8.2"/>
							<g>
								<path fill="#7057A1" d="M194.8,230.3c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
									C188.9,232.9,191.6,230.3,194.8,230.3 M194.8,228c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2
									C203,231.6,199.3,228,194.8,228L194.8,228z"/>
							</g>
							<circle fill="#7057A1" cx="194.8" cy="236.2" r="2.7"/>
						</g>
						<g id="Stockton_On_Tees" class="datacentre-map__marker datacentre-map__marker--offices js-marker" data-heading="Stockton-on-Tees" data-address="Onyx House (Stockton), 9 Cheltenham Road, Portrack Interchange Business Park, TS18 2AD" data-link="offices">
							<circle fill="transparent" cx="298.7" cy="355.4" r="8.2"/>
							<g>
								<g>
									<path fill="#7057A1" d="M298.7,349.6c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8
										C292.9,352.2,295.5,349.6,298.7,349.6 M298.7,347.2c-4.5,0-8.2,3.7-8.2,8.2c0,4.5,3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2
										C306.9,350.9,303.3,347.2,298.7,347.2L298.7,347.2z"/>
								</g>
							</g>
							<circle fill="#7057A1" cx="298.7" cy="355.4" r="2.7"/>
						</g>
						<g id="London_Cannon_Street" class="datacentre-map__marker datacentre-map__marker--offices js-marker" data-heading="London" data-address="4th Floor, 33 Cannon Street, London, EC4M 5SB" data-link="offices">
							<circle fill="transparent" cx="321.6" cy="605.7" r="8.2"/>
							<g>
								<g>
									<path fill="#7057A1" d="M321.6,599.9c3.2,0,5.8,2.6,5.8,5.8s-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8S318.4,599.9,321.6,599.9
										 M321.6,597.5c-4.5,0-8.2,3.7-8.2,8.2s3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2S326.2,597.5,321.6,597.5L321.6,597.5z"/>
								</g>
							</g>
							<circle fill="#7057A1" cx="321.6" cy="605.7" r="2.7"/>
						</g>
						<g id="London_New_Broad_Street" class="datacentre-map__marker datacentre-map__marker--offices js-marker" data-heading="London New Broad" data-address="County House, 46 New Broad Street, London, EC2M 1JH" data-link="offices">
							<circle fill="transparent" cx="305.6" cy="614.7" r="8.2"/>
							<g>
								<g>
									<path fill="#7057A1" d="M305.6,608.9c3.2,0,5.8,2.6,5.8,5.8s-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8S302.4,608.9,305.6,608.9
										 M305.6,606.5c-4.5,0-8.2,3.7-8.2,8.2s3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2S310.2,606.5,305.6,606.5L305.6,606.5z"/>
								</g>
							</g>
							<circle fill="#7057A1" cx="305.6" cy="614.7" r="2.7"/>
						</g>
						<g id="Reading_Head_Office" class="datacentre-map__marker datacentre-map__marker--offices js-marker" data-heading="Reading Head Office" data-address="Cadogan House, Rose Kiln Lane, Reading, RG2 0HP" data-link="offices">
							<circle fill="transparent" cx="266.6" cy="610.7" r="8.2"/>
							<g>
								<g>
									<path fill="#7057A1" d="M266.6,604.9c3.2,0,5.8,2.6,5.8,5.8s-2.6,5.8-5.8,5.8c-3.2,0-5.8-2.6-5.8-5.8S263.4,604.9,266.6,604.9
										 M266.6,602.5c-4.5,0-8.2,3.7-8.2,8.2s3.7,8.2,8.2,8.2s8.2-3.7,8.2-8.2S271.2,602.5,266.6,602.5L266.6,602.5z"/>
								</g>
							</g>
							<circle fill="#7057A1" cx="266.6" cy="610.7" r="2.7"/>
						</g>
					</g>
				</g>
				</svg>
			</div>
		</div>
		<div class="datacentre-map__facility">
			<span class="datacentre-map__facility-header">Select a facility</span>
			<ul class="block-menu">
				<li class="block-menu__item is-selected js-button" data-marker-group-to-show="datacentres">
					<i class="target-circle"><i></i></i> 
					Datacentres
				</li>
				<li class="block-menu__item js-button" data-marker-group-to-show="workplace-recovery">
					<i class="target-circle target-circle--green"><i></i></i>
					Workplace Recovery
				</li>
				<li class="block-menu__item js-button" data-marker-group-to-show="offices">
					<i class="target-circle target-circle--purple"><i></i></i>
					Offices
				</li>
			</ul>

			<div class="datacentre-map__info js-info-box">
				<p class="datacentre-map__info-heading js-info-box-heading"></p>
				<p class="datacentre-map__info-address js-info-box-address"></p>
				<a href="#" class="datacentre-map__info-link js-info-box-link">View Datacentre</a>
			</div>
		</div>
	</div>
	<div class="hero-banner-mask js-hero-banner-mask"></div>
</section>

<section class="ptb-xl bgc-white inner-wrap-@-sm world-map js-world-map">
	<div class="section-wrap section-wrap--restricted world-map__wrap">

		<div class="world-map__box fs-s js-country-box is-hidden" data-country-box="hong-kong">
			<div class="close-cross js-close-cross">
				<i>
					<span class="close-cross__cross"></span>
				</i>
			</div>
			<h3 class="world-map__box-header fs-l">Hong Kong</h3>
			<p>
				Our Hong Kong colocation facilities are strategically situated in one of Asia’s busiest centers for international finance and trade. Four buildings totalling more than 20,000 square meters (215,000 square feet) of colocation space with connections to 55+ network service providers.
			</p>
			<a href="/wp-content/uploads/2017/10/Data-Sheet_HongKong.pdf" target="_blank">Download datasheet</a>
		</div>

		<div class="world-map__box fs-s js-country-box is-hidden" data-country-box="amsterdam">
			<div class="close-cross js-close-cross">
				<i>
					<span class="close-cross__cross"></span>
				</i>
			</div>
			<h3 class="world-map__box-header fs-l">Amsterdam</h3>
			<p>
				Over 80 + network carriers to choose from and connection to 80% of Europe within 50 milliseconds. Not only is our Amsterdam facility one of the most connected. It is also one of the most sustainable with underground Aquifer Thermal Energy Storage.
			</p>
			<a href="/wp-content/uploads/2017/10/Data-Sheet_Amsterdam.pdf" target="_blank">Download datasheet</a>
		</div>

		<div class="world-map__box fs-s js-country-box is-hidden" data-country-box="frankfurt">
			<div class="close-cross js-close-cross">
				<i>
					<span class="close-cross__cross"></span>
				</i>
			</div>
			<h3 class="world-map__box-header fs-l">Frankfurt</h3>
			<p>
				Situated at the heart of Europe, Frankfurt is a truly international metropolis and is regarded as the key telecommunications hub for the whole of Germany and is a real gateway to Eastern Europe. Situated in the southwest of the city less than 15 minutes drive away from Frankfurt Airport and the city centre. Certified to ISO 27001, ISO 9001, ISO 14001, OHSAS 18001. Compliant: PCI DSS and SSAE16
			</p>
			<a href="/wp-content/uploads/2017/10/Data-Sheet_Frankfurt.pdf" target="_blank">Download datasheet</a>
		</div>

		<div class="world-map__box fs-s js-country-box is-hidden" data-country-box="boston">
			<div class="close-cross js-close-cross">
				<i>
					<span class="close-cross__cross"></span>
				</i>
			</div>
			<h3 class="world-map__box-header fs-l">Boston</h3>
			<p>
				Internap datacentres located near downtown Boston offer plenty of room to grow, utilising superior modular designs to allow for enhanced flexibility and increased reliability. Data centre features offer enterprise-class infrastructure with carrier diversity, an energy efficient design, N+1 redundancy of generator and high power density.
			</p>
			<a href="/wp-content/uploads/2017/10/Data-Sheet_Boston.pdf" target="_blank">Download datasheet</a>
		</div>

		<div class="world-map__box fs-s js-country-box is-hidden" data-country-box="new-york">
			<div class="close-cross js-close-cross">
				<i>
					<span class="close-cross__cross"></span>
				</i>
			</div>
			<h3 class="world-map__box-header fs-l">New York</h3>
			<p>
				Telx’s flagship facility serves as the nerve centre for international communications and offers access to physical connection points to the world’s telecommunications networks and Internet backbones. Occupying multiple floors with interconnectivity between all points, in this site Telx offers low-latency access to more than 400 carriers, financial exchanges and application, media, content, and software as a service providers with just a single connection.
			</p>
			<a href="/wp-content/uploads/2017/10/Data-Sheet_NewYork.pdf" target="_blank">Download datasheet</a>
		</div>

		<div class="world-map__box fs-s js-country-box is-hidden" data-country-box="dallas">
			<div class="close-cross js-close-cross">
				<i>
					<span class="close-cross__cross"></span>
				</i>
			</div>
			<h3 class="world-map__box-header fs-l">Dallas</h3>
			<p>
				With over 50,850 square feet of raised floor featuring the latest in UPS power equipment, Synergy Park has one of the most powerful, yet efficient cooling systems. Fully supported with the same enterprise-class infrastructure and support you expect from Pulsant.
			</p>
			<a href="/wp-content/uploads/2017/10/Data-Sheet_Dallas.pdf" target="_blank">Download datasheet</a>
		</div>

		<div class="world-map__map">
			<div class="world-map__map-wrap">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 973.8 560.8" enable-background="new 0 0 973.8 560.8" xml:space="preserve">
					<g>
						<g>
							<path fill="#F3F3F4" d="M968.6,131.8l-4.1,0.2l-2.7-2.2l-1.9,5.6l-1.7-8.3L944.7,117l-10.9-4.9h-12.7l1.1,4.7l-2.6,2.6
								c0,0-4.4-3.6-5.6-3.6c-1.2,0,2.4-0.7,2.4-0.7l1.7-2.9l-4.1-1.5l-3.9,3.9l-6.8-2.2l-5.3,2.9l-6.3-1.7v-5.1l-3.2-4.1l-3.4-1.5
								l-6.6,0.2l-5.8,1.7l-1.9-1.5l-2.2-1.9l-2-3.4c0,0-1.7-3.2-1.7-4.9c0-1.7-2.4-2.9-2.4-2.9l-3.2,1c0,0-1.9-0.7-3.4-0.7
								c-1.5,0-3.2-2.2-3.2-2.2L846,87c0,0-1.9-0.7-3.2-0.7c-1.2,0-1.5,0.7-0.7,1.5c0.7,0.7-2.4,1.2-2.4,1.2s-1.7,0.5-2.7,0.5
								c-1,0-0.4,2.8,0.5,3.7c0.9,0.8-1.7,1.2-1.7,1.2l-2.4,2.9l-3.4,0.7l-4.6-1.7l-2.7,3.2l-2-1.9l-1.5-3.7l-2.9,1.9l-3.2,10.5l-6.3-8.5
								l3.9-8l-2.2-9.2l-6.8-3.2l-3.4-3.4l-6.3,2.4l-0.5,7.5l-5.1,2.4c0,0-4.9-0.7-5.6-1.5c-0.7-0.7-1.2-6.3-1.2-6.3l-9.5,0.2l-5.6,0.5
								l1.2-3.9v-3.7l-2.9-3.4l3.6-3.6v-7.8l-1.7-5.4l-8.8-3.2l-7.3,3.6l-1.7-8l-6.1-4.4l-9,6.3l0.2,6.3l-4.6-1.2l-3.7,3.9l-5.8-2.4
								l-8.5,4.1c0,0-7.8,3.7-8.5,4.4c-0.7,0.7-7.1,1.9-7.1,1.9l-4.9,5.4l1.5,6.1l-2.2,4.1l-12.7,1.2l1.7,5.1l-1,6.3h5.1l1.2,4.9
								l-3.4,0.5l-3.2-3.2l-4.6-3.7l-4.4-1.9c0,0-1.7,3.9-2.4,4.6c-0.7,0.7-2.9-6.1-2.9-6.1l-1.5,6.1l-1.5,1.5l-5.1,7.8l1.5-6.8V87
								l-3.2-1.5l-1.2-4.9l-3.2-1v1.7l-0.5,2.7l-1.9,2.4L642.5,97l-4.4,5.4l-0.7,4.6l1.5,3.4l-0.5,4.4h2.9l2.4,4.9l-1.5,3.6l-2.9-4.4
								l-8.8-5.3l-9.7-2.2l-3.2-4.6l-2.2,2.4l1.2,3.9l4.4,1l1.2,3.9l-2.4,1.9l-2.2-1.9l-5.1,1.9l-9-2.4l-4.1,3.4l-4.1,0.5l-8.3,5.4
								l-0.5,4.9l-3.4,1.5l-2.7-2.7c0,0,0.7-2.4,0.7-3.6c0-1.2,3.2,0,3.2,0l-2.2-5.4l-3.9-0.2l-2.9-1.7l-1.2,1.9l2.2,2.2l-0.5,2.9l-1,5.4
								h2.9l-1.5,7.5l-0.5-3.9l-3.7-0.5l-3.2,3.2l-4.6,2.7l1.7,3.6l-1.2,4.4l-3.9-2.4l-0.7-3.6l-3.9,2.7l4.1,6.8l-6.8-2.4l-3.2-5.6
								l1.7-5.1l-6.6-6.8l11.9,5.4l7.1,0.2l4.4-6.6l-1-4.6l-15.6-10.5l-4.9-1.2l-2.2-2.2l-7.1-0.2l4-3l-5.2-5.2l-5.4-1.5l-2.9,3.4
								l-0.7-3.9c0,0-3.7,2.2-4.6,2.2c-1,0-6.6,2.7-6.6,2.7l-3.4,4.1l-3.6-2.2l-4.1,4.4l-3.9,3.9l-3.9,0.2l-5.1,5.4l-1.7,3.4l5.6-2.9
								l-1.9,7.5l-4.4,4.6l-0.7,7.5l-6.3,10.2l-3.7-0.7l-10.5,12.9l2.7,18.3l2.4,4.1h4.4l3.4-5.6h5.1l3,13l-2.1,2.1v5.1
								c0,0,2.8-3.3,3.9-4.4c1.1-1.1,0,2.2,0,2.2h3.4v-3.2l6.1-0.2l0.2-5.3l1.5-6.6l3.9-1.9l0.5-6.6l-4.1-1.7l-0.2-9.5l3.7-6.8l8.3-8.8
								v-5.6l1.7-4.9l5.8,0.2l2.4,3.9l-10.5,14.8l-1.2,5.4l1,2.2l-0.7,8l-2.9,1v2.7l8.5,1.2l13.4-3.9l5.1,1.7l-7.5,3.6l-5.6-0.2l-7.5,2.7
								l-2.4,5.1l4.9-2.2h4.4l-0.4,4.5l-2.8,2.8l-1.9-4.4l-2.4,1l-3.7,3.4l0.7,7.8l-1.9,3.6l-2.9,0.7l-1.5-3.4l-10.9,5.6l-3.4-4.4
								l-5.6,3.4l-3.2-2.7V198l2.7-2.7l-1.5-2.2l-0.7-4.6l-5.1,2.2v5.6l-1.5,2.4l2.2,2.2l0.2,5.8l-10,1.5l-2.2,7.1l-6.4,6.4l-0.1,2.8
								l-2.8,1.3l-2.3,2.3l-3.9-1.2v3.2l-4.4-1.2l-4.9,1l1.5,3.2l5.1,1.5l4.1,5.8l-2.9,9.5l-6.8-1l-8.5-1l-5,3.3l1.8,1.8l0.5,5.6
								l-3.2,6.6l2.7,3.2v4.6l2.8,0.2l2.4-1.1l2.7,4.5l2.1-0.1l2.6-2.6l5.7,0.2l0.4-1l1.3-1.3l2.1-1.1l0.7-2.2l1.8-2.4l-0.9-0.9l-0.1-1.9
								l2.4-4.5c0,0,4.4-1.6,4.9-1.6c0.5,0,1.6-1.6,1.6-1.6l0.2-2.6l-0.5-2.7l2.4-1.2l6.5,1.6l2.6-2.6l2.2-0.4l1-2.1l1.6-0.2l2.6,2.6
								l1.3,3.8l2.6,1.2l3.3,4.3l3.9,0.5l3.5,3.5l1.8,2.9l-0.5,3.4l-2.6,1.9l-6.6-1.5l-0.4,2.2c0,0,5.7,4.5,6.2,5
								c0.5,0.5,2.1-1.9,2.1-1.9l-0.9-2.8l3-0.6c0,0,1.1-2.3,1.1-2.9c0-0.6,1.6-0.9,1.6-0.9l-1.7-3.3l1.1-2.8l2.6,1l1.5,1.5l0.6-2.2
								c0,0-5.7-3.8-6.4-3.8c-0.7,0,0-1.8,0-1.8l-2.4,0.2l-3-2.8l-2.1-3.9l-3.3-3l0.7-3.3l2.6-0.9l1.3,2.7l1.3-1.3l4,6.7l4,1.2l5.5,5.4
								l-0.4,4.6l1.4,1.4l2.2,2.6l1.1,3l3.3,0.4l-2.3,1.1c0,0-0.1,2.7-0.1,3.3c0,0.6,2.3,1.5,2.3,1.5l1.6,2.2l1-2.7c0,0-1.6-1.7-1.6-2.3
								c0-0.6,2.4,0.6,2.4,0.6l-2.3-3.5l4,2.6v-2.1l1.2,1.2l-0.5-2.9l-2.3-0.4l-1.9-3.8l-0.1-3l0.9,2.2c0,0,3.4-1.9,3.5-2
								c0.1-0.1,3.9-1,3.9-1l1.6,0.8l-0.3,1.4l1.7-0.8l1.5-1.9l2.9,0l0.6-1.3l-1.9-2.1l-0.6-2.7l1.3-2.3c0,0,0.7-3,0.7-3.7
								c0-0.6,2.8-1.7,2.8-1.7l0.6-2.6l1.8-2.4l2.3-1.6l1.8,1.8l4.4,0.1l-3.2,1.8l1.9,0.6l1.3,1.1l-0.9,1.8l2.4-0.1l3.3-1.2l2.9-1.1
								l0.9-1.8l-3.7,0.4l-1.7-1.7l1.5-1.5l5-1.7l1.7-1.5l1.1,1.1l-1.8,1.5l0.7,3l-2.1,1.8l-0.4,1.2l2.2,1.1l3.6,3.3l4.3,3.2l2.4,2.7
								l-1.3,2.7c0,0-5.8,1.3-6.4,1.3c-0.6,0-5.8-1-5.8-1l-6.7-2.6c0,0-3.5,0.6-4.1,0.6c-0.6,0-3.8,2.4-3.8,2.4h-3.8l-2.3-0.2l-0.4,1.2
								l1,1l-2.7,1h-2.4l-2.3,0.9l-0.9,2.3h1.3l0.7,2.3l-2.3,2.1l3.6,1.7L529,270c0,0,1.9,1.8,2.4,1.8c0.5,0-1.1,1.8-1.1,1.8l2.4-1.1v1.6
								l2.7-0.2l0.6,2.5h2.1l2.1-2.3l2.8,0.6l2.8,2.2l4.9-2.4c0,0,2.6,0.5,2.8,0.5c0.2,0,0.8-1,0.8-1l0.6,0l-0.7,2.2l-0.2,5.4l-1.9,4.4
								l-1.8,4.1l-2.8,1.3l-3.4-1.1h-5l-2.9,1.6l-15.2-4.1l-2.3-1.3l-3.9-0.6l-3.6,2.3l0.1,3.5l-2.9,1.6l-3.2-1.7l-5.6-1.2l-1.1-2.7
								l-6.2-1.6l-3.3-0.1l-2.3-2.3l-3-0.6l2.3-1.2l1.5-3.5c0,0-1.6-2.2-2.6-2.2c-1,0,1-4,1-4l-3.5-0.6l-2.3,1.3l-6.8,0.5l-1.5,1.5
								l-0.7-1.5h-6.1l-5.2,1.1l-3.2,1.8l-2.6,1.1l-1.4,2.3L451,280l-7.4-0.5l-1.2-2.2l-1.2,0.3l-2.6,4.5l-3.6,1.3l-3.9,4.9l-1.3,6.2
								l-3.9,3.9l-4.2,1l-1.9,2.6l-2.6,2.9l-1,3.9l-2.9,3.9l-2.9,6.5l2.9,7.1l-0.6,6.8l-2.9,3.9l1.3,4.5v4.9l8.4,5.8l1.3,4.9l4.2,2.6
								l11.4,6.8l5.2-2.6l9.7,1.6l11-4.9h6.8l1.5,3.7l2.8,2.8l7.6-2.1l2.8,2.8l-1.3,6.5l-1,6.5l3.7,7l5,5l2.6,10.4l1.6,5.8v3.6l-2.6,2.9
								l-3.2,8.1v5.8l3.2,3.6l4.2,9.7l3.2,14.6l3.2,4.5l2.9,8.1l1.9,5.5l5.5,1.3l6.5-1.9h5.2l11.7-4.9l5.5-6.8l3.6-3.2l0.6-7.8l6.8-4.2
								v-7.8l-0.6-4.5l5.8-5.8l7.5-4.2c0,0,2.3-7.1,2.3-9.4c0-2.3-0.3-6.8-0.3-6.8l-2.6-3.6v-5.5l-1.6-5.5l6.2-9.4l7.5-10.1l4.9-3.9
								l7.1-6.2l2.9-6.8l2.8-4.8l1.8-5.1l-0.7-3.5l-4,1.8l-7.4,1.6l-3-0.2l-2.2,2.2c0,0-1.8-0.8-2.7-1.7c-0.9-0.9-2.1-2.1-2.1-2.1
								l0.5-2.1c0,0-7.2-7.9-7.7-7.9c-0.5,0-2.5-1.5-2.5-1.5l-1.8-5.2c0,0-3.3-4.1-3.9-4.7c-0.6-0.6-0.3-6.4-0.3-6.4l-4.4-5l-0.3-2.6
								l-4.1-7.1c0,0-4.9-8.9-4.9-9.3c0-0.5,4.3,6,4.3,6l1.1-0.1l1.5-4.7l-0.2,3.1l1.3,0.7l4.6,7.1l2.7,4.9l2.4,2.4v3.7l1.2,3.7l4.4,2.2
								l1.2,3.4l3.2,3.2l1,5.1l1.7,7.5l2.9,1l3.2-3.4h5.8l3.7-1.7l7.8-2.9l0.5-2.9l7.5-0.5v-3.2l2.8-1.1l1.8-1.8h2.9l1.2-3.9l3.9-6.1
								l-3.6-3.7l-4.1-1.5l-1.7-6.1l-7.8,5.8l-5.4-0.7v-4.9l-1.7,3.2l-0.2-4.1l-5.6-5.6l-1-3.4l5.6-0.7l3.7,6.3l5.4,2.7h5.1h3.2l2.2,2.2
								l7.5,2.9l17.5-0.7l3,3l2.3,2.3v3.2l5.1,3.7l5.6,2.2l2.2,10.7l3.2,6.3l0.1,4.5l2.8,2.8l2.7,7.1l2.1,2.1l2.6-2.6h3.4v7.3l2.4,2.4
								l3.7-1l0.2-4.1l-4.9-7.1l-0.7-4.4l1.7-3.9l-1.9-5.4l2.4-2.4l5.1-1v-2.9l4.6-4.9l4.6-1.5l4.9-4.9l7.3-1.5l4.4-1.2l2.4,5.4h1.9
								l-0.7,3.9h1.9l1,2.7l-2.2,4.9l4.4,1.5l5.4-4.1l1.7,2.7l1.5,8.5l-0.1,14.2l4.3,4.3l2.2,7.5v4.4L726,369l-5.8-5.6h-5.6l6,7.7
								l7.7,7.7l3.2,8l8.8,9.2l4.4-0.5l0.7-4.6l2.2-3.9l-2.9-3.2l-2.2,0.7l-3.9-5.1c0,0,0.7-11.9,0.7-13.1c0-1.2-5.1-6.6-5.1-6.6h-2.9
								l-2.2-6.3l-2.2-1.9l2.4-9h1.9l4.1,4.1l6.3,4.1l1.2,5.1l10.7-11.4l-0.7-7.5l-7.5-11.7l7.5-5.8l17.3-3.7l10.7-7.3l6.6-7.8l1-12.7
								l-6.3-8.8l4.1-4.1l3.9-0.5l1.5-4h-5.4h-4l-3.5-3.5l2.2-3.4l8-3.9l-3.7,3.4l2.7,2.9l5.6-3.7l3.7,1.5l0.2,5.1l2.2,2.9l1.2,3.9
								l-1.5,4.4l3.6,2.2l6.1-2.4l-0.5-8.3l-5.1-7.1v-2.2l5.4-1.9l2.4-5.8l4.4-4.4l1.7,1.7l7.3-3.4l7.8-10.2l5.3-6.1l1-9.7
								c0,0,1.2-6.1,1.9-6.8c0.7-0.7,2.7,3.6,2.7,3.6l-0.7,14.8l-0.2,6.3l2.2-3.4l3.2,1.9c0,0-2.9-3.9-2.9-5.6c0-1.7,1.5-4.6,1.5-5.6
								c0-1,3.9,1.5,3.9,1.5l-3.9-12.7l1.2-6.6l-2.4-5.6l-4.5,4.5c0,0-2.6-2.6-2.6-4.3c0-1.7-3.2,0-3.2,0l-1.8,3.3l-3.5-3.5h-3.7
								l-1.2-2.9l20.7-19.5l9.7-1.7l6.2,0.4l2.1-2.1c0,0,4.6,1.5,6.1,1.5l-1.8,2.3l2.1,2.1l3.9-2.9l5.4-0.5l-2.2-3.2l3.6-4.1l4.4-5.6
								l4.6-1.9l2.4,1v3.2l0.5,5.4l7.5-8l1-6.8h4.4l-1.9,2.9l-1.2,8.8l-3.7,1.7l-4.1,4.4l-2.9,4.9l-7.1,4.4l-2.9,4.1l-1.7,2.7l1.7,19.7
								l2.2,5.8l4.4-4.9l0.7-4.1h2.9v-3.2l0.7-2.4l4.6-1v-2.9l0.2-3.7l1.7-2.7l1.7,1.7v-4.1l-1-2.2l1-2.7h-2.2v-2.9l2.7-5.6l1-1.7h3.9
								l2.9-2.2l-0.5,3.4l4.4-4.4l4.4,1.2l2.2,2.2l9.2-10.5h2.9l5.1-3.4l2.2-3.2l5.1,2.4l1.7-3.6l-1.7-4.9l-1.2-6.6l3.9-1.9l1.2-4.9
								l-1.5-1.5l-0.5-3.4h3.7l-0.5,2.2l3,4.3l2.3-2.3l2.7,0.2v5.1h2.4l3.9,4.4l3.4-1.2v-8.5l3.2,1.5l4-5.5L968.6,131.8z M601.1,273.8
								l-3.2,0.9l-2.2-1.8l-2.9-1.8l-2.3-0.1l-0.7-4.1l1.6-2.6l0.5-3.4l1.9-0.9l-1.6-1.3l-5.2-7.3l-0.2-4.4l-1.3-2.7l1.6-3.3l5.1-3.7
								l4.3-1.5c0,0,4.4,1.5,4.4,2.1c0,0.6-1.9,4.1-1.9,4.1l-3.5,1.3l-1,3.6l3.5,4.6l1.5,1.1l-0.1,2.9l1.1,3.4l0.6,3.8
								c0,0,2.3,2.7,2.3,3.3c0,0.6,0.2,3.8,0.2,4.7C603.4,271.8,601.1,273.8,601.1,273.8z"/>
							<path fill="#F3F3F4" d="M357.6,393.1l-8.8-6.3h-3.4l-8-1.2l-1.7-2.4l-9.7-2.4l-3.2-7.3l-2.9-1l-2.4-5.4l-7.3-4.9h-6.3l-3.9-1.2
								l-3.2-5.1l-3.6-1.2l-3.7-4.9l-6.6-0.2l-4,1.3l-1.8-1.8l-5.6,0.7l-1.9-2.4h-6.1l-1-3.2l-7.5,3.2l-3.7,2v3.2l-3.9,2.7l-1.9-2.4
								l-4.6-0.2l-5.3,2.2l-5.8-6.6v-6.6l1.5-2.2l-1-2.9l-2.4-2l-12.4,0.7l0.7-1.7l4.6-11.7v-3.3H213l-2.3,2.3l-1.2,4.4l-3.9,1.9
								l-4.6,0.7l-5.6-3.2l-2.4-3.6v-2.7l-1.5-2.2l0.2-7.3l1.9-1.9l-1.2-6.1l5-2.1l2.6-2.6l3.2-0.5l5.4,1.9l5.1-0.7l2.4-2.4l6.6-1
								l1.9,2.9l4.7-2.3l2.6,2.6l-0.2,3.9l6.1,8l1.9-4.6l-2.4-7.8v-4.9l6.6-6.6l8-5.1l-1.2-6.8l2.7-4.6l4.1-8.3l8.8-1.9l-1.2-7.1l1.5-1.5
								l1.9-1.9l4.6-1.5l3.7-3.6h4.6l-1.9,1.7l-2.7,3.9l2.2,2.2l3.2-3.7l8-2.9l3.2-8l-5.4,4.9l-6.6-4.1l1.2-3.7H279l3.2-4.4l-4.9-1
								l-9.4,5.2l5.7-5.7l4.1-2.9l13.9,0.7l7.1-3.4l2.4-2.4l3.7-2.4l-0.1-3.2v-4l-2.3-2.3v-2.7l-2.2-2.2l-4.1-0.5l-5.1-5.6l-2.2-7.1
								l-4.9-7.8l-2.9-6.6l-3.2,7.3l-2.9,2.7l-3.7-1.5l-3.9-3.9v-6.8h-2.4l-1.7-3.4l-3.2-4.4l-3.7-3.2l-2.7,2.7l-7.5-2.9l-1.9,3.7
								l2.2,4.6l-2.4,3.7l1.7,1.7l1,5.4l-2.8,2.8l4.3,4.3l0.7,7.5l-3.4,5.6l-5.6,2.4l2.4,2.4l0.5,4.9l1.2,5.3l-4.1,3.4l-1.5-2.2l-2.7-2.7
								h2.7l-1.7-3.7l-2.2-2.2l-1.2-3.6l0.5-3.9H227l-2.4-2.2l-6.2-3.3l-2.3-2.3l-5.4-2.2H207l-2.7-7.8l-4.1,0.2l-1.2-8.3l2.7-2.7
								l0.5-2.9l4.1-3.9l1.9-4.6h2.2l0.2-4.1l2.9-3.6l3.2,0.7l3.7-7.1l-0.2-4.6l3.4-2.9l-2,8.8v3.2l-1.7,2l1.1,4.3l2.6-2.6l1.7,4.9
								l5.4-4.6v-3.6l2.2,4.4l4.4,1.9l1-4.4l-2.4-2.4l-3.2-3.6l-3.7-3.6l-1-3.9l3.7,1.4l-3.1-8.2l4.5,2.5l2.5-2.5l1.4-4l-2.4-6.1l1.3-1.3
								v-7.4h2.8l4.8-4.8l2.3,0.3l0.3,6.5l2.5,4.2l3.1-2l-0.3,3.1l-4.8,2.5l-1.1,5.7l6.2-1.1l2.3-7.1l4,2.3l2.6,5.4l-4.8,4.8l0.8,4.8
								l-3.1,4.5L248,141l-5.4,5.4l4.2,4.2l6.8-0.8l5.4,2l4.2,9.6h4l11.3,5.4l-8.5-11.9l11.6,8.2l0.3-9.9l-1.7-5.1l-4.5-2.8l-2.8-9.1
								l11.9,9.3l5.9-13l-14.2-8.8l-5-4.4l3.5-3.5l-8.5-12.2l-4.5-5.1l-6.8-1.4l-2.3-5.1l-4.1-3.5l2.1-2.1l-5.1-6.5h-7.6l3.1,7.4l3.4,0.8
								l-4.4,3l-2.7,2.7v-6.5l-1.7-8.2h-4.5l-5.7,7.4l-1.1,4.2l2.3,13.6l-4.5-6.5v-9.9l4-10.2h-6.2l-4.8,2.8L212,96.6l2.8,10.8l9.9,5.7
								v6.2l-3.1,10.8l-2.5-4l1.7-4.8l-5.4-4.5l-3.4,3.4l-0.3-5.4l-3.4-1.4v-3.7l-3.4-6.8l-2.3-6.8v-3.7l0.6-3.1h3.4l6.2-14.2h-5.9h-8.2
								l-2,7.9l1.1,13.9l-4.2,4.8l-0.1,8.9l8.1,3.5l-1.6,5.2l2.4,2.4L197,128l-2-6.2l2.5-7.1l-5.9-4.5l-5.4,5.7v2.8l4,1.7l-4,5.7
								l-10.8-0.9l-6.2-5.7l-7.6,3.4l-0.1,6.1l-3.5-3.5l-6.8,1.1l-6.5-1.4l2.8-2.8l-4-5.4h-8.2l-6.5-4.2H124l-2,2h-2.3l-1.1-5.9l-1.7,1.7
								l-1.7,5.1l-3.4-6.5l-4-3.1l-14.5,6.5l-6.8,1.4l-3.1,3.1l-4.8-3.7H73l-1.7-2.8l-3.8-1l-2.4,2.4l-3.7-1.1h-4.5l-1.7-3.1h-9.6
								l-5.4-3.1l-2.3-2.3l-7.1-0.8l-3.7,3.7l-4.2,1.1l-2.6,2.5h-3.7l-2.8,5.7l-2.8,3.7H5.7l0.8,5.9l4.8,4.8l2.5,2.3l4,2.3v3.4l-4,0.3
								l-2-2l-4-1.1L0,142.4l3.7,7.9l6.5-1.7h5.1l4.5-2.8l-1.1,7.1l-5.9,0.6l-6.5-0.6l-0.8,4.2l-3.5,6.5l3.8,13.9l6.6,2.4l2.4,2.4
								l3.1,3.4l2.5-1.7l3.3-1.3l2.1,2.1h3.7l-2.5,5.1l-5.1,4.8l-4.5,1.7l-7.9,6.8L8.6,204l5.7-2.8L26,196l11.1-8.2l4.5-4.5l-2.6-0.6
								l4.7-4.7l0.7,3.7l7.3-3.4l4.1,1.7l2.9-2.9H63l10.5,1.7l7.3,2.9l4.6,5.4l5.4,9.2l-0.7-5.5l-0.5-9.6l1.6,9.6l3.5-3.5l-1.2,4.4v8.3
								l4.5,4l2.1-2.1l1.9,2.9v4.9l4.4,3.6l2.9,5.6l0.2,2.9l9,9l2.9,5.8l-2.9,14.8l0.7,5.6l-1,2.9l2.7,5.8l4.9,7.8l3.2,8.3h3.4l6.1,4.4
								l5.1,9.2l1.9,1.9l1,2.4l-1.9,1.5l5.1,1.9l2.7,2.7l0.5,2.7l3.7,3.6l1.3,2.8l2.1-2.1l-2.4-3.6l-2.9-5.1l-4.6-6.1l-4.4-3.9l-0.7-3.9
								l4.1,1.2l3.5,5.2l7.2,7.2l0.7,2.4l9.7,9.2l1.2,3.7l-1.5,3.4l1,1.7l3.2,1.2l6.8,4.1l13.1,5.1l4.1-1.2l3.4-0.5l6.3,6.3l5.8,1.5
								l4.3,1.3l6,6.2v3.7l4.9,1l1.2,3.3h4.3l2.1,2.1h2.1v-2.3l1.8-1.8h3.2l3.2,6.6l0.7,8l-3.9,3.4l-3.2,4.6l-2.7,3.9l-0.4,5.2l2.3,2.3
								l-3.9,3.9l1.5,2.2v1.5l2.7,2.7l10,18.7v1.9l2.9,3.2l6.8,3.9l7.1,6.6l-1,10.9c0,0,0,5.8,0,7.5c0,1.7-3.4,12.2-3.4,12.2l0.2,8.3
								l-4.9,13.9l0.2,13.9l-1.7,9l3.9-7.3l-4.3,21L252,526v9.5l4.1,14.6l5.6,7.3l8.5,3.4l5.1-1.7l4.9-1.9l-2.4-2.2l-4.4-4.6l-0.4-3.8
								l-1.6-1.6l-1-6.6v-2.7l2.4,0.5l1.5-7.5l4.1-3.7l0.5-2.4l-3.3-0.1l-1.8-1.8l1-3.4l1.9-1.9h1.9l1.5-6.1l3.9-2.2l-0.2-2.9h-3.4V499
								l5.1,2.2l3.9-2.9v-4.4l10.2-3.6l3.4-5.6l-1.2-3.6l3.7-3.2h3.9l2.4-2.7l4.6-4.6l2.4-2.4l2.8-4.7l2.8-2.8l1-2.9l0.2-4.6l4.9-4.9
								l14.4-6.3l1-2.7l3.4-7.3l2.9-6.6V417l3.9-5.6l4.6-4.9l1.9-5.8l-1.2-7.5H357.6z"/>
							<path fill="#F3F3F4" d="M251.4,319.3l-3.6-0.8l-2.9-2.8h-4.4l-2.6-2.6h-6l-4.7,2.3l0.3,1.8c0.8,0,8.3,1.6,8.3,1.6l1.6-1.1l3.6,2.4
								l7.1,3.1l7.5-1.3L251.4,319.3z"/>
							<polygon fill="#F3F3F4" points="243.4,327.3 245.7,329.2 251.2,328.7 248.6,326.6 244.9,326.3 		"/>
							<path fill="#F3F3F4" d="M261.1,327.8l1.5,1.5l2.3-1.7h3.1l3.9-0.5l-4.9-2.9l-2-2l-7.7,0.9l1.5,1.5l1.1,1.1l-5.4,1.1
								c0.5,0.5,2.3,1.6,2.3,1.6L261.1,327.8z"/>
							<polygon fill="#F3F3F4" points="273.9,326.8 275.2,328.6 279.8,327.8 278.6,326.6 		"/>
							<polygon fill="#F3F3F4" points="288.4,228.9 289.6,227.9 287.6,225.9 281.7,225.7 		"/>
							<polygon fill="#F3F3F4" points="306.6,217.9 302.1,220.1 297.5,228.7 297.9,230.7 294.8,232.8 296.1,234.9 298.6,233.8 
								306.6,233.8 304.7,235.6 305.7,236.7 310,234.3 310.6,237 312,237.8 314.3,233.7 312,232.2 313.1,229.8 311,229.8 311,227.3 
								305.1,227 305.1,225.1 302.1,225.1 305.7,221.3 		"/>
							<path fill="#F3F3F4" d="M400.2,30.2l-1.2,4.5h2.9l2.4-4.5l1.5,6.9l-1,7.1h1.9l0.7,4.1l-2.9,1.2l-3.2,1.7l2.7,2.9v5.1l0.5,5.4
								l-1.9,3.4l5.4,1.5l-2.9,3.7l-3.9,1.5l1.2,2.4l-1,4.9h-2.2l-3.2,1.2l-4.1-3.7l2.4,5.4h3.4l-1.9,9.5l-4.9-4.9l-2.4-5.4l1.2,7.5
								l6.6,6.6l2.2,10h-4.4h-2.7l-1.2-4.6l-2.7-2.2v4.1l-4.5,4.4l4.2-1.7l2.9,2.2h3.9l2.9,0.7l-7.5,5.6l-4.9,4.6l-4.4,1L371,125
								l-4.1,3.2l-3.6,6.8l-5.6,5.8H353l-2.7,1l-3.4,3.6l0.5,8l-4.4,7.1l0.5,5.4l-1.7,4.4l-2.2,7.8l-3.9,1.2l-3.4-3.6l-2.7-1.7l-2.7,1.7
								l-5.6-9.5c0,0-1.5-4.6-1.5-6.1c0-1.5-4.4-7.5-4.4-7.5l-0.2-9l-4.6-5.8v-3.9l-1-4.4l3.4-8.3l4.6-0.2l-1.5-7.3l-4.9,5.3l-4.4-3.6
								l1-5.8l0.7-2.4l2.9-1h3.9l-2.9-2.7l-1.5-4.6l-2.2,2.7l-3.4-1.7l-2.9-17l-1-8l1.9-3.2l-4.4-4.6l-0.7-7.3l-13.1-5.8l-6.6,4.9
								l-0.5-2.7l-4.6,2.2l-5.7-3.5l4-4h-6.8l-1.9-4.6l10.5-1l1,3.2l3.9-2.7l-2.4-3.6l-8,1.7l-6.8-3.4l-3.6-3.9c0,0,7.5-5.9,8.5-5.9
								l3.5-0.7l1.7,0.6l1.9-1.7l2,2l2.6-3.9h4.6l1.2-1l4.1-1.9l-1.8-1.8l-4.7,2l-2.3-3.7l5.8-2.7h4.3v-1.9l3.7,1.4v-4.3l5.2-1.9l3.3,3.3
								l1,5.2h2.3l-4.1-8.9h10.1l1.9,3.7l2.2-2.2l6.5,2.6l-1.4-4.3l2-2l4.2,2.8h3.9l4.1,5.2v-4.5h2.7l8.1,1.4l-3.4-3.4h-8V5.2l5.8-1.7
								l2.1,3.3l7.2,0.8l-3.3-2.4l3.7-2.6h4.3l1.3-1.3l5.1-1.3l14.7,1.9l-8.5,3.4h-7l0.6,1.4l-1.6,1.6h-2.4l1.1,1.1l1.3,1.3l3.4-3.4h4.3
								l6.8-2.7l5,0.4l2.7-1.2v3.5h3.5l-1,3.1h-4.8l-4.6,1.4l-8.5-1l-1.7,1.7h2.1l-2.4,2.4l-1.8,1.8v1l3.5-1.7l3.9-0.8l8.9-2.3v1.7l-3,3
								v1.8l3-2.9l3.2-0.8l1.7-1v-2.9h3.3l2.1,2.5l-2.3,2.3l-2.7,5.6v1.5l3.1-2.7v-1.2l1.9-1l1.2-1.2l1.2-3.9l2.5,3.4h1.4l1.8,1.8
								l1.8-1.1v-3.1l4.1,0.4l4.3-0.8l3.9,2.1l-2.5,2.5h-3.1l-0.2,3.9l-8.7,1l4.6,3.4L400.2,30.2z"/>
							<path fill="#F3F3F4" d="M412.8,152.9l2.1-2.4l1.9-0.3l1.6-2.9l1.3-6.5l-1.8-1.3l-1.5-4.2l-1.5,1.5l-2.3-2.7l-1,2.9
								c0,0-2.1-1-2.1-0.3c0,0.6-0.5,2.6-0.5,2.6l-1.9-2.3h-5.5l-0.5,5.2l-1.6-0.2l0.2-4.5l-4.9-3.6l-2.1,2.8l-1,2.6l-1.5,1.5l0.8,2.4
								l4.2-1.3l1.3,1.3l-0.8,1.6l-4.1,0.3v2.3l2.1,0.2h2.1l1.5,3.1l-2.6-0.3c0,0.8,1.1,3.6,1.1,3.6l2.9-0.3l5.2,2.8l4.5-1l1.8-1.8
								L412.8,152.9z"/>
							<path fill="#F3F3F4" d="M429.6,218.7h3.3c0,0,3.4-2.2,4-2.8l2.7-0.7l1.9-3.8l-1-4.7l2.3-2.2l-3-3.9l-5.1-0.5l-1.8,4.5l-2.3-1
								l-1,5.1l1.5,2.7l-3.2,3.5L429.6,218.7z"/>
							<path fill="#F3F3F4" d="M439.1,190.5l2.2,1.8l-1.7,2.4l1.7,1.2l-1.1,2.1l1,2.1c0,0,1.6-2.2,2.1-2.7c0.5-0.5,1,1.1,1,1.6
								c0,0.5-1.3,2.1-1.3,2.1l0.8,2.1l3.2-0.5l1.1,2.8l0.4,3.8l-3.8-0.4l1,3v1.7l-3.2,1.5l0.2,2.9l1.8-0.6l1.6,1.6l-4.5,5.8l2.9-0.7
								l2.4-0.1l1.3-1.3l5.6,0.6l4.4-1.6l2.8-0.7v-1.5l-1.9-0.7l2.9-4.9l-1.7-2.8l-2.2,0.6v-2.7l-1.1-3.6l-3.2-1.9l-1.3-5.2l-2.8-3l2.3-3
								l1.5-3.5l-7.2-0.6l2.2-1.5c0,0,1.7-4.3,2.2-4.7c0.5-0.5-1.9-1.6-1.9-1.6l-1.1,3.7l-3.3,0.4l-1.9,1.9l-1.8,2.7l-0.6-4.4
								c0,0-4.4,2.4-4.4,2.9c0,0.5,1.2,5.5,1.2,5.5L439.1,190.5z"/>
							<path fill="#F3F3F4" d="M590.5,412.5l-2.6,4.1l-1.1,2.9l-5.2,4.8l-4.4,0.2l-1.3,5.2l1.3,6l-2.8,4.2l0.7,5.1c0,0,0,2.9,0,3.4
								l2.7,2.3l2,1.4c0,0,2.4-0.9,2.4-1.5c0-0.6,2.6,0,2.6,0l1.3-3l1.2-4.6c0,0,1.6-2.6,1.6-3.1c0-0.5,0.5-4.9,0.5-4.9l1.7-3.2l0.3-6.4
								l2.3-2.2l-0.5-6.2L590.5,412.5z"/>
							<polygon fill="#F3F3F4" points="597.4,99.9 600.9,102 602.9,106.7 613.1,107.6 613.6,105.2 609.5,100.5 608.4,90.1 611.2,84 
								616.8,72.9 624,65.8 629.3,60.8 640.5,56.5 643.4,53.9 644.7,48.1 641.5,46.5 636.1,49.6 632.8,52.9 628,53.8 623.2,53 609.9,64 
								607.8,67.9 608.7,72.1 605.5,75 601.9,77.6 603.4,82.2 600.6,84.8 599.6,89.3 600,93.4 596.1,93.4 		"/>
							<path fill="#F3F3F4" d="M845,247.5c0,0-1-3.5-1.6-4.1c-0.6-0.6-1.6,3-1.6,3l-2.9,6.9l-0.8,9.6l-1.8,4.7l-4,4.7l-2.7,1l-0.4-2.7
								c0,0-1.5,1.6-1.5,3.2c0,1.6-2.1,3.9-2.1,3.9h-1.7l-7.5,0.9l-3.6,4c-1.1,1.1-5.4,2.8-5,3.2c0.4,0.4,3.8,0.9,3.8,0.9s-1.7,3-1.2,3.5
								c0.5,0.5,2.9,1.8,2.9,1.8l1.3-2.6l0.6-3.2l-1.5-2.7l5.8-1.1l-2.6,2.7l1.8,2.2l1.8-2.7h2.1c0,0-1.3-3.6-0.7-3.6
								c0.6,0,3.5,0.4,3.5,0.4l-1.6,1.9l2.2,1.3l1.9-2.3l4.4-3l4.4-1.1l2.4-3.9l1-5l2.4-4.6l-1-5.7l-1.7-3.4l2.1-2.3L845,247.5z"/>
							<path fill="#F3F3F4" d="M781.5,404.5l19.5-3.6l-17.5,1.1l-1.8,1.1l-4.7-1.5l-5.2,0.8l-6.8-2.1l3.1-1.5l-7.1-1.3l-2.8-0.8l-1.5,1.8
								l-2.6-0.6l-1.8-1.1l-6.5-0.5l-2.4,2.4c0.5,0.5,13.1,4.1,13.1,4.1l15.2,0.5L781.5,404.5z"/>
							<polygon fill="#F3F3F4" points="776.6,357.9 769.3,365.2 765.9,370.8 760.8,370.8 758.6,375.1 754.5,373.4 754,382.7 755.9,388.5 
								760.3,389 762.7,389 767.6,389 768.1,391 773,389 773.1,383.8 776.4,380.5 776.9,377.3 780,376.6 777.8,373.9 777.1,369.3 
								780.3,363.9 		"/>
							<polygon fill="#F3F3F4" points="779.3,389 782,391 781,395.8 784.2,395.3 784.6,390.4 784,387.6 785.4,387 785.4,389.9 
								786.6,390.5 786.4,393.4 788.9,392.3 789.6,395.5 791.5,395.6 791.7,391.3 789.3,390.3 789.2,388.4 787.4,385.1 789.8,384.6 
								793,381.4 786.9,381.3 784.7,382.9 783.3,380.5 783.9,377.1 780.7,383.2 		"/>
							<polygon fill="#F3F3F4" points="792.5,407.2 791.4,409.5 795.9,407.8 797.5,406.2 801.3,404.7 803.2,403.3 797.5,403.4 
								795.8,405.1 		"/>
							<polygon fill="#F3F3F4" points="780,406.8 782.6,407.7 785.9,408 783.6,405.9 781.1,405.8 		"/>
							<path fill="#F3F3F4" d="M788,333.6l1.7-3.5l-1.2-2.6l-3.8-0.9l-1.1,6l-1.6,0.5l1.8,5.4l1.2,1.5l-1.1,1.1l2.6,4l1.1-1.7l-1.7-2.6
								h1.5l3-0.9l3.9,4.5l1.2,0.1l-0.4,2.3c0,0,1.1,2.6,1.1,3.2c0,0.6,2.1-2.1,2.1-2.1l-0.8-4.3c0,0-2.9,0.4-2.4,0c0.4-0.4,0-3.2,0-3.2
								s-4.1-1.2-4.5-1.6c-0.4-0.4-3.2,0.6-3.2,0.6s0-2.2-0.4-2.6c-0.4-0.4,0-2.4,0-2.4L788,333.6z"/>
							<path fill="#F3F3F4" d="M797.4,364.1v-3.8l1.1-1.1l1.7,2.1c0,0-1.6-9.7-1.6-10.3c0-0.6-0.8,2.9-0.8,2.9l-2.7,0.4l-1.2,2.1
								l-1.3-1.3l-3.9,1.5c0,0-1.1,3.9-1.1,4.4l1.7-0.2l0.9-2.9l1,1l1.5-1.3h1.5l-0.1,4.9L797.4,364.1z"/>
							<path fill="#F3F3F4" d="M791,354l1.2-3.1l3.3,0.6l-1.5-4.4l-2.2,1.8l-0.6-2.2l-2.8-1.1c-0.4,0.4-0.4,3.7,0.1,3.7
								c0.5,0,2.1,1.3,2.1,1.3l-1.5,1.5L791,354z"/>
							<path fill="#F3F3F4" d="M859.2,397.5l-3.7-1.3l-4.7-4.4l-12-4.9l-8.3-2.8l-4.5,4.5l-3.1-2.1l-1.1-4.2l-3.9-2.8l-5.5,1.8l3.9,3.2
								l4.2,1.6c0,0-3.7,1.1-4.5,1.1c-0.8,0,2.9,3.6,2.9,3.6l1.8-1.8l3.7,2.6l6,1.6l3.2,5.2l-5.7,3.9l2.6,1.3l6.5-1.1l3.6,2.9l5.2,0.2
								l2.4-4.2l5.2,0.2l4.5,6.7h8.6l-5-2.3c0,0-4.3-5.9-5-6.6C855.7,398.7,859.2,397.5,859.2,397.5z"/>
							<path fill="#F3F3F4" d="M866.8,443.1l-2.3-0.5l-2.3-4.1l-0.3-1.8l-8.6-3.7l-1.1-5c0,0-0.2-4.2-0.2-4.9c0-0.6-5.2-5.4-5.2-5.4
								l-1.1-7.6l-2.8-0.7l-2.1,10.4l0.2,4.9l-3.9,5.7l-1.8-2.1l-3.9-2.6l-3.6-2.1l-3.2-1.6l3.2-4.7l-0.5-5.3l-7,0.8l-6.2-2.8l-4.4,1
								l1.9,2.8l-5.5,7.5l-4.5-3.9l-4.5,2.4l-2.8,5.2l-5.5,1.5l-0.2,4.4l-4.2,4.7h-3.2l-4.2,2.6l-1.9-1l-5.5,3.1l-1.3,2.3l-1.9-1.9
								l-1.8,7.6l2.4,4.9l-3.2-1.6l3.2,6.2l2.1,4.7l0.2,2.4l2.1,4.5l2.3,4.9h-4.9l4.4,4.2l6.8-1.3l2.8-1.5l8.8-0.2l1.9-2.1
								c0,0,5-2.4,5.5-2.9c0.5-0.5,3.7,0.3,3.7,0.3l4.5-1.8l6.5-0.2l6,1l2.4,3.2l2.4,4.9l5.5-3.9l2.4,6.2l2.4,0.5v4.7l4.2,5.3h3.4
								l5.2,1.5l2.8-2.9l3.2,3.9l4.1-2.4l7.3-8.1l7.6-14.6c0,0,0.7-12,0.7-12.8c0-0.8-5.5-8.8-5.5-8.8L866.8,443.1z"/>
							<polygon fill="#F3F3F4" points="858.7,494.8 858.2,498.6 855.2,500.8 850.2,498.1 851.7,504.7 853.1,508.2 856.3,509 860.1,502.4 
								859.7,498 861.3,497.3 		"/>
							<path fill="#F3F3F4" d="M551.9,278.6l-3.1,1.3l-3.6-0.2c0,0-3.5,1.7-4.3,2.6l3.2,1.3l4.9-1.3L551.9,278.6z"/>
						</g>
						<g id="amsterdam" class="world-map__marker js-marker">
							<circle fill="transparent" cx="452.8" cy="233.5" r="8.2"/>
							<g>
								<path fill="#1D9FDA" d="M452.8,226.3c4,0,7.3,3.3,7.3,7.3s-3.3,7.3-7.3,7.3s-7.3-3.3-7.3-7.3S448.7,226.3,452.8,226.3
									 M452.8,223.3c-5.7,0-10.3,4.6-10.3,10.3s4.6,10.3,10.3,10.3s10.3-4.6,10.3-10.3S458.4,223.3,452.8,223.3L452.8,223.3z"/>
							</g>
							<circle fill="#1D9FDA" cx="452.8" cy="233.5" r="3.4"/>
						</g>
						<g id="frankfurt" class="world-map__marker js-marker">
							<circle fill="transparent" cx="481.8" cy="241.5" r="8.2"/>
							<g>
								<path fill="#1D9FDA" d="M481.8,234.3c4,0,7.3,3.3,7.3,7.3s-3.3,7.3-7.3,7.3s-7.3-3.3-7.3-7.3S477.7,234.3,481.8,234.3
									 M481.8,231.3c-5.7,0-10.3,4.6-10.3,10.3s4.6,10.3,10.3,10.3c5.7,0,10.3-4.6,10.3-10.3S487.4,231.3,481.8,231.3L481.8,231.3z"/>
							</g>
							<circle fill="#1D9FDA" cx="481.8" cy="241.5" r="3.4"/>
						</g>
						<g id="dallas" class="world-map__marker js-marker">
							<circle fill="transparent" cx="194" cy="298.9" r="8.2"/>
							<g>
								<path fill="#1D9FDA" d="M194,291.6c4,0,7.3,3.3,7.3,7.3s-3.3,7.3-7.3,7.3c-4,0-7.3-3.3-7.3-7.3S190,291.6,194,291.6 M194,288.6
									c-5.7,0-10.3,4.6-10.3,10.3s4.6,10.3,10.3,10.3s10.3-4.6,10.3-10.3S199.6,288.6,194,288.6L194,288.6z"/>
							</g>
							<circle fill="#1D9FDA" cx="194" cy="298.9" r="3.4"/>
						</g>
						<g id="hong-kong" class="world-map__marker js-marker">
							<circle fill="transparent" cx="775.8" cy="302.5" r="8.2"/>
							<g>
								<path fill="#1D9FDA" d="M775.8,295.3c4,0,7.3,3.3,7.3,7.3s-3.3,7.3-7.3,7.3c-4,0-7.3-3.3-7.3-7.3S771.7,295.3,775.8,295.3
									 M775.8,292.3c-5.7,0-10.3,4.6-10.3,10.3s4.6,10.3,10.3,10.3c5.7,0,10.3-4.6,10.3-10.3S781.4,292.3,775.8,292.3L775.8,292.3z"/>
							</g>
							<circle fill="#1D9FDA" cx="775.8" cy="302.5" r="3.4"/>
						</g>
						<g id="new-york" class="world-map__marker js-marker">
							<circle fill="transparent" cx="246.2" cy="271.9" r="8.2"/>
							<g>
								<path fill="#1D9FDA" d="M246.2,264.6c4,0,7.3,3.3,7.3,7.3s-3.3,7.3-7.3,7.3s-7.3-3.3-7.3-7.3S242.2,264.6,246.2,264.6
									 M246.2,261.6c-5.7,0-10.3,4.6-10.3,10.3s4.6,10.3,10.3,10.3s10.3-4.6,10.3-10.3S251.8,261.6,246.2,261.6L246.2,261.6z"/>
							</g>
							<circle fill="#1D9FDA" cx="246.2" cy="271.9" r="3.4"/>
						</g>
						<g id="boston" class="world-map__marker js-marker">
							<circle fill="transparent" cx="259.8" cy="253.5" r="8.2"/>
							<g>
								<path fill="#1D9FDA" d="M259.8,246.3c4,0,7.3,3.3,7.3,7.3c0,4-3.3,7.3-7.3,7.3c-4,0-7.3-3.3-7.3-7.3
									C252.5,249.5,255.7,246.3,259.8,246.3 M259.8,243.3c-5.7,0-10.3,4.6-10.3,10.3c0,5.7,4.6,10.3,10.3,10.3
									c5.7,0,10.3-4.6,10.3-10.3C270,247.9,265.4,243.3,259.8,243.3L259.8,243.3z"/>
							</g>
							<circle fill="#1D9FDA" cx="259.8" cy="253.5" r="3.4"/>
						</g>
						<g id="uk" class="world-map__marker js-uk-marker">
							<circle fill="transparent" cx="452" cy="202.3" r="8.2"/>
							<g>
								<path fill="#1D9FDA" d="M452,195c4,0,7.3,3.3,7.3,7.3s-3.3,7.3-7.3,7.3s-7.3-3.3-7.3-7.3S448,195,452,195 M452,192
									c-5.7,0-10.3,4.6-10.3,10.3c0,5.7,4.6,10.3,10.3,10.3s10.3-4.6,10.3-10.3C462.3,196.6,457.7,192,452,192L452,192z"/>
							</g>
							<circle fill="#1D9FDA" cx="452" cy="202.3" r="3.4"/>
						</g>
					</g>
				</svg>
			</div>
		</div>
		<div class="world-map__info js-world-map-info">
			<div class="service-text-collection">
				<h2 class="motif fw-semibold fs-xl fc-dark-blue world-map__heading">International partner datacentre footprint</h2>
				<div class="service-text-collection__copy">
					<p>We have selected our International partners based on a number of critical factors when delivering a consistent national and international customer experience to our customers.</p>
					<p>
						Our footprint ensures a wide choice in international carriers and highly available low latency multi-home network configurations, backed by high quality local network support and monitoring.
					</p>
					<ul class="location-list location-list--two">
						<li><a href="/wp-content/uploads/2017/10/Data-Sheet_Amsterdam.pdf" target="_blank">Amsterdam</a></li>
						<li><a href="/wp-content/uploads/2017/10/Data-Sheet_Frankfurt.pdf" target="_blank">Frankfurt</a></li>
						<li><a href="/wp-content/uploads/2017/10/Data-Sheet_Boston.pdf" target="_blank">Boston</a></li>
						<li><a href="/wp-content/uploads/2017/10/Data-Sheet_NewYork.pdf" target="_blank">New York</a></li>
						<li><a href="/wp-content/uploads/2017/10/Data-Sheet_Dallas.pdf" target="_blank">Dallas</a></li>
						<li><a href="/wp-content/uploads/2017/10/Data-Sheet_HongKong.pdf" target="_blank">Hong Kong</a></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
</section>
	
<?php

if ( have_rows( 'elements' ) ) :
	$global_cta_printed = false;
	while ( have_rows( 'elements' ) ) : the_row();
		$layout = get_row_layout();
		get_template_part( 'inc/components/' . $layout );
		if ( $global_cta && ! $global_cta_printed && $global_cta_location == 'all' && strpos($layout, 'hero') !== false ) {
			get_template_part( 'inc/components/global_cta' );
			$global_cta_printed = true;
		}
	endwhile;
endif;

get_template_part( 'inc/partials/footer' );


<?php
define( "THEME_DIR", get_template_directory_uri() );
add_theme_support( 'post-thumbnails' );
//add_theme_support( 'post-formats', array( 'video' ) );

// Load in Hubspot form support
require_once $_SERVER['DOCUMENT_ROOT'] . '/api/hubspot.php';

/**
 *  Include all custom post type includes
 */
foreach ( glob( get_template_directory() . "/inc/post_types/*_post_type.php" ) as $filename ) {
	include_once( $filename );
}

/**
 * Define the menus for the theme
 */
function register_pulsant_menus() {
	register_nav_menus(
		array(
			'header-menu'   => __( 'Header Menu' ),
			'footer-menu-1' => __( 'Footer Menu (Useful Links)' ),
			'footer-menu-2' => __( 'Footer Menu (Knowledge Hub)' ),
		)
	);

	require_once( 'inc/nav_walker.php' );
}

/**
 *  Register custom website section taxonomy for post types
 */
function themes_taxonomy() {
	/*		register_taxonomy(
				'resource_type',
				array(
					'post',
				),        //post type name
				array(
					'hierarchical' => true,
					'label'        => 'Resource Type',  //Display name
					'query_var'    => TRUE,
					'rewrite'      => array(
						'slug'       => 'resource', // This controls the base slug that will display before each term
						'with_front' => FALSE // Don't display the category base before
					)
				)
			);*/
}

/**
 *
 */
function pulsant_menu_classes( $classes, $item, $args ) {
	if ( $args->menu == 3 || $args->menu == 4 ) {
		$classes = array( 'pb-s' );
	}

	return $classes;
}

/**
 * This is easier than a custom walker to just add a class to footer menu links
 */
function pulsant_menu_link_classes( $nav_menu, $args ) {
	if ( $args->menu > 2 ) {
		return preg_replace( '/<a /', '<a class="no-underline"', $nav_menu );
	} else {
		return $nav_menu;
	}
}

/**
 * Add native SVG support
 */
function svg_mime_type( $mimes = array() ) {
	$mimes['svg']  = 'image/svg+xml';
	$mimes['svgz'] = 'image/svg+xml';

	return $mimes;
}

/**
 * Add custom JS and CSS to the theme
 */
function register_pulsant_scripts() {
	wp_enqueue_style( 'pulsant', THEME_DIR . '/css/combined.min.css', array(), base_convert( filemtime( get_theme_file_path() . '/css/combined.min.css' ), 10, 36 ) );

	if ( strpos( $_SERVER["HTTP_HOST"], 'pulsant.com' ) !== FALSE ) {
		wp_enqueue_script( 'pulsant-combined', THEME_DIR . '/js/combined.min.js', array(), base_convert( filemtime( get_theme_file_path() . '/js/combined.min.js' ), 10, 36 ), TRUE );
	} else {
		wp_enqueue_script( 'tweenmax-js', THEME_DIR . '/js/TweenMax.min.js', array(), '1.0.0', TRUE );
		wp_enqueue_script( 'drawsvg-js', THEME_DIR . '/js/DrawSVGPlugin.min.js', array(), '1.0.0', TRUE );
		wp_enqueue_script( 'morphsvg-js', THEME_DIR . '/js/MorphSVGPlugin.min.js', array(), '1.0.0', TRUE );
		wp_enqueue_script( 'jquery-js', THEME_DIR . '/js/jquery-1.12.0.min.js', array(), '1.12.0', TRUE );
		wp_enqueue_script( 'parsley-js', THEME_DIR . '/js/parsley.min.js', array(), '1.1.0', TRUE );
		wp_enqueue_script( 'velocity', THEME_DIR . '/js/velocity.min.js', array(), '1.0.0', TRUE );
		wp_enqueue_script( 'webpacked-js', THEME_DIR . '/js/project.webpacked.js', array(), base_convert( filemtime( get_theme_file_path() . '/js/project.webpacked.js' ), 10, 36 ), TRUE );
	}

	if ( is_page_template( 'page-templates/page-timeline.php' ) ) {
		wp_enqueue_script( 'timeline', THEME_DIR . '/js/timeline.js', array(), NULL );
		wp_enqueue_style( 'timeline', THEME_DIR . '/css/timeline.css', array(), NULL );
	}

}

/**
 * Insert the image column at the start of a table list
 */
function add_acf_columns( $columns ) {
	$column_thumbnail = array( 'image' => 'Image' );
	$columns          = array_slice( $columns, 0, 1, TRUE ) + $column_thumbnail + array_slice( $columns, 1, NULL, TRUE );

	return $columns;
}

function add_acf_columns2( $columns ) {
	return array_merge( $columns, array(
		'blog_type' => __( 'Type' ),
	) );
}

/**
 * Create image thumb for posts
 */
function custom_column_display( $column ) {
	global $post;
	switch ( $column ) {
		case 'image':
			$image = get_field( 'image' );
			echo '<img src="' . $image['url'] . '" width="100" />';
			break;
	}
}


function post_filter_blog_type( $query ) {
	global $pagenow;
	$type = 'post';

	if ( isset( $_GET['post_type'] ) ) {
		$type = $_GET['post_type'];
	}
	if ( 'post' == $type && is_admin() && $pagenow == 'edit.php' && isset( $_GET['acf_filter_type'] ) && $_GET['acf_filter_type'] != '' ) {
		$query->query_vars['meta_key']   = 'type';
		$query->query_vars['meta_value'] = $_GET['acf_filter_type'];
	}
}

function admin_posts_filter_blog_type() {
	$type = 'post';
	if ( isset( $_GET['post_type'] ) ) {
		$type = $_GET['post_type'];
	}

	if ( 'post' == $type ) {
		$type_list = get_field_object( 'field_599fff5dc04db' );
		$type_list = $type_list['choices'];

		?>
		<select name="acf_filter_type">
			<option value=""><?php _e( '- Post Type -', 'wose45436' ); ?></option>
			<?php
			$current_v = isset( $_GET['acf_filter_type'] ) ? $_GET['acf_filter_type'] : '';
			foreach ( $type_list as $value => $label ) {
				printf
				(
					'<option value="%s"%s>%s</option>',
					$value,
					$value == $current_v ? ' selected="selected"' : '',
					$label
				);
			}
			?>
		</select>
		<?php
	}
}

/*
* Add columns to post list
*/
function post_list_custom_column( $column, $post_id ) {
	switch ( $column ) {
		case 'blog_type':
			$type = get_field( 'type', $post_id );
			echo $type['label'];
			break;
	}
}

/**
 * Latest ACF plugin breaks the modal stylised dropdowns - this pushes it up to the front
 */
function acf_modal_fix() {
	echo '<style>
    span[class*="-container--open"] {
    z-index: 999999!important;
    }
  </style>';
}

/**
 * Remove comments from the CMS
 */
function remove_menus() {
	remove_menu_page( 'edit-comments.php' );
}

/**
 * Ajax callback for SVGs in the media browser
 */
function get_attachment_url_media_library() {

	$url          = '';
	$attachmentID = isset( $_REQUEST['attachmentID'] ) ? $_REQUEST['attachmentID'] : '';

	if ( $attachmentID ) {
		$url = wp_get_attachment_url( $attachmentID );
	}

	die( $url );
}

/**
 * Remove Yoast from custom post types
 */
function remove_yoast_metabox_custom_posts() {
	remove_meta_box( 'wpseo_meta', 'cta', 'normal' );
	remove_meta_box( 'wpseo_meta', 'faq', 'normal' );
	remove_meta_box( 'wpseo_meta', 'resource', 'normal' );
	remove_meta_box( 'wpseo_meta', 'svg', 'normal' );
	remove_meta_box( 'wpseo_meta', 'hs_form', 'normal' );
}

/**
 * Re-generate the json for the search keywords file
 */
function update_json( $post_id ) {
	$file = get_home_path() . 'data/search-data.json';
	$args = array(
		'offset'      => 0,
		'post_type'   => 'page',
		'post_status' => 'publish'
	);

	// Fetch URL to prime cache
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, get_permalink( $post_id ));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($ch);
	curl_close($ch);

	// Fetch home to prime cache
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, get_home_url());
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($ch);
	curl_close($ch);

	if ( $pages = get_pages( $args ) ) {
		$page_data = array();

		foreach ( $pages as $page ) {
			$url = parse_url(get_permalink( $page->ID ));

			$page_data[] = array(
				'id'    => $url['path'],
				'title' => $page->post_title,
				'terms' => $page->post_title . ' ' . get_field( 'keywords', $page->ID ),
			);
		}

		$data = json_encode( $page_data, JSON_UNESCAPED_SLASHES );
		file_put_contents( $file, $data );
	}
}

function knowledgehub_url_rewrite() {
	global $wp_rewrite;
	add_rewrite_tag( '%hubcat%', '([^&]+)' );
	add_rewrite_tag( '%type%', '([^&]+)' );

	add_rewrite_rule( '^knowledge-hub/page/([0-9]+)/?', 'index.php?page_id=609&paged=$matches[1]', 'top' );
	add_rewrite_rule( '^knowledge-hub/category/([a-z\-0-9]+)/page/([0-9]+)/?', 'index.php?page_id=609&hubcat=$matches[1]&paged=$matches[2]', 'top' );
	add_rewrite_rule( '^knowledge-hub/category/([a-z\-0-9]+)/?', 'index.php?page_id=609&hubcat=$matches[1]', 'top' );

	$type_list = get_field_object( 'field_599fff5dc04db' );

	if ( $type_list ) {
		foreach ( $type_list['choices'] as $k => $type ) {
			add_rewrite_rule( '^knowledge-hub/' . $k . '/page/([0-9]+)/?', 'index.php?page_id=609&type=' . $k . '&paged=$matches[1]', 'top' );
			add_rewrite_rule( '^knowledge-hub/' . $k . '$', 'index.php?page_id=609&type=' . $k, 'top' );
		}
	}

	$wp_rewrite->flush_rules( TRUE );
}

function posts_link_attributes_next() {
	return 'rel="next"';
}
function posts_link_attributes_prev() {
	return 'rel="prev"';
}

function add_taxonomies_to_pages() {
	register_taxonomy_for_object_type( 'category', 'page' );
}

function add_query_vars_filter( $vars ) {
	$vars[] = "hubcat";
	$vars[] = "type";

	return $vars;
}

function kh_seo_title( $title ) {

	$type = get_query_var( 'type', NULL );
	if ( $type && is_page_template('page-templates/page-hub.php') ) {
		$term = get_term_by( 'slug', $type, 'category' );
		if ( $term && $meta = get_option( 'wpseo_taxonomy_meta' ) ) {
			$meta = $meta['category'];
			if ( isset( $meta[ $term->term_id ] ) ) {
				$title = $meta[ $term->term_id ]['wpseo_title'];
			}
		}
	}

	return $title;
}

function kh_seo_desc( $keywords ) {
	$type = get_query_var( 'type', NULL );
	if ( $type && is_page_template('page-templates/page-hub.php') ) {
		$term = get_term_by( 'slug', $type, 'category' );

		if ( $term && $meta = get_option( 'wpseo_taxonomy_meta' ) ) {
			$meta = $meta['category'];
			if ( isset( $meta[ $term->term_id ] ) ) {
				$keywords = $meta[ $term->term_id ]['wpseo_desc'];
			}
		}
	}

	return $keywords;
}

function custom_menu_order( $__return_true ) {

	return array(
		'index.php', // Dashboard
		'edit.php?post_type=page',
		'edit.php', // Posts
		'link-manager.php', // Links
		'edit-comments.php', // Comments
		'separator1', // Second separator
		'edit.php?post_type=video_article',
		'edit.php?post_type=whitepaper',
		'edit.php?post_type=faq',
		'edit.php?post_type=cta',
		'edit.php?post_type=logo',
		'edit.php?post_type=svg',
		'edit.php?post_type=hs_form',
		'separator2', // Second separator
		'upload.php', // Media
		'options-general.php', // Settings
		'users.php', // Users
		'plugins.php', // Plugins
		'themes.php', // Appearance
		'tools.php', // Tools
		'separator-last', // Last separator
	);
}

function df_terms_clauses( $clauses, $taxonomy, $args ) {
	if ( ! empty( $args['post_type'] ) ) {
		global $wpdb;
		$post_types = array();
		foreach ( $args['post_type'] as $cpt ) {
			$post_types[] = "'" . $cpt . "'";
		}

		if ( ! empty( $post_types ) ) {
			$clauses['fields'] = 'DISTINCT ' . str_replace( 'tt.*', 'tt.term_taxonomy_id, tt.term_id, tt.taxonomy, tt.description, tt.parent', $clauses['fields'] ) . ', COUNT(t.term_id) AS count';
			$clauses['join'] .= ' INNER JOIN ' . $wpdb->term_relationships . ' AS r ON r.term_taxonomy_id = tt.term_taxonomy_id INNER JOIN ' . $wpdb->posts . ' AS p ON p.ID = r.object_id';
			$clauses['where'] .= ' AND p.post_type IN (' . implode( ',', $post_types ) . ')';
			$clauses['orderby'] = 'GROUP BY t.term_id ' . $clauses['orderby'];
		}
	}

	return $clauses;
}


// Backend
// Customise admin content lists columns
add_filter( 'manage_cta_posts_columns', 'add_acf_columns' );
add_filter( 'manage_resource_posts_columns', 'add_acf_columns' );
add_filter( 'upload_mimes', 'svg_mime_type' );
add_filter( 'custom_menu_order', 'custom_menu_order' ); // Activate custom_menu_order
add_filter( 'menu_order', 'custom_menu_order' );

// custom filters for ACF field in post
add_filter( 'manage_posts_columns', 'add_acf_columns2' );
add_filter( 'parse_query', 'post_filter_blog_type' );

add_action( 'init', 'themes_taxonomy' );
add_action( 'init', 'add_taxonomies_to_pages' );
add_action( 'admin_head', 'acf_modal_fix' );
add_action( 'admin_menu', 'remove_menus' );
add_action( 'manage_posts_custom_column', 'custom_column_display', 10, 1 );
add_action( 'manage_posts_custom_column', 'post_list_custom_column', 10, 2 );
add_action( 'restrict_manage_posts', 'admin_posts_filter_blog_type' );

add_action( 'wp_ajax_svg_get_attachment_url', 'get_attachment_url_media_library' );
add_action( 'add_meta_boxes', 'remove_yoast_metabox_custom_posts', 11 );
add_action( 'save_post', 'update_json' );

// Front end
add_filter( 'nav_menu_css_class', 'pulsant_menu_classes', 1, 3 );
add_filter( 'wp_nav_menu', 'pulsant_menu_link_classes', PHP_INT_MAX, 2 );
add_filter( 'query_vars', 'add_query_vars_filter' );
add_filter( 'terms_clauses', 'df_terms_clauses', 10, 3 );

add_action( 'init', 'knowledgehub_url_rewrite', 10, 0 );
add_action( 'init', 'register_pulsant_menus' );
add_action( 'wp_enqueue_scripts', 'register_pulsant_scripts' );

add_filter( 'wpseo_title', 'kh_seo_title', 10, 1 );
add_filter( 'wpseo_metadesc', 'kh_seo_desc', 100, 1 );

add_filter('next_posts_link_attributes', 'posts_link_attributes_next');
add_filter('previous_posts_link_attributes', 'posts_link_attributes_prev');

add_image_size( 'thumb-button', 345, 200, TRUE );
add_image_size( 'gallery-image', 675, 400, TRUE );
add_image_size( 'gallery-image-2x', 1350, 800, TRUE );
add_image_size( 'hub-thumb', 302, 222, TRUE );
add_image_size( 'resource-thumb', 583, 392, TRUE );

remove_action( 'wp_head', 'wp_generator' );

if ( ! is_admin() ) {
	wp_deregister_script( 'jquery' );
} else {
	wp_enqueue_script( 'svg', THEME_DIR . '/js/admin_svg.js', array(), '1.0.0', TRUE );
}

if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page( array(
		'page_title' => 'Global CTA Settings',
		'menu_title' => 'Global CTA',
		'menu_slug'  => 'global-cta',
		'capability' => 'edit_posts',
		'redirect'   => FALSE
	) );

}

function custom_pagination( $numpages = '', $pagerange = '', $paged = '' ) {

	if ( empty( $pagerange ) ) {
		$pagerange = 2;
	}

	global $paged;
	if ( empty( $paged ) ) {
		$paged = 1;
	}
	if ( $numpages == '' ) {
		global $wp_query;
		$numpages = $wp_query->max_num_pages;
		if ( ! $numpages ) {
			$numpages = 1;
		}
	}

	$pagination_args = array(
		'base'         => get_pagenum_link( 1 ) . '%_%',
		'format'       => 'page/%#%',
		'total'        => $numpages,
		'current'      => $paged,
		'show_all'     => FALSE,
		'end_size'     => 1,
		'mid_size'     => $pagerange,
		'prev_next'    => FALSE,
		'prev_text'    => __( '&laquo;' ),
		'next_text'    => __( '&raquo;' ),
		'type'         => 'plain',
		'add_args'     => FALSE,
		'add_fragment' => ''
	);

	$paginate_links = paginate_links( $pagination_args );

	if ( $paginate_links ) {
		echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
		echo $paginate_links;
	}

}

// sub-nav functions used for the service selector

function get_nav_menu_item_children( $parent_id, $nav_menu_items, $depth = TRUE ) {
	$nav_menu_item_list = array();
	foreach ( (array) $nav_menu_items as $nav_menu_item ) {
		if ( $nav_menu_item->menu_item_parent == $parent_id ) {
			$nav_menu_item_list[] = $nav_menu_item;
			if ( $depth ) {
				if ( $children = get_nav_menu_item_children( $nav_menu_item->ID, $nav_menu_items ) ) {
					$nav_menu_item_list = array_merge( $nav_menu_item_list, $children );
				}
			}
		}
	}

	return $nav_menu_item_list;
}

function list_sub_nav( $main_parent ) {
	$nav_items = wp_get_nav_menu_items( 'header-menu' );
	if ( $sub_list = get_nav_menu_item_children( $main_parent, $nav_items, 3 ) ) {

		$menu_list = array();

		foreach ( $nav_items as $tl ) {
			if ( $tl->ID == $main_parent ) {
				$menu_list['parent'] = array(
					'title' => $tl->title,
					'url'   => $tl->url,
				);
				break;
			}
		}

		foreach ( $sub_list as $item ) {

			// Hide AMP and Layer V links from service aelector
			// TODO: make this less rubbish - maybe pass an array of items to ignore when using the list_sub_nav function?
			if ( strpos( $item->title, 'AMP' ) === FALSE && strpos( $item->title, 'Layer V' ) === FALSE ) {

				if ( $item->menu_item_parent == $main_parent ) {
					$menu_list['items'][ $item->ID ] = array(
						'title' => $item->title,
						'url'   => $item->url,
					);
				} else {
					$menu_list['items'][ $item->menu_item_parent ]['sub_pages'][ $item->ID ] = array(
						'title' => $item->title,
						'url'   => $item->url,
					);
				}

			}
		}
	} else {
		// no sub items, still return parent
		if ( $nav_items ) {
			foreach ( $nav_items as $item ) {
				if ( $item->ID == $main_parent ) {
					$menu_list = array(
						'parent' => array(
							'title' => $item->title,
							'url'   => $item->url,
						),
					);
					break;
				}
			}
		}

	}

	return $menu_list;
}
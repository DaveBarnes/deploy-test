<?php
/*
Template Name: AMP - Technology
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">

  <title>Technology that Powers AMP: Azure & Azure Stack | Pulsant</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en" />
<meta name="robots" content="index, follow">

<link rel="icon" type="img/png" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/img/pulsant-favicon-16x16.png">
<link rel="apple-touch-icon" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">

<meta name="description" content="Microsoft Azure Stack is an extension of Azure, bringing the agility and fast-paced innovation of cloud computing to on-premises or Pulsant infrastructure.">
<meta name="author" content="Pulsant" />

<meta property="og:url" content="" />
<meta property="og:site_name" content="Pulsant" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Technology that Powers AMP: Azure & Azure Stack | Pulsant" />
<meta property="og:image" content="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />
<meta property="og:description" content="Microsoft Azure Stack is an extension of Azure, bringing the agility and fast-paced innovation of cloud computing to on-premises or Pulsant infrastructure." />

<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="Microsoft Azure Stack is an extension of Azure, bringing the agility and fast-paced innovation of cloud computing to on-premises or Pulsant infrastructure."/>
<meta name="twitter:title" content="Technology that Powers AMP: Azure & Azure Stack | Pulsant"/>
<meta name="twitter:site" content="@pulsantuk"/>
<meta name="twitter:creator" content="@pulsantuk"/>

  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/vendor.min.css">
  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/style.min.css">
  <script src=" https://use.typekit.net/gkc7sbg.js "></script>
  <script>try{Typekit.load({ async: false });}catch(e){}</script>
  <style>
        .main-info-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd.jpg);
        }
        .main-info-bg.isv-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd.jpg);
        }
        .main-info-bg.road-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd.jpg);
        }
        .main-info-bg.boost-product-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd.jpg);
        }
        .main-info-bg.scaleup-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd.jpg);
        }
        .main-info-bg.staytuned-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd.jpg);
        }
        .main-info-bg.boost-business-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd.jpg);
        }
        .main-info-bg.performance-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd.jpg);
        }
        .main-info-bg.try-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd.jpg);
        }
        .modal-block-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd.jpg);
        }
        .footer{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd.jpg);
        }
        @media (-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd-2x.jpg);
            }
        }
        @media screen and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:1280px), (min-resolution: 144dpi) and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape-2x.jpg);
            }
        }
        @media screen and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:980px), (min-resolution: 144dpi) and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet-2x.jpg);
            }
        }
    </style>
</head>

<body>
  <?php $lighttheme = "light-theme" ?>
  <?php $roadpage = "" ?>
  <div id="wrapper">
    <?php include("inc/header.php"); ?>
    <main class="main">
        <section class="main-info main-info-bg light-theme tech-page">
            <header class="meta">
                <h1 class="big-title">THE TECHNOLOGY</h1>
                <p>that powers AMP</p>
            </header>
        </section>
        <section class="tech-block">
            <div class="container">
                <header class="meta">
                    <div class="text-hold">
                        <h1 class="heading">Azure</h1>
                        <p>Microsoft Azure is a growing collection of cloud services that enables developers and IT professionals to develop, build, deploy and manage applications on a globally available cloud network, supporting the tools, applications and frameworks of your choice.</p>
                    </div>
                    <div class="logo-hold">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo-azure.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo-azure-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo-azure.png" alt="Microsoft Azure">
                    </div>
                </header>
                <strong class="title">Popular services in Azure</strong>
                <div class="items-wrap">
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico17.svg" width="69" height="66" alt="image description">
                        </div>
                        <strong class="min-heading">Virtual machines</strong>
                        <p>Provision Windows and Linux virtual machines in seconds</p>
                    </div>
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico18.svg" width="69" height="69" alt="image description">
                        </div>
                        <strong class="min-heading">App Service</strong>
                        <p>Deploy web apps on Linux using containers</p>
                    </div>
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico19.svg" width="69" height="62" alt="image description">
                        </div>
                        <strong class="min-heading">SQL Database</strong>
                        <p>Managed relational SQL Database-as- a -service</p>
                    </div>
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico20.svg" width="69" height="61" alt="image description">
                        </div>
                        <strong class="min-heading">Storage</strong>
                        <p>Managed relational SQL Database-as- a -service</p>
                    </div>
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico21.svg" width="69" height="68" alt="image description">
                        </div>
                        <strong class="min-heading">Cloud Services</strong>
                        <p>Create highly-available, infinitely-scalable cloud applications and APIs</p>
                    </div>
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico22.svg" width="69" height="69" alt="image description">
                        </div>
                        <strong class="min-heading">Azure Cosmos DB</strong>
                        <p>Try Azure Cosmos DB for a globally distributed, multi-model database</p>
                    </div>
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico23.svg" width="69" height="66" alt="image description">
                        </div>
                        <strong class="min-heading">Azure Active Directory</strong>
                        <p>Synchronize on-premises directories and enable single sign-on</p>
                    </div>
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico24.svg" width="69" height="63" alt="image description">
                        </div>
                        <strong class="min-heading">Backup</strong>
                        <p>Simple and reliable server backup to the cloud</p>
                    </div>
                </div>
                <div class="link-holder">
                    <a href="https://azure.microsoft.com/en-gb/services/" target="_blank" class="link">Discover all services on the Azure website <span class="arrow"><span class="sr-only">arrow</span></span></a>
                </div>
            </div>
        </section>
        <section class="tech-block inverse">
            <div class="container">
                <header class="meta">
                    <div class="text-hold">
                        <h1 class="heading">Azure Stack</h1>
                        <p>Microsoft Azure Stack is an extension of Azure, bringing the agility and fast-paced innovation of cloud computing to on-premises or Pulsant infrastructure. Organisations can deploy applications across hybrid cloud environments, balancing the right amount of flexibility and control.</p>
                    </div>
                    <div class="logo-hold">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo-azure-stack.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo-azure-stack-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo-azure-stack.png" alt="Microsoft Azure">
                    </div>
                </header>
                <strong class="title">Services available in Azure Stack</strong>
                <div class="items-wrap">
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico25.svg" width="116" height="105" alt="image description">
                        </div>
                        <strong class="min-heading">Azure Services</strong>
                        <ul class="list-services">
                            <li>IaaS – Compute, Storage, Network</li>
                            <li>PaaS – Storage (blobs, tables, queues)</li>
                            <li>Key Vault</li>
                            <li>App Service: Web Apps, Mobile Apps, API Apps</li>
                            <li>Functions</li>
                            <li>MySQL RP</li>
                            <li>SQL Server RP</li>
                        </ul>
                    </div>
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico26.svg" width="115" height="121" alt="image description">
                        </div>
                        <strong class="min-heading">Azure Marketplace Content - Key IaaS workloads</strong>
                        <ul class="list-services">
                            <li>Microsoft SQL</li>
                            <li>Mesos template</li>
                            <li>Cloud Foundry template</li>
                            <li>Blockchain template</li>
                            <li>More solutions from the Azure Marketplace</li>
                        </ul>
                    </div>
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico27.svg" width="115" height="118" alt="image description">
                        </div>
                        <strong class="min-heading">Tool support</strong>
                        <ul class="list-services">
                            <li>Visual Studio</li>
                            <li>PowerShell</li>
                            <li>Azure CLI</li>
                            <li>Operations Management Suite guest extension</li>
                        </ul>
                    </div>
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico28.svg" width="115" height="118" alt="image description">
                        </div>
                        <strong class="min-heading">Azure Marketplace Content - Images and extensions</strong>
                        <ul class="list-services">
                            <li>LINUX: RedHat, SuSE</li>
                            <li>Windows Server</li>
                            <li>Azure Docker Extension</li>
                            <li>DSC Extension</li>
                            <li>Chef</li>
                        </ul>
                    </div>
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico29.svg" width="115" height="111" alt="image description">
                        </div>
                        <strong class="min-heading">System Capabilities</strong>
                        <ul class="list-services">
                            <li>Monitoring and diagnostics</li>
                            <li>Security and privacy</li>
                            <li>Patching and updating</li>
                            <li>Field replacement of parts</li>
                            <li>3rd party management integration</li>
                        </ul>
                    </div>
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico30.svg" width="115" height="107" alt="image description">
                        </div>
                        <strong class="min-heading">Scale</strong>
                        <ul class="list-services">
                            <li>4-12 nodes (physical servers);</li>
                            <li>Single Region, Single Scale Unit</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="partner-block">
            <div class="container">
                <div class="item">
                    <div class="logo-holder">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/partner1.png, <?php bloginfo('template_url'); ?>/amp/assets/images/partner1-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/partner1.png" alt="image description">
                    </div>
                    <p>Make teams collaborate effectively and work anywhere they want. Office 365 is the premiere productivity suite to allow organisations to work in modern ways. Available with Pulsant in hybrid deployments, so you can keep your data close.</p>
                </div>
                <div class="item">
                    <div class="logo-holder">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/partner2.png, <?php bloginfo('template_url'); ?>/amp/assets/images/partner2-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/partner2.png" alt="image description">
                    </div>
                    <p>We’re proud to partner with Dell EMC to deliver enterprise grade server technology and the network stack that powers the hybrid cloud. Purpose-built for Microsoft Azure Stack.</p>
                </div>
            </div>
        </section>
        <section class="cloud-block tech-page">
            <div class="container">
                <div class="meta">
                    <h1 class="middle-title">Journey To The Cloud<br> and amplify your business</h1>
                    <p>From the discovery phase to design and deployment, our Journey To The Cloud offers services like complete assesments of your environments, providing test set-ups, technical consultancy, implementation and deployment to fully managed services like monitoring, support and reporting.</p>
                </div>
                <div class="inner">
                    <span class="line"><span class="circle"><span class="sr-only">line</span></span></span>
                    <div class="items-hold">
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud1.svg" width="123" height="160" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Foundation Services:<br>Readiness &AMP; Transformation</strong>
                                <ul class="link-list">
                                    <li>Cloud Readiness Assesment</li>
                                    <li>Cloud Licensing Advisory</li>
                                    <li>Azure benchmark services</li>
                                    <li>Azure Stack Proof of Concept</li>
                                    <li>Azure Everywhere Workshop</li>
                                    <li>Azure Risk Advisory</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud2.svg" width="175" height="158" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Consultancy Services:<br>Strategy &AMP; Technical Implementation</strong>
                                <ul class="link-list">
                                    <li>Digital Transformation Programme</li>
                                    <li>Migration</li>
                                    <li>Technical Implementation</li>
                                    <li>Cloud Management Stack</li>
                                    <li>Azure Specialist Service</li>
                                    <li>Cloud Compliance</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud3.svg" width="169" height="172" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Managed Services</strong>
                                <ul class="link-list">
                                    <li>Service Management</li>
                                    <li>Monitoring &AMP; Reporting</li>
                                    <li>Protection &AMP; Security</li>
                                    <li>Patch &AMP; Backup management</li>
                                    <li>Identity &AMP; Access Management</li>
                                    <li>Continuous Compliance</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/amp/journey-to-cloud" class="btn-transparent">Journey To the Cloud</a>
            </div>
        </section>
        <section class="banner tech-page">
            <div class="img-wrap">
                <div class="img-holder">
                    <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/banner-img-tech.jpg, <?php bloginfo('template_url'); ?>/amp/assets/images/banner-img-tech-2x.jpg 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/banner-img-tech.jpg" alt="image description">
                </div>
            </div>
            <div class="text">
                <h1 class="title">Try Azure Stack today</h1>
                <p>A single one node Azure Stack solution to help you explore the possibilities.</p>
                <a href="/amp/one-node-as-a-service" class="btn-transparent">Discover how</a>
            </div>
        </section>
    </main>
    <?php include("inc/footer.php"); ?>
  </div>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/vendor.min.js" defer></script>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/app.js" defer></script>
</body>

</html>
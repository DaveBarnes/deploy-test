<?php
/*
Template Name: AMP - About
*/
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">

  <title>About Us | Pulsant AMP</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en" />
<meta name="robots" content="index, follow">

<link rel="icon" type="img/png" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/img/pulsant-favicon-16x16.png">
<link rel="apple-touch-icon" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">

<meta name="description" content="Pulsant has joined with Microsoft to create AMP - high-end managed hybrid cloud solutions based on Microsoft services.">
<meta name="author" content="Pulsant" />

<meta property="og:url" content="" />
<meta property="og:site_name" content="Pulsant" />
<meta property="og:type" content="website" />
<meta property="og:title" content="About Us | Pulsant AMP" />
<meta property="og:image" content="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />
<meta property="og:description" content="Pulsant has joined with Microsoft to create AMP - high-end managed hybrid cloud solutions based on Microsoft services." />

<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="Pulsant has joined with Microsoft to create AMP - high-end managed hybrid cloud solutions based on Microsoft services."/>
<meta name="twitter:title" content="About Us | Pulsant AMP"/>
<meta name="twitter:site" content="@pulsantuk"/>
<meta name="twitter:creator" content="@pulsantuk"/>

  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/vendor.min.css">
  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/style.min.css">
  <script src=" https://use.typekit.net/gkc7sbg.js "></script>
  <script>try{Typekit.load({ async: false });}catch(e){}</script>
  <style>
        .main-info-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd.jpg);
        }
        .main-info-bg.isv-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd.jpg);
        }
        .main-info-bg.road-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd.jpg);
        }
        .main-info-bg.boost-product-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd.jpg);
        }
        .main-info-bg.scaleup-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd.jpg);
        }
        .main-info-bg.staytuned-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd.jpg);
        }
        .main-info-bg.boost-business-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd.jpg);
        }
        .main-info-bg.performance-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd.jpg);
        }
        .main-info-bg.try-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd.jpg);
        }
        .modal-block-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd.jpg);
        }
        .footer{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd.jpg);
        }
        @media (-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd-2x.jpg);
            }
        }
        @media screen and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:1280px), (min-resolution: 144dpi) and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape-2x.jpg);
            }
        }
        @media screen and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:980px), (min-resolution: 144dpi) and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet-2x.jpg);
            }
        }
    </style>
</head>

<body>
  <?php $lighttheme = "light-theme" ?>
  <?php $roadpage = "" ?>
  <div id="wrapper">
    <?php include("inc/header.php"); ?>
    <main class="main">
        <section class="main-info main-info-bg light-theme about-page">
            <header class="meta">
                <h1 class="big-title">GROW AND THRIVE IN A CONSTANTLY CHANGING WORLD</h1>
            </header>
        </section>
        <section class="about-block">
            <div class="container">
                <div class="item">
                    <div class="text-part">
                        <strong class="bold-title">Technology has changed the face of business</strong>
                        <p>What began as mere tools to run a more efficient operation, transformed into a complete ecosystem of technology solutions and services that form the beating heart of business itself. It’s the companies that reinforce the shift towards strategic and Result Driven Technology that develops success.</p>
                    </div>
                    <div class="img-holder">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/about-img1.jpg, <?php bloginfo('template_url'); ?>/amp/assets/images/about-img1-2x.jpg 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/about-img1.jpg" alt="image description">
                    </div>
                </div>
                <div class="item">
                    <div class="text-part">
                        <strong class="bold-title">Fortunately, there’s help to make that crucial shift</strong>
                        <p>Pulsant has joined with Microsoft to create AMP - high-end managed hybrid cloud solutions based on Microsoft services - to amplify your business and generate true and meaningful results: Now, and in the future.</p>
                    </div>
                    <div class="img-holder">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/about-img2.jpg, <?php bloginfo('template_url'); ?>/amp/assets/images/about-img2-2x.jpg 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/about-img2.jpg" alt="image description">
                    </div>
                </div>
            </div>
        </section>
        <section class="cloud-block about-page">
            <div class="container approach">
                <div class="meta">
                    <h1 class="middle-title">Pulsant will help you in your transformation with our end-to-end approach</h1>
                </div>
                <div class="img-big-holder">
                    <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/about-approach.jpg, <?php bloginfo('template_url'); ?>/amp/assets/images/about-approach-2x.jpg 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/about-approach-2x.jpg" alt="image description">
                </div>
                <div class="inner">
                    <div class="items-hold">
                        <div class="item">
<!--
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/about-why-pulsant.svg" width="123" height="160" alt="image description">
                            </div>
-->
                            <div class="text">
                                <strong class="bold-title">Why choose Pulsant?</strong>
                                <ul class="link-list">
                                    <li>We are a leading UK mid-market service provider, helping over 3,500 organisations across the UK.</li>
                                    <li>We offer pragmatic transformation consultancy to enable you to take advantage of hybrid cloud technology.</li>
                                    <li>Experts in both datacentre &amp; Cloud infrastructure management.</li>
                                    <li>Continuous Security &amp; Compliance Monitoring to safeguard your organisation and comply to regulatory frameworks.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
<!--
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud3.svg" width="175" height="158" alt="image description">
                            </div>
-->
                            <div class="text">
                                <strong class="bold-title">Why choose Azure?</strong>
                                <ul class="link-list">
                                    <li>Extend your existing technology paths to Azure.</li>
                                    <li>Utilise a secure private stack for high-level security for you and your customers.</li>
                                    <li>Access to a global network of cloud services.</li>
                                    <li> Develop on the platform for next generation applications.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
<!--
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/img1.svg" width="169" height="172" alt="image description">
                            </div>
-->
                            <div class="text">
                                <strong class="bold-title">Why Pulsant for Azure?</strong>
                                <ul class="link-list">
                                    <li>We optimise Hybrid Azure to be fully configured to your needs</li>
                                    <li>Our expert teams assist with the legacy transformation to Azure</li>
                                    <li>End-to-end management &amp; SLAs to keep you running, at all times</li>
                                    <li>Continuous Compliance Platform to make sure you comply with regulatory frameworks.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="shadow-block">
            <div class="container">
                <div class="meta">
                    <h1 class="middle-title">Our characters</h1>
                </div>
                <div class="holder">
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico14.svg" width="143" height="143" alt="image description">
                        </div>
                        <strong class="title">We are agile</strong>
                        <p>From commercial advice to IT solutions, everything is tailored to fit our customers’ business needs. Flexibility and agility are the core of our character.</p>
                    </div>
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico15.svg" width="143" height="145" alt="image description">
                        </div>
                        <strong class="title">We act</strong>
                        <p>We’re a team of enthusiastic, hard working people with a ‘we can do, and do better’ attitude, fully committed to deliver bestin-class solutions.</p>
                    </div>
                    <div class="item">
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico16.svg" width="144" height="151" alt="image description">
                        </div>
                        <strong class="title">We’re knowledgeable</strong>
                        <p>We have a strong history in traditional IT and vast experience in large and complex Cloud infrastructure and the ambition to become a thought leader in Hybrid Cloud.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="logos-block about-page">
            <div class="container">
                <h1 class="middle-title">Accreditated by</h1>
                <ul class="logos-list">
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo1.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo1-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo1.png" alt="BSI ISO">
                    </li>
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo2.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo2-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo2.png" alt="BSI ISO">
                    </li>
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo3.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo3-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo3.png" alt="BSI ISO">
                    </li>
                </ul>
                <ul class="logos-list second">
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo4.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo4-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo4.png" alt="Digital Marketplace supplier">
                    </li>
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo5.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo5-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo5.png" alt="PCI">
                    </li>
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo6.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo6-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo6.png" alt="CSA START certification">
                    </li>
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo7.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo7-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo7.png" alt="the green grid member">
                    </li>
                </ul>
            </div>
        </section>
        <section class="cloud-block about-page">
            <div class="container">
                <div class="meta">
                    <h1 class="middle-title">Journey To The Cloud<br> and amplify your business</h1>
                    <p>From the discovery phase to design and deployment, our Journey To The Cloud offers services like complete assesments of your environments, providing test set-ups, technical consultancy, implementation and deployment to fully managed services like monitoring, support and reporting.</p>
                </div>
                <div class="inner">
                    <span class="line"><span class="circle"><span class="sr-only">line</span></span></span>
                    <div class="items-hold">
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud1.svg" width="123" height="160" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Foundation Services:<br>Readiness &AMP; Transformation</strong>
                                <ul class="link-list">
                                    <li>Cloud Readiness Assesment</li>
                                    <li>Cloud Licensing Advisory</li>
                                    <li>Azure benchmark services</li>
                                    <li>Azure Stack Proof of Concept</li>
                                    <li>Azure Everywhere Workshop</li>
                                    <li>Azure Risk Advisory</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud2.svg" width="175" height="158" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Consultancy Services:<br>Strategy &AMP; Technical Implementation</strong>
                                <ul class="link-list">
                                    <li>Digital Transformation Programme</li>
                                    <li>Migration</li>
                                    <li>Technical Implementation</li>
                                    <li>Cloud Management Stack</li>
                                    <li>Azure Specialist Service</li>
                                    <li>Cloud Compliance</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud3.svg" width="169" height="172" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Managed Services</strong>
                                <ul class="link-list">
                                    <li>Service Management</li>
                                    <li>Monitoring &AMP; Reporting</li>
                                    <li>Protection &AMP; Security</li>
                                    <li>Patch &AMP; Backup management</li>
                                    <li>Identity &AMP; Access Management</li>
                                    <li>Continuous Compliance</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/amp/journey-to-cloud" class="btn-transparent">Journey To the Cloud</a>
            </div>
        </section>
        <section class="banner about-page">
            <div class="img-wrap">
                <div class="img-holder">
                    <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/banner-img-scaleup.jpg, <?php bloginfo('template_url'); ?>/amp/assets/images/banner-img-scaleup-2x.jpg 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/banner-img-scaleup.jpg" alt="image description">
                </div>
            </div>
            <div class="text">
                <h1 class="title">Try Azure Stack today</h1>
                <p>A single one node Azure Stack solution to help you explore the possibilities.</p>
                <a href="/amp/one-node-as-a-service" class="btn-transparent">Discover how</a>
            </div>
        </section>
    </main>
    <?php include("inc/footer.php"); ?>
  </div>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/vendor.min.js" defer></script>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/app.js" defer></script>
</body>

</html>
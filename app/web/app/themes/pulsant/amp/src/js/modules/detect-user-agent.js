var dUserAgent = {
    dUserAgentInit: function() {
        var browser = navigator.userAgent.toLowerCase();
        if(browser.indexOf('firefox') > -1) {
            $('body').addClass('moz');
        } else if(navigator.userAgent.indexOf('Trident') != -1 && navigator.userAgent.indexOf('MSIE') == -1){
        	$('body').addClass('ms');
        }

    }

};

export default dUserAgent;
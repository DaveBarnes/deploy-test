var scrollDown = {
    scrollDownInit: function() {
        var $scrollDownBtn = $('.scroll-down'),
            $mainSection = $('.scroll-to');

        if (!$scrollDownBtn.length) {
            return;
        }

        $scrollDownBtn.on('click', function() {
            scrollDownHandler.call(this);
        });

        function scrollDownHandler(){
            var currentPosition = $mainSection.offset().top,
                prevElemMargin = parseInt($mainSection.prev().css('margin-bottom'));
            $('html, body').animate({
                scrollTop: currentPosition - prevElemMargin
            }, 400);
        }

    }

};

export default scrollDown;
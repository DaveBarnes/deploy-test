// You can write a call and import your functions in this file.
//
// This file will be compiled into app.js and will not be minified.
// Feel free with using ES6 here.

import burger from './modules/burger';
import submenu from './modules/submenu';
import validation from './modules/validation';
import dUserAgent from './modules/detect-user-agent';
import wowAnimate from './modules/wow-animate';
import footer from './modules/footer';
import accordion from './modules/accordion';
import scrollDown from './modules/scroll-down';

( ($) => {
  'use strict';

  // When DOM is ready
  $(() => {
    burger.burgerInit();
    submenu.submenuInit();
    validation.validationInit();
    dUserAgent.dUserAgentInit();
    wowAnimate.wowAnimateInit();
    accordion.accordionInit();
    scrollDown.scrollDownInit();
  });

  footer.footerInit();

})(jQuery);


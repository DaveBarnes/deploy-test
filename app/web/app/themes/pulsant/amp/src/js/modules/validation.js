var validation = {
    validationInit: function() {
        var $feedbackForm = $('.feedback-form');

        if (!$feedbackForm.length) {
            return;
        }
        $feedbackForm.each(function(){
            $(this).validate({
                errorPlacement: function(error, element) {
                    return false;
                },
                rules: {
                    name: {
                        required: true
                    },
                    company: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        number: true
                    }
                }
            });
        });
    }
};
export default validation;
var wowAnimate = {
    wowAnimateInit: function() {
        var $wowElem = $('.wow');

        if (!$wowElem.length) {
            return;
        }

        new WOW().init();

    }

};

export default wowAnimate;
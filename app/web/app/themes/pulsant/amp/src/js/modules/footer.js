var footer = {
    footerInit: function() {
        var $footerWrap = $('.footer');

        if (!$footerWrap.length) {
            return;
        }

        if($(window).width() > 767){
        	$footerWrap.footerReveal({
	            shadow: false
	        });
        }

    }

};

export default footer;
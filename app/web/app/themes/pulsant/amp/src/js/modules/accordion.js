var accordion = {
    accordionInit: function() {
        var $accordionLink = $('.accordion .ac-link'),
            heights = [];

        if (!$accordionLink.length) {
            return;
        }

        $accordionLink.on('click', function(e){
          e.preventDefault();
          showText.call(this);
        });

        function showText(){
          $(this).closest('.accordion .item').toggleClass('active');
          $(this).next().slideToggle(200);
        }

    }

};

export default accordion;
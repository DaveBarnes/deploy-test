var submenu = {
    submenuInit: function() {
        var $submenuItem = $('.navbar .subtitle > a'),
            $submenuWrap = $('.navbar .subtitle'),
            $header = $('.header');

        if (!$submenuItem.length) {
            return;
        }

        submenuEventsTrigger();
        $(window).resize(submenuEventsTrigger);

        function submenuEventsTrigger(){
            if($(window).width() < 1200){
                $submenuItem.off('click.submenu', handler);
                $submenuWrap.off('mouseenter.submenu', hoverInFunc);
                $submenuWrap.off('mouseleave.submenu', hoverOutFunc);
                $submenuItem.on('click.submenu', handler);
            } else {
                $submenuWrap.off('mouseenter.submenu', hoverInFunc);
                $submenuWrap.off('mouseleave.submenu', hoverOutFunc);
                $submenuItem.off('click.submenu', handler);
                $submenuWrap.on('mouseenter.submenu', hoverInFunc);
                $submenuWrap.on('mouseleave.submenu', hoverOutFunc);
            }
        }

        function hoverInFunc(){
            submenuOnHandler.call(this);
        }

        function hoverOutFunc(){
            submenuOffHandler.call(this);
        }

        function handler(e){
            e.preventDefault();
            submenuHandler.call(this);
        }

        function submenuHandler(){
            $(this).closest('.subtitle').toggleClass('current-menu-item');
            $(this).closest('.subtitle').find('ul').slideToggle(200).stop(true, true);
        }

        function submenuOnHandler(){
            $(this).closest('.subtitle').addClass('current-menu-item');
            $(this).closest('.subtitle').find('ul').slideDown(200).stop(true, true);
            $header.addClass('menu-open');
            $('.overlay').fadeIn(200);
        }

        function submenuOffHandler(){
            $(this).closest('.subtitle').removeClass('current-menu-item');
            $(this).closest('.subtitle').find('ul').slideUp(200).stop(true, true);
            $header.removeClass('menu-open');
            $('.overlay').fadeOut(200);
        }

    }

};

export default submenu;
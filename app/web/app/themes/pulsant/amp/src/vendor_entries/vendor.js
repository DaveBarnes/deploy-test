// All imports in this file will be compiled into vendors.js file.
//
// Note: ES6 support for these imports is not supported in base build

module.exports = [
  './node_modules/jquery/dist/jquery.js',
  './node_modules/jquery-validation/dist/jquery.validate.js',
  './node_modules/wowjs/dist/wow.min.js',
  './src/js/plugin/footer-reveal.js',
  './node_modules/bootstrap-sass/assets/javascripts/bootstrap/transition.js',
  './node_modules/bootstrap-sass/assets/javascripts/bootstrap/modal.js'
];
<?php
/*
Template Name: AMP - Azure LP
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">

  <title><?php echo get_the_title(); ?> - Pulsant AMP</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en" />
<meta name="robots" content="index, follow">

<link rel="icon" type="img/png" href="<?php bloginfo('template_url'); ?>/amp/img/pulsant-favicon-16x16.png">
<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/amp/apple-touch-icon.png" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">

<meta name="description" content="<?php if ( get_field('amp_azure_hero_subheading') ) { echo get_field('amp_azure_hero_subheading'); } ?>">
<meta name="author" content="Pulsant" />

<meta property="og:url" content="" />
<meta property="og:site_name" content="Pulsant" />
<meta property="og:type" content="website" />
<meta property="og:title" content="<?php echo get_the_title(); ?> - Pulsant AMP" />
<meta property="og:image" content="<?php bloginfo('template_url'); ?>/amp/apple-touch-icon.png" />
<meta property="og:description" content="<?php if ( get_field('amp_azure_hero_subheading') ) { echo get_field('amp_azure_hero_subheading'); } ?>" />

<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="<?php if ( get_field('amp_azure_hero_subheading') ) { echo get_field('amp_azure_hero_subheading'); } ?>"/>
<meta name="twitter:title" content="<?php echo get_the_title(); ?> - Pulsant AMP"/>
<meta name="twitter:site" content="@pulsantuk"/>
<meta name="twitter:creator" content="@pulsantuk"/>

  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/vendor.min.css">
  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/style.min.css">
  <script src=" https://use.typekit.net/gkc7sbg.js "></script>
  <script>try{Typekit.load({ async: false });}catch(e){}</script>

    <?php
        $amp_azure_hero_banner_image = get_field('amp_azure_hero_banner_image');

        if( !empty($amp_azure_hero_banner_image) ) {

            // thumbnail
            $hd = 'hero-azure-hd';
            $hd_2x = 'hero-azure-hd-2x';
            $landscape = 'hero-azure-landscape';
            $landscape_2x = 'hero-azure-landscape-2x';
            $tablet = 'hero-azure-tablet';
            $tablet_2x = 'hero-azure-tablet-2x';

            $hero_azure_hd = $amp_azure_hero_banner_image['sizes'][ $hd ];
            $hero_azure_hd_2x = $amp_azure_hero_banner_image['sizes'][ $hd_2x ];
            $hero_azure_landscape = $amp_azure_hero_banner_image['sizes'][ $landscape ];
            $hero_azure_landscape_2x = $amp_azure_hero_banner_image['sizes'][ $landscape_2x ];
            $hero_azure_tablet = $amp_azure_hero_banner_image['sizes'][ $tablet ];
            $hero_azure_tablet_2x = $amp_azure_hero_banner_image['sizes'][ $tablet_2x ];

        }

    ?>
  <style>
        .main-info-bg.hero-img{
            background-image: url(<?php echo $hero_azure_hd; ?>);
        }
        .modal-block-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd.jpg);
        }
        .footer{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd.jpg);
        }
        @media (-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi) {
            .main-info-bg.hero-img{
                background-image: url(<?php echo $hero_azure_hd_2x; ?>);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd-2x.jpg);
            }
        }
        @media screen and (max-width:1280px) {
            .main-info-bg.hero-img{
                background-image: url(<?php echo $hero_azure_landscape; ?>);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:1280px), (min-resolution: 144dpi) and (max-width:1280px) {
            .main-info-bg.hero-img{
                background-image: url(<?php echo $hero_azure_landscape_2x; ?>);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape-2x.jpg);
            }
        }
        @media screen and (max-width:980px) {
            .main-info-bg.hero-img{
                background-image: url(<?php echo $hero_azure_tablet; ?>);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:980px), (min-resolution: 144dpi) and (max-width:980px) {
            .main-info-bg.hero-img{
                background-image: url(<?php echo $hero_azure_tablet_2x; ?>);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet-2x.jpg);
            }
        }
    </style>
</head>

<body class="modal-z">
  <?php $lighttheme = "" ?>
  <?php $roadpage = "" ?>
  <div id="wrapper">
    <?php include("inc/header.php"); ?>
    <main class="main">
        <section class="main-info main-info-bg hero-img">
            <header class="meta">

                <h1 class="big-title"><?php echo get_the_title(); ?></h1>
                <?php if ( get_field('amp_azure_hero_subheading') ) : ?> <p><?php echo get_field('amp_azure_hero_subheading'); ?></p><?php endif; ?>


                <?php if ( get_field('amp_azure_hero_cta_type') == 'url_link' ) : ?>
                    <a href="<?php echo get_field('amp_azure_hero_cta_link'); ?>" class="btn-transparent btn-header"><?php echo get_field('amp_azure_hero_cta_text'); ?></a>
                <?php else : ?>
                    <a href="#" data-toggle="modal" data-target="#feedback_modal2" class="btn-transparent btn-header"><?php echo get_field('amp_azure_hero_cta_text'); ?></a>
                <?php endif; ?>


            </header>
            <div class="text-wrap">
                <div class="container">
                    <?php if ( get_field('amp_azure_hero_text_heading') ) : ?><strong class="middle-title"><?php echo get_field('amp_azure_hero_text_heading'); ?></strong><?php endif; ?>
                    <?php if ( get_field('amp_azure_hero_text_introduction') ) : ?>
                        <div class="text">
                            <?php echo get_field('amp_azure_hero_text_introduction'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <section class="discover-block azure-block">
            <div class="container">
                <div class="meta">
                    <?php if ( get_field('amp_azure_hero_content_heading') ) : ?><h1 class="middle-title"><?php echo get_field('amp_azure_hero_content_heading'); ?></h1><?php endif; ?>
                    <?php if ( get_field('amp_azure_hero_content_itroduction') ) : ?>
                        <div class="text">
                            <?php echo get_field('amp_azure_hero_content_itroduction'); ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="inner">

                    <?php

                    if( have_rows('download_blocks') ):
                        $f = 1;
                        while ( have_rows('download_blocks') ) : the_row();

                            //vars
                            $left_image = get_sub_field('left_image');
                            $left_block_type = get_sub_field('left_block_type');
                            $left_heading = get_sub_field('left_heading');
                            $left_introduction = get_sub_field('left_introduction');
                            $left_cta_type = get_sub_field('left_cta_type');
                            $left_cta_text = get_sub_field('left_cta_text');
                            $left_cta_url_link = get_sub_field('left_cta_url_link');
                            $left_cta_file = get_sub_field('left_cta_file');
                            $left_download_form_heading = get_sub_field('left_download_form_heading');
                            $left_download_form_introduction = get_sub_field('left_download_form_introduction');

                            $right_image = get_sub_field('right_image');
                            $right_block_type = get_sub_field('right_block_type');
                            $right_heading = get_sub_field('right_heading');
                            $right_introduction = get_sub_field('right_introduction');
                            $right_cta_type = get_sub_field('right_cta_type');
                            $right_cta_text = get_sub_field('right_cta_text');
                            $right_cta_url_link = get_sub_field('right_cta_url_link');
                            $right_cta_file = get_sub_field('right_cta_file');
                            $right_download_form_heading = get_sub_field('right_download_form_heading');
                            $right_download_form_introduction = get_sub_field('right_download_form_introduction');

                        ?>

                            <div class="item item--width">
                                <?php if ( $left_image ) : ?>
                                    <div class="img-holder p--o">
                                        <img src="<?php echo $left_image; ?>" width="350" alt="Azure Stack — <?php get_the_title(); ?>">
                                    </div>
                                <?php endif; ?>

                                <?php if ( $left_heading || $left_introduction ) : ?>
                                    <div class="info">
                                        <?php if ( $left_block_type ) : ?><strong class="title"><?php echo $left_block_type; ?></strong><?php endif; ?>
                                        <?php if ( $left_heading ) : ?><strong class="description"><?php echo $left_heading; ?></strong><?php endif; ?>
                                        <?php if ( $left_introduction ) { echo $left_introduction; } ?>

                                        <?php if ( $left_cta_type == 'url_link' ) : ?>
                                            <a href="<?php echo $left_cta_url_link; ?>" class="btn-transparent"><?php if ( $left_cta_text ) { echo $left_cta_text; } else { echo 'Read more'; } ?></a>
                                        <?php else : ?>
                                            <a href="#" data-toggle="modal" data-target="#feedback_modal_l<?php echo $f; ?>" class="btn-transparent"><?php echo $left_cta_text; ?></a>
                                        <?php endif; ?>

                                    </div>
                                <?php endif; ?>
                            </div>

                            <div class="item item--width">
                                <?php if ( $right_image ) : ?>
                                    <div class="img-holder p--o">
                                        <img src="<?php echo $right_image; ?>" width="350" alt="Azure Stack — <?php get_the_title(); ?>">
                                    </div>
                                <?php endif; ?>

                                <?php if ( $right_heading || $right_introduction ) : ?>
                                    <div class="info">
                                        <?php if ( $right_block_type ) : ?><strong class="title"><?php echo $right_block_type; ?></strong><?php endif; ?>
                                        <?php if ( $right_heading ) : ?><strong class="description"><?php echo $right_heading; ?></strong><?php endif; ?>
                                        <?php if ( $right_introduction ) { echo $right_introduction; } ?>

                                        <?php if ( $right_cta_type == 'url_link' ) : ?>
                                            <a href="<?php echo $right_cta_url_link; ?>" class="btn-transparent"><?php if ( $right_cta_text ) { echo $right_cta_text; } else { echo 'Read more'; } ?></a>
                                        <?php else : ?>
                                            <a href="#" data-toggle="modal" data-target="#feedback_modal_r<?php echo $f; ?>" class="btn-transparent"><?php echo $right_cta_text; ?></a>
                                        <?php endif; ?>

                                    </div>
                                <?php endif; ?>
                            </div>

                            <?php //LEFT DOWNLOAD FORM
                            if ( $left_cta_type == 'file' ) : ?>

                                <div class="modal fade" id="feedback_modal_l<?php echo $f; ?>" tabindex="-1" role="dialog" aria-labelledby="feedback_modal">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="meta">
                                              <?php if ( $left_download_form_heading ) : ?><strong class="modal-title"><?php echo $left_download_form_heading; ?></strong><?php endif; ?>
                                              <?php if ( $left_download_form_introduction ) { echo $left_download_form_introduction; } ?>
                                            </div>

                                            <form action="<?php bloginfo('template_url'); ?>/amp/hubspot-form-api2.php" method="post" name="contactform_l<?php echo $f; ?>" id="contactform_l<?php echo $f; ?>" class="validate feedback-form">

                                              <input type="hidden" id="formtype" name="form" value="download-form" />
                                              <input type="hidden" name="pageurl" value="//<?php echo $_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI] ?>" />
                                              <input type="hidden" name="eventcategory" id="eventcategory" value="Download" />
                                              <input type="hidden" name="eventaction" id="eventaction" value="<?php echo $left_cta_text; ?>" />
                                              <input type="hidden" name="eventlabel" id="eventlabel" value="Azure Stack – <?php echo get_the_title(); ?>" />
                                              <input type="hidden" id="downloadpath" name="downloadpath" value="<?php echo $left_cta_file; ?>">

                                              <input type="text" required="required" class="form-control simple-input contact-custom required" id="downloadname" name="downloadname" placeholder="First name">
                                              <input type="text" required="required" class="form-control simple-input contact-custom required" id="downloadlastname" name="downloadlastname" placeholder="Last Name">
                                              <input type="email" required="required" id="downloademail" name="downloademail" class="form-control simple-input contact-custom required" placeholder="Email Address" data-regex="^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$">

                                              <div>
                                                  <?php $num1=rand(2,4); $num2=rand(2,5); $total=$num1+$num2; ?>
                                                  <p class="margin-top-25">CAPTCHA question: What is <?php echo $num1; ?> + <?php echo $num2; ?>?</p>
                                                  <input type="tel" id="captcha" name="captcha" class="form-control simple-input contact-custom required" data-regex="^([<?php echo $total; ?>]{1})$" />
                                                  <input type="hidden" value="<?php echo $total; ?>" name="validation">
                                              </div>

                                              <button type="submit" class="btn-fill" id="submit" name="Submit">Submit</button>

                                            </form>

                                        </div>
                                    </div>
                                </div>

                            <?php endif; ?>

                            <?php //RIGHT DOWNLOAD FORM
                            if ( $right_cta_type == 'file' ) : ?>

                                <div class="modal fade" id="feedback_modal_r<?php echo $f; ?>" tabindex="-1" role="dialog" aria-labelledby="feedback_modal">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="meta">
                                              <?php if ( $right_download_form_heading ) : ?><strong class="modal-title"><?php echo $right_download_form_heading; ?></strong><?php endif; ?>
                                              <?php if ( $right_download_form_introduction ) { echo $right_download_form_introduction; } ?>
                                            </div>

                                            <form action="<?php bloginfo('template_url'); ?>/amp/hubspot-form-api2.php" method="post" name="contactform_r<?php echo $f; ?>" id="contactform_r<?php echo $f; ?>" class="validate feedback-form">

                                              <input type="hidden" id="formtype" name="form" value="download-form" />
                                              <input type="hidden" name="pageurl" value="//<?php echo $_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI] ?>" />
                                              <input type="hidden" name="eventcategory" id="eventcategory" value="Download" />
                                              <input type="hidden" name="eventaction" id="eventaction" value="<?php echo $right_cta_text; ?>" />
                                              <input type="hidden" name="eventlabel" id="eventlabel" value="Azure Stack – <?php echo get_the_title(); ?>" />
                                              <input type="hidden" id="downloadpath" name="downloadpath" value="<?php echo $right_cta_file; ?>">

                                              <input type="text" required="required" class="form-control simple-input contact-custom required" id="downloadname" name="downloadname" placeholder="First name">
                                              <input type="text" required="required" class="form-control simple-input contact-custom required" id="downloadlastname" name="downloadlastname" placeholder="Last Name">
                                              <input type="email" required="required" id="downloademail" name="downloademail" class="form-control simple-input contact-custom required" placeholder="Email Address" data-regex="^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$">

                                              <div>
                                                  <?php $num1=rand(2,4); $num2=rand(2,5); $total=$num1+$num2; ?>
                                                  <p class="margin-top-25">CAPTCHA question: What is <?php echo $num1; ?> + <?php echo $num2; ?>?</p>
                                                  <input type="tel" id="captcha" name="captcha" class="form-control simple-input contact-custom required" data-regex="^([<?php echo $total; ?>]{1})$" />
                                                  <input type="hidden" value="<?php echo $total; ?>" name="validation">
                                              </div>

                                              <button type="submit" class="btn-fill" id="submit" name="Submit">Submit</button>

                                            </form>

                                        </div>
                                    </div>
                                </div>

                            <?php endif; ?>

                    <?php

                        $f++;

                        endwhile;

                    else :

                        // no rows found

                    endif;

                    ?>

                </div>
            </div>
        </section>

        <section class="modal-block modal-block-bg">
            <div class="container">
                <span class="heading">Interested to learn more?</span>
                <a href="#" id="amp-get-in-touch" class="btn-fill" data-toggle="modal" data-target="#feedback_modal">Get in touch</a>
            </div>
        </section>

    </main>

    <?php include("inc/footer.php"); ?>
      <div class="modal fade" id="feedback_modal" tabindex="-1" role="dialog" aria-labelledby="feedback_modal">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="meta">
                      <strong class="modal-title">Get in touch using the form below</strong>
                      <p>We’ll get back to you within 1 business day. You can also call us directly on <a href="tel:03451199911">0345 119 9911</a> or email us at <a href="mailto:sales@pulsant.com">sales@pulsant.com</a></p>
                  </div>

                  <form action="/api/webforms.php" method="post" name="contactform" id="contactform" class="validate feedback-form">

                      <input type="hidden" id="formtype" name="form" value="contact-form" />
                      <input type="hidden" name="pageurl" value="<?php $pageurl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; echo $pageurl; ?>" />
                      <input type="hidden" name="eventcategory" id="eventcategory" value="Form Submission" />
                      <input type="hidden" name="eventaction" id="eventaction" value="AMP Partner" />
                      <input type="hidden" name="eventlabel" id="eventlabel" value="<?php the_title(); ?>" />

                      <input type="text" class="form-control simple-input contact-custom required" id="name" name="name" placeholder="Name">
                      <input type="email" id="email" name="email" class="form-control simple-input contact-custom required" placeholder="Email Address" data-regex="^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$">
                      <input type="text" class="form-control simple-input contact-custom required" id="company" name="company" placeholder="Company Name">
                      <input type="tel" class="form-control simple-input contact-custom" id="phone" name="phone" placeholder="Telephone number (optional)">
                      <textarea type="textarea" class="form-control simple-input textarea text-custom" id="message" rows="8" name="message" placeholder="Your message (optional)"></textarea>


                      <div>
                          <?php $num1=rand(2,4); $num2=rand(2,5); $total=$num1+$num2; ?>
                          <p class="margin-top-25">CAPTCHA question: What is <?php echo $num1; ?> + <?php echo $num2; ?>?</p>
                          <input type="tel" id="captcha" name="captcha" class="form-control simple-input contact-custom required" data-regex="^([<?php echo $total; ?>]{1})$" />
                          <input type="hidden" value="<?php echo $total; ?>" name="validation">
                      </div>

                      <button type="submit" class="btn-fill" id="submit" name="Submit">Submit</button>

                  </form>

              </div>
          </div>
      </div>


      <?php if ( get_field('amp_azure_hero_cta_type') == 'file' ) : ?>

          <div class="modal fade" id="feedback_modal2" tabindex="-1" role="dialog" aria-labelledby="feedback_modal">
              <div class="modal-dialog" role="document">
                  <div class="modal-content">
                      <div class="meta">
                          <?php if ( get_field('hero_download_form_heading') ) : ?><strong class="modal-title"><?php echo get_field('hero_download_form_heading'); ?></strong><?php endif; ?>
                          <?php if ( get_field('hero_download_form_introduction') ) { echo get_field('hero_download_form_introduction'); } ?>
                      </div>

                      <form action="<?php bloginfo('template_url'); ?>/amp/hubspot-form-api2.php" method="post" name="contactform_hero" id="contactform_hero" class="validate feedback-form">

                          <input type="hidden" id="formtype" name="form" value="whitepaper-download-form" />
                          <input type="hidden" name="pageurl" value="//<?php echo $_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI] ?>" />
                          <input type="hidden" name="eventcategory" id="eventcategory" value="Download" />
                          <input type="hidden" name="eventaction" id="eventaction" value="<?php echo get_field('amp_azure_hero_cta_text'); ?>" />
                          <input type="hidden" name="eventlabel" id="eventlabel" value="Azure Stack – <?php echo get_the_title(); ?>" />
                          <input type="hidden" id="downloadpath" name="downloadpath" value="<?php echo get_field('amp_azure_hero_cta_file'); ?>">

                          <input type="text" required="required" class="form-control simple-input contact-custom required" id="downloadname" name="downloadname" placeholder="First name">
                          <input type="text" required="required" class="form-control simple-input contact-custom required" id="downloadlastname" name="downloadlastname" placeholder="Last Name">
                          <input type="email" required="required" id="downloademail" name="downloademail" class="form-control simple-input contact-custom required" placeholder="Email Address" data-regex="^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$">

                          <div>
                              <?php $num1=rand(2,4); $num2=rand(2,5); $total=$num1+$num2; ?>
                              <p class="margin-top-25">CAPTCHA question: What is <?php echo $num1; ?> + <?php echo $num2; ?>?</p>
                              <input type="tel" id="captcha" name="captcha" class="form-control simple-input contact-custom required" data-regex="^([<?php echo $total; ?>]{1})$" />
                              <input type="hidden" value="<?php echo $total; ?>" name="validation">
                          </div>

                          <button type="submit" class="btn-fill" id="submit" name="Submit">Submit</button>

                      </form>

                  </div>
              </div>
          </div>

      <?php endif; ?>


  </div>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/vendor.min.js" defer></script>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/app.js" defer></script>
</body>

</html>
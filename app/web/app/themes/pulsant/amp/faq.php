<?php
/*
Template Name: AMP - FAQ
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">

  <title>Microsoft Azure Stack FAQs | Pulsant AMP</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en" />
<meta name="robots" content="index, follow">

<link rel="icon" type="img/png" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/img/pulsant-favicon-16x16.png">
<link rel="apple-touch-icon" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">

<meta name="description" content="Microsoft Azure Stack is an extension of Microsoft Azure, bringing the agility and fast paced innovation of cloud computing to on-premises environments.">
<meta name="author" content="Pulsant" />

<meta property="og:url" content="" />
<meta property="og:site_name" content="Pulsant" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Microsoft Azure Stack FAQs | Pulsant AMP" />
<meta property="og:image" content="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />
<meta property="og:description" content="Microsoft Azure Stack is an extension of Microsoft Azure, bringing the agility and fast paced innovation of cloud computing to on-premises environments." />

<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="Microsoft Azure Stack is an extension of Microsoft Azure, bringing the agility and fast paced innovation of cloud computing to on-premises environments."/>
<meta name="twitter:title" content="Microsoft Azure Stack FAQs | Pulsant AMP"/>
<meta name="twitter:site" content="@pulsantuk"/>
<meta name="twitter:creator" content="@pulsantuk"/>

  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/vendor.min.css">
  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/style.min.css">
  <script src=" https://use.typekit.net/gkc7sbg.js "></script>
  <script>try{Typekit.load({ async: false });}catch(e){}</script>
  <style>
        .main-info-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd.jpg);
        }
        .main-info-bg.isv-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd.jpg);
        }
        .main-info-bg.road-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd.jpg);
        }
        .main-info-bg.boost-product-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd.jpg);
        }
        .main-info-bg.scaleup-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd.jpg);
        }
        .main-info-bg.staytuned-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd.jpg);
        }
        .main-info-bg.boost-business-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd.jpg);
        }
        .main-info-bg.performance-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd.jpg);
        }
        .main-info-bg.try-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd.jpg);
        }
        .modal-block-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd.jpg);
        }
        .footer{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd.jpg);
        }
        @media (-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd-2x.jpg);
            }
        }
        @media screen and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:1280px), (min-resolution: 144dpi) and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape-2x.jpg);
            }
        }
        @media screen and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:980px), (min-resolution: 144dpi) and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet-2x.jpg);
            }
        }
    </style>
</head>

<body>
  <?php $lighttheme = "light-theme" ?>
  <?php $roadpage = "" ?>
  <div id="wrapper">
    <?php include("inc/header.php"); ?>
    <main class="main">
        <section class="main-info main-info-bg light-theme faq-page">
            <header class="meta">
                <h1 class="big-title">Microsoft Azure Stack FAQs</h1>
                <p>FREQUENTLY ASKED QUESTIONS</p>
            </header>
        </section>
        <section class="accordion-block">
            <div class="container">
                <div class="accordion-wrap">
                    <h2 class="accordion-title">AZURE &AMP; AZURE STACK</h2>
                    <ul class="accordion">
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">What is Azure Stack?</span></a>
                            <div class="text">
                                <p>Microsoft Azure Stack is an extension of Microsoft Azure, bringing the agility and fast paced innovation of cloud computing to on-premises environments. Working together, Azure and Azure Stack deliver a consistent hybrid cloud platform that can help you modernize your application portfolio. Azure Stack delivers cloud efficiencies to your on-premises environments with pay-as-you-use pricing.</p>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">How do I buy and license Azure Stack?</span></a>
                            <div class="text">
                                <p>Azure Stack services and Azure Stack itself can be purchased and licensed directly from Pulsant as a Cloud Solution Provider (CSP) and MS Gold Partner. Azure Stack can be added to your existing Azure CSP enrolment. If you get your Azure services through Pulsant already, you can continue doing so with Azure Stack. You can use the same subscriptions, monetary commitment, and invoice for both Azure and Azure Stack. As a Microsoft managed service provider Pulsant can license Azure Stack from Microsoft through CSP, and then provide finished services or value-add offerings just as we do with Azure. Existing on-premises windows Server or SQL Server licenses may be used with Azure Stack as long as usage complies with existing product and licensing terms. If you use a Windows Server or SQL Server on-premises license with Azure Stack, you will pay only for base virtual machine consumption. Marketplace services will also be available on a bring-your-own-license basis. Pulsant has a complete cloud licensing discovery and planning workshop to help you figure out how your existing licensing will work in the cloud and can provide you with a clear strategy on how best to leverage your existing investments.</p>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">How do I buy Azure Stack directly if I want to?</span></a>
                            <div class="text">
                                <p>Azure Stack is delivered as an integrated system, with software preinstalled on hardware. You purchase Azure Stack directly from Pulsant who will work with Dell to ensure you have the right solution. You purchase Azure services from Pulsant as a CSP and Gold Partner.</p>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">What is the business model for Azure Stack software?</span></a>
                            <div class="text">
                                <p>Azure Stack services will be priced on a pay-as-you-use basis, the same as Azure. There are no up-front licensing fees for Azure Stack. This means you do not pay anything for software until you use a service. The following services will be metered and charged on a consumption basis at general availability: virtual machines (base virtual machines and Windows Server virtual machines), Azure Storage (Azure blobs, tables, and queues), App Service, and Azure Functions.</p>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">What is the price for Azure Stack software?</span></a>
                            <div class="text">
                                <p>There are no up-front fees for Azure Stack software—you pay for services on a usage basis, just as you do for Azure services. Because you work with Pulsant on the cost of infrastructure ownership, operations and support as a single hybrid cloud solution, Azure Stack service prices may be lower than Azure service prices.</p>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">How will I be billed for Azure Stack?</span></a>
                            <div class="text">
                                <p>Azure Stack contains metering technologies similar to the ones used in Azure. On a periodic basis, a summary of this information will be transmitted to the Microsoft commerce systems that provide invoicing for Azure. You will be billed for Azure Stack usage as part of your regular Azure invoice. Azure Stack services will be denoted with a distinct region to differentiate them from Azure service charges. Pulsant as your CSP provider will continue to provide you with an end to end billing service as it does already for your Azure consumption.</p>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">What are the licensing mechanisms for Azure Stack?</span></a>
                            <div class="text">
                                <p>Azure Stack will be available via EA and CSP. If you have an existing EA Azure subscription, you can use that same subscription for your Azure Stack service consumption. Similarly, Pulsant as a CSP provider can provide the same tenant subscriptions across Azure and Azure Stack for you as required.</p>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">Can I use MSDN, Free Trial, or Biz Spark with Azure Stack?</span></a>
                            <div class="text">
                                <p>Azure Stack is available only via EA and CSP. MSDN, Free Trial, and Biz Spark offers cannot be used in conjunction with Azure Stack.</p>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">I am an EA customer. Can my monetary commitment be used for Azure Stack?</span></a>
                            <div class="text">
                                <p>Your monetary commitment can be applied toward both your Azure and Azure Stack consumption as long as the subscriptions you use fall under the same enrolment. Pulsant can provide advice and guidance through its Azure Cloud licensing consultant assessment services.</p>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">Will my on-premises licenses work with Azure Stack?</span></a>
                            <div class="text">
                                <p>You may use on-premises Windows Server and SQL Server licenses with Azure Stack. If you use your own Windows Server or SQL Server license, you will pay only an Azure Stack consumption fee on the base virtual machine. Usage of Windows Server and SQL Server on the Azure Stack system must comply with product licensing guidance. Pulsant can provide advice and guidance through its Azure Cloud licensing consultant assessment services.</p>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">Can I use Windows Server and SQL Server SPLA licenses with Azure Stack provided by Pulsant?</span></a>
                            <div class="text">
                                <p>Yes, you may use Windows Server and SQL Server SPLA licenses provided by Pulsant with Azure Stack. If you bring your own Windows Server or SQL Server license, you will pay only an Azure Stack consumption fee on the base virtual machine. Usage of Windows Server and SQL Server on the Azure Stack system must comply with product licensing guidance. Pulsant can provide advice and guidance through its Azure Cloud licensing consultant assessment services.</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="accordion-wrap">
                    <h2 class="accordion-title">AZURE STACK AMP LAUNCH PAD PROOF OF CONCEPT</h2>
                    <ul class="accordion">
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">Why is Azure Stack and the AMP Launch Pad POC important for me?</span></a>
                            <div class="text">
                                <p>Hardware and Software Modernisation:<br />
	                                Learn how software defined networking and storage models will affect IT service delivery.</p>
                                <br />
                                <p>Workload composition:<br />
	                                Review workload models determine how faster, standardised design and deployment cycles will affect app/data platforms.</p>
                                <br />
                                <p>Workload Management:<br />
	                                Adopt a technology lifecycle model that moves at the speed of public cloud releases.</p>
                                <br />
                                <p>Operations:<br />
	                                Enable introduction of new operational processes, service models, and oversight can help your teams make the most of deployment from start.</p>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">Why would I want to sign up for the AMP Launch Pad POC?</span></a>
                            <div class="text">
                                <ul>
	                                <li>Demonstrating Azure Stack and Services / Applications on Azure Stack</li>
	                                <li>‘Kicking the tires’ of Azure / Azure Stack Production (Dev/Test)</li>
									<li>Developing Hybrid and / or Modern Applications</li>
									<li>The cloud administrator for Azure Stack</li>
                                </ul>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">What can I do with the AMP Launch Pad POC?</span></a>
                            <div class="text">
                            	<ul>
	                                <li>A non-production deployment of Azure Stack</li>
	                                <li>A great place to start when exploring Hybrid and/ or Modern Application development</li>
									<li>A single host that you can put together and deploy on your own</li>
									<li>Your organisation’s first impression of Azure Stack (and possibly Azure) Services</li>
                                </ul>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">What can’t I do with the AMP Launch Pad POC?</span></a>
                            <div class="text">
                                <ul>
	                                <li>Can be used as a production environment for Azure/ Azure Stack workloads</li>
	                                <li>Is scalable beyond the one-node</li>
									<li>Is a highly available solution</li>
									<li>Would be deployed outside the defined configuration scripts (do not modify the scripts to fit an unsupported configuration)</li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="accordion-wrap">
                    <h2 class="accordion-title">GENERAL QUESTIONS</h2>
                    <ul class="accordion">
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">Why should I use AMP to help me?</span></a>
                            <div class="text">
                                <p>AMP (and Pulsant) have over 20 years of hybrid cloud expertise and experience. AMP is a Pulsant initiative, powered by Microsoft Azure in partnership with Dell allowing you to take advantage of the opportunities ‘true’ hybrid cloud has to offer. Offering a powerful combination of Microsoft cloud technology coupled with assessment, implementation and management service, Pulsant and AMP as a leading Microsoft Gold Partner work closely with Microsoft and Dell own engineering teams to ensure you have access to the leading expertise and knowledge at all times. Our own engineering, support and consulting teams provide unrivalled hybrid cloud thought leadership and expertise to help you make the correct decisions when it comes to complex hybrid cloud environments.</p>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">How do I sign up and get started?</span></a>
                            <div class="text">
                                <p>Register your interest here on the site and provide a short description of what you want to do or what your intended use case is for Azure Stack. Some examples might include:</p>
                                <ul>
									<li>Understanding Azure Stack Admin and Tenant Experiences</li>
									<li>Developing DevOps/ Infrastructure-as-Code with ARM templates</li>
									<li>Understanding the Visual Studio and PowerShell experience on Azure/Azure Stack</li>
									<li>Try out Azure Stack functionality including; Web/Service Fabric services and others coming at GA</li>
									<li>Try out Infrastructure-as-a-Service on Azure Stack and see how workloads can be passed between private and public Azure clouds</li>
                            	</ul>
                            </div>
                        </li>
                        <li class="item">
                            <a href="#" class="ac-link"><span class="span">Will AMP help me develop my strategy and use cases for Azure Stack?</span></a>
                            <div class="text">
                                <p>Yes, we offer a full ‘Journey to the Cloud’ consulting service which includes a range of individual services focussed on helping you on your journey to the cloud. For the Azure Stack AMP Launch Pad PoC we will work with you to define your intended use case up front, then install and provide training on the Azure Stack AMP Launch Pad PoC solution and can then prove a deep dive around your intended use case.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="text-block">
            <div class="container">
                <div class="text-col">
                    <h3 class="heading-little">Economics</h3>
                    <p>Azure Stack brings the cloud economic model to on-premises. As in Azure, there are no up-front licensing fees for services; you pay only when you use a service. You pay for Azure Stack services using the same units as in Azure.</p>
                    <table class="table">
                        <tr>
                            <th>Service</th>
                            <th>Metering unit</th>
                        </tr>
                        <tr>
                            <td>Base virtual machine</td>
                            <td>Per vCPU/min</td>
                        </tr>
                        <tr>
                            <td>Windows Server virtual machine</td>
                            <td>Per vCPU/min or Base virtual machine meter + bring your own license</td>
                        </tr>
                        <tr>
                            <td>Azure storage (blob, table or queue storage) </td>
                            <td>Per GB (no transaction fee)</td>
                        </tr>
                        <tr>
                            <td>Azure app service</td>
                            <td>Per vCPU/min</td>
                        </tr>
                        <tr>
                            <td>Azure functions</td>
                            <td>Per GB-sec execution time and per milion executions</td>
                        </tr>
                    </table>
                    <p>Because you take on the hardware and operating costs, Azure Stack service fees will typically be lower than Azure prices. With Azure Stack, you can take advantage of hourly rates for Windows Server on-premises.</p>
                    <p>Alternatively, you can use your own existing Windows Server Enterprise Agreement (EA) or Service Provider License Agreement (SPLA) licenses in conjunction with Azure Stack services.</p>
                </div>
                <div class="text-col">
                    <div class="text-hold">
                        <h3 class="heading-little">Licensing</h3>
                        <p>Azure Stack services can be licensed directly from Pulsant as a Cloud Solution Provider (CSP) and MS Gold Partner. Azure Stack can be added to your existing Azure CSP enrolment. If you get your Azure services through Pulsant already, you can continue doing so with Azure Stack. You can use the same subscriptions, monetary commitment, and invoice for both Azure and Azure Stack.</p>
                        <p>As a Microsoft managed service provider, Pulsant can license Azure Stack from Microsoft through CSP, and then provide finished services or value-add offerings just as we do with Azure. Existing on-premises windows Server or SQL Server licenses may be used with Azure Stack as long as usage complies with existing product and licensing terms. If you use a Windows Server or SQL Server onpremises license with Azure Stack, you will pay only for base virtual machine consumption.</p>
                        <p>Marketplace services will also be available on a bring-your-own-license basis. Pulsant has a complete cloud licensing discovery and planning workshop to help you figure out how your existing licensing will work in the cloud and can provide you with a clear strategy on how best to leverage your exsiting investments.</p>
                    </div>
                    <div class="text-hold">
                        <h3 class="heading-little">Purchasing</h3>
                        <p>Azure Stack is delivered as an integrated system, with software pre-installed on hardware. You purchase Azure Stack directly from Pulsant who will work with Dell EMC to ensure you have the right solution. You purchase Azure services from Pulsant as a CSP and Gold Partner.</p>
                    </div>
                    <div class="text-hold">
                        <h3 class="heading-little">Support</h3>
                        <p>Your Azure Stack will be fully supported by Pulsant who in turn are supported by two contracts: one with Dell EMC and another with Microsoft. As Pulsant have an existing Premier and Azure support contract with Microsoft, our services support for Azure Stack is already covered. Pulsant offers a fully comprehensive 24/7/365 support service so you are fully covered for all your cloud services.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="cloud-block faq-page">
            <div class="container">
                <div class="meta">
                    <h1 class="middle-title">Journey To The Cloud<br> and amplify your business</h1>
                    <p>From the discovery phase to design and deployment, our Journey To The Cloud offers services like complete assesments of your environments, providing test set-ups, technical consultancy, implementation and deployment to fully managed services like monitoring, support and reporting.</p>
                </div>
                <div class="inner">
                    <span class="line"><span class="circle"><span class="sr-only">line</span></span></span>
                    <div class="items-hold">
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud1.svg" width="123" height="160" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Foundation Services:<br>Readiness &AMP; Transformation</strong>
                                <ul class="link-list">
                                    <li>Cloud Readiness Assesment</li>
                                    <li>Cloud Licensing Advisory</li>
                                    <li>Azure benchmark services</li>
                                    <li>Azure Stack Proof of Concept</li>
                                    <li>Azure Everywhere Workshop</li>
                                    <li>Azure Risk Advisory</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud2.svg" width="175" height="158" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Consultancy Services:<br>Strategy &AMP; Technical Implementation</strong>
                                <ul class="link-list">
                                    <li>Digital Transformation Programme</li>
                                    <li>Migration</li>
                                    <li>Technical Implementation</li>
                                    <li>Cloud Management Stack</li>
                                    <li>Azure Specialist Service</li>
                                    <li>Cloud Compliance</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud3.svg" width="169" height="172" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Managed Services</strong>
                                <ul class="link-list">
                                    <li>Service Management</li>
                                    <li>Monitoring &AMP; Reporting</li>
                                    <li>Protection &AMP; Security</li>
                                    <li>Patch &AMP; Backup management</li>
                                    <li>Identity &AMP; Access Management</li>
                                    <li>Continuous Compliance</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="journey-to-cloud" class="btn-transparent">Journey To the Cloud</a>
            </div>
        </section>
        <section class="banner">
            <div class="img-wrap">
                <div class="img-holder">
                    <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/banner-img.jpg, <?php bloginfo('template_url'); ?>/amp/assets/images/banner-img-2x.jpg 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/banner-img.jpg" alt="image description">
                </div>
            </div>
            <div class="text">
                <h1 class="title">Try Azure Stack today</h1>
                <p>A single one node Azure Stack solution to help you to explore the possibilities, quickly.</p>
                <a href="one-node-as-a-service" class="btn-transparent">Discover how</a>
            </div>
        </section>
    </main>
    <?php include("inc/footer.php"); ?>
  </div>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/vendor.min.js" defer></script>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/app.js" defer></script>
</body>

</html>
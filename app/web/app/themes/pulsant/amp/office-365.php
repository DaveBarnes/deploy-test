<?php
/*
Template Name: AMP - Office 365
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">

  <title>AMP - Hybrid Office 365 Solutions | Pulsant</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en" />
<meta name="robots" content="index, follow">

<link rel="icon" type="img/png" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/img/pulsant-favicon-16x16.png">
<link rel="apple-touch-icon" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">

<meta name="description" content="Improve collaboration and teamwork with hybrid Office 365. Boost productivity, securely by utilising Office 365 in a hybrid model.">
<meta name="author" content="Pulsant" />

<meta property="og:url" content="" />
<meta property="og:site_name" content="Pulsant" />
<meta property="og:type" content="website" />
<meta property="og:title" content="AMP - Hybrid Office 365 Solutions | Pulsant" />
<meta property="og:image" content="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />
<meta property="og:description" content="Improve collaboration and teamwork with hybrid Office 365. Boost productivity, securely by utilising Office 365 in a hybrid model." />

<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="Improve collaboration and teamwork with hybrid Office 365. Boost productivity, securely by utilising Office 365 in a hybrid model."/>
<meta name="twitter:title" content="AMP - Hybrid Office 365 Solutions | Pulsant"/>
<meta name="twitter:site" content="@pulsantuk"/>
<meta name="twitter:creator" content="@pulsantuk"/>

  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/vendor.min.css">
  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/style.min.css">
  <script src=" https://use.typekit.net/gkc7sbg.js "></script>
  <script>try{Typekit.load({ async: false });}catch(e){}</script>
  <style>
        .main-info-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd.jpg);
        }
        .main-info-bg.isv-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd.jpg);
        }
        .main-info-bg.road-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd.jpg);
        }
        .main-info-bg.boost-product-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd.jpg);
        }
        .main-info-bg.scaleup-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd.jpg);
        }
        .main-info-bg.staytuned-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd.jpg);
        }
        .main-info-bg.boost-business-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd.jpg);
        }
        .main-info-bg.performance-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd.jpg);
        }
        .main-info-bg.try-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd.jpg);
        }
        .modal-block-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd.jpg);
        }
        .footer{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd.jpg);
        }
        @media (-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd-2x.jpg);
            }
        }
        @media screen and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:1280px), (min-resolution: 144dpi) and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape-2x.jpg);
            }
        }
        @media screen and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:980px), (min-resolution: 144dpi) and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet-2x.jpg);
            }
        }
    </style>
</head>

<body>
  <?php $lighttheme = "" ?>
  <?php $roadpage = "" ?>
  <div id="wrapper">
    <?php include("inc/header.php"); ?>
    <main class="main">
        <section class="main-info main-info-bg boost-product-page">
            <header class="meta">
                <h1 class="big-title">AMP Hybrid Office 365</h1>
                <p>BOOST PRODUCTIVITY, SECURELY</p>
                <span class="scroll-down">by utilising Office 365 in a hybrid model<span class="arrow"><span class="sr-only">arrow</span></span></span>
            </header>
        </section>
        <section class="simple-text-block boost-product-page">
            <div class="container">
                <p>Keep your data and information secure while you take advantage of modern tools to collaborate more easily, work anywhere and utilise time in a more efficient way.</p>
            </div>
        </section>
        <section class="image-text-block boost-product-page scroll-to">
            <div class="container">
                <div class="item">
                    <div class="text">
                        <strong class="bold-title">Improve collaboration and teamwork with hybrid Office 365</strong>
                        <div class="hold-text">
                            <p>Employees and organisations want to take advantage of modern tools to collaborate more easily, work anywhere and utilise their time in a more efficient way.</p>
                            <p>With hybrid Office 365 solutions, you can enable your employees to improve collaboration with Skype for Business and Teams, and use PowerApps to connect your systems and data to build useful business apps quickly.</p>
                        </div>
                    </div>
                    <div class="img-holder">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/img4.png, <?php bloginfo('template_url'); ?>/amp/assets/images/img4-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/img4.png" alt="image description">
                    </div>
                </div>
                <ul class="icons-list">
                    <li>
                        <div class="ico-holder large-icon">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico6.svg" width="69" height="80" alt="image description">
                        </div>
                        <div class="description-wrap">
                            <p>Host your critical data in Pulsant Modern infrastructure or on-premises.</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico9.svg" width="69" height="61" alt="image description">
                        </div>
                        <div class="description-wrap">
                            <p>Employees get the full Office 365, everywhere.</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico7.svg" width="69" height="63" alt="image description">
                        </div>
                        <div class="description-wrap">
                            <p>Enforce governance policies, ensure compliance, and easily manage content.</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico10.svg" width="69" height="59" alt="image description">
                        </div>
                        <div class="description-wrap">
                            <p>Active directory synchronises user accounts, passwords and logins across environments.</p>
                        </div>
                    </li>
                    <li>
                        <div class="ico-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico8.svg" width="69" height="79" alt="image description">
                        </div>
                        <div class="description-wrap">
                            <p>Secure mail routing between on-premises and Exchange Online.</p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="list-section">
            <div class="container">
                <div class="meta">
                    <h1 class="middle-title">Meet Pulsant AMP Managed Office 365</h1>
                    <p>Pulsant understands that implementing Office 365 on your own can be quite the challenge. From assessing the needs of your organisation to the implementation and management, we have the expertise and certified personnel to get you, and let you stay, up and running.</p>
                </div>
                <div class="lists-wrapper">
                    <div class="item">
                        <strong class="heading">Assess your needs</strong>
                        <ul class="list">
                            <li>
                                <div class="holder"><strong class="title"><a href="#">AMP Cloud Readiness</a></strong>
                                    A step-by-step approach to determine your business needs and create a clear plan with timelines.</div>
                            </li>
                            <li>
                                <div class="holder"><strong class="title"><a href="#">AMP Licensing Advisory</a></strong>
                                    A deep dive into your current software licensing and assistance with moving to cloud based licensing models.</div>
                            </li>
                            <li>
                                <div class="holder"><strong class="title"><a href="#">AD Security Advisory</a></strong>
                                    Determining correct security and compliance policies to ensure a secure and protected Office 365 deployment.</div>
                            </li>
                        </ul>
                    </div>
                    <div class="item">
                        <strong class="heading">Migration &AMP; Implementation</strong>
                        <ul class="list">
                            <li>
                                <div class="holder"><strong class="title"><a href="#">Migration</a></strong>
                                    Our certified engineers ensure migration of data, users and emailboxes with a 100% guarantee.</div>
                            </li>
                            <li>
                                <div class="holder"><strong class="title"><a href="#">Desktop deployment</a></strong>
                                    Installing desktop applications and configuration of user accounts on devices, from laptops to mobile devices.</div>
                            </li>
                            <li>
                                <div class="holder"><strong class="title"><a href="#">User onboarding</a></strong>
                                    Setting up Active Directory integration, users accounts and permissions and management during lifecycle.</div>
                            </li>
                        </ul>
                    </div>
                    <div class="item">
                        <strong class="heading">Operations &AMP; Support</strong>
                        <ul class="list">
                            <li>
                                <div class="holder"><strong class="title"><a href="#">Service Desk</a></strong>
                                    <p>User account management &AMP; advise. Logging, recording and basic diagnosis of all reported incidents and problems.</p>
                                    <p>Resolution of simple, known or fully documented technical incidents and problems. Escalation of complex issues.</p>
                                </div>
                            </li>
                            <li>
                                <div class="holder"><strong class="title"><a href="#">Tech support</a></strong>
                                    <p>Resolution of complex, business critical or unknown technical issues. Escalation to specialist teams or Microsoft. Complex changes.</p>
                                    <p>IT Team support: 24x7 SLA<br>
                                    End User support: 5x7 SLA</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="cloud-block boost-product-page">
            <div class="container">
                <div class="meta">
                    <h1 class="middle-title">Journey To The Cloud and amplify your business</h1>
                    <p>From the discovery phase to design and deployment, our Journey To The Cloud offers services like complete assesments of your environments, providing test set-ups, technical consultancy, implementation and deployment to fully managed services like monitoring, support and reporting.</p>
                </div>
                <div class="inner">
                    <span class="line"><span class="circle"><span class="sr-only">line</span></span></span>
                    <div class="items-hold">
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud1.svg" width="123" height="160" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Foundation Services:<br>Readiness &AMP; Transformation</strong>
                                <ul class="link-list">
                                    <li>Cloud Readiness Assesment</li>
                                    <li>Cloud Licensing Advisory</li>
                                    <li>Azure benchmark services</li>
                                    <li>Azure Stack Proof of Concept</li>
                                    <li>Azure Everywhere Workshop</li>
                                    <li>Azure Risk Advisory</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud2.svg" width="175" height="158" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Consultancy Services:<br>Strategy &AMP; Technical Implementation</strong>
                                <ul class="link-list">
                                    <li>Digital Transformation Programme</li>
                                    <li>Migration</li>
                                    <li>Technical Implementation</li>
                                    <li>Cloud Management Stack</li>
                                    <li>Azure Specialist Service</li>
                                    <li>Cloud Compliance</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud3.svg" width="169" height="172" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Managed Services</strong>
                                <ul class="link-list">
                                    <li>Service Management</li>
                                    <li>Monitoring &AMP; Reporting</li>
                                    <li>Protection &AMP; Security</li>
                                    <li>Patch &AMP; Backup management</li>
                                    <li>Identity &AMP; Access Management</li>
                                    <li>Continuous Compliance</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/amp/journey-to-cloud" class="btn-transparent">Journey To the Cloud</a>
            </div>
        </section>
        <section class="banner">
            <div class="img-wrap">
                <div class="img-holder">
                    <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/banner-img.jpg, <?php bloginfo('template_url'); ?>/amp/assets/images/banner-img-2x.jpg 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/banner-img.jpg" alt="image description">
                </div>
            </div>
            <div class="text">
                <h1 class="title">Try Azure Stack today</h1>
                <p>A single one node Azure Stack solution to help you to explore the possibilities, quickly.</p>
                <a href="/amp/one-node-as-a-service" class="btn-transparent">Discover how</a>
            </div>
        </section>
    </main>
    <?php include("inc/footer.php"); ?>
  </div>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/vendor.min.js" defer></script>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/app.js" defer></script>
</body>

</html>
<?php
/*
Template Name: AMP - Azure Stack
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">

  <title>AMP Launch Pad - Get Ready for Azure Stack | Pulsant</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en" />
<meta name="robots" content="index, follow">

<link rel="icon" type="img/png" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/img/pulsant-favicon-16x16.png">
<link rel="apple-touch-icon" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">

<meta name="description" content="AMP Launch Pad from Microsoft and Dell, fully supported by Pulsant, allows you to experience the benefits of hybrid cloud and get your questions answered.">
<meta name="author" content="Pulsant" />

<meta property="og:url" content="" />
<meta property="og:site_name" content="Pulsant" />
<meta property="og:type" content="website" />
<meta property="og:title" content="AMP Launch Pad - Get Ready for Azure Stack | Pulsant" />
<meta property="og:image" content="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />
<meta property="og:description" content="AMP Launch Pad from Microsoft and Dell, fully supported by Pulsant, allows you to experience the benefits of hybrid cloud and get your questions answered." />

<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="AMP Launch Pad from Microsoft and Dell, fully supported by Pulsant, allows you to experience the benefits of hybrid cloud and get your questions answered."/>
<meta name="twitter:title" content="AMP Launch Pad - Get Ready for Azure Stack | Pulsant"/>
<meta name="twitter:site" content="@pulsantuk"/>
<meta name="twitter:creator" content="@pulsantuk"/>

  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/vendor.min.css">
  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/style.min.css">
  <script src=" https://use.typekit.net/gkc7sbg.js "></script>
  <script>try{Typekit.load({ async: false });}catch(e){}</script>
  <style>
        .main-info-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd.jpg);
        }
        .main-info-bg.isv-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd.jpg);
        }
        .main-info-bg.road-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd.jpg);
        }
        .main-info-bg.boost-product-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd.jpg);
        }
        .main-info-bg.scaleup-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd.jpg);
        }
        .main-info-bg.staytuned-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd.jpg);
        }
        .main-info-bg.boost-business-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd.jpg);
        }
        .main-info-bg.performance-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd.jpg);
        }
        .main-info-bg.try-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd.jpg);
        }
        .main-info-bg.azure-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-azure-hd.jpg);
        }
        .modal-block-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd.jpg);
        }
        .footer{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd.jpg);
        }
        @media (-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd-2x.jpg);
            }
            .main-info-bg.azure-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-azure-hd-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd-2x.jpg);
            }
        }
        @media screen and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape.jpg);
            }
            .main-info-bg.azure-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-azure-landscape.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:1280px), (min-resolution: 144dpi) and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape-2x.jpg);
            }
            .main-info-bg.azure-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-azure-landscape-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape-2x.jpg);
            }
        }
        @media screen and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet.jpg);
            }
            .main-info-bg.azure-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-azure-tablet.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:980px), (min-resolution: 144dpi) and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet-2x.jpg);
            }
            .main-info-bg.azure-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-azure-tablet-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet-2x.jpg);
            }
        }
    </style>
</head>

<body>
  <?php $lighttheme = "" ?>
  <?php $roadpage = "" ?>
  <div id="wrapper">
    <?php include("inc/header.php"); ?>
    <main class="main">
        <section class="main-info main-info-bg azure-page">
            <header class="meta">
                <h1 class="big-title">Get ready<br>for Azure Stack</h1>
                <p>Stay ahead of your competition and try the new hybrid cloud platform today</p>
                <a href="#" data-toggle="modal" data-target="#feedback_modal2" class="btn-transparent btn-header">DOWNLOAD TECH PREVIEW</a>
            </header>
            <div class="text-wrap">
                <div class="container">
                    <strong class="middle-title">In a Hybrid World</strong>
                    <div class="text">
                        <p>In a hybrid cloud world, enjoying the benefits of both public and private environments, how do you make sure you&#8217;re getting the solution that&#8217;s right for your business? In addition, what about integration? Security? Management?</p>
                        <p>AMP Launch Pad from Microsoft and Dell, fully supported by Pulsant, is your chance to experience the benefits of hybrid cloud and get your questions answered.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="discover-block azure-block">
            <div class="container">
                <div class="meta">
                    <h1 class="middle-title">Test out Azure Stack capabilities</h1>
                    <div class="text">
                        <p>This exclusive proof of concept is the ideal way to test out Azure Stack&#8217;s capabilities before its official launch. By familiarising yourself with its features and functionality you will be able to gauge the impact it will have on your organisation, and discover where it will add value to your processes, workflow and development.</p>
                        <p>It&#8217;s an exciting time in the evolution of cloud — so take your first step towards a fully hybrid cloud environment with Microsoft Azure Stack.</p>
                    </div>
                </div>
                <div class="inner">
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/amp_azure_blog.jpg" width="350" alt="Azure Stack — The future of cloud is here">
                        </div>
                        <div class="info">
                            <strong class="title">Blog</strong>
                            <strong class="description">Test it, try it, trial it —<br>Microsoft hybrid cloud</strong>
                            <p>Take a look at what our in-house Microsoft expert has to say about the opportunities AMP Launch Pad presents</p>
                            <a href="https://blog.pulsant.com/azure-stack-the-future-of-cloud-is-here" class="btn-transparent">Read the blog</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/amp_azure_whitepaper.jpg" width="350" alt="Azure Stack AMP Launch Pad">
                        </div>
                        <div class="info">
                            <strong class="title">Technical Preview</strong>
                            <span>Stack to the future:<strong class="description">The path to hybrid cloud<br>has never been clearer </strong></span>
                            <p>Our exclusive technical preview sets out the benefits and opportunities that AMP Launch Pad brings to the market, as well as how you can use it to gain the most value.</p>
                            <a href="#" data-toggle="modal" data-target="#feedback_modal2" class="btn-transparent">Download it here</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="modal-block modal-block-bg">
            <div class="container">
                <span class="heading">Interested to learn more?</span>
                <a href="#" id="amp-get-in-touch" class="btn-fill" data-toggle="modal" data-target="#feedback_modal">Get in touch</a>
            </div>
        </section>
    </main>
    <?php include("inc/footer.php"); ?>
      <div class="modal fade" id="feedback_modal" tabindex="-1" role="dialog" aria-labelledby="feedback_modal">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="meta">
                      <strong class="modal-title">Get in touch using the form below</strong>
                      <p>We’ll get back to you within 1 business day. You can also call us directly on <a href="tel:03451199911">0345 119 9911</a> or email us at <a href="mailto:sales@pulsant.com">sales@pulsant.com</a></p>
                  </div>

                  <form action="/api/webforms.php" method="post" name="contactform" id="contactform" class="validate feedback-form">

                      <input type="hidden" id="formtype" name="form" value="contact-form" />
                      <input type="hidden" name="pageurl" value="<?php $pageurl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; echo $pageurl; ?>" />
                      <input type="hidden" name="eventcategory" id="eventcategory" value="Form Submission" />
                      <input type="hidden" name="eventaction" id="eventaction" value="AMP Partner" />
                      <input type="hidden" name="eventlabel" id="eventlabel" value="<?php the_title(); ?>" />

                      <input type="text" class="form-control simple-input contact-custom required" id="name" name="name" placeholder="Name">
                      <input type="email" id="email" name="email" class="form-control simple-input contact-custom required" placeholder="Email Address" data-regex="^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$">
                      <input type="text" class="form-control simple-input contact-custom required" id="company" name="company" placeholder="Company Name">
                      <input type="tel" class="form-control simple-input contact-custom" id="phone" name="phone" placeholder="Telephone number (optional)">
                      <textarea type="textarea" class="form-control simple-input textarea text-custom" id="message" rows="8" name="message" placeholder="Your message (optional)"></textarea>


                      <div>
                          <?php $num1=rand(2,4); $num2=rand(2,5); $total=$num1+$num2; ?>
                          <p class="margin-top-25">CAPTCHA question: What is <?php echo $num1; ?> + <?php echo $num2; ?>?</p>
                          <input type="tel" id="captcha" name="captcha" class="form-control simple-input contact-custom required" data-regex="^([<?php echo $total; ?>]{1})$" />
                          <input type="hidden" value="<?php echo $total; ?>" name="validation">
                      </div>

                      <button type="submit" class="btn-fill" id="submit" name="Submit">Submit</button>

                  </form>


                  <?php /* ORIGINAL FORM
                <form action="#" method="post" class="feedback-form">
                    <input type="text" name="name" class="simple-input" placeholder="Name*">
                    <input type="text" name="company" class="simple-input" placeholder="Company Name*">
                    <input type="email" name="email" class="simple-input" placeholder="Email address*">
                    <input type="tel" name="phone" class="simple-input" placeholder="Phone Number (optional)">
                    <textarea class="simple-input textarea" placeholder="Your message (optional)"></textarea>
                    <button type="submit" class="btn-fill">Get back to me</button>
                </form>
                */ ?>
              </div>
          </div>
      </div>
      <div class="modal fade" id="feedback_modal2" tabindex="-1" role="dialog" aria-labelledby="feedback_modal">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="meta">
                      <strong class="modal-title">Azure Stack – AMP Launch Pad</strong>
                      <p>Stack to the future</p>
                      <p>We take a look at the benefits of hybrid cloud, the impact that Azure Stack will have on business, and ways in which companies can be first in line to experience this new technology.</p>
                      <p>Get an exclusive view of the future of hybrid cloud and what we mean by true hybrid cloud. With the launch of Azure Stack, the cloud landscape will be revolutionised.</p>
                  </div>
	              <?php
	              $form = new HubSpotForm( 'd9c570f5-05df-4d64-99d7-d96622df3a1b', $page_title, '//' . $_SERVER['HTTP_HOST'] . '/wp-content/uploads/Stack-to-the-future_Whitepaper.pdf' );
	              ?>
                  <form action="/api/" method="post" name="contactform" id="contactform" class="validate feedback-form">

                      <input type="hidden" id="formtype" name="form" value="whitepaper-download-form" />
                      <input type="hidden" name="pageurl" value="//<?php echo $_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI] ?>" />
                      <input type="hidden" name="eventcategory" id="eventcategory" value="Download" />
                      <input type="hidden" name="eventaction" id="eventaction" value="Whitepaper" />
	                  <input type="hidden" name="eventlabel" id="eventlabel" value="Azure Stack – AMP Launch Pad"/>

	                  <input type="text" required="required" class="form-control simple-input contact-custom required" id="downloadname" name="firstname" placeholder="First name">
	                  <input type="text" required="required" class="form-control simple-input contact-custom required" id="downloadlastname" name="lastname" placeholder="Last Name">
	                  <input type="email" required="required" id="downloademail" name="email" class="form-control simple-input contact-custom required" placeholder="Email Address" data-regex="^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$">
	                  <?php
	                  echo $form->formData['hiddenFields'];
	                  ?>

                      <button type="submit" class="btn-fill" id="submit" name="Submit">Submit</button>

                  </form>
              </div>
          </div>
      </div>
  </div>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/vendor.min.js" defer></script>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/app.js" defer></script>
</body>

</html>
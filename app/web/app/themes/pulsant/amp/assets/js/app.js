(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _burger = require('./modules/burger');

var _burger2 = _interopRequireDefault(_burger);

var _submenu = require('./modules/submenu');

var _submenu2 = _interopRequireDefault(_submenu);

var _validation = require('./modules/validation');

var _validation2 = _interopRequireDefault(_validation);

var _detectUserAgent = require('./modules/detect-user-agent');

var _detectUserAgent2 = _interopRequireDefault(_detectUserAgent);

var _wowAnimate = require('./modules/wow-animate');

var _wowAnimate2 = _interopRequireDefault(_wowAnimate);

var _footer = require('./modules/footer');

var _footer2 = _interopRequireDefault(_footer);

var _accordion = require('./modules/accordion');

var _accordion2 = _interopRequireDefault(_accordion);

var _scrollDown = require('./modules/scroll-down');

var _scrollDown2 = _interopRequireDefault(_scrollDown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// You can write a call and import your functions in this file.
//
// This file will be compiled into app.js and will not be minified.
// Feel free with using ES6 here.

(function ($) {
  'use strict';

  // When DOM is ready

  $(function () {
    _burger2.default.burgerInit();
    _submenu2.default.submenuInit();
    _validation2.default.validationInit();
    _detectUserAgent2.default.dUserAgentInit();
    _wowAnimate2.default.wowAnimateInit();
    _accordion2.default.accordionInit();
    _scrollDown2.default.scrollDownInit();
  });

  _footer2.default.footerInit();
})(jQuery);

},{"./modules/accordion":2,"./modules/burger":3,"./modules/detect-user-agent":4,"./modules/footer":5,"./modules/scroll-down":6,"./modules/submenu":7,"./modules/validation":8,"./modules/wow-animate":9}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var accordion = {
    accordionInit: function accordionInit() {
        var $accordionLink = $('.accordion .ac-link'),
            heights = [];

        if (!$accordionLink.length) {
            return;
        }

        $accordionLink.on('click', function (e) {
            e.preventDefault();
            showText.call(this);
        });

        function showText() {
            $(this).closest('.accordion .item').toggleClass('active');
            $(this).next().slideToggle(200);
        }
    }

};

exports.default = accordion;

},{}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var burger = {
    burgerInit: function burgerInit() {
        var $burgerBtn = $('.burger-btn');

        if (!$burgerBtn.length) {
            return;
        }

        $burgerBtn.on('click', function () {
            burgerHandler.call(this);
        });

        function burgerHandler() {
            $(this).toggleClass('active');
            $('.header .navbar').toggleClass('active');
        }
    }

};

exports.default = burger;

},{}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var dUserAgent = {
    dUserAgentInit: function dUserAgentInit() {
        var browser = navigator.userAgent.toLowerCase();
        if (browser.indexOf('firefox') > -1) {
            $('body').addClass('moz');
        } else if (navigator.userAgent.indexOf('Trident') != -1 && navigator.userAgent.indexOf('MSIE') == -1) {
            $('body').addClass('ms');
        }
    }

};

exports.default = dUserAgent;

},{}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var footer = {
    footerInit: function footerInit() {
        var $footerWrap = $('.footer');

        if (!$footerWrap.length) {
            return;
        }

        if ($(window).width() > 767) {
            $footerWrap.footerReveal({
                shadow: false
            });
        }
    }

};

exports.default = footer;

},{}],6:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var scrollDown = {
    scrollDownInit: function scrollDownInit() {
        var $scrollDownBtn = $('.scroll-down'),
            $mainSection = $('.scroll-to');

        if (!$scrollDownBtn.length) {
            return;
        }

        $scrollDownBtn.on('click', function () {
            scrollDownHandler.call(this);
        });

        function scrollDownHandler() {
            var currentPosition = $mainSection.offset().top,
                prevElemMargin = parseInt($mainSection.prev().css('margin-bottom'));
            $('html, body').animate({
                scrollTop: currentPosition - prevElemMargin
            }, 400);
        }
    }

};

exports.default = scrollDown;

},{}],7:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var submenu = {
    submenuInit: function submenuInit() {
        var $submenuItem = $('.navbar .subtitle > a'),
            $submenuWrap = $('.navbar .subtitle'),
            $header = $('.header');

        if (!$submenuItem.length) {
            return;
        }

        submenuEventsTrigger();
        $(window).resize(submenuEventsTrigger);

        function submenuEventsTrigger() {
            if ($(window).width() < 1200) {
                $submenuItem.off('click.submenu', handler);
                $submenuWrap.off('mouseenter.submenu', hoverInFunc);
                $submenuWrap.off('mouseleave.submenu', hoverOutFunc);
                $submenuItem.on('click.submenu', handler);
            } else {
                $submenuWrap.off('mouseenter.submenu', hoverInFunc);
                $submenuWrap.off('mouseleave.submenu', hoverOutFunc);
                $submenuItem.off('click.submenu', handler);
                $submenuWrap.on('mouseenter.submenu', hoverInFunc);
                $submenuWrap.on('mouseleave.submenu', hoverOutFunc);
            }
        }

        function hoverInFunc() {
            submenuOnHandler.call(this);
        }

        function hoverOutFunc() {
            submenuOffHandler.call(this);
        }

        function handler(e) {
            e.preventDefault();
            submenuHandler.call(this);
        }

        function submenuHandler() {
            $(this).closest('.subtitle').toggleClass('current-menu-item');
            $(this).closest('.subtitle').find('ul').slideToggle(200).stop(true, true);
        }

        function submenuOnHandler() {
            $(this).closest('.subtitle').addClass('current-menu-item');
            $(this).closest('.subtitle').find('ul').slideDown(200).stop(true, true);
            $header.addClass('menu-open');
            $('.overlay').fadeIn(200);
        }

        function submenuOffHandler() {
            $(this).closest('.subtitle').removeClass('current-menu-item');
            $(this).closest('.subtitle').find('ul').slideUp(200).stop(true, true);
            $header.removeClass('menu-open');
            $('.overlay').fadeOut(200);
        }
    }

};

exports.default = submenu;

},{}],8:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var validation = {
    validationInit: function validationInit() {
        var $feedbackForm = $('.feedback-form');

        if (!$feedbackForm.length) {
            return;
        }

        $feedbackForm.validate({
            errorPlacement: function errorPlacement(error, element) {
                return false;
            },
            rules: {
                name: {
                    required: true
                },
                company: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    number: true
                }
            }
        });
    }

};

exports.default = validation;

},{}],9:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var wowAnimate = {
    wowAnimateInit: function wowAnimateInit() {
        var $wowElem = $('.wow');

        if (!$wowElem.length) {
            return;
        }

        new WOW().init();
    }

};

exports.default = wowAnimate;

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvYXBwLmpzIiwic3JjL2pzL21vZHVsZXMvYWNjb3JkaW9uLmpzIiwic3JjL2pzL21vZHVsZXMvYnVyZ2VyLmpzIiwic3JjL2pzL21vZHVsZXMvZGV0ZWN0LXVzZXItYWdlbnQuanMiLCJzcmMvanMvbW9kdWxlcy9mb290ZXIuanMiLCJzcmMvanMvbW9kdWxlcy9zY3JvbGwtZG93bi5qcyIsInNyYy9qcy9tb2R1bGVzL3N1Ym1lbnUuanMiLCJzcmMvanMvbW9kdWxlcy92YWxpZGF0aW9uLmpzIiwic3JjL2pzL21vZHVsZXMvd293LWFuaW1hdGUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7OztBQ0tBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7OztBQVpBO0FBQ0E7QUFDQTtBQUNBOztBQVdBLENBQUUsVUFBQyxDQUFELEVBQU87QUFDUDs7QUFFQTs7QUFDQSxJQUFFLFlBQU07QUFDTixxQkFBTyxVQUFQO0FBQ0Esc0JBQVEsV0FBUjtBQUNBLHlCQUFXLGNBQVg7QUFDQSw4QkFBVyxjQUFYO0FBQ0EseUJBQVcsY0FBWDtBQUNBLHdCQUFVLGFBQVY7QUFDQSx5QkFBVyxjQUFYO0FBQ0QsR0FSRDs7QUFVQSxtQkFBTyxVQUFQO0FBRUQsQ0FoQkQsRUFnQkcsTUFoQkg7Ozs7Ozs7O0FDZEEsSUFBSSxZQUFZO0FBQ1osbUJBQWUseUJBQVc7QUFDdEIsWUFBSSxpQkFBaUIsRUFBRSxxQkFBRixDQUFyQjtBQUFBLFlBQ0ksVUFBVSxFQURkOztBQUdBLFlBQUksQ0FBQyxlQUFlLE1BQXBCLEVBQTRCO0FBQ3hCO0FBQ0g7O0FBRUQsdUJBQWUsRUFBZixDQUFrQixPQUFsQixFQUEyQixVQUFTLENBQVQsRUFBVztBQUNwQyxjQUFFLGNBQUY7QUFDQSxxQkFBUyxJQUFULENBQWMsSUFBZDtBQUNELFNBSEQ7O0FBS0EsaUJBQVMsUUFBVCxHQUFtQjtBQUNqQixjQUFFLElBQUYsRUFBUSxPQUFSLENBQWdCLGtCQUFoQixFQUFvQyxXQUFwQyxDQUFnRCxRQUFoRDtBQUNBLGNBQUUsSUFBRixFQUFRLElBQVIsR0FBZSxXQUFmLENBQTJCLEdBQTNCO0FBQ0Q7QUFFSjs7QUFuQlcsQ0FBaEI7O2tCQXVCZSxTOzs7Ozs7OztBQ3ZCZixJQUFJLFNBQVM7QUFDVCxnQkFBWSxzQkFBVztBQUNuQixZQUFJLGFBQWEsRUFBRSxhQUFGLENBQWpCOztBQUVBLFlBQUksQ0FBQyxXQUFXLE1BQWhCLEVBQXdCO0FBQ3BCO0FBQ0g7O0FBRUQsbUJBQVcsRUFBWCxDQUFjLE9BQWQsRUFBdUIsWUFBVztBQUM5QiwwQkFBYyxJQUFkLENBQW1CLElBQW5CO0FBQ0gsU0FGRDs7QUFJQSxpQkFBUyxhQUFULEdBQXdCO0FBQ3RCLGNBQUUsSUFBRixFQUFRLFdBQVIsQ0FBb0IsUUFBcEI7QUFDQSxjQUFFLGlCQUFGLEVBQXFCLFdBQXJCLENBQWlDLFFBQWpDO0FBQ0Q7QUFFSjs7QUFqQlEsQ0FBYjs7a0JBcUJlLE07Ozs7Ozs7O0FDckJmLElBQUksYUFBYTtBQUNiLG9CQUFnQiwwQkFBVztBQUN2QixZQUFJLFVBQVUsVUFBVSxTQUFWLENBQW9CLFdBQXBCLEVBQWQ7QUFDQSxZQUFHLFFBQVEsT0FBUixDQUFnQixTQUFoQixJQUE2QixDQUFDLENBQWpDLEVBQW9DO0FBQ2hDLGNBQUUsTUFBRixFQUFVLFFBQVYsQ0FBbUIsS0FBbkI7QUFDSCxTQUZELE1BRU8sSUFBRyxVQUFVLFNBQVYsQ0FBb0IsT0FBcEIsQ0FBNEIsU0FBNUIsS0FBMEMsQ0FBQyxDQUEzQyxJQUFnRCxVQUFVLFNBQVYsQ0FBb0IsT0FBcEIsQ0FBNEIsTUFBNUIsS0FBdUMsQ0FBQyxDQUEzRixFQUE2RjtBQUNuRyxjQUFFLE1BQUYsRUFBVSxRQUFWLENBQW1CLElBQW5CO0FBQ0E7QUFFSjs7QUFUWSxDQUFqQjs7a0JBYWUsVTs7Ozs7Ozs7QUNiZixJQUFJLFNBQVM7QUFDVCxnQkFBWSxzQkFBVztBQUNuQixZQUFJLGNBQWMsRUFBRSxTQUFGLENBQWxCOztBQUVBLFlBQUksQ0FBQyxZQUFZLE1BQWpCLEVBQXlCO0FBQ3JCO0FBQ0g7O0FBRUQsWUFBRyxFQUFFLE1BQUYsRUFBVSxLQUFWLEtBQW9CLEdBQXZCLEVBQTJCO0FBQzFCLHdCQUFZLFlBQVosQ0FBeUI7QUFDckIsd0JBQVE7QUFEYSxhQUF6QjtBQUdBO0FBRUo7O0FBZFEsQ0FBYjs7a0JBa0JlLE07Ozs7Ozs7O0FDbEJmLElBQUksYUFBYTtBQUNiLG9CQUFnQiwwQkFBVztBQUN2QixZQUFJLGlCQUFpQixFQUFFLGNBQUYsQ0FBckI7QUFBQSxZQUNJLGVBQWUsRUFBRSxZQUFGLENBRG5COztBQUdBLFlBQUksQ0FBQyxlQUFlLE1BQXBCLEVBQTRCO0FBQ3hCO0FBQ0g7O0FBRUQsdUJBQWUsRUFBZixDQUFrQixPQUFsQixFQUEyQixZQUFXO0FBQ2xDLDhCQUFrQixJQUFsQixDQUF1QixJQUF2QjtBQUNILFNBRkQ7O0FBSUEsaUJBQVMsaUJBQVQsR0FBNEI7QUFDeEIsZ0JBQUksa0JBQWtCLGFBQWEsTUFBYixHQUFzQixHQUE1QztBQUFBLGdCQUNJLGlCQUFpQixTQUFTLGFBQWEsSUFBYixHQUFvQixHQUFwQixDQUF3QixlQUF4QixDQUFULENBRHJCO0FBRUEsY0FBRSxZQUFGLEVBQWdCLE9BQWhCLENBQXdCO0FBQ3BCLDJCQUFXLGtCQUFrQjtBQURULGFBQXhCLEVBRUcsR0FGSDtBQUdIO0FBRUo7O0FBckJZLENBQWpCOztrQkF5QmUsVTs7Ozs7Ozs7QUN6QmYsSUFBSSxVQUFVO0FBQ1YsaUJBQWEsdUJBQVc7QUFDcEIsWUFBSSxlQUFlLEVBQUUsdUJBQUYsQ0FBbkI7QUFBQSxZQUNJLGVBQWUsRUFBRSxtQkFBRixDQURuQjtBQUFBLFlBRUksVUFBVSxFQUFFLFNBQUYsQ0FGZDs7QUFJQSxZQUFJLENBQUMsYUFBYSxNQUFsQixFQUEwQjtBQUN0QjtBQUNIOztBQUVEO0FBQ0EsVUFBRSxNQUFGLEVBQVUsTUFBVixDQUFpQixvQkFBakI7O0FBRUEsaUJBQVMsb0JBQVQsR0FBK0I7QUFDM0IsZ0JBQUcsRUFBRSxNQUFGLEVBQVUsS0FBVixLQUFvQixJQUF2QixFQUE0QjtBQUN4Qiw2QkFBYSxHQUFiLENBQWlCLGVBQWpCLEVBQWtDLE9BQWxDO0FBQ0EsNkJBQWEsR0FBYixDQUFpQixvQkFBakIsRUFBdUMsV0FBdkM7QUFDQSw2QkFBYSxHQUFiLENBQWlCLG9CQUFqQixFQUF1QyxZQUF2QztBQUNBLDZCQUFhLEVBQWIsQ0FBZ0IsZUFBaEIsRUFBaUMsT0FBakM7QUFDSCxhQUxELE1BS087QUFDSCw2QkFBYSxHQUFiLENBQWlCLG9CQUFqQixFQUF1QyxXQUF2QztBQUNBLDZCQUFhLEdBQWIsQ0FBaUIsb0JBQWpCLEVBQXVDLFlBQXZDO0FBQ0EsNkJBQWEsR0FBYixDQUFpQixlQUFqQixFQUFrQyxPQUFsQztBQUNBLDZCQUFhLEVBQWIsQ0FBZ0Isb0JBQWhCLEVBQXNDLFdBQXRDO0FBQ0EsNkJBQWEsRUFBYixDQUFnQixvQkFBaEIsRUFBc0MsWUFBdEM7QUFDSDtBQUNKOztBQUVELGlCQUFTLFdBQVQsR0FBc0I7QUFDbEIsNkJBQWlCLElBQWpCLENBQXNCLElBQXRCO0FBQ0g7O0FBRUQsaUJBQVMsWUFBVCxHQUF1QjtBQUNuQiw4QkFBa0IsSUFBbEIsQ0FBdUIsSUFBdkI7QUFDSDs7QUFFRCxpQkFBUyxPQUFULENBQWlCLENBQWpCLEVBQW1CO0FBQ2YsY0FBRSxjQUFGO0FBQ0EsMkJBQWUsSUFBZixDQUFvQixJQUFwQjtBQUNIOztBQUVELGlCQUFTLGNBQVQsR0FBeUI7QUFDckIsY0FBRSxJQUFGLEVBQVEsT0FBUixDQUFnQixXQUFoQixFQUE2QixXQUE3QixDQUF5QyxtQkFBekM7QUFDQSxjQUFFLElBQUYsRUFBUSxPQUFSLENBQWdCLFdBQWhCLEVBQTZCLElBQTdCLENBQWtDLElBQWxDLEVBQXdDLFdBQXhDLENBQW9ELEdBQXBELEVBQXlELElBQXpELENBQThELElBQTlELEVBQW9FLElBQXBFO0FBQ0g7O0FBRUQsaUJBQVMsZ0JBQVQsR0FBMkI7QUFDdkIsY0FBRSxJQUFGLEVBQVEsT0FBUixDQUFnQixXQUFoQixFQUE2QixRQUE3QixDQUFzQyxtQkFBdEM7QUFDQSxjQUFFLElBQUYsRUFBUSxPQUFSLENBQWdCLFdBQWhCLEVBQTZCLElBQTdCLENBQWtDLElBQWxDLEVBQXdDLFNBQXhDLENBQWtELEdBQWxELEVBQXVELElBQXZELENBQTRELElBQTVELEVBQWtFLElBQWxFO0FBQ0Esb0JBQVEsUUFBUixDQUFpQixXQUFqQjtBQUNBLGNBQUUsVUFBRixFQUFjLE1BQWQsQ0FBcUIsR0FBckI7QUFDSDs7QUFFRCxpQkFBUyxpQkFBVCxHQUE0QjtBQUN4QixjQUFFLElBQUYsRUFBUSxPQUFSLENBQWdCLFdBQWhCLEVBQTZCLFdBQTdCLENBQXlDLG1CQUF6QztBQUNBLGNBQUUsSUFBRixFQUFRLE9BQVIsQ0FBZ0IsV0FBaEIsRUFBNkIsSUFBN0IsQ0FBa0MsSUFBbEMsRUFBd0MsT0FBeEMsQ0FBZ0QsR0FBaEQsRUFBcUQsSUFBckQsQ0FBMEQsSUFBMUQsRUFBZ0UsSUFBaEU7QUFDQSxvQkFBUSxXQUFSLENBQW9CLFdBQXBCO0FBQ0EsY0FBRSxVQUFGLEVBQWMsT0FBZCxDQUFzQixHQUF0QjtBQUNIO0FBRUo7O0FBNURTLENBQWQ7O2tCQWdFZSxPOzs7Ozs7OztBQ2hFZixJQUFJLGFBQWE7QUFDYixvQkFBZ0IsMEJBQVc7QUFDdkIsWUFBSSxnQkFBZ0IsRUFBRSxnQkFBRixDQUFwQjs7QUFFQSxZQUFJLENBQUMsY0FBYyxNQUFuQixFQUEyQjtBQUN2QjtBQUNIOztBQUVELHNCQUFjLFFBQWQsQ0FBdUI7QUFDbkIsNEJBQWdCLHdCQUFTLEtBQVQsRUFBZ0IsT0FBaEIsRUFBeUI7QUFDckMsdUJBQU8sS0FBUDtBQUNILGFBSGtCO0FBSW5CLG1CQUFPO0FBQ0gsc0JBQU07QUFDRiw4QkFBVTtBQURSLGlCQURIO0FBSUgseUJBQVM7QUFDTCw4QkFBVTtBQURMLGlCQUpOO0FBT0gsdUJBQU87QUFDSCw4QkFBVSxJQURQO0FBRUgsMkJBQU87QUFGSixpQkFQSjtBQVdILHVCQUFPO0FBQ0gsNEJBQVE7QUFETDtBQVhKO0FBSlksU0FBdkI7QUFxQkg7O0FBN0JZLENBQWpCOztrQkFpQ2UsVTs7Ozs7Ozs7QUNqQ2YsSUFBSSxhQUFhO0FBQ2Isb0JBQWdCLDBCQUFXO0FBQ3ZCLFlBQUksV0FBVyxFQUFFLE1BQUYsQ0FBZjs7QUFFQSxZQUFJLENBQUMsU0FBUyxNQUFkLEVBQXNCO0FBQ2xCO0FBQ0g7O0FBRUQsWUFBSSxHQUFKLEdBQVUsSUFBVjtBQUVIOztBQVZZLENBQWpCOztrQkFjZSxVIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIi8vIFlvdSBjYW4gd3JpdGUgYSBjYWxsIGFuZCBpbXBvcnQgeW91ciBmdW5jdGlvbnMgaW4gdGhpcyBmaWxlLlxuLy9cbi8vIFRoaXMgZmlsZSB3aWxsIGJlIGNvbXBpbGVkIGludG8gYXBwLmpzIGFuZCB3aWxsIG5vdCBiZSBtaW5pZmllZC5cbi8vIEZlZWwgZnJlZSB3aXRoIHVzaW5nIEVTNiBoZXJlLlxuXG5pbXBvcnQgYnVyZ2VyIGZyb20gJy4vbW9kdWxlcy9idXJnZXInO1xuaW1wb3J0IHN1Ym1lbnUgZnJvbSAnLi9tb2R1bGVzL3N1Ym1lbnUnO1xuaW1wb3J0IHZhbGlkYXRpb24gZnJvbSAnLi9tb2R1bGVzL3ZhbGlkYXRpb24nO1xuaW1wb3J0IGRVc2VyQWdlbnQgZnJvbSAnLi9tb2R1bGVzL2RldGVjdC11c2VyLWFnZW50JztcbmltcG9ydCB3b3dBbmltYXRlIGZyb20gJy4vbW9kdWxlcy93b3ctYW5pbWF0ZSc7XG5pbXBvcnQgZm9vdGVyIGZyb20gJy4vbW9kdWxlcy9mb290ZXInO1xuaW1wb3J0IGFjY29yZGlvbiBmcm9tICcuL21vZHVsZXMvYWNjb3JkaW9uJztcbmltcG9ydCBzY3JvbGxEb3duIGZyb20gJy4vbW9kdWxlcy9zY3JvbGwtZG93bic7XG5cbiggKCQpID0+IHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIC8vIFdoZW4gRE9NIGlzIHJlYWR5XG4gICQoKCkgPT4ge1xuICAgIGJ1cmdlci5idXJnZXJJbml0KCk7XG4gICAgc3VibWVudS5zdWJtZW51SW5pdCgpO1xuICAgIHZhbGlkYXRpb24udmFsaWRhdGlvbkluaXQoKTtcbiAgICBkVXNlckFnZW50LmRVc2VyQWdlbnRJbml0KCk7XG4gICAgd293QW5pbWF0ZS53b3dBbmltYXRlSW5pdCgpO1xuICAgIGFjY29yZGlvbi5hY2NvcmRpb25Jbml0KCk7XG4gICAgc2Nyb2xsRG93bi5zY3JvbGxEb3duSW5pdCgpO1xuICB9KTtcblxuICBmb290ZXIuZm9vdGVySW5pdCgpO1xuXG59KShqUXVlcnkpO1xuXG4iLCJ2YXIgYWNjb3JkaW9uID0ge1xuICAgIGFjY29yZGlvbkluaXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgJGFjY29yZGlvbkxpbmsgPSAkKCcuYWNjb3JkaW9uIC5hYy1saW5rJyksXG4gICAgICAgICAgICBoZWlnaHRzID0gW107XG5cbiAgICAgICAgaWYgKCEkYWNjb3JkaW9uTGluay5sZW5ndGgpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgICRhY2NvcmRpb25MaW5rLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpe1xuICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICBzaG93VGV4dC5jYWxsKHRoaXMpO1xuICAgICAgICB9KTtcblxuICAgICAgICBmdW5jdGlvbiBzaG93VGV4dCgpe1xuICAgICAgICAgICQodGhpcykuY2xvc2VzdCgnLmFjY29yZGlvbiAuaXRlbScpLnRvZ2dsZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAkKHRoaXMpLm5leHQoKS5zbGlkZVRvZ2dsZSgyMDApO1xuICAgICAgICB9XG5cbiAgICB9XG5cbn07XG5cbmV4cG9ydCBkZWZhdWx0IGFjY29yZGlvbjsiLCJ2YXIgYnVyZ2VyID0ge1xuICAgIGJ1cmdlckluaXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgJGJ1cmdlckJ0biA9ICQoJy5idXJnZXItYnRuJyk7XG5cbiAgICAgICAgaWYgKCEkYnVyZ2VyQnRuLmxlbmd0aCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgJGJ1cmdlckJ0bi5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGJ1cmdlckhhbmRsZXIuY2FsbCh0aGlzKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgZnVuY3Rpb24gYnVyZ2VySGFuZGxlcigpe1xuICAgICAgICAgICQodGhpcykudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICQoJy5oZWFkZXIgLm5hdmJhcicpLnRvZ2dsZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgfVxuXG4gICAgfVxuXG59O1xuXG5leHBvcnQgZGVmYXVsdCBidXJnZXI7IiwidmFyIGRVc2VyQWdlbnQgPSB7XG4gICAgZFVzZXJBZ2VudEluaXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgYnJvd3NlciA9IG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKTtcbiAgICAgICAgaWYoYnJvd3Nlci5pbmRleE9mKCdmaXJlZm94JykgPiAtMSkge1xuICAgICAgICAgICAgJCgnYm9keScpLmFkZENsYXNzKCdtb3onKTtcbiAgICAgICAgfSBlbHNlIGlmKG5hdmlnYXRvci51c2VyQWdlbnQuaW5kZXhPZignVHJpZGVudCcpICE9IC0xICYmIG5hdmlnYXRvci51c2VyQWdlbnQuaW5kZXhPZignTVNJRScpID09IC0xKXtcbiAgICAgICAgXHQkKCdib2R5JykuYWRkQ2xhc3MoJ21zJyk7XG4gICAgICAgIH1cblxuICAgIH1cblxufTtcblxuZXhwb3J0IGRlZmF1bHQgZFVzZXJBZ2VudDsiLCJ2YXIgZm9vdGVyID0ge1xuICAgIGZvb3RlckluaXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgJGZvb3RlcldyYXAgPSAkKCcuZm9vdGVyJyk7XG5cbiAgICAgICAgaWYgKCEkZm9vdGVyV3JhcC5sZW5ndGgpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmKCQod2luZG93KS53aWR0aCgpID4gNzY3KXtcbiAgICAgICAgXHQkZm9vdGVyV3JhcC5mb290ZXJSZXZlYWwoe1xuXHQgICAgICAgICAgICBzaGFkb3c6IGZhbHNlXG5cdCAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgIH1cblxufTtcblxuZXhwb3J0IGRlZmF1bHQgZm9vdGVyOyIsInZhciBzY3JvbGxEb3duID0ge1xuICAgIHNjcm9sbERvd25Jbml0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyICRzY3JvbGxEb3duQnRuID0gJCgnLnNjcm9sbC1kb3duJyksXG4gICAgICAgICAgICAkbWFpblNlY3Rpb24gPSAkKCcuc2Nyb2xsLXRvJyk7XG5cbiAgICAgICAgaWYgKCEkc2Nyb2xsRG93bkJ0bi5sZW5ndGgpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgICRzY3JvbGxEb3duQnRuLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgc2Nyb2xsRG93bkhhbmRsZXIuY2FsbCh0aGlzKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgZnVuY3Rpb24gc2Nyb2xsRG93bkhhbmRsZXIoKXtcbiAgICAgICAgICAgIHZhciBjdXJyZW50UG9zaXRpb24gPSAkbWFpblNlY3Rpb24ub2Zmc2V0KCkudG9wLFxuICAgICAgICAgICAgICAgIHByZXZFbGVtTWFyZ2luID0gcGFyc2VJbnQoJG1haW5TZWN0aW9uLnByZXYoKS5jc3MoJ21hcmdpbi1ib3R0b20nKSk7XG4gICAgICAgICAgICAkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgc2Nyb2xsVG9wOiBjdXJyZW50UG9zaXRpb24gLSBwcmV2RWxlbU1hcmdpblxuICAgICAgICAgICAgfSwgNDAwKTtcbiAgICAgICAgfVxuXG4gICAgfVxuXG59O1xuXG5leHBvcnQgZGVmYXVsdCBzY3JvbGxEb3duOyIsInZhciBzdWJtZW51ID0ge1xuICAgIHN1Ym1lbnVJbml0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyICRzdWJtZW51SXRlbSA9ICQoJy5uYXZiYXIgLnN1YnRpdGxlID4gYScpLFxuICAgICAgICAgICAgJHN1Ym1lbnVXcmFwID0gJCgnLm5hdmJhciAuc3VidGl0bGUnKSxcbiAgICAgICAgICAgICRoZWFkZXIgPSAkKCcuaGVhZGVyJyk7XG5cbiAgICAgICAgaWYgKCEkc3VibWVudUl0ZW0ubGVuZ3RoKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBzdWJtZW51RXZlbnRzVHJpZ2dlcigpO1xuICAgICAgICAkKHdpbmRvdykucmVzaXplKHN1Ym1lbnVFdmVudHNUcmlnZ2VyKTtcblxuICAgICAgICBmdW5jdGlvbiBzdWJtZW51RXZlbnRzVHJpZ2dlcigpe1xuICAgICAgICAgICAgaWYoJCh3aW5kb3cpLndpZHRoKCkgPCAxMjAwKXtcbiAgICAgICAgICAgICAgICAkc3VibWVudUl0ZW0ub2ZmKCdjbGljay5zdWJtZW51JywgaGFuZGxlcik7XG4gICAgICAgICAgICAgICAgJHN1Ym1lbnVXcmFwLm9mZignbW91c2VlbnRlci5zdWJtZW51JywgaG92ZXJJbkZ1bmMpO1xuICAgICAgICAgICAgICAgICRzdWJtZW51V3JhcC5vZmYoJ21vdXNlbGVhdmUuc3VibWVudScsIGhvdmVyT3V0RnVuYyk7XG4gICAgICAgICAgICAgICAgJHN1Ym1lbnVJdGVtLm9uKCdjbGljay5zdWJtZW51JywgaGFuZGxlcik7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICRzdWJtZW51V3JhcC5vZmYoJ21vdXNlZW50ZXIuc3VibWVudScsIGhvdmVySW5GdW5jKTtcbiAgICAgICAgICAgICAgICAkc3VibWVudVdyYXAub2ZmKCdtb3VzZWxlYXZlLnN1Ym1lbnUnLCBob3Zlck91dEZ1bmMpO1xuICAgICAgICAgICAgICAgICRzdWJtZW51SXRlbS5vZmYoJ2NsaWNrLnN1Ym1lbnUnLCBoYW5kbGVyKTtcbiAgICAgICAgICAgICAgICAkc3VibWVudVdyYXAub24oJ21vdXNlZW50ZXIuc3VibWVudScsIGhvdmVySW5GdW5jKTtcbiAgICAgICAgICAgICAgICAkc3VibWVudVdyYXAub24oJ21vdXNlbGVhdmUuc3VibWVudScsIGhvdmVyT3V0RnVuYyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBob3ZlckluRnVuYygpe1xuICAgICAgICAgICAgc3VibWVudU9uSGFuZGxlci5jYWxsKHRoaXMpO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gaG92ZXJPdXRGdW5jKCl7XG4gICAgICAgICAgICBzdWJtZW51T2ZmSGFuZGxlci5jYWxsKHRoaXMpO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gaGFuZGxlcihlKXtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIHN1Ym1lbnVIYW5kbGVyLmNhbGwodGhpcyk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBzdWJtZW51SGFuZGxlcigpe1xuICAgICAgICAgICAgJCh0aGlzKS5jbG9zZXN0KCcuc3VidGl0bGUnKS50b2dnbGVDbGFzcygnY3VycmVudC1tZW51LWl0ZW0nKTtcbiAgICAgICAgICAgICQodGhpcykuY2xvc2VzdCgnLnN1YnRpdGxlJykuZmluZCgndWwnKS5zbGlkZVRvZ2dsZSgyMDApLnN0b3AodHJ1ZSwgdHJ1ZSk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBzdWJtZW51T25IYW5kbGVyKCl7XG4gICAgICAgICAgICAkKHRoaXMpLmNsb3Nlc3QoJy5zdWJ0aXRsZScpLmFkZENsYXNzKCdjdXJyZW50LW1lbnUtaXRlbScpO1xuICAgICAgICAgICAgJCh0aGlzKS5jbG9zZXN0KCcuc3VidGl0bGUnKS5maW5kKCd1bCcpLnNsaWRlRG93bigyMDApLnN0b3AodHJ1ZSwgdHJ1ZSk7XG4gICAgICAgICAgICAkaGVhZGVyLmFkZENsYXNzKCdtZW51LW9wZW4nKTtcbiAgICAgICAgICAgICQoJy5vdmVybGF5JykuZmFkZUluKDIwMCk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBzdWJtZW51T2ZmSGFuZGxlcigpe1xuICAgICAgICAgICAgJCh0aGlzKS5jbG9zZXN0KCcuc3VidGl0bGUnKS5yZW1vdmVDbGFzcygnY3VycmVudC1tZW51LWl0ZW0nKTtcbiAgICAgICAgICAgICQodGhpcykuY2xvc2VzdCgnLnN1YnRpdGxlJykuZmluZCgndWwnKS5zbGlkZVVwKDIwMCkuc3RvcCh0cnVlLCB0cnVlKTtcbiAgICAgICAgICAgICRoZWFkZXIucmVtb3ZlQ2xhc3MoJ21lbnUtb3BlbicpO1xuICAgICAgICAgICAgJCgnLm92ZXJsYXknKS5mYWRlT3V0KDIwMCk7XG4gICAgICAgIH1cblxuICAgIH1cblxufTtcblxuZXhwb3J0IGRlZmF1bHQgc3VibWVudTsiLCJ2YXIgdmFsaWRhdGlvbiA9IHtcbiAgICB2YWxpZGF0aW9uSW5pdDogZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciAkZmVlZGJhY2tGb3JtID0gJCgnLmZlZWRiYWNrLWZvcm0nKTtcblxuICAgICAgICBpZiAoISRmZWVkYmFja0Zvcm0ubGVuZ3RoKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICAkZmVlZGJhY2tGb3JtLnZhbGlkYXRlKHtcbiAgICAgICAgICAgIGVycm9yUGxhY2VtZW50OiBmdW5jdGlvbihlcnJvciwgZWxlbWVudCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBydWxlczoge1xuICAgICAgICAgICAgICAgIG5hbWU6IHtcbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWVcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGNvbXBhbnk6IHtcbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWVcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGVtYWlsOiB7XG4gICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICBlbWFpbDogdHJ1ZVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgcGhvbmU6IHtcbiAgICAgICAgICAgICAgICAgICAgbnVtYmVyOiB0cnVlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgIH1cblxufTtcblxuZXhwb3J0IGRlZmF1bHQgdmFsaWRhdGlvbjsiLCJ2YXIgd293QW5pbWF0ZSA9IHtcbiAgICB3b3dBbmltYXRlSW5pdDogZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciAkd293RWxlbSA9ICQoJy53b3cnKTtcblxuICAgICAgICBpZiAoISR3b3dFbGVtLmxlbmd0aCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgbmV3IFdPVygpLmluaXQoKTtcblxuICAgIH1cblxufTtcblxuZXhwb3J0IGRlZmF1bHQgd293QW5pbWF0ZTsiXX0=

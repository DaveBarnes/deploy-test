<header class="header <?php (!empty($lighttheme) && $lighttheme == 'light-theme') ? print 'light-theme' : '' ?>">
    <div class="logo">
        <a href="/amp/">
            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo.svg" width="165" height="73" class="logo-default" alt="AMP Result driven technology by pulsant">
            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo-blue.svg" width="165" height="73" class="logo-blue" alt="AMP Result driven technology by pulsant">
        </a>
    </div>
    <button class="burger-btn">
        <span>Menu</span>
    </button>
    <nav class="navbar">
        <ul class="menu">
            <li class="back"><a href="/">Back to Pulsant.com</a></li>
            <li class="subtitle">
                <a href="#">Hybrid Azure Solutions <span class="arrow"><span class="sr-only">arrow</span></span></a>
                <ul>
                    <li><a href="/amp/business-agility-and-scalability">Business Agility and Scalability</a></li>
                    <li><a href="/amp/data-governance-security-and-compliance">Data Governance, Security and Continuity</a></li>
                    <li><a href="/amp/disconnected-solutions">Disconnected Solution</a></li>
                    <li><a href="/amp/business-intelligence">Business Intelligence</a></li>
                </ul>
            </li>
            <li><a href="/amp/office-365">Office 365</a></li>
            <li><a href="/amp/amp-launch-pad">AMP Launch Pad</a></li>
            <li><a href="/amp/technology">Technology</a></li>
            <li><a href="/amp/journey-to-cloud">Journey To Cloud</a></li>
            <li><a href="/amp/partner">Partner</a></li>
            <li class="subtitle">
                <a href="#">About AMP <span class="arrow"><span class="sr-only">arrow</span></span></a>
                <ul>
                    <li><a href="/amp/about-us">About Us</a></li>
                    <li><a href="/amp/faq">FAQ</a></li>
                </ul>
            </li>
            <li><a href="/amp/one-node-as-a-service">Contact us</a></li>
        </ul>
    </nav>
    <div class="overlay"><span class="sr-only">overlay</span></div>
</header>
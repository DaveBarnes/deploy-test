<footer class="footer <?php (!empty($roadpage) && $roadpage == 'road-page') ? print 'road-page' : '' ?>">
    <div class="container">
        <div class="footer-block">
            <div class="top">
                <strong class="title">A <span class="logo-wrap"><img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/pulsant.png, <?php bloginfo('template_url'); ?>/amp/assets/images/pulsant-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/pulsant.png" alt="Pulsant"></span> initiative, in collaboration with</strong>
                <ul class="list-logo">
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/ms-azure.png, <?php bloginfo('template_url'); ?>/amp/assets/images/ms-azure-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/ms-azure.png" alt="Microsoft Azure">
                    </li>
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/dell-emc.png, <?php bloginfo('template_url'); ?>/amp/assets/images/dell-emc-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/dell-emc.png" alt="Dell EMC">
                    </li>
                </ul>
                <p>With 20 years of experience and over 400 employees, Pulsant is the premier provider of managed hybrid cloud solutions throughout the United Kingdom and beyond.</p>
            </div>
        </div>
        <div class="inner">
            <div class="item">
                <ul class="footer-list">
                    <li><a href="#">Solutions</a></li>
                    <li class="thin"><a href="/amp/business-agility-and-scalability">Agility &AMP; Scalability</a></li>
                    <li class="thin"><a href="/amp/data-governance-security-and-compliance">Governance, Security &AMP; Continuity</a></li>
                    <li class="thin"><a href="/amp/disconnected-solutions">Disconnected Solutions</a></li>
                    <li class="thin"><a href="/amp/business-intelligence">Business Intelligence</a></li>
                </ul>
            </div>
            <div class="item">
                <ul class="footer-list">
                    <li><a href="/amp/technology">Technology</a></li>
                    <li><a href="/amp/journey-to-cloud">Journey to Cloud</a></li>
                    <li><a href="/amp/office-365">Office 365</a></li>
                    <li><a href="/amp/amp-launch-pad">AMP Launch Pad</a></li>
                    <li><a href="/amp/faq">FAQ</a></li>
                </ul>
            </div>
            <div class="item">
                <ul class="footer-list">
                    <li><a href="/amp/about-us">About us</a></li>
                    <li><a href="/amp/one-node-as-a-service">Contact us</a></li>
                    <li class="thin">Telephone: <a href="tel:+3451199911">0345 119 9911</a></li>
                    <li class="thin">Email: <a href="mailto:sales@pulsant.com">sales@pulsant.com</a></li>
                </ul>
            </div>
        </div>
        <div class="copyright">&copy; Copyright 2017 Pulsant - All rights reserved - <a href="https://www.pulsant.com/terms-of-use/" target="_blank">General Terms &AMP; Conditions</a> - <a href="https://www.pulsant.com/privacy-cookie-policy/" target="_blank">Privacy Policy</a> - <a href="https://www.pulsant.com/wp-content/themes/pulsant/amp/QMTH_Landing_Page_Content.pdf" target="_blank">Qualified Multitenant Hoster (QMTH) Program</a></div>
    </div>
    
</footer>


<!-- Analytics -->
<script type="text/javascript" async>
    var _gaq = _gaq || [];
    var pluginUrl =
        '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
        _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
        _gaq.push(['_setAccount', 'UA-504794-5']);
        _gaq.push(['_setDomainName', 'pulsant.com']);
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['addIgnoreRef', 'pulsant.com']);
        _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>

<!-- WOW Async Tracking Code Start -->
<script data-cfasync='false' type='text/javascript'>
    var _wow = _wow || [];
    (function () {
        try{
            _wow.push(['setClientId', '6c8484a6-dec7-4914-8da7-bc763086cad4']);
            _wow.push(['enableDownloadTracking']);
            _wow.push(['trackPageView']);
            var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
            g.type = 'text/javascript'; g.defer = true; g.async = true;
            g.src = '//t.wowanalytics.co.uk/Scripts/tracker.js';
            s.parentNode.insertBefore(g, s);
        }catch(err){}})();
</script>
<!-- WOW Async Tracking Code End -->

<!-- Lead Forensics -->
<script type="text/javascript" src="http://www.s3network1.com/js/61634.js" ></script>
<noscript><img src="http://www.s3network1.com/61634.png" style="display:none;" /></noscript>

<!-- Click Dimensions -->
<script type="text/javascript">
    var cdJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
    document.write(unescape("%3Cscript src='" + cdJsHost + "analytics-eu.clickdimensions.com/ts.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
    var cdAnalytics = new clickdimensions.Analytics('analytics-eu.clickdimensions.com');
    cdAnalytics.setAccountKey('ayjByqztN0KZ9jeWZ29B6g');
    cdAnalytics.setDomain('pulsant.com');
    cdAnalytics.setScore(typeof(cdScore) == "undefined" ? 0 : (cdScore == 0 ? null : cdScore));
    cdAnalytics.trackPage();
</script>

<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/2535600.js"></script>
<!-- End of HubSpot Embed Code -->

<!-- Schema Embed -->
<script type='application/ld+json'>
{
        "@context": "http://schema.org/",
        "@type": "Service",
        "hasOfferCatalog": {
                "@type": "OfferCatalog",
                "description": "A consistent cloud architecture in both Azure Stack and Azure.",
                "mainEntityOfPage": "https://www.pulsant.com/amp/",
                "name": "Managed Microsoft Azure
        },
        "provider": "Pulsant"
}
</script>


<!-- Bing UET Code -->
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5563916"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5563916&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

<!-- LinkedIn Insight Tag -->
<script type="text/javascript">
    _linkedin_data_partner_id = "42767";
</script>
<script type="text/javascript">
    (function(){var s = document.getElementsByTagName("script")[0];
    var b = document.createElement("script");
    b.type = "text/javascript";b.async = true;
    b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
    s.parentNode.insertBefore(b, s);})();
</script>
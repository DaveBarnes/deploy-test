<?php
//Process a new form submission in HubSpot in order to create a new Contact.

$hubspotutk      = $_COOKIE['hubspotutk']; //grab the cookie from the visitors browser.
$ip_addr         = $_SERVER['REMOTE_ADDR']; //IP address too.
$download_path   = $_POST['downloadpath']; //file download path
$hs_context      = array(
    'ipAddress' => $ip_addr,
    'pageUrl' => $_POST['pageurl'],
    'pageName' => $_POST['eventlabel']
);
$hs_context_json = json_encode($hs_context);

//Need to populate these variable with values from the form.
$str_post = "firstname=" . urlencode($_POST['downloadname'])
    . "&lastname=" . urlencode($_POST['downloadlastname'])
    . "&email=" . urlencode($_POST['downloademail'])
    //. "&phone=" . urlencode($phonenumber)
    //. "&company=" . urlencode($company)
    . "&hs_context=" . urlencode($hs_context_json); //Leave this one be

//replace the values in this URL with your portal ID and your form GUID
$endpoint = 'https://forms.hubspot.com/uploads/form/v2/2535600/d9c570f5-05df-4d64-99d7-d96622df3a1b';

$ch = @curl_init();
@curl_setopt($ch, CURLOPT_POST, true);
@curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
@curl_setopt($ch, CURLOPT_URL, $endpoint);
@curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded'
));
@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response    = @curl_exec($ch); //Log the response from HubSpot as needed.
$status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE); //Log the response status code
@curl_close($ch);
//echo $status_code . " " . $response;
//header("Location:$download_path");
//header('Location: http://www.pulsant.com/form-submission-failed');

if( $status_code == 204 || $status_code == 302 ) {
    echo '<h2>Thank you for downloading our whitepaper.</h2><p><a href="'.$download_path.'" target="_blank">Click here to download the whitepaper</a></p>';
} else {
    echo "<h2>There was an error with form submiting. Please try again.</h2>";
}
?>

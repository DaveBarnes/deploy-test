<?php
/*
Template Name: AMP - Governance
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">

  <title>AMP - Data Governance, Security & Continuity</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en" />
<meta name="robots" content="index, follow">

<link rel="icon" type="img/png" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/img/pulsant-favicon-16x16.png">
<link rel="apple-touch-icon" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">

<meta name="description" content="Serve your clients better while keeping their data secure. A secure, compliant hybrid cloud platform delivered by AMP by Pulsant.">
<meta name="author" content="Pulsant" />

<meta property="og:url" content="" />
<meta property="og:site_name" content="Pulsant" />
<meta property="og:type" content="website" />
<meta property="og:title" content="AMP - Data Governance, Security & Continuity" />
<meta property="og:image" content="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />
<meta property="og:description" content="Serve your clients better while keeping their data secure. A secure, compliant hybrid cloud platform delivered by AMP by Pulsant." />

<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="Serve your clients better while keeping their data secure. A secure, compliant hybrid cloud platform delivered by AMP by Pulsant."/>
<meta name="twitter:title" content="AMP - Data Governance, Security & Continuity"/>
<meta name="twitter:site" content="@pulsantuk"/>
<meta name="twitter:creator" content="@pulsantuk"/>

  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/vendor.min.css">
  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/style.min.css">
  <script src=" https://use.typekit.net/gkc7sbg.js "></script>
  <script>try{Typekit.load({ async: false });}catch(e){}</script>
  <style>
        .main-info-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd.jpg);
        }
        .main-info-bg.isv-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd.jpg);
        }
        .main-info-bg.road-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd.jpg);
        }
        .main-info-bg.boost-product-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd.jpg);
        }
        .main-info-bg.scaleup-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd.jpg);
        }
        .main-info-bg.staytuned-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd.jpg);
        }
        .main-info-bg.boost-business-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd.jpg);
        }
        .main-info-bg.performance-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd.jpg);
        }
        .main-info-bg.try-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd.jpg);
        }
        .modal-block-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd.jpg);
        }
        .footer{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd.jpg);
        }
        @media (-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd-2x.jpg);
            }
        }
        @media screen and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:1280px), (min-resolution: 144dpi) and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape-2x.jpg);
            }
        }
        @media screen and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:980px), (min-resolution: 144dpi) and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet-2x.jpg);
            }
        }
    </style>
</head>

<body>
  <?php $lighttheme = "" ?>
  <?php $roadpage = "" ?>
  <div id="wrapper">
    <?php include("inc/header.php"); ?>
    <main class="main">
        <section class="main-info main-info-bg scaleup-page">
            <header class="meta">
                <h1 class="big-title">AMP Data Governance, Security &amp; Continuity</h1>
                <p>SCALE-UP YOUR BUSINESS</p>
                <span class="scroll-down">WHILE KEEPING YOUR DATA CLOSE<span class="arrow"><span class="sr-only">arrow</span></span></span>
            </header>
        </section>
        <section class="simple-text-block scaleup-page">
            <div class="container">
                <p>Store your data where it’s safe; on-premises or in Pulsant datacentres, aligned with your specific regulatory environment and utilise the benefits of the cloud to offer your customers highly availble cloud applications.</p>
            </div>
        </section>
        <section class="image-text-block scaleup-page scroll-to">
            <div class="container">
                <div class="item">
                    <div class="text">
                        <strong class="bold-title">Serve your clients better while keeping their data secure<span class="description">A secure, compliant hybrid cloud platform</span></strong>
                        <ul class="list">
                            <li>Keep data secure, on-premises or in our datacenters, while serving customer’s reliable and scalable front-end services.</li>
                            <li>Aligns perfectly with any regulatory environment, making sure you can innovate and improve services.</li>
                            <li>Build hybrid applications that span on-premises infrastructure and Azure, without compromising privacy or performance.</li>
                        </ul>
                    </div>
                    <div class="img-holder">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/img5.png, <?php bloginfo('template_url'); ?>/amp/assets/images/img5-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/img5.png" alt="image description">
                    </div>
                </div>
                <div class="item">
                    <div class="text">
                        <strong class="bold-title">Safeguard business continuity of on-premises applications</strong>
                        <ul class="list">
                            <li>Orchestrated, automated protection and disaster recovery makes sure you have ongoing replication of data at all times.</li>
                            <li>Continuous health monitoring ensures your backups and replicated data are always valid.</li>
                            <li>Test recovery plans without any impact on day to day business or application performance.</li>
                        </ul>
                    </div>
                    <div class="img-holder">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/img6.png, <?php bloginfo('template_url'); ?>/amp/assets/images/img6-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/img6.png" alt="image description">
                    </div>
                </div>
                <div class="item">
                    <div class="text">
                        <strong class="bold-title">Deploy applications globally without comprimising regulations and compliance</strong>
                        <p>No need to create multiple applications for different regulatory environments. One application pattern overns your deployment worldwide, with per-country or location-based policies to make sure data and security is compliant with rules and regulations.</p>
                    </div>
                    <div class="img-holder">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/development-img2.png, <?php bloginfo('template_url'); ?>/amp/assets/images/development-img2-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/development-img2.png" alt="image description">
                    </div>
                </div>
            </div>
        </section>
        <section class="compliance-block scaleup-page">
            <div class="container">
                <div class="meta">
                    <h1 class="title">Continuous Compliance with the Pulsant Compliance Platform</h1>
                    <p>A continuous 360° view on the state of your IT compliance</p>
                </div>
                <div class="inner">
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico1.svg" width="115" height="131" alt="image description">
                        </div>
                        <div class="text">
                            <strong class="mini-title">We continually ensure your IT is fully compliant with relevant regulatory frameworks</strong>
                            <p>Whatever your regulatory and industry requirements are, such as FCA, ISO27001, PCI. We ensure compliance 24x7x365.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico2.svg" width="115" height="132" alt="image description">
                        </div>
                        <div class="text">
                            <strong class="mini-title">Our compliance platform monitors your IT in real time</strong>
                            <p>With 24/7 monitoring, non-intrusive vulnerability &AMP; penetration testing we automatically generate non-compliance reports.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico3.svg" width="115" height="118" alt="image description">
                        </div>
                        <div class="text">
                            <strong class="mini-title">Out-of-the-box support for any IT environment</strong>
                            <p>Whether you utilise Azure public cloud, Azure Stack on-premises or a different hybrid model, we ensure compliance across your environments.</p>
                        </div>
                    </div>
                </div>
                <a href="/amp/technology" class="btn-transparent">Explore the technology in amplify Data Governance</a>
            </div>
        </section>
        <section class="image-list-box scaleup-page wow slideInUp">
            <div class="container">
                <div class="inner">
                    <div class="img-holder">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/img-box-isv.jpg, <?php bloginfo('template_url'); ?>/amp/assets/images/img-box-isv-2x.jpg 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/img-box-isv.jpg" alt="image description">
                    </div>
                    <div class="text">
                        <strong class="bold-title">Easily manage cost with the same pricing model across environments.</strong>
                        <ul class="text-list">
                            <li>No upfront licensing fees for Azure services in a hybrid scenario.</li>
                            <li>With our CSP status you have no additional licensing model to worry about.</li>
                            <li>Only pay when you use services.</li>
                            <li>Metering is done using the same units as Azure.</li>
                            <li>Lower pricing in Azure stack, since you operate some of the hardware and facilities yourself.</li>
                            <li>A fixed-price “capacity model” available for disconnected systems on-premises.</li>
                            <li>A single unified invoice for all your services.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="cloud-block scaleup-page">
            <div class="container">
                <div class="meta">
                    <h1 class="middle-title">Journey To The Cloud and amplify your business</h1>
                    <p>From the discovery phase to design and deployment, our Journey To The Cloud offers services like complete assesments of your environments, providing test set-ups, technical consultancy, implementation and deployment to fully managed services like monitoring, support and reporting.</p>
                </div>
                <div class="inner">
                    <span class="line"><span class="circle"><span class="sr-only">line</span></span></span>
                    <div class="items-hold">
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud1.svg" width="123" height="160" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Foundation Services:<br>Readiness &AMP; Transformation</strong>
                                <ul class="link-list">
                                    <li>Cloud Readiness Assesment</li>
                                    <li>Cloud Licensing Advisory</li>
                                    <li>Azure benchmark services</li>
                                    <li>Azure Stack Proof of Concept</li>
                                    <li>Azure Everywhere Workshop</li>
                                    <li>Azure Risk Advisory</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud2.svg" width="175" height="158" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Consultancy Services:<br>Strategy &AMP; Technical Implementation</strong>
                                <ul class="link-list">
                                    <li>Digital Transformation Programme</li>
                                    <li>Migration</li>
                                    <li>Technical Implementation</li>
                                    <li>Cloud Management Stack</li>
                                    <li>Azure Specialist Service</li>
                                    <li>Cloud Compliance</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud3.svg" width="169" height="172" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Managed Services</strong>
                                <ul class="link-list">
                                    <li>Service Management</li>
                                    <li>Monitoring &AMP; Reporting</li>
                                    <li>Protection &AMP; Security</li>
                                    <li>Patch &AMP; Backup management</li>
                                    <li>Identity &AMP; Access Management</li>
                                    <li>Continuous Compliance</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/amp/journey-to-cloud" class="btn-transparent">Journey To the Cloud</a>
            </div>
        </section>
        <section class="banner scaleup-page">
            <div class="img-wrap">
                <div class="img-holder">
                    <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/banner-img-scaleup.jpg, <?php bloginfo('template_url'); ?>/amp/assets/images/banner-img-scaleup-2x.jpg 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/banner-img-scaleup.jpg" alt="image description">
                </div>
            </div>
            <div class="text">
                <h1 class="title">Want to explore secure hybrid Azure quickly?</h1>
                <p>A single one node Azure Stack solution to help you explore the possibilities.</p>
                <a href="/amp/one-node-as-a-service" class="btn-transparent">Discover how</a>
            </div>
        </section>
    </main>
    <?php include("inc/footer.php"); ?>
  </div>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/vendor.min.js" defer></script>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/app.js" defer></script>
</body>

</html>

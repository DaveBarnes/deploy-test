<?php
/*
Template Name: AMP - One Node
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">

  <title>Azure Stack one node as a Service - Pulsant AMP</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en" />
<meta name="robots" content="index, follow">

<link rel="icon" type="img/png" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/img/pulsant-favicon-16x16.png">
<link rel="apple-touch-icon" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">

<meta name="description" content="Try Azure Stack today with our One-Node-as-a-Service proof of concept deployment and discover the benefits of true hybrid cloud.">
<meta name="author" content="Pulsant" />

<meta property="og:url" content="" />
<meta property="og:site_name" content="Pulsant" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Azure Stack one node as a Service - Pulsant AMP" />
<meta property="og:image" content="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />
<meta property="og:description" content="Try Azure Stack today with our One-Node-as-a-Service proof of concept deployment and discover the benefits of true hybrid cloud." />

<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="Try Azure Stack today with our One-Node-as-a-Service proof of concept deployment and discover the benefits of true hybrid cloud."/>
<meta name="twitter:title" content="Azure Stack one node as a Service - Pulsant AMP"/>
<meta name="twitter:site" content="@pulsantuk"/>
<meta name="twitter:creator" content="@pulsantuk"/>

  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/vendor.min.css">
  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/style.min.css">
  <script src=" https://use.typekit.net/gkc7sbg.js "></script>
  <script>try{Typekit.load({ async: false });}catch(e){}</script>
  <style>
        .main-info-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd.jpg);
        }
        .main-info-bg.isv-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd.jpg);
        }
        .main-info-bg.road-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd.jpg);
        }
        .main-info-bg.boost-product-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd.jpg);
        }
        .main-info-bg.scaleup-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd.jpg);
        }
        .main-info-bg.staytuned-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd.jpg);
        }
        .main-info-bg.boost-business-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd.jpg);
        }
        .main-info-bg.performance-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd.jpg);
        }
        .main-info-bg.try-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd.jpg);
        }
        .modal-block-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd.jpg);
        }
        .footer{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd.jpg);
        }
        @media (-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd-2x.jpg);
            }
        }
        @media screen and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:1280px), (min-resolution: 144dpi) and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape-2x.jpg);
            }
        }
        @media screen and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:980px), (min-resolution: 144dpi) and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet-2x.jpg);
            }
        }
    </style>
</head>

<body>
  <?php $lighttheme = "" ?>
  <?php $roadpage = "" ?>
  <div id="wrapper">
    <?php include("inc/header.php"); ?>
    <main class="main">
        <section class="main-info main-info-bg try-page">
            <header class="meta">
                <h1 class="big-title">try azure stack today</h1>
                <p>And learn how it can amplify your business</p>
                <span class="scroll-down">with AMP Launch Pad <span class="arrow"><span class="sr-only">arrow</span></span></span>
            </header>
        </section>
        <section class="simple-text-block try-page">
            <div class="container">
                <p>AMP Launch Pad is a proof of concept proposition to help you get started with Azure Stack quickly, allowing you to prepare your organisation for new operating models.</p>
            </div>
        </section>
        <section class="try-block scroll-to">
            <div class="container">
                <div class="item">
                    <div class="ico-holder">
                        <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico31.svg" width="115" height="120" alt="image description">
                    </div>
                    <strong class="title">Hit the ground running</strong>
                    <p>Azure Stack ONaaS can be rolled out within one day, allowing you to start exploring the possibilities immediately.</p>
                </div>
                <div class="item">
                    <div class="ico-holder">
                        <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico32.svg" width="115" height="107" alt="image description">
                    </div>
                    <strong class="title">Understand the impact on IT service delivery</strong>
                    <p>Get an understanding and learn about how Azure Stack can power your hybrid IT environments.</p>
                </div>
                <div class="item">
                    <div class="ico-holder">
                        <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico33.svg" width="115" height="108" alt="image description">
                    </div>
                    <strong class="title">Prepare your IT teams</strong>
                    <p>Amplify your team's development and make them hands-on so they are ready sooner rather than later to make the move.</p>
                </div>
                <div class="item">
                    <div class="ico-holder">
                        <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico34.svg" width="115" height="117" alt="image description">
                    </div>
                    <strong class="title">Experience new capabilities</strong>
                    <p>Open your eyes to the world of true hybrid Azure to ensure you get the most out of the latest capabilities.</p>
                </div>
            </div>
        </section>
        <section class="try-form-block">
            <div class="container">
                <div class="col list-hold">
                    <strong class="bold-title">AMP Launch Pad.<br>A truly impressive package.</strong>
                    <ul class="list">
                        <li>A single node Azure Stack appliance, powered by Dell Enterprise Server technology and hosted in a Pulsant Datacenter.</li>
                        <li>A great place to start when exploring Hybrid and/or Modern Application development.</li>
                        <li>Free, complementary Azure Stack Design Consultancy to define an Azure roadmap and guide your transformation.</li>
                    </ul>
                    <span class="arrow-holder">Want to learn more or interested in AMP Launch Pad? Get in touch.</span>
                </div>
                <div class="col form-hold">
                    <strong class="bold-title">Start exploring Azure today.</strong>
                    <p>Get in touch with our consultants.<br>We’ll get back to you within one business day.</p>

                    <form action="/api/webforms.php" method="post" name="contactform" id="contactform" class="validate feedback-form">

                        <input type="hidden" id="formtype" name="form" value="contact-form" />
                        <input type="hidden" name="pageurl" value="<?php $pageurl = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; echo $pageurl; ?>" />
                        <input type="hidden" name="eventcategory" id="eventcategory" value="Form Submission" />
                        <input type="hidden" name="eventaction" id="eventaction" value="AMP Contact Us" />
                        <input type="hidden" name="eventlabel" id="eventlabel" value="<?php the_title(); ?>" />



                        <input type="text" class="form-control simple-input contact-custom required" id="name" name="name" placeholder="Name">
                        <input type="email" id="email" name="email" class="form-control simple-input contact-custom required" placeholder="Email Address" data-regex="^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$">
                        <input type="text" class="form-control simple-input contact-custom required" id="company" name="company" placeholder="Company Name">
                        <input type="tel" class="form-control simple-input contact-custom" id="phone" name="phone" placeholder="Telephone number (optional)">


                        <div>
                            <?php $num1=rand(2,4); $num2=rand(2,5); $total=$num1+$num2; ?>
                            <p class="margin-top-25">CAPTCHA question: What is <?php echo $num1; ?> + <?php echo $num2; ?>?</p>
                            <input type="tel" id="captcha" name="captcha" class="form-control simple-input contact-custom required" data-regex="^([<?php echo $total; ?>]{1})$" />
                            <input type="hidden" value="<?php echo $total; ?>" name="validation">
                        </div>


                        <button type="submit" class="btn-fill" id="submit" name="Submit">Get back to me</button>


                    <?php /* ORIGINAL FORM
                        <form action="#" method="post" class="feedback-form">
                        <input type="text" name="name" class="simple-input" placeholder="Name*">
                        <input type="text" name="company" class="simple-input" placeholder="Company Name*">
                        <input type="email" name="email" class="simple-input" placeholder="Email address*">
                        <input type="tel" name="phone" class="simple-input" placeholder="Phone Number (optional)">
                        <button type="submit" class="btn-fill">Get back to me</button> */ ?>
                    </form>
                </div>
            </div>
        </section>
    </main>
    <?php include("inc/footer.php"); ?>
  </div>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/vendor.min.js" defer></script>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/app.js" defer></script>
</body>

</html>
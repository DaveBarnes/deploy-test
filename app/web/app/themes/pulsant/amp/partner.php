<?php
/*
Template Name: AMP - Partner
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">

  <title>The AMP Microsoft Azure Partner Programme | Pulsant</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en" />
<meta name="robots" content="index, follow">

<link rel="icon" type="img/png" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/img/pulsant-favicon-16x16.png">
<link rel="apple-touch-icon" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">

<meta name="description" content="Boost your Business with the AMP Partner Programme. With the Pulsant AMP suite of solutions, deliver the hybrid Azure solutions your customers need.">
<meta name="author" content="Pulsant" />

<meta property="og:url" content="" />
<meta property="og:site_name" content="Pulsant" />
<meta property="og:type" content="website" />
<meta property="og:title" content="The AMP Microsoft Azure Partner Programme | Pulsant" />
<meta property="og:image" content="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />
<meta property="og:description" content="Boost your Business with the AMP Partner Programme. With the Pulsant AMP suite of solutions, deliver the hybrid Azure solutions your customers need." />

<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="Boost your Business with the AMP Partner Programme. With the Pulsant AMP suite of solutions, deliver the hybrid Azure solutions your customers need."/>
<meta name="twitter:title" content="The AMP Microsoft Azure Partner Programme | Pulsant"/>
<meta name="twitter:site" content="@pulsantuk"/>
<meta name="twitter:creator" content="@pulsantuk"/>

  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/vendor.min.css">
  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/style.min.css">
  <script src=" https://use.typekit.net/gkc7sbg.js "></script>
  <script>try{Typekit.load({ async: false });}catch(e){}</script>
  <style>
        .main-info-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd.jpg);
        }
        .main-info-bg.isv-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd.jpg);
        }
        .main-info-bg.road-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd.jpg);
        }
        .main-info-bg.boost-product-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd.jpg);
        }
        .main-info-bg.scaleup-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd.jpg);
        }
        .main-info-bg.staytuned-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd.jpg);
        }
        .main-info-bg.boost-business-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd.jpg);
        }
        .main-info-bg.performance-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd.jpg);
        }
        .main-info-bg.try-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd.jpg);
        }
        .modal-block-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd.jpg);
        }
        .footer{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd.jpg);
        }
        @media (-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd-2x.jpg);
            }
        }
        @media screen and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:1280px), (min-resolution: 144dpi) and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape-2x.jpg);
            }
        }
        @media screen and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:980px), (min-resolution: 144dpi) and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet-2x.jpg);
            }
        }
    </style>
</head>

<body>
  <?php $lighttheme = "" ?>
  <?php $roadpage = "" ?>
  <div id="wrapper">
    <?php include("inc/header.php"); ?>
    <main class="main">
        <section class="main-info main-info-bg boost-business-page">
            <header class="meta">
                <h1 class="big-title">With the AMP Partner Programme</h1>
                <p>boost your business</p>
            </header>
        </section>
        <section class="simple-text-block boost-business-page">
            <div class="container">
                <p>The world of technology is changing at blazing speeds, with your customer's IT needs becoming increasingly demanding. With the Pulsant AMP suite of solutions, deliver the hybrid Azure solutions your customers need. Even when you are not a provider with Microsoft CSP status, benefit from our partnership and amplify your business.</p>
            </div>
        </section>
        <section class="compliance-block boost-business-page">
            <div class="container">
                <div class="inner">
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico11.svg" width="115" height="124" alt="image description">
                        </div>
                        <div class="text">
                            <strong class="mini-title">Deliver complete integrated solutions</strong>
                            <p>From modern hybrid development streets to compliant solutions for the financial industry.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico12.svg" width="115" height="122" alt="image description">
                        </div>
                        <div class="text">
                            <strong class="mini-title">Our team of consultants and experts stand ready</strong>
                            <p>Microsoft Certified engineers can assist you from start to finish with developing and implementing solutions.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico13.svg" width="115" height="122" alt="image description">
                        </div>
                        <div class="text">
                            <strong class="mini-title">No CSP status required</strong>
                            <p>With our CSP status you can benefit from easy licensing possibilities and simple billing &AMP; invoicing.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="modal-block modal-block-bg">
            <div class="container">
                <span class="heading">Interested to learn more?</span>
                <a href="#" class="btn-fill" data-toggle="modal" data-target="#feedback_modal">Get in touch</a>
            </div>
        </section>
    </main>
    <?php include("inc/footer.php"); ?>
    <div class="modal fade" id="feedback_modal" tabindex="-1" role="dialog" aria-labelledby="feedback_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="meta">
                    <strong class="modal-title">Get in touch using the form below</strong>
                    <p>We’ll get back to you within 1 business day. You can also call us directly on <a href="tel:03451199911">0345 119 9911</a> or email us at <a href="mailto:sales@pulsant.com">sales@pulsant.com</a></p>
                </div>

                <form action="/api/webforms.php" method="post" name="contactform" id="contactform" class="validate feedback-form">

                    <input type="hidden" id="formtype" name="form" value="contact-form" />
                    <input type="hidden" name="pageurl" value="<?php $pageurl = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; echo $pageurl; ?>" />
                    <input type="hidden" name="eventcategory" id="eventcategory" value="Form Submission" />
                    <input type="hidden" name="eventaction" id="eventaction" value="AMP Partner" />
                    <input type="hidden" name="eventlabel" id="eventlabel" value="<?php the_title(); ?>" />

                    <input type="text" class="form-control simple-input contact-custom required" id="name" name="name" placeholder="Name">
                    <input type="email" id="email" name="email" class="form-control simple-input contact-custom required" placeholder="Email Address" data-regex="^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$">
                    <input type="text" class="form-control simple-input contact-custom required" id="company" name="company" placeholder="Company Name">
                    <input type="tel" class="form-control simple-input contact-custom" id="phone" name="phone" placeholder="Telephone number (optional)">
                    <textarea type="textarea" class="form-control simple-input textarea text-custom" id="message" rows="8" name="message" placeholder="Your message (optional)"></textarea>


                    <div>
                        <?php $num1=rand(2,4); $num2=rand(2,5); $total=$num1+$num2; ?>
                        <p class="margin-top-25">CAPTCHA question: What is <?php echo $num1; ?> + <?php echo $num2; ?>?</p>
                        <input type="tel" id="captcha" name="captcha" class="form-control simple-input contact-custom required" data-regex="^([<?php echo $total; ?>]{1})$" />
                        <input type="hidden" value="<?php echo $total; ?>" name="validation">
                    </div>

                    <button type="submit" class="btn-fill" id="submit" name="Submit">Submit</button>

                </form>


                <?php /* ORIGINAL FORM
                <form action="#" method="post" class="feedback-form">
                    <input type="text" name="name" class="simple-input" placeholder="Name*">
                    <input type="text" name="company" class="simple-input" placeholder="Company Name*">
                    <input type="email" name="email" class="simple-input" placeholder="Email address*">
                    <input type="tel" name="phone" class="simple-input" placeholder="Phone Number (optional)">
                    <textarea class="simple-input textarea" placeholder="Your message (optional)"></textarea>
                    <button type="submit" class="btn-fill">Get back to me</button>
                </form>
                */ ?>
            </div>
        </div>
    </div>
  </div>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/vendor.min.js" defer></script>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/app.js" defer></script>
</body>

</html>
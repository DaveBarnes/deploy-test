<?php
/*
Template Name: AMP - Journey
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">

  <title>The Journey to the Cloud | Pulsant</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en" />
<meta name="robots" content="index, follow">

<link rel="icon" type="img/png" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/img/pulsant-favicon-16x16.png">
<link rel="apple-touch-icon" href="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">

<meta name="description" content="We start each engagement with a solid stack of foundation services to make sure we understand your business objectives, IT architecture, cloud readiness.">
<meta name="author" content="Pulsant" />

<meta property="og:url" content="" />
<meta property="og:site_name" content="Pulsant" />
<meta property="og:type" content="website" />
<meta property="og:title" content="The Journey to the Cloud | Pulsant" />
<meta property="og:image" content="https://www.pulsant.com/wp-content/themes/pulsant/amp/apple-touch-icon.png" />
<meta property="og:description" content="We start each engagement with a solid stack of foundation services to make sure we understand your business objectives, IT architecture, cloud readiness." />

<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="We start each engagement with a solid stack of foundation services to make sure we understand your business objectives, IT architecture, cloud readiness."/>
<meta name="twitter:title" content="The Journey to the Cloud | Pulsant"/>
<meta name="twitter:site" content="@pulsantuk"/>
<meta name="twitter:creator" content="@pulsantuk"/>

  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/vendor.min.css">
  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/style.min.css">
  <script src=" https://use.typekit.net/gkc7sbg.js "></script>
  <script>try{Typekit.load({ async: false });}catch(e){}</script>
  <style>
        .main-info-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd.jpg);
        }
        .main-info-bg.isv-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd.jpg);
        }
        .main-info-bg.road-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd.jpg);
        }
        .main-info-bg.boost-product-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd.jpg);
        }
        .main-info-bg.scaleup-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd.jpg);
        }
        .main-info-bg.staytuned-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd.jpg);
        }
        .main-info-bg.boost-business-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd.jpg);
        }
        .main-info-bg.performance-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd.jpg);
        }
        .main-info-bg.try-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd.jpg);
        }
        .modal-block-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd.jpg);
        }
        .footer{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd.jpg);
        }
        @media (-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd-2x.jpg);
            }
        }
        @media screen and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:1280px), (min-resolution: 144dpi) and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape-2x.jpg);
            }
        }
        @media screen and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:980px), (min-resolution: 144dpi) and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet-2x.jpg);
            }
        }
    </style>
</head>

<body>
  <?php $lighttheme = "" ?>
  <?php $roadpage = "road-page" ?>
  <div id="wrapper">
    <?php include("inc/header.php"); ?>
    <main class="main">
        <section class="main-info main-info-bg road-page">
            <header class="meta">
                <h1 class="big-title">The Journey To Cloud</h1>
                <p>HOW WE AMPLIFY YOUR BUSINESS</p>
            </header>
        </section>
        <section class="cloud-block road-page">
            <div class="container">
                <div class="inner">
                    <span class="line"><span class="circle"><span class="sr-only">line</span></span></span>
                    <div class="items-hold">
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud1.svg" width="123" height="160" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title"><span class="thin">Foundation Services:</span><br>Readiness &AMP; Transformation</strong>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud2.svg" width="175" height="158" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title"><span class="thin">Consultancy Services:</span><br>Strategy &AMP; Technical Implementation</strong>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud3.svg" width="169" height="172" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Managed Services</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="lists-block">
            <div class="container">
                <div class="item">
                    <div class="images-row">
                        <div class="text">
                            <h2 class="heading">Readiness &AMP; Transformation</h2>
                            <p>We start each engagement with a solid stack of foundation services to make sure we understand your business objectives, IT architecture, cloud readiness. This helps us define a clear use case and roadmap.</p>
                        </div>
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/list-img1.svg" width="204" height="263" alt="image description">
                        </div>
                    </div>
                    <ul class="list">
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Cloud Readiness Assesment</a></strong>
                                A step by step approach to get a clear view on your organisations’ readiness to move to the Cloud.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Cloud licensing advisory</a></strong>
                                A deep dive into your current software licensing and assistance with moving to cloud based licensing models.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Azure benchmark services</a></strong>
                                A personal testing environment to help you scope, discover and learn about the capabilities in Azure.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Azure Stack Proof of Concept</a></strong>
                                A dedicated one node Azure Stack environment to test Azure Stack on-premises.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Azure Everywhere Workshop</a></strong>
                                A daylong workshop to learn everything you need to know about how hybrid Azure solutions can amplify your business.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Risk advisory</a></strong>
                                Moving to cloud has impact on your compliance with regulations. We assess and determine the best course of action.</div>
                        </li>
                    </ul>
                </div>
                <div class="item">
                    <div class="images-row">
                        <div class="text">
                            <h2 class="heading">Strategy &AMP; Technical Implementation</h2>
                            <p>To make sure you get the most out of your hybrid Azure deployment we offer various consultancy services that identify challenges, accelerate your go-to-market and customise deployments to your needs.</p>
                        </div>
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/list-img2.svg" width="292" height="260" alt="image description">
                        </div>
                    </div>
                    <ul class="list three-columns">
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Digital Transformation Programme</a></strong>
                                To help you transform into the future using modern technology and development practices.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Enterprise Resource Layer</a></strong>
                                We consult to determine the correct hybrid approach: Azure Stack on-premises, in our modern infrastructure or public Azure.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Azure Core Application Service</a></strong>
                                Design of the application development street so developers can focus on their code and quickly reach a stable and highly scalable production state.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Migration</a></strong>
                                We can move any existing workload from your infrastructure to Azure Stack or Azure in the cloud.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Cloud Platform Layer</a></strong>
                                We assist you with making existing systems cloud-ready for automation of resources in your cloud environment.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Azure Specialist Service</a></strong>
                                Our team of Azure Specialists have the competencies to help you with any challenge you face and the experience to help your organisation move to the cloud.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Technical implementation</a></strong>
                                We help you set up tenants, environments, network infrastructure and more to get you up and running.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Cloud Management Stack</a></strong>
                                We help you transform and manage your cloud stack, including applications, for maximum integration and performance.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Cloud Compliance</a></strong>
                                Based on the risk assessment we ensure your new cloud environment is compliant with your regulatory framework.</div>
                        </li>
                    </ul>
                </div>
                <div class="item">
                    <div class="images-row">
                        <div class="text">
                            <h2 class="heading">Managed services</h2>
                            <p>With a range of managed services we ensure you can focus on your business, while we take care of the rest. From managing services directly to ensuring compliance on an ongoing basis, we’ve got your back.</p>
                        </div>
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/list-img3.svg" width="282" height="282" alt="image description">
                        </div>
                    </div>
                    <ul class="list">
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Service management</a></strong>
                                We can manage every aspect of Azure for you, from it’s IaaS and PaaS layers to Cortana Analytics Suite.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Patch &AMP; Backup management</a></strong>
                                Based on your requirements we can create tailor made patch &AMP; backup schedules to have minimal impact on day-to-day business.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Monitoring &AMP; Reporting</a></strong>
                                24/7 monitoring and rapid response teams make sure you stay up and running. Monthly reporting gives you additional insights into performance and uptime.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Identity &AMP; Access Management</a></strong>
                                We make sure the right people have access to the right information and prevent unauthorised access.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Security &AMP; protection services</a></strong>
                                We offer a host of services to harden your security, protect your mission critical data and safeguard your business against threats.</div>
                        </li>
                        <li>
                            <div class="holder"><strong class="title"><a href="#">Continuous Compliance</a></strong>
                                Our continuous compliance platform and team of compliance engineers ensure your compliance with regulations, 24x7x365.</div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="logos-block">
            <div class="container">
                <h1 class="middle-title">Accreditated by</h1>
                <ul class="logos-list">
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo1.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo1-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo1.png" alt="BSI ISO">
                    </li>
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo2.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo2-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo2.png" alt="BSI ISO">
                    </li>
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo3.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo3-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo3.png" alt="BSI ISO">
                    </li>
                </ul>
                <ul class="logos-list second">
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo4.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo4-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo4.png" alt="Digital Marketplace supplier">
                    </li>
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo5.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo5-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo5.png" alt="PCI">
                    </li>
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo6.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo6-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo6.png" alt="CSA START certification">
                    </li>
                    <li>
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/logo7.png, <?php bloginfo('template_url'); ?>/amp/assets/images/logo7-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/logo7.png" alt="the green grid member">
                    </li>
                </ul>
            </div>
        </section>
        <section class="join-block">
            <div class="container">
                <div class="meta">
                    <h1 class="middle-title">Join our Azure<br>Stack Workshop</h1>
                    <p>A single day workshop to learn everything you need to know about how Azure Stack and hybrid Azure solutions could amplify your business.</p>
                </div>
                <div class="wrap">
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/join1.svg" width="233" height="207" alt="image description">
                        </div>
                        <strong class="title">What will you learn?</strong>
                        <p>How to take advantage of hybrid cloud solutions, The advantages of Azure Stack in hybrid scenarios and opportunities for developers and IT personnel.</p>
                    </div>
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/join2.svg" width="233" height="214" alt="image description">
                        </div>
                        <strong class="title">What are you taking home?</strong>
                        <p>A clear understanding for your business to take advantage of all the benefits hybrid cloud solutions have to offer.</p>
                    </div>
                </div>
            </div>
            <div class="feedback-block">
                <div class="title-wrap">
                    <strong class="title">Start your road to hybrid cloud today</strong>
                    <p>Get in touch with our consultants. We’ll get back to you within one business day.</p>
                </div>

                <form action="/api/webforms.php" method="post" name="contactform" id="contactform" class="validate feedback-form">

                        <input type="hidden" id="formtype" name="form" value="contact-form" />
                        <input type="hidden" name="pageurl" value="<?php $pageurl = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; echo $pageurl; ?>" />
                        <input type="hidden" name="eventcategory" id="eventcategory" value="Form Submission" />
                        <input type="hidden" name="eventaction" id="eventaction" value="AMP The Journey To Cloud" />
                        <input type="hidden" name="eventlabel" id="eventlabel" value="<?php the_title(); ?>" />



                        <input type="text" class="form-control simple-input contact-custom required" id="name" name="name" placeholder="Name">
                        <input type="email" id="email" name="email" class="form-control simple-input contact-custom required" placeholder="Email Address" data-regex="^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$">
                        <input type="text" class="form-control simple-input contact-custom required" id="company" name="company" placeholder="Company Name">
                        <input type="tel" class="form-control simple-input contact-custom" id="phone" name="phone" placeholder="Telephone number (optional)">


                        <div>
                            <?php $num1=rand(2,4); $num2=rand(2,5); $total=$num1+$num2; ?>
                            <p class="margin-top-25">CAPTCHA question: What is <?php echo $num1; ?> + <?php echo $num2; ?>?</p>
                            <input type="tel" id="captcha" name="captcha" class="form-control simple-input contact-custom required" data-regex="^([<?php echo $total; ?>]{1})$" />
                            <input type="hidden" value="<?php echo $total; ?>" name="validation">
                        </div>


                        <button type="submit" class="btn-transparent" id="submit" name="Submit">Get back to me</button>

                </form>

                <?php /* ORIGINAL FORM
                <form action="#" method="post" class="feedback-form">
                    <input type="text" name="name" class="simple-input" placeholder="Name*">
                    <input type="text" name="company" class="simple-input" placeholder="Company Name*">
                    <input type="email" name="email" class="simple-input" placeholder="Email address*">
                    <input type="tel" name="phone" class="simple-input" placeholder="Phone Number">
                    <button type="submit" class="btn-transparent">Get back to me</button>
                </form>
                */?>
            </div>
        </section>
    </main>
    <?php include("inc/footer.php"); ?>
  </div>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/vendor.min.js" defer></script>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/app.js" defer></script>
</body>

</html>
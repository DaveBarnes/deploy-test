<?php
/*
Template Name: AMP - Home
*/
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>AMP - A Pulsant Initiative, powered by Microsoft Azure</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="en" />
	<meta name="robots" content="index, follow">

	<link rel="icon" type="img/png" href="https://www.pulsant.com/wp-content/themes/pulsant/AMP/img/pulsant-favicon-16x16.png">
	<link rel="apple-touch-icon" href="https://www.pulsant.com/wp-content/themes/pulsant/AMP/apple-touch-icon.png" />

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes">

	<meta name="description" content="Hybrid Cloud Solutions, Powered by Microsoft Azure. Pulsant is the premier provider of managed hybrid cloud solutions throughout the UK and Beyond.">
	<meta name="author" content="Pulsant" />

	<meta property="og:url" content="" />
	<meta property="og:site_name" content="Pulsant" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="AMP - A Pulsant Initiative, powered by Microsoft Azure" />
	<meta property="og:image" content="https://www.pulsant.com/wp-content/themes/pulsant/AMP/apple-touch-icon.png" />
	<meta property="og:description" content="Hybrid Cloud Solutions, Powered by Microsoft Azure. Pulsant is the premier provider of managed hybrid cloud solutions throughout the UK and Beyond." />

	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:description" content="Hybrid Cloud Solutions, Powered by Microsoft Azure. Pulsant is the premier provider of managed hybrid cloud solutions throughout the UK and Beyond."/>
	<meta name="twitter:title" content="AMP - A Pulsant Initiative, powered by Microsoft Azure"/>
	<meta name="twitter:site" content="@pulsantuk"/>
	<meta name="twitter:creator" content="@pulsantuk"/>


  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/vendor.min.css">
  <link media="all" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/amp/assets/css/style.min.css">
  <script src=" https://use.typekit.net/gkc7sbg.js "></script>
  <script>try{Typekit.load({ async: false });}catch(e){}</script>
  <style>
        .main-info-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd.jpg);
        }
        .main-info-bg.isv-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd.jpg);
        }
        .main-info-bg.road-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd.jpg);
        }
        .main-info-bg.boost-product-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd.jpg);
        }
        .main-info-bg.scaleup-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd.jpg);
        }
        .main-info-bg.staytuned-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd.jpg);
        }
        .main-info-bg.boost-business-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd.jpg);
        }
        .main-info-bg.performance-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd.jpg);
        }
        .main-info-bg.try-page{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd.jpg);
        }
        .modal-block-bg{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd.jpg);
        }
        .footer{
            background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd.jpg);
        }
        @media (-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-hd-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-hd-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-hd-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-hd-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-hd-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-hd-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-hd-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-hd-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-hd-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-hd-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-hd-2x.jpg);
            }
        }
        @media screen and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:1280px), (min-resolution: 144dpi) and (max-width:1280px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-landscape-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-landscape-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-landscape-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-landscape-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-landscape-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-landscape-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-landscape-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-landscape-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-landscape-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-landscape-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-landscape-2x.jpg);
            }
        }
        @media screen and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet.jpg);
            }
        }
        @media (-webkit-min-device-pixel-ratio: 1.5) and (max-width:980px), (min-resolution: 144dpi) and (max-width:980px) {
            .main-info-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-home-tablet-2x.jpg);
            }
            .main-info-bg.isv-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-isv-tablet-2x.jpg);
            }
            .main-info-bg.road-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-road-tablet-2x.jpg);
            }
            .main-info-bg.boost-product-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-product-tablet-2x.jpg);
            }
            .main-info-bg.scaleup-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-scaleup-tablet-2x.jpg);
            }
            .main-info-bg.staytuned-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-staytuned-tablet-2x.jpg);
            }
            .main-info-bg.boost-business-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-boost-business-tablet-2x.jpg);
            }
            .main-info-bg.performance-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-performance-tablet-2x.jpg);
            }
            .main-info-bg.try-page{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/header-try-tablet-2x.jpg);
            }
            .modal-block-bg{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/modal-block-tablet-2x.jpg);
            }
            .footer{
                background-image: url(<?php bloginfo('template_url'); ?>/amp/assets/images/footer-bg-tablet-2x.jpg);
            }
        }
    </style>
</head>

<body>
  <?php $lighttheme = "" ?>
  <?php $roadpage = "" ?>
  <div id="wrapper">
    <?php include("inc/header.php"); ?>
    <main class="main">
        <section class="main-info main-info-bg">
            <header class="meta">
                <h1 class="big-title">Turn up the volume of your business</h1>
                <p>with AMP True Hybrid Azure Solutions<sup>&copy;</sup></p>
            </header>
            <div class="text-wrap">
                <div class="container">
                    <strong class="middle-title" style="max-width: 100%;">Pulsant acquires LayerV</strong>
                    <div class="text" style="max-width: 500px; margin: 0 auto;">
                        <p><a href="https://www.pulsant.com/knowledge-hub/blog/pulsant-boosts-hybrid-capabilities-with-the-acquisition-of-layerv/" class="btn-fill" style="max-width: 250px; margin: 0 auto;">Read the announcement</a></p>
                        <p>We are amplifying our hybrid cloud capabilities with continuous compliance and the ability to deliver end-to-end secure platforms</p>
                    </div>
<!--
                    <strong class="middle-title">Amplify your business with Pulsant AMP</strong>
                    <div class="text">
                        <p>AMP is a Pulsant initiative, powered by Microsoft Azure, allowing you to take advantage of the opportunities true hybrid cloud offers. A powerful combination of Microsoft cloud technology and the assessment, consultancy, implementation and management services at Pulsant to help you in your transformation.</p>
                        <p>From scalable application hosting and disconnected scenarios, to secure and regulated data hosting, we have the in-house capabilities to support any workload your business might need. We’re not just an IT supplier, we are the business partner that helps your business get further.</p>
                    </div>
-->
                </div>
            </div>
        </section>
        <section class="image-text-block">
            <div class="container">
                <div class="item">
                    <div class="text">
                        <strong class="bold-title">A consistent, harmonious and portable hybrid cloud platform<span class="description">Powered by Azure &AMP; Azure Stack</span></strong>
                        <ul class="list">
                            <li>1:1 parity. A consistent cloud architecture in both Azure Stack and Azure.</li>
                            <li>Increase speed to market. Deploy apps the same way, whether they run on-premises or in the cloud.</li>
                            <li>Future-proof. The Azure platform stays up to date, both on-premises and in the cloud.</li>
                        </ul>
                    </div>
                    <div class="img-holder">
                        <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/img1.svg" width="435" height="312" alt="image description">
                    </div>
                </div>
                <div class="item">
                    <div class="text">
                        <strong class="bold-title">Scale up your business and comply with any regulation</strong>
                        <ul class="list">
                            <li>Keep data secure and local, and utilise cloud services to scale your front-end apps.</li>
                            <li>Ensure business continuity by backing up data across multiple locations at high speeds.</li>
                            <li>Utilise policy based protection to prevent unauthorised access across your cloud deployment.</li>
                        </ul>
                    </div>
                    <div class="img-holder">
                        <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/img2.svg" width="406" height="310" alt="image description">
                    </div>
                </div>
                <div class="item">
                    <div class="text">
                        <strong class="bold-title">Keep your disconnected locations connected</strong>
                        <ul class="list">
                            <li>Azure Stack services can run in a disconnected state while using the same Azure cloud platform.</li>
                            <li>Take advantage of Azure even in high-latency situations like remote manufacturing locations or cruiseships.</li>
                            <li>Focus on your core business and application deployment, not on the headaches of disconnected IT scenario’s</li>
                        </ul>
                    </div>
                    <div class="img-holder">
                        <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/img3.svg" width="385" height="291" alt="image description">
                    </div>
                </div>
            </div>
        </section>
        <section class="compliance-block">
            <div class="container">
                <div class="meta">
                    <h1 class="title">Continuous Compliance with the Pulsant Compliance Platform</h1>
                    <p>A continuous 360° view on the state of your IT compliance</p>
                </div>
                <div class="inner">
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico1.svg" width="115" height="131" alt="image description">
                        </div>
                        <div class="text">
                            <strong class="mini-title">We continually ensure your IT is fully compliant with relevant regulatory frameworks</strong>
                            <p>Whatever your regulatory and industry requirements are, such as FCA, ISO27001, PCI. We ensure compliance 24x7x365.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico2.svg" width="115" height="132" alt="image description">
                        </div>
                        <div class="text">
                            <strong class="mini-title">Our compliance platform monitors your IT in real time</strong>
                            <p>With 24/7 monitoring, non-intrusive vulnerability &AMP; penetration testing we automatically generate non-compliance reports.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico3.svg" width="115" height="118" alt="image description">
                        </div>
                        <div class="text">
                            <strong class="mini-title">Out-of-the-box support for any IT environment</strong>
                            <p>Whether you utilise Azure public cloud, Azure Stack on-premises or a different hybrid model, we assure compliance across your environments.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="discover-block">
            <div class="container">
                <h1 class="middle-title">Discover our hybrid Azure solutions</h1>
                <div class="inner">
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/discover1.svg" width="233" height="167" alt="image description">
                        </div>
                        <div class="info">
                            <strong class="title">Example use case</strong>
                            <strong class="description">Software Developers</strong>
                            <p>Consistent modern application development, Agility &AMP; Scalability in Azure, on-premises and in the cloud.</p>
                            <a href="business-agility-and-scalability" class="btn-transparent">AMP Business Agility &AMP; Scalability</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/discover2.svg" width="233" height="164" alt="image description">
                        </div>
                        <div class="info">
                            <strong class="title">use case</strong>
                            <strong class="description">Finance, Legal, Insurance and Local Gov’s</strong>
                            <p>Serve customers and citizens while keeping their data close, secure and be fully in control.</p>
                            <a href="data-governance-security-and-compliance" class="btn-transparent">AMP Data Governance, Security &AMP; Continuity</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/discover3.svg" width="232" height="163" alt="image description">
                        </div>
                        <div class="info">
                            <strong class="title">use case</strong>
                            <strong class="description">Manufacturing, Utilities and Services</strong>
                            <p>Utilise the capabilities of the cloud in disconnected or high-latency locations and environments.</p>
                            <a href="disconnected-solutions" class="btn-transparent">AMP Disconnected Solutions</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/discover4.svg" width="232" height="177" alt="image description">
                        </div>
                        <div class="info">
                            <strong class="title">use case</strong>
                            <strong class="description">Transportation &AMP; Logistics</strong>
                            <p>Make better decisions and improve customer experience with hybrid business intelligence.</p>
                            <a href="business-intelligence" class="btn-transparent">AMP Business Intelligence</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="move-block">
            <div class="container">
                <h1 class="middle-title">Make the move to true hybrid Azure</h1>
                <div class="row">
                    <div class="item">
                        <strong class="title">Traditional Hybrid</strong>
                        <span class="description">Multi Cloud, Multi Platform</span>
                        <div class="img-holder">
                            <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/move-img1.png, <?php bloginfo('template_url'); ?>/amp/assets/images/move-img1-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/move-img1.png" alt="image description">
                        </div>
                    </div>
                    <div class="arrow">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/move-img-arrow.png, <?php bloginfo('template_url'); ?>/amp/assets/images/move-img-arrow-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/move-img-arrow.png" alt="image description">
                    </div>
                    <div class="item">
                        <strong class="title">True Hybrid Azure</strong>
                        <span class="description">Multi Cloud, One consistent platform</span>
                        <div class="img-holder">
                            <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/move-img2.png, <?php bloginfo('template_url'); ?>/amp/assets/images/move-img2-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/move-img2.png" alt="image description">
                        </div>
                        <div class="tooltip-wrap">
                            <div class="triangle"><span class="sr-only">triangle</span></div>
                            <ul class="tooltip">
                                <li>Maximise productivity by empowering developers to build and deploy the same way accross environments.</li>
                                <li>Adopt common DevOps processes and tools across Azure and Azure Stack.</li>
                                <li>Choose from multiple Linux distributions, Docker-integrated containers and Apache Mesos implementations.</li>
                                <li>Effortlessly move workloads between environments.</li>
                                <li>Endless extendability and scalability.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="platform-block">
            <div class="container">
                <h1 class="middle-title">A consistent Cloud platform for developers and IT</h1>
                <div class="inner">
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico4.svg" width="82" height="67" alt="image description">
                        </div>
                        <strong class="title">Azure Stack</strong>
                        <span class="description">Hosted on-premises or with Pulsant</span>
                        <ul class="list-link">
                            <li>Portal | PowerShell | DevOps tools</li>
                            <li>Azure Resource Manager</li>
                            <li>Azure IaaS | Azure PaaS</li>
                            <li>Cloud Infrastructure (Integrated systems)</li>
                        </ul>
                    </div>
                    <div class="middle-item">
                        <strong class="min-title">Developers</strong>
                        <div class="img-wrap">
                            <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/dev1.png, <?php bloginfo('template_url'); ?>/amp/assets/images/dev1-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/dev1.png" alt="image description">
                        </div>
                        <strong class="min-mid-title">CONSISTENCY</strong>
                        <div class="img-wrap">
                            <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/dev2.png, <?php bloginfo('template_url'); ?>/amp/assets/images/dev2-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/dev2.png" alt="image description">
                        </div>
                        <strong class="min-title">IT</strong>
                    </div>
                    <div class="item">
                        <div class="img-holder">
                            <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/ico5.svg" width="82" height="62" alt="image description">
                        </div>
                        <strong class="title">Azure</strong>
                        <span class="description">public</span>
                        <ul class="list-link">
                            <li>Portal | PowerShell | DevOps tools</li>
                            <li>Azure Resource Manager</li>
                            <li>Azure IaaS | Azure PaaS</li>
                            <li>Cloud Infrastructure</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="image-list-box wow slideInUp">
            <div class="container">
                <div class="inner">
                    <div class="img-holder">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/img-box.jpg, <?php bloginfo('template_url'); ?>/amp/assets/images/img-box-2x.jpg 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/img-box.jpg" alt="image description">
                    </div>
                    <div class="text">
                        <strong class="bold-title">The same economic model as the cloud with pay-as-you-use pricing.</strong>
                        <ul class="text-list">
                            <li>No upfront licensing fees for Azure services in a hybrid scenario.</li>
                            <li>With our CSP status you have no additional licensing model to worry about.</li>
                            <li>Only pay when you use services.</li>
                            <li>Metering is done using the same units as Azure.</li>
                            <li>Lower pricing in Azure stack, since you operate some of the hardware and facilities yourself.</li>
                            <li>A fixed-price “capacity model” available for disconnected systems on-premises.</li>
                            <li>A single unified invoice for all your services.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="cloud-block image-tooltip">
            <div class="container">
                <div class="meta">
                    <h1 class="middle-title">Host Azure Stack in one of many Pulsant Datacenters</h1>
                    <p>We own and operate a nationwide network of 15 UK enterprise-class datacentres with an extension to a global presence in the US, Asia and Europe.</p>
                </div>
                <div class="img-big-holder1">
                    <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/azure-stack-datacenters-img1.png, <?php bloginfo('template_url'); ?>/amp/assets/images/azure-stack-datacenters-img1-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/azure-stack-datacenters-img1-2x.png" alt="image description">
                </div>
                <div class="tooltip-wrap wow slideInUp">
                    <div class="triangle"><span class="sr-only">triangle</span></div>
                    <div class="key">
                        <h5 class="reduced">Network legenda</h5>
                        <p><span class="dc"></span>Pulsant Datacentre Site</p>
                        <p><span class="intdc"></span>Pulsant 3rd Party International Datacentre</p>
                        <p><span class="point"></span>Point of Presence Inter-Connectivity</p>
                        <p><span class="df"></span>Pulsant contracted diverse Dark Fibre</p>
                        <p><span class="df3"></span>Pulsant third party diverse Dark Fibre</p>
                    </div>
                </div>
                <div class="meta">
                    <p>We offer state-of-the-art infrastructure, resilient utilities and high levels of site security. A carefully controlled environment, to optimise server performance and minimise running costs. On-site systems consultants and technical staff provide intelligent advice and support whenever necessary. We own and operate all our sites to ensure a consistent, quality experience.</p>
                </div>

                <div class="inner">
                    <div class="img-holder">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/azure-stack-datacenters-img-l.png, <?php bloginfo('template_url'); ?>/amp/assets/images/azure-stack-datacenters-img-l-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/azure-stack-datacenters-img-l.png" alt="image description">
                    </div>
                    <div class="img-holder">
                        <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/azure-stack-datacenters-img-r.png, <?php bloginfo('template_url'); ?>/amp/assets/images/azure-stack-datacenters-img-r-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/azure-stack-datacenters-img-r.png" alt="image description">
                    </div>
                </div>




<!--
                <div class="img-big-holder2">
                    <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/azure-stack-datacenters-img2.png, <?php bloginfo('template_url'); ?>/amp/assets/images/azure-stack-datacenters-img2-2x.png 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/azure-stack-datacenters-img2-2x.png" alt="image description">
                </div>
-->
            </div>
        </section>
        <section class="cloud-block">
            <div class="container">
                <div class="meta">
                    <h1 class="middle-title">Successfully amplify your business with our journey to the cloud</h1>
                    <p>From the discovery phase to design and deployment. Our Journey To The Cloud offers services like complete assesments of your environments, providing test set-ups, technical consultancy, implementation and deployment to fully managed services like monitoring, support and reporting.</p>
                </div>
                <div class="inner">
                    <span class="line"><span class="circle"><span class="sr-only">line</span></span></span>
                    <div class="items-hold">
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud1.svg" width="123" height="160" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Foundation Services:<br>Readiness &AMP; Transformation</strong>
                                <ul class="link-list">
                                    <li>Cloud Readiness Assesment</li>
                                    <li>Cloud Licensing Advisory</li>
                                    <li>Azure benchmark services</li>
                                    <li>Azure Stack Proof of Concept</li>
                                    <li>Azure Everywhere Workshop</li>
                                    <li>Azure Risk Advisory</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud2.svg" width="175" height="158" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Consultancy Services:<br>Strategy &AMP; Technical Implementation</strong>
                                <ul class="link-list">
                                    <li>Digital Transformation Programme</li>
                                    <li>Migration</li>
                                    <li>Technical Implementation</li>
                                    <li>Cloud Management Stack</li>
                                    <li>Azure Specialist Service</li>
                                    <li>Cloud Compliance</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-holder">
                                <img src="<?php bloginfo('template_url'); ?>/amp/assets/images/cloud3.svg" width="169" height="172" alt="image description">
                            </div>
                            <div class="text">
                                <strong class="mini-title">Managed Services</strong>
                                <ul class="link-list">
                                    <li>Service Management</li>
                                    <li>Monitoring &AMP; Reporting</li>
                                    <li>Protection &AMP; Security</li>
                                    <li>Patch &AMP; Backup management</li>
                                    <li>Identity &AMP; Access Management</li>
                                    <li>Continuous Compliance</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/amp/journey-to-cloud" class="btn-transparent">Journey to the Cloud</a>
            </div>
        </section>
        <section class="banner">
            <div class="img-wrap">
                <div class="img-holder">
                    <img srcset="<?php bloginfo('template_url'); ?>/amp/assets/images/banner-img.jpg, <?php bloginfo('template_url'); ?>/amp/assets/images/banner-img-2x.jpg 2x" src="<?php bloginfo('template_url'); ?>/amp/assets/images/banner-img.jpg" alt="image description">
                </div>
            </div>
            <div class="text">
                <h1 class="title">Try Azure Stack today</h1>
                <p>A single one node Azure Stack solution to help you to explore the possibilities, quickly.</p>
                <a href="/amp/one-node-as-a-service" class="btn-transparent">Discover how</a>
            </div>
        </section>
    </main>
    <?php include("inc/footer.php"); ?>
  </div>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/vendor.min.js" defer></script>
  <script src="<?php bloginfo('template_url'); ?>/amp/assets/js/app.js" defer></script>
</body>

</html>
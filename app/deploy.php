<?php
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'testing');

// Project repository
set('repository', '');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
set('shared_files', []);
set('shared_dirs', []);

// Writable dirs by web server 
set('writable_dirs', []);

host('pulsantstatic.bbi.agency')
    ->stage('production')    
    ->set('deploy_path', '/var/www/bbi.agency/pulsantstatic.bbi.agency');

task('pwd', function () {
    $result = run('pwd');
    writeln("Current dir: $result");
});
